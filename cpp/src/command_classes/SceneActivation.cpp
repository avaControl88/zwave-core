//-----------------------------------------------------------------------------
//
//	SceneActivation.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SCENE_ACTIVATION
//
//	Copyright (c) 2012 Greg Satz <satz@iranger.com>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "SceneActivation.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Notification.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum SceneActivationCmd
{
	SceneActivationCmd_Set				= 0x01
};

enum
{
	SceneActivationIndex_Activate,
	SceneActivationIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <SceneActivation::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool SceneActivation::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( SceneActivationCmd_Set == (SceneActivationCmd)_data[0] )
	{
		// Scene Activation Set received so send notification
		char msg[64];
		if( _data[2] == 0 )
			snprintf( msg, sizeof(msg), "now" );
		else if( _data[2] <= 0x7F )
			snprintf( msg, sizeof(msg), "%d seconds", _data[2] );
		else if( _data[2] <= 0xFE )
			snprintf( msg, sizeof(msg), "%d minutes", _data[2] );
		else
			snprintf( msg, sizeof(msg), "via configuration" );
		Log::Write( LogLevel_Info, GetNodeId(), "Received Scene Activation set from node %d: scene id=%d %s. Sending event notification.", GetNodeId(), _data[1], msg );
		Notification* notification = new Notification( Notification::Type_SceneEvent );
		notification->SetHomeAndNodeIds( GetHomeId(), GetNodeId() );
		notification->SetSceneId( _data[1] );
		GetDriver()->QueueNotification( notification );
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SceneActivation::SetValue>
// Set the state.
//-----------------------------------------------------------------------------
bool SceneActivation::SetValue (Value const& _value)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case SceneActivationIndex_Activate:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				//(Scene ID)-(Dimming Duration)
				string sceneID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, sceneID_str.size()+1);
				string duration_str = tmp_str;
				int sceneID = atoi(sceneID_str.c_str());
				int duration = atoi(duration_str.c_str());
				if (sceneID > 0 && duration > 0) {
					Msg* msg = new Msg( "SceneActivationCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 4 );
					msg->Append( GetCommandClassId() );
					msg->Append( SceneActivationCmd_Set );
					msg->Append((uint8)sceneID);
					msg->Append((uint8)duration);
					msg->Append( GetDriver()->GetTransmitOptions() );
					SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			}
			return true;
		}
		case SceneActivationIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, SceneActivationIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <SceneActivation::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void SceneActivation::CreateVars (uint8 const _instance)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, SceneActivationIndex_Activate, "Scene Activate", "", false, true, "", 0);
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SceneActivationIndex_IconName, "SceneActivation Icon", "icon", false, false, "", 0);

		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SceneActivationIndex_Activate))) {
			string info = "format (Scene ID)-(Dimming Duration). "
				"Duration: 0(Instantly), 1~127(1~127 seconds), 128~254(1~127 minutes), 255(default)";
			m_value->SetHelp(info);
			m_value->Release();
		}
	}
}

void SceneActivation::initSupportSetting()
{
}
