//-----------------------------------------------------------------------------
//
//	MTPWindowCovering.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_MTP_WINDOW_COVERING
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "MTPWindowCovering.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"
#include "ValueByte.h"

using namespace OpenZWave;

enum MTPWindowCoveringCmd
{
	MTPWindowCoveringCmd_Set		= 0x01,
	MTPWindowCoveringCmd_Get		= 0x02,
	MTPWindowCoveringCmd_Report		= 0x03
};

enum
{
	MTPWindowCoveringIndex_Level = 0,
	MTPWindowCoveringIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <MTPWindowCovering::RequestState>
// Get the static MTP window covering details from the device
//-----------------------------------------------------------------------------
bool MTPWindowCovering::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current state
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}
	return false;
}

//-----------------------------------------------------------------------------
// <MTPWindowCovering::RequestValue>
// Get the MTP window covering details from the device
//-----------------------------------------------------------------------------
bool MTPWindowCovering::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	// Request the current state
	Msg* msg = new Msg( "MTPWindowCoveringCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( MTPWindowCoveringCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <MTPWindowCovering::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool MTPWindowCovering::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( MTPWindowCoveringCmd_Report == (MTPWindowCoveringCmd)_data[0] )
	{
		uint8 _level = _data[1];
		if (ValueByte* valueByte = static_cast<ValueByte*>(GetValue(_instance, MTPWindowCoveringIndex_Level))) {
			valueByte->OnValueRefreshed(_level);
			valueByte->Release();
		}
		Log::Write( LogLevel_Info, GetNodeId(), "Received MTPWindowCoveringCmd_Report: level=%d", _level);		
		return true;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// <MTPWindowCovering::SetValue>
// Set the device's state
//-----------------------------------------------------------------------------
bool MTPWindowCovering::SetValue (Value const& _value) 
{
	uint8 instance = _value.GetID().GetInstance();
	switch((MTPWindowCoveringCmd)_value.GetID().GetIndex()) {
		case MTPWindowCoveringIndex_Level:
		{
			if (ValueByte const* m_value = static_cast<ValueByte const*>(&_value)) {
				Msg* msg = new Msg( "MTPWindowCoveringCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->SetInstance( this, instance );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( MTPWindowCoveringCmd_Set );
				msg->Append(m_value->GetValue());
				msg->Append( GetDriver()->GetTransmitOptions() );
				return SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
			break;
		}
		case MTPWindowCoveringIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, MTPWindowCoveringIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <MTPWindowCovering::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void MTPWindowCovering::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, MTPWindowCoveringIndex_Level, "Level", "", false, false, 0, 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, MTPWindowCoveringIndex_IconName, 
			"MTPWindowCovering Icon", "icon", false, false, "", 0);
	}
}
