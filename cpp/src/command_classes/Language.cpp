//-----------------------------------------------------------------------------
//
//	Language.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_LANGUAGE
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "Language.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum LanguageCmd
{
	LanguageCmd_Set		= 0x01,
	LanguageCmd_Get		= 0x02,
	LanguageCmd_Report	= 0x03
};

enum
{
	LanguageIndex_Language	= 0,
	LanguageIndex_Country,

	LanguageIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <Language::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool Language::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Language::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool Language::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _instance != 1 )
	{
		// This command class doesn't work with multiple instances
		return false;
	}

	Msg* msg = new Msg( "LanguageCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( LanguageCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <Language::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Language::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( LanguageCmd_Report == (LanguageCmd)_data[0] )
	{
		char language[4];
		char country[3];
		bool has_country = false;
		if (_length >= 7)
			has_country = true;

		language[0] = _data[1];
		language[1] = _data[2];
		language[2] = _data[3];
		language[3] = 0;

		if (has_country) {
			country[0] = _data[4];
			country[1] = _data[5];
			country[2] = 0;
		} else
			memset(country, 0, sizeof(country));

		Log::Write( LogLevel_Info, GetNodeId(), "Received Language report: Language=%s, Country=%s", language, country );
		ClearStaticRequest( StaticRequest_Values );

		if( ValueString* languageValue = static_cast<ValueString*>( GetValue( _instance, LanguageIndex_Language ) ) )
		{
			languageValue->OnValueRefreshed( language );
			languageValue->Release();
		}
		if( ValueString* countryValue = static_cast<ValueString*>( GetValue( _instance, LanguageIndex_Country ) ) )
		{
			countryValue->OnValueRefreshed( country );
			countryValue->Release();
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Language::SetValue>
// Set the Language's state
//-----------------------------------------------------------------------------
bool Language::SetValue( Value const& _value ) {
	uint8 instance = _value.GetID().GetInstance();
	if (_value.GetID().GetIndex() == LanguageIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, LanguageIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	} else if (_value.GetID().GetIndex() == LanguageIndex_Language) {
		ValueString const* m_value = static_cast<ValueString const*>(&_value);
		string language_str = m_value->GetValue();
		string country_str;
		if (ValueString* m_value2 = static_cast<ValueString*>( GetValue(instance, LanguageIndex_Country))) {
			country_str = m_value2->GetValue();
			m_value2->Release();
		}

		Msg* msg = new Msg( "LanguageCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
		msg->Append( GetNodeId() );
		msg->Append( 7 );
		msg->Append( GetCommandClassId() );
		msg->Append( LanguageCmd_Set );
		switch(language_str.size()) {
			case 0:
			{
				msg->Append('e');
				msg->Append('n');
				msg->Append('g');
				break;
			}
			case 1:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(0);
				msg->Append(0);
				break;
			}
			case 2:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(language_str.c_str()[1]);
				msg->Append(0);
				break;
			}
			default:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(language_str.c_str()[1]);
				msg->Append(language_str.c_str()[2]);
				break;
			}
		}
		if (country_str.size() == 0) {
			msg->Append('U');
			msg->Append('S');
		} else if (country_str.size() == 1) {
			msg->Append(country_str.c_str()[0]);
			msg->Append(0);
		} else {
			msg->Append(country_str.c_str()[0]);
			msg->Append(country_str.c_str()[1]);
		}
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	} else if (_value.GetID().GetIndex() == LanguageIndex_Country) {
		ValueString const* m_value = static_cast<ValueString const*>(&_value);
		string country_str = m_value->GetValue();
		string language_str;
		if (ValueString* m_value2 = static_cast<ValueString*>( GetValue(instance, LanguageIndex_Language))) {
			language_str = m_value2->GetValue();
			m_value2->Release();
		}

		Msg* msg = new Msg( "LanguageCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
		msg->Append( GetNodeId() );
		msg->Append( 7 );
		msg->Append( GetCommandClassId() );
		msg->Append( LanguageCmd_Set );
		switch(language_str.size()) {
			case 0:
			{
				msg->Append('e');
				msg->Append('n');
				msg->Append('g');
				break;
			}
			case 1:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(0);
				msg->Append(0);
				break;
			}
			case 2:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(language_str.c_str()[1]);
				msg->Append(0);
				break;
			}
			default:
			{
				msg->Append(language_str.c_str()[0]);
				msg->Append(language_str.c_str()[1]);
				msg->Append(language_str.c_str()[2]);
				break;
			}
		}
		if (country_str.size() == 0) {
			msg->Append('U');
			msg->Append('S');
		} else if (country_str.size() == 1) {
			msg->Append(country_str.c_str()[0]);
			msg->Append(0);
		} else {
			msg->Append(country_str.c_str()[0]);
			msg->Append(country_str.c_str()[1]);
		}
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <Language::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void Language::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, (uint8)LanguageIndex_Language, "Language", "", false, false, "", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, (uint8)LanguageIndex_Country, "Country", "", false, false, "", 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, LanguageIndex_IconName, "Language Icon", "icon", false, false, "", 0);
	}
}

