//-----------------------------------------------------------------------------
//
//	ThermostatSetpoint.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_THERMOSTAT_SETPOINT
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ThermostatSetpoint.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueDecimal.h"
#include "ValueString.h"

using namespace OpenZWave;

enum ThermostatSetpointCmd
{
	ThermostatSetpointCmd_Set				= 0x01,
	ThermostatSetpointCmd_Get				= 0x02,
	ThermostatSetpointCmd_Report			= 0x03,
	ThermostatSetpointCmd_SupportedGet		= 0x04,
	ThermostatSetpointCmd_SupportedReport	= 0x05,

	//Version 3
	ThermostatSetpointCmd_CapabilitiesGet	= 0x09,
	ThermostatSetpointCmd_CapabilitiesReport = 0x0a
};

enum
{
	ThermostatSetpoint_Unused0	= 0,
	ThermostatSetpoint_Heating1,
	ThermostatSetpoint_Cooling1,
	ThermostatSetpoint_Unused3,
	ThermostatSetpoint_Unused4,
	ThermostatSetpoint_Unused5,
	ThermostatSetpoint_Unused6,
	ThermostatSetpoint_Furnace,
	ThermostatSetpoint_DryAir,
	ThermostatSetpoint_MoistAir,
	ThermostatSetpoint_AutoChangeover,
	ThermostatSetpoint_HeatingEcon,
	ThermostatSetpoint_CoolingEcon,
	ThermostatSetpoint_AwayHeating,
	ThermostatSetpoint_AwayCooling,
	ThermostatSetpoint_FullPower,
	ThermostatSetpoint_Count
};

enum
{
	ThermostatSetpointIndex_Setpoint = 0,

	ThermostatSetpointIndex_IconName = 99
};

static char const* c_setpointName[] = 
{
	"Reserved",
	"Heating 1",
	"Cooling 1",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Furnace",
	"Dry Air",
	"Moist Air",
	"Auto Changeover",
	"Energy Save Heating",
	"Energy Save Cooling",
	"Away Heating",
	"Away Cooling",
	"Full Power"
};

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::ThermostatSetpoint>
// Constructor
//-----------------------------------------------------------------------------
ThermostatSetpoint::ThermostatSetpoint
(
	uint32 const _homeId,
	uint8 const _nodeId
):
	CommandClass( _homeId, _nodeId ), m_setPointBase( 0 )
{
	SetStaticRequest( StaticRequest_Values ); 
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::ReadXML>
// Read the saved change-counter value
//-----------------------------------------------------------------------------
void ThermostatSetpoint::ReadXML
( 
	TiXmlElement const* _ccElement
)
{
	CommandClass::ReadXML( _ccElement );

	int intVal;
	if( TIXML_SUCCESS == _ccElement->QueryIntAttribute( "base", &intVal ) )
	{
		m_setPointBase = (uint8)intVal;
	}
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::WriteXML>
// Write the change-counter value
//-----------------------------------------------------------------------------
void ThermostatSetpoint::WriteXML
( 
	TiXmlElement* _ccElement
)
{
	CommandClass::WriteXML( _ccElement );

	char str[8];
	snprintf( str, 8, "%d", m_setPointBase );
	_ccElement->SetAttribute( "base", str );
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::RequestState>
// Get the static thermostat setpoint details from the device
//-----------------------------------------------------------------------------
bool ThermostatSetpoint::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		requests |= RequestValue( _requestFlags, 0xff, _instance, _queue );
	}

	if( _requestFlags & RequestFlag_Session )
	{
		for( uint8 i=0; i<ThermostatSetpoint_Count; ++i )
		{
			requests |= RequestValue( _requestFlags, i, _instance, _queue );
		}
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::RequestValue>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool ThermostatSetpoint::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _setPointIndex,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _setPointIndex == 0xff )		// check for supportedget
	{
		// Request the supported setpoints
		Msg* msg = new Msg( "Request Supported Thermostat Setpoints", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatSetpointCmd_SupportedGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}

	Value* value = GetValue( 1, _setPointIndex );
	if( value != NULL )
	{
		value->Release();
		// Request the setpoint value
		Msg* msg = new Msg( "Request Current Thermostat Setpoint", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatSetpointCmd_Get );
		msg->Append( _setPointIndex );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ThermostatSetpoint::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ThermostatSetpointCmd_Report == (ThermostatSetpointCmd)_data[0] )
	{
		// We have received a thermostat setpoint value from the Z-Wave device
		if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, _data[1] ) ) )
		{
			uint8 scale;
			uint8 precision = 0;
			string temperature = ExtractValue( &_data[2], &scale, &precision );

			value->SetUnits( scale ? "F" : "C" );
			value->OnValueRefreshed( temperature );
			if( value->GetPrecision() != precision )
			{
				value->SetPrecision( precision );
			}
			value->Release();

			Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat setpoint report: Setpoint %s = %s%s", value->GetLabel().c_str(), value->GetValue().c_str(), value->GetUnits().c_str() );		
		}
		return true;
	} else if( ThermostatSetpointCmd_SupportedReport == (ThermostatSetpointCmd)_data[0] ) {
		if( Node* node = GetNodeUnsafe() )
		{
			// We have received the supported thermostat setpoints from the Z-Wave device
			Log::Write( LogLevel_Info, GetNodeId(), "Received supported thermostat setpoints" );		

			// Parse the data for the supported setpoints
			for( uint32 i=1; i<_length-1; ++i )
			{
				for( int32 bit=0; bit<8; ++bit )
				{
					if( ( _data[i] & (1<<bit) ) != 0 )
					{
						// Add supported setpoint
						int32 index = (int32)((i-1)<<3) + bit + m_setPointBase;
						if (index >= 3)
							index += 4;
						if( index < ThermostatSetpoint_Count )
						{
						  	node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, c_setpointName[index], "C", false, false, "0.0", 0 );
							Log::Write( LogLevel_Info, GetNodeId(), "    Added setpoint: %s", c_setpointName[index] );

							if (GetVersion() >= 3) {
								Msg* msg = new Msg( "ThermostatSetpointCmd_CapabilitiesGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
								msg->SetInstance( this, _instance );
								msg->Append( GetNodeId() );
								msg->Append( 3 );
								msg->Append( GetCommandClassId() );
								msg->Append( ThermostatSetpointCmd_CapabilitiesGet );
								msg->Append(index);
								msg->Append( GetDriver()->GetTransmitOptions() );
								SendSecureMsg(msg, Driver::MsgQueue_Send);
							}
						}
					}
				}
			}
		}
		ClearStaticRequest( StaticRequest_Values );
		return true;
	} else if ( ThermostatSetpointCmd_CapabilitiesReport == (ThermostatSetpointCmd)_data[0] ) {
		// We have received a thermostat setpoint value from the Z-Wave device
		uint8 type = _data[1]&0x0f;
		if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, type ) ) ) {
			uint8 max_scale;
			uint8 min_scale;
			uint8 max_precision = 0;
			uint8 min_precision = 0;
			string _max = ExtractValue( &_data[2], &max_scale, &max_precision );
			uint8 max_length = _data[2]&0x07;
			value->Release();
			string _min = ExtractValue( &_data[2+max_length], &min_scale, &min_precision );

			Log::Write( LogLevel_Info, GetNodeId(), "Received ThermostatSetpointCmd_CapabilitiesReport: Setpoint %s: Max:%s%s, Min:%s%s.", 
				c_setpointName[type], _max.c_str(), (max_scale ? "F" : "C"), _min.c_str(), (min_scale ? "F" : "C") );		
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::SetValue>
// Set a thermostat setpoint temperature
//-----------------------------------------------------------------------------
bool ThermostatSetpoint::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	if( ValueID::ValueType_Decimal == _value.GetID().GetType() )
	{
		ValueDecimal const* value = static_cast<ValueDecimal const*>(&_value);
		uint8 scale = strcmp( "C", value->GetUnits().c_str() ) ? 1 : 0;

		Msg* msg = new Msg( "Set Thermostat Setpoint", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
		msg->SetInstance( this, _value.GetID().GetInstance() );
		msg->Append( GetNodeId() );
		msg->Append( 4 + GetAppendValueSize( value->GetValue() ) );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatSetpointCmd_Set );
		msg->Append( value->GetID().GetIndex() );
		AppendValue( msg, value->GetValue(), scale );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	}

	if (_value.GetID().GetIndex() == ThermostatSetpointIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ThermostatSetpointIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetpoint::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ThermostatSetpoint::CreateVars
(
	uint8 const _instance,
	uint8 const _index
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, _index, "Setpoint", "C", false, false, "0.0", 0  );

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatSetpointIndex_IconName, "ThermostatSetpoint Icon", "icon", false, false, "", 0);
	}
}
