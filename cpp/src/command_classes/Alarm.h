//-----------------------------------------------------------------------------
//
//	Alarm.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_ALARM
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _Alarm_H
#define _Alarm_H

#include "CommandClass.h"

enum NotificationType
{
	NotificationType_Reserved = 0,
	NotificationType_Smoke,
	NotificationType_CO,
	NotificationType_CO2,
	NotificationType_Heat,
	NotificationType_Water,
	NotificationType_AccessControl,
	NotificationType_Burglar,
	NotificationType_PowerManagement,
	NotificationType_System,
	NotificationType_Emergency,
	NotificationType_Clock,
	NotificationType_Appliance,
	NotificationType_HomeHealth,
	NotificationType_MaxType, //no more than 30

	NotificationType_First = 0xff
};

static char const* c_notificationTypeNames[] = {
	"Undefined",
	"Smoke",
	"CO",
	"CO2",
	"Heat",
	"Water",
	"Access Control",
	"Home Security",
	"Power Management",
	"System",
	"Emergency",
	"Clock",
	"Appliance",
	"Home Health"
};

enum
{
	AlarmIndex_Type = 245,
	AlarmIndex_Level,

	AlarmIndex_Mode,
	AlarmIndex_TypeName,
	AlarmIndex_IconName = 250
};

namespace OpenZWave
{
	class ValueByte;
	class ValueList;
	class ValueString;

	/** \brief Implements COMMAND_CLASS_ALARM (0x71), a Z-Wave device command class.
	 */
	class Alarm: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new Alarm( _homeId, _nodeId ); }
		virtual ~Alarm(){}

		/** \brief Get command class ID (1 byte) identifying this command class. */
		static uint8 const StaticGetCommandClassId(){ return 0x71; }		
		/** \brief Get a string containing the name of this command class. */
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_ALARM"; }

		// From CommandClass
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		/** \brief Get command class ID (1 byte) identifying this command class. (Inherited from CommandClass) */
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		/** \brief Get a string containing the name of this command class. (Inherited from CommandClass) */
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		/** \brief Handle a response to a message associated with this command class. (Inherited from CommandClass) */
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );

		virtual bool SetValue( Value const& _value );

		virtual uint8 GetMaxVersion(){ return 4; }
		virtual void ReadXML( TiXmlElement const* _ccElement );
		virtual void WriteXML( TiXmlElement* _ccElement );

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		Alarm( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ){
			proprietary_alarm = false;
		}
		ValueList::Item CreateItem(string _label, int32 _value);
		bool HandleEvent(uint8 const* _data, uint32 const _length, uint32 const _instance);
		bool HandleAlarmReport_V1(uint8 const* _data, uint32 const _length, uint32 const _instance);
		string ExtractEventMsg(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event);

		/**
		 * \return command class id for event.
		 */
		uint8 CheckNormalEventParams(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event);

		/**
		 * \return true if has param.
		 */
		bool CheckSpecificEventParams(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event, uint8* _param_length);

		bool proprietary_alarm;
	};

} // namespace OpenZWave

#endif
