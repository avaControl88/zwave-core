//-----------------------------------------------------------------------------
//
//	SensorBinary.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SENSOR_BINARY
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "SensorBinary.h"
#include "WakeUp.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueBool.h"
#include "ValueString.h"

using namespace OpenZWave;

enum SensorBinaryCmd
{
	SensorBinaryCmd_Get		= 0x02,
	SensorBinaryCmd_Report		= 0x03,

	//Version 2
	SensorBinaryCmd_SupportedSensorGet = 0x01,
	SensorBinaryCmd_SupportedSensorReport = 0x04
};

enum {
	SensorBinaryIndex_Sensor = 240,
	SensorBinaryIndex_IconName = 250
};

enum SensorBinaryType {
	SensorBinaryType_Reserved = 0,
	SensorBinaryType_GeneralPurpose,
	SensorBinaryType_Smoke,
	SensorBinaryType_CO,
	SensorBinaryType_CO2,
	SensorBinaryType_Heat,
	SensorBinaryType_Water,
	SensorBinaryType_Freeze,
	SensorBinaryType_Tamper,
	SensorBinaryType_Aux,
	SensorBinaryType_DoorWindow,
	SensorBinaryType_Tilt,
	SensorBinaryType_Motion,
	SensorBinaryType_GlassBreak,
	SensorBinaryType_MaxType,

	SensorBinaryType_First = 0xff,
};

static char const* c_sensorBinaryTypeNames[] = {
	"Reserved",
	"General purpose",
	"Smoke",
	"CO",
	"CO2",
	"Heat",
	"Water",
	"Freeze",
	"Tamper",
	"Aux",
	"Door/Window",
	"Tilt",
	"Motion",
	"Glass Break"
};

//-----------------------------------------------------------------------------
// <SensorBinary::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool SensorBinary::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if (_requestFlags & RequestFlag_Static) {
		if (GetVersion() >= 2) {
			Msg* msg = new Msg( "SensorBinaryCmd_SupportedSensorGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( SensorBinaryCmd_SupportedSensorGet );
			msg->Append( GetDriver()->GetTransmitOptions() );
			res |= SendSecureMsg(msg, _queue);
		}
	}
	if( _requestFlags & RequestFlag_Dynamic )
	{
		res |= RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return res;
}

//-----------------------------------------------------------------------------
// <SensorBinary::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool SensorBinary::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if (GetVersion() > 1) {
		for( uint8 i = 0; i < SensorBinaryType_MaxType; i++ )
		{
			Value* value = GetValue( _instance, i );
			if( value != NULL ) {
				value->Release();
				Msg* msg = new Msg( "SensorBinaryCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->SetInstance( this, _instance );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( SensorBinaryCmd_Get );
				msg->Append( i );
				msg->Append( GetDriver()->GetTransmitOptions() );
				res |= SendSecureMsg(msg, _queue);
			}
		}
	} else {
		Msg* msg = new Msg( "SensorBinaryCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( SensorBinaryCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		res |= SendSecureMsg(msg, _queue);
	}
	return res;
}

//-----------------------------------------------------------------------------
// <SensorBinary::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool SensorBinary::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	if (SensorBinaryCmd_Report == (SensorBinaryCmd)_data[0]) {
		uint8 _sensor_value = _data[1];
		uint8 _sensor_type;

		if( ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, SensorBinaryIndex_Sensor ) ) )
		{
			value->OnValueRefreshed( _data[1] != 0 );
			value->Release();
		}

		if (GetVersion() == 1) {
			Log::Write( LogLevel_Info, GetNodeId(), "Received SensorBinary report: State=%s", _data[1] ? "On" : "Off" );
		} else {
			_sensor_type = _data[2];
			ValueBool* m_value = static_cast<ValueBool*>( GetValue( _instance, _sensor_type ) );
			if (m_value == NULL) {
				node->CreateValueBool(ValueID::ValueGenre_User, GetCommandClassId(), _instance, _sensor_type, c_sensorBinaryTypeNames[_sensor_type], "", true, false, false, 0  );
				m_value = static_cast<ValueBool*>( GetValue( _instance, _sensor_type ) );
			}
			m_value->OnValueRefreshed(_data[1] != 0);
			m_value->Release();
		}
		return true;
	} else if (SensorBinaryCmd_SupportedSensorReport == (SensorBinaryCmd)_data[0]) {
		string msg = "";
		for( uint8 i = 1; i <= ( _length - 2 ); i++ ) {
			for( uint8 j = 0; j < 8; j++ ) {
				if( _data[i] & ( 1 << j ) ) {
					if( msg != "" )
						msg += ", ";
					uint8 index = ( ( i - 1 ) * 8 ) + j;
					if (index < SensorBinaryType_MaxType)
						msg += c_sensorBinaryTypeNames[index];
					else {
						char temp_chr[128];
						memset(temp_chr, 0, sizeof(temp_chr));
						snprintf(temp_chr, sizeof(temp_chr), "Proprietary Type 0x%x", index);
						msg += temp_chr;
					}
					ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, index ) );
					if( value == NULL) {
						node->CreateValueBool(  ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, c_sensorBinaryTypeNames[index], "", true, false, false, 0  );
					}
				}
			}
		}
		Log::Write( LogLevel_Info, GetNodeId(), "Received SensorBinaryCmd_SupportedSensorReport from node %d: %s", GetNodeId(), msg.c_str() );
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SensorBinary::SetValueBasic>
// Update class values based in BASIC mapping
//-----------------------------------------------------------------------------
void SensorBinary::SetValueBasic
(
	uint8 const _instance,
	uint8 const _value
)
{
	if (mapBasicSet) {
		if( ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, SensorBinaryIndex_Sensor ) ) )
		{
			value->OnValueRefreshed( _value != 0 );
			value->Release();
		}
		return;
	}

	// Send a request for new value to synchronize it with the BASIC set/report.
	// In case the device is sleeping, we set the value anyway so the BASIC set/report
	// stays in sync with it. We must be careful mapping the uint8 BASIC value
	// into a class specific value.
	// When the device wakes up, the real requested value will be retrieved.
	RequestValue( 0, 0, _instance, Driver::MsgQueue_Send );
	if( Node* node = GetNodeUnsafe() )
	{
		if( WakeUp* wakeUp = static_cast<WakeUp*>( node->GetCommandClass( WakeUp::StaticGetCommandClassId() ) ) )
		{
			if( !wakeUp->IsAwake() )
			{
				if( ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, SensorBinaryIndex_Sensor ) ) )
				{
					value->OnValueRefreshed( _value != 0 );
					value->Release();
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
// <SensorBinary::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void SensorBinary::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueBool(  ValueID::ValueGenre_User, GetCommandClassId(), _instance, SensorBinaryIndex_Sensor, "Sensor", "", true, false, false, 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SensorBinaryIndex_IconName, "SensorBinary Icon", "icon", false, false, "", 0);
	}
}

void SensorBinary::ReadXML( TiXmlElement const* _ccElement ) {
	CommandClass::ReadXML( _ccElement );
	char const* str = _ccElement->Attribute("map_basic_set");
	if (str) {
		mapBasicSet = !strcmp( str, "true");
	}
}

void SensorBinary::WriteXML( TiXmlElement* _ccElement ) {
	CommandClass::WriteXML( _ccElement );
	if (mapBasicSet) {
		_ccElement->SetAttribute( "map_basic_set", "true" );
	}
}

//-----------------------------------------------------------------------------
// <SensorBinary::SetValue>
// Set the device's SensorBinary state
//-----------------------------------------------------------------------------
bool SensorBinary::SetValue (Value const& _value)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case SensorBinaryIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, SensorBinaryIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}
