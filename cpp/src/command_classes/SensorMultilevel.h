//-----------------------------------------------------------------------------
//
//	SensorMultilevel.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_SENSOR_MULTILEVEL
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _SensorMultilevel_H
#define _SensorMultilevel_H

#include "CommandClass.h"

enum SensorType
{
	SensorType_Temperature = 1,
	SensorType_General,
	SensorType_Luminance,
	SensorType_Power,
	SensorType_RelativeHumidity,
	SensorType_Velocity,
	SensorType_Direction,
	SensorType_AtmosphericPressure,
	SensorType_BarometricPressure,
	SensorType_SolarRadiation,
	SensorType_DewPoint,
	SensorType_RainRate,
	SensorType_TideLevel,
	SensorType_Weight,
	SensorType_Voltage,
	SensorType_Current,
	SensorType_CO2,
	SensorType_AirFlow,
	SensorType_TankCapacity,
	SensorType_Distance,
	SensorType_AnglePosition,
	SensorType_Rotation,
	SensorType_WaterTemperature,
	SensorType_SoilTemperature,
	SensorType_SeismicIntensity,
	SensorType_SeismicMagnitude,
	SensorType_Ultraviolet,
	SensorType_ElectricalResistivity,
	SensorType_ElectricalConductivity,
	SensorType_Loudness,
	SensorType_Moisture,
	SensorType_Frequency,
	SensorType_Time,
	SensorType_TargetTemperature,
	SensorType_ParticulateMatter,
	SensorType_FormaldehydeLevel,
	SensorType_RadonConcentration,
	SensorType_MethaneDensity,
	SensorType_VolatileOrganicCompound,
	SensorType_CarbonMonoxide,
	SensorType_SoilHumidity,
	SensorType_SoilReactivity,
	SensorType_SoilSalinity,
	SensorType_HeartRate,
	SensorType_BloodPressure,
	SensorType_MuscleMass,
	SensorType_FatMass,
	SensorType_BoneMass,
	SensorType_TotalBodyWater,
	SensorType_BasicMetabolicRate,
	SensorType_BodyMassIndex,
	SensorType_MaxType
};

static char const* c_sensorTypeNames[] = 
{
	"Undefined",
	"Temperature",
	"General",
	"Luminance",
	"Power",
	"Relative Humidity",
	"Velocity",
	"Direction",
	"Atmospheric Pressure",
	"Barometric Pressure",
	"Solar Radiation",
	"Dew Point",
	"Rain Rate",
	"Tide Level",
	"Weight",
	"Voltage",
	"Current",
	"CO2 Level",
	"Air Flow",
	"Tank Capacity",
	"Distance",
	"Angle Position",
	"Rotation",
	"Water Temperature",
	"Soil Temperature",
	"Seismic Intensity",
	"Seismic Magnitude",
	"Ultraviolet",
	"Electrical Resistivity",
	"Electrical Conductivity",
	"Loudness",
	"Moisture",
	"Frequency",
	"Time",
	"Target Temperature",
	"Particulate Matter",
	"Formaldehyde Level",
	"Radon Concentration",
	"Methane Density",
	"Volatile Organic Compound",
	"Carbon Monoxide",
	"Soil Humidity",
	"Soil Reactivity",
	"Soil Salinity",
	"Heart Rate",
	"Blood Pressure",
	"Muscle Mass",
	"Fat Mass",
	"Bone Mass",
	"Total Body Water",
	"Basic Metabolic Rate",
	"Body Mass Index"
};

namespace OpenZWave
{
	class ValueDecimal;

	/** \brief Implements COMMAND_CLASS_SENSOR_MULTILEVEL (0x31), a Z-Wave device command class.
	 */
	class SensorMultilevel: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new SensorMultilevel( _homeId, _nodeId ); }
		virtual ~SensorMultilevel(){}

		static uint8 const StaticGetCommandClassId(){ return 0x31; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_SENSOR_MULTILEVEL"; }

		// From CommandClass
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _dummy, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );
		
		virtual uint8 GetMaxVersion(){ return 7; }

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		SensorMultilevel( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ){}
	};

} // namespace OpenZWave


#endif

