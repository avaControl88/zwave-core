//-----------------------------------------------------------------------------
//
//	DoorLock.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_DOOR_LOCK
//
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "DoorLock.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"
#include "Security.h"
#include "WakeUp.h"

#include "ValueBool.h"
#include "ValueByte.h"
#include "ValueString.h"
#include "ValueList.h"

using namespace OpenZWave;

enum DoorLockCmd
{
	DoorLockCmd_Version					= 0x01,
	DoorLockCmd_OperationSet			= 0x01,
	DoorLockCmd_OperationGet			= 0x02,
	DoorLockCmd_OperationReport			= 0x03,
	DoorLockCmd_ConfigurationSet		= 0x04,
	DoorLockCmd_ConfigurationGet		= 0x05,
	DoorLockCmd_ConfigurationReport		= 0x06
};

enum DoorLockMode
{
	DoorLockMode_Operation_Unsecured					= 0x00,
	DoorLockMode_Operation_UnsecuredTimeout				= 0x01,
	DoorLockMode_Operation_UnsecuredInside				= 0x10,
	DoorLockMode_Operation_UnsecuredInsideTimeout		= 0x11,
	DoorLockMode_Operation_UnsecuredOutside				= 0x20,
	DoorLockMode_Operation_UnsecuredOutsideTimeout		= 0x21,
	DoorLockMode_Operation_StateUnknown					= 0xfe,
	DoorLockMode_Operation_Secured						= 0xff,

	DoorLockMode_Operation_Properties1_InsideMask		= 0x0f,
	DoorLockMode_Operation_Properties1_OutsideMask		= 0xf0,
	DoorLockMode_Operation_Properties1_OutsideShift		= 0x04,

	DoorLockMode_Configuration_ConstantOperation		= 0x01,
	DoorLockMode_Configuration_TimedOperation			= 0x02,

	DoorLockMode_Configuration_Properties1_InsideMask	= 0x0f,
	DoorLockMode_Configuration_Properties1_OutsideMask	= 0xf0,
	DoorLockMode_Configuration_Properties1_OutsideShift	= 0x04, 
};

enum DoorLockHandleMode
{
	DoorHandleMode_1 = 0x01,
	DoorHandleMode_2 = 0x02,
	DoorHandleMode_3 = 0x04,
	DoorHandleMode_4 = 0x08
};

enum DoorLockCondition
{
	DoorLockCondition_DoorOpenClose		= 0x01,
	DoorLockCondition_BoltLockUnlock	= 0x01 << 1,
	DoorLockCondition_LatchOpenClose	= 0x01 << 2,
	DoorLockCondition_Reserved1			= 0x01 << 3,
	DoorLockCondition_Reserved2			= 0x01 << 4,
	DoorLockCondition_Reserved3			= 0x01 << 5,
	DoorLockCondition_Reserved4			= 0x01 << 6,
	DoorLockCondition_Reserved5			= 0x01 << 7,
	DoorLockTimeout_NoUnlockPeriod		= 0xfe
};

enum
{
	DoorLockIndex_Mode = 0, 
	DoorLockIndex_Type, 
	DoorLockIndex_OutDoor1,
	DoorLockIndex_OutDoor2,
	DoorLockIndex_OutDoor3,
	DoorLockIndex_OutDoor4,
	DoorLockIndex_InDoor1,
	DoorLockIndex_InDoor2,
	DoorLockIndex_InDoor3,
	DoorLockIndex_InDoor4,
	DoorLockIndex_DoorOpenClose,
	DoorLockIndex_BoltLockUnlock,
	DoorLockIndex_LatchOpenClose,
	DoorLockIndex_TimeoutMinute,
	DoorLockIndex_TimeoutSecond,
	DoorLockIndex_ConfigManage,

	DoorLockIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <DoorLock::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool DoorLock::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( _requestFlags & RequestFlag_Dynamic ) {
		requests |= RequestValue( _requestFlags, DoorLockIndex_Mode, _instance, _queue );
		//requests |= RequestValue( _requestFlags, DoorLockIndex_ConfigManage, _instance, _queue );
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <DoorLock::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool DoorLock::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _index,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_index == DoorLockIndex_Mode) {
		Msg* msg = new Msg( "DoorLockCmd_OperationGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( DoorLockCmd_OperationGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (_index == DoorLockIndex_ConfigManage) {
		Msg* msg = new Msg( "DoorLockCmd_ConfigurationGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( DoorLockCmd_ConfigurationGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <DoorLock::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool DoorLock::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (DoorLockCmd)_data[0] )
	{
		case DoorLockCmd_OperationReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received DoorLock operation report.");
			
			Log::Write(LogLevel_Info,  GetNodeId(), "Operation mode:0x%.2x", _data[1]);
			if (ValueList* m_list = static_cast<ValueList*>( GetValue(_instance, DoorLockIndex_Mode))) {
				m_list->OnValueRefreshed(_data[1]);
				m_list->Release();
			}

			bool handle1, handle2, handle3, handle4;
			uint8 inside = (_data[2] & DoorLockMode_Operation_Properties1_InsideMask);
			uint8 outside = (_data[2] & 
				DoorLockMode_Operation_Properties1_OutsideMask) >> DoorLockMode_Configuration_Properties1_OutsideShift;
			handle1 = ((inside & DoorHandleMode_1) != 0); handle2 = ((inside & DoorHandleMode_2) != 0);
			handle3 = ((inside & DoorHandleMode_3) != 0); handle4 = ((inside & DoorHandleMode_4) !=0);
			Log::Write(LogLevel_Info,  GetNodeId(), "Inside door handles mode: 1:%s, 2:%s, 3:%s, 4:%s", 
				(handle1 ? "active":"inactive"), (handle2 ? "active":"inactive"), 
				(handle3 ? "active":"inactive"), (handle4 ? "active":"inactive"));
			if (ValueBool* m_value_In_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor1))) {
				m_value_In_1->OnValueRefreshed(handle1);
				m_value_In_1->Release();
			}
			if (ValueBool* m_value_In_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor2))) {
				m_value_In_2->OnValueRefreshed(handle2);
				m_value_In_2->Release();
			}
			if (ValueBool* m_value_In_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor3))) {
				m_value_In_3->OnValueRefreshed(handle3);
				m_value_In_3->Release();
			}
			if (ValueBool* m_value_In_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor4))) {
				m_value_In_4->OnValueRefreshed(handle4);
				m_value_In_4->Release();
			}
			handle1 = ((outside & DoorHandleMode_1) != 0); handle2 = ((outside & DoorHandleMode_2) != 0);
			handle3 = ((outside & DoorHandleMode_3) != 0); handle4 = ((outside & DoorHandleMode_4) != 0);
			Log::Write(LogLevel_Info,  GetNodeId(), "Outside door handles mode: 1:%s, 2:%s, 3:%s, 4:%s", 
				(handle1 ? "active":"inactive"), (handle2 ? "active":"inactive"), 
				(handle3 ? "active":"inactive"), (handle4 ? "active":"inactive"));
			if (ValueBool* m_value_Out_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor1))) {
				m_value_Out_1->OnValueRefreshed(handle1);
				m_value_Out_1->Release();
			}
			if (ValueBool* m_value_Out_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor2))) {
				m_value_Out_2->OnValueRefreshed(handle2);
				m_value_Out_2->Release();
			}
			if (ValueBool* m_value_Out_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor3))) {
				m_value_Out_3->OnValueRefreshed(handle3);
				m_value_Out_3->Release();
			}
			if (ValueBool* m_value_Out_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor4))) {
				m_value_Out_4->OnValueRefreshed(handle4);
				m_value_Out_4->Release();
			}

			bool door_state, bolt_state, latch_state;
			door_state = ((_data[3] & DoorLockCondition_DoorOpenClose) != 0);
			bolt_state = ((_data[3] & DoorLockCondition_BoltLockUnlock) != 0);
			latch_state = ((_data[3] & DoorLockCondition_LatchOpenClose) != 0);
			Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock: door %s, bolt %s, latch %s", 
				(door_state ? "open":"closed"), (bolt_state ? "locked":"unlocked"), (latch_state ? "open":"closed"));
			if (ValueBool* m_value_D_opcl = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_DoorOpenClose))) {
				m_value_D_opcl->OnValueRefreshed(door_state);
				m_value_D_opcl->Release();
			}
			if (ValueBool* m_value_lul = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_BoltLockUnlock))) {
				m_value_lul->OnValueRefreshed(bolt_state);
				m_value_lul->Release();
			}
			if (ValueBool* m_value_L_opcl = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_LatchOpenClose))) {
				m_value_L_opcl->OnValueRefreshed(latch_state);
				m_value_L_opcl->Release();
			}

			if ( _data[4] == DoorLockTimeout_NoUnlockPeriod )
				Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock timeout: No unlock period.");
			else
				Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock timeout: %d min %s sec.", _data[4], _data[5]);
			if (ValueByte* m_value_min = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutMinute))) {
				m_value_min->OnValueRefreshed(_data[4]);
				m_value_min->Release();
			}
			if (ValueByte* m_value_sec = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutSecond))) {
				m_value_sec->OnValueRefreshed(_data[5]);
				m_value_sec->Release();
			}

			if (ValueString* m_value_config = static_cast<ValueString*>(GetValue(_instance, DoorLockIndex_ConfigManage))) {
				char tmp_chr[256];
				string tmp_value;
				uint8 opType = 0;
				memset(tmp_chr, 0, sizeof(tmp_chr));

				if (ValueList* m_list = static_cast<ValueList*>( GetValue(_instance, DoorLockIndex_Type))) {
					ValueList::Item m_item = m_list->GetItem();
					opType = m_item.m_value;
					m_list->Release();
				}
				snprintf(tmp_chr, sizeof(tmp_chr), "%d-%d-%d:%d", opType, _data[2], _data[4], _data[5]);
				tmp_value = tmp_chr;
				m_value_config->OnValueRefreshed(tmp_value);
				m_value_config->Release();
			}
			break;
		}
		case DoorLockCmd_ConfigurationReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received DoorLockCmd_ConfigurationReport.");

			Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock operation type: %s", 
				((_data[1] & DoorLockMode_Configuration_ConstantOperation) ? 
				"Constant operation":((_data[1] & DoorLockMode_Configuration_TimedOperation) ? "Timed operation":"NULL")));
			if (ValueList* m_list = static_cast<ValueList*>( GetValue(_instance, DoorLockIndex_Type))) {
				m_list->OnValueRefreshed(_data[1]);
				m_list->Release();
			}

			bool handle1, handle2, handle3, handle4;
			uint8 inside = (_data[2] & DoorLockMode_Operation_Properties1_InsideMask);
			uint8 outside = (_data[2] & 
				DoorLockMode_Operation_Properties1_OutsideMask) >> DoorLockMode_Configuration_Properties1_OutsideShift;
			handle1 = ((inside & DoorHandleMode_1) != 0); handle2 = ((inside & DoorHandleMode_2) != 0);
			handle3 = ((inside & DoorHandleMode_3) != 0); handle4 = ((inside & DoorHandleMode_4) != 0);
			Log::Write(LogLevel_Info,  GetNodeId(), "Inside door handles mode: 1:%s, 2:%s, 3:%s, 4:%s", 
				(handle1 ? "active":"inactive"), (handle2 ? "active":"inactive"), 
				(handle3 ? "active":"inactive"), (handle4 ? "active":"inactive"));
			if (ValueBool* m_value_In_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor1))) {
				m_value_In_1->OnValueRefreshed(handle1);
				m_value_In_1->Release();
			}
			if (ValueBool* m_value_In_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor2))) {
				m_value_In_2->OnValueRefreshed(handle2);
				m_value_In_2->Release();
			}
			if (ValueBool* m_value_In_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor3))) {
				m_value_In_3->OnValueRefreshed(handle3);
				m_value_In_3->Release();
			}
			if (ValueBool* m_value_In_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor4))) {
				m_value_In_4->OnValueRefreshed(handle4);
				m_value_In_4->Release();
			}
			handle1 = ((outside & DoorHandleMode_1) != 0); handle2 = ((outside & DoorHandleMode_2) != 0);
			handle3 = ((outside & DoorHandleMode_3) != 0); handle4 = ((outside & DoorHandleMode_4) != 0);
			Log::Write(LogLevel_Info,  GetNodeId(), "Outside door handles mode: 1:%s, 2:%s, 3:%s, 4:%s", 
				(handle1 ? "active":"inactive"), (handle2 ? "active":"inactive"), 
				(handle3 ? "active":"inactive"), (handle4 ? "active":"inactive"));
			if (ValueBool* m_value_Out_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor1))) {
				m_value_Out_1->OnValueRefreshed(handle1);
				m_value_Out_1->Release();
			}
			if (ValueBool* m_value_Out_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor2))) {
				m_value_Out_2->OnValueRefreshed(handle2);
				m_value_Out_2->Release();
			}
			if (ValueBool* m_value_Out_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor3))) {
				m_value_Out_3->OnValueRefreshed(handle3);
				m_value_Out_3->Release();
			}
			if (ValueBool* m_value_Out_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor4))) {
				m_value_Out_4->OnValueRefreshed(handle4);
				m_value_Out_4->Release();
			}

			if ( _data[3] == DoorLockTimeout_NoUnlockPeriod )
				Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock timeout: No unlock period.");
			else
				Log::Write(LogLevel_Info,  GetNodeId(), "DoorLock timeout: %d min %s sec.", _data[3], _data[4]);
			if (ValueByte* m_value_min = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutMinute))) {
				m_value_min->OnValueRefreshed(_data[3]);
				m_value_min->Release();
			}
			if (ValueByte* m_value_sec = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutSecond))) {
				m_value_sec->OnValueRefreshed(_data[4]);
				m_value_sec->Release();
			}

			if (ValueString* m_value_config = static_cast<ValueString*>(GetValue(_instance, DoorLockIndex_ConfigManage))) {
				char tmp_chr[256];
				string tmp_value;
				uint8 opType = 0;
				memset(tmp_chr, 0, sizeof(tmp_chr));

				if (ValueList* m_list = static_cast<ValueList*>( GetValue(_instance, DoorLockIndex_Type))) {
					ValueList::Item m_item = m_list->GetItem();
					opType = m_item.m_value;
					m_list->Release();
				}
				snprintf(tmp_chr, sizeof(tmp_chr), "%d-%d-%d:%d", opType, _data[2], _data[3], _data[4]);
				tmp_value = tmp_chr;
				m_value_config->OnValueRefreshed(tmp_value);
				m_value_config->Release();
			}
			break;
		}
		default:
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// <DoorLock::SetValueBasic>
// Update class values based in BASIC mapping
//-----------------------------------------------------------------------------
void DoorLock::SetValueBasic (uint8 const _instance, uint8 const _value) {
	// Send a request for new value to synchronize it with the BASIC set/report.
	// In case the device is sleeping, we set the value anyway so the BASIC set/report
	// stays in sync with it. We must be careful mapping the uint8 BASIC value
	// into a class specific value.
	// When the device wakes up, the real requested value will be retrieved.
	RequestValue( 0, DoorLockIndex_Mode, _instance, Driver::MsgQueue_Send );
	if( Node* node = GetNodeUnsafe() )
	{
		if( WakeUp* wakeUp = static_cast<WakeUp*>( node->GetCommandClass( WakeUp::StaticGetCommandClassId() ) ) )
		{
			if( !wakeUp->IsAwake() )
			{
				if( ValueList* value_list = static_cast<ValueList*>( GetValue( _instance, DoorLockIndex_Mode ) ) )
				{
					if (_value == 0)
						value_list->OnValueRefreshed(DoorLockMode_Operation_Unsecured);
					else 
						value_list->OnValueRefreshed(DoorLockMode_Operation_Secured);
					value_list->Release();
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
// <DoorLock::SetValue>
// Set the DoorLock's state
//-----------------------------------------------------------------------------
bool DoorLock::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case DoorLockIndex_Mode: 
		{
			ValueList const* m_value = static_cast<ValueList const*>(&_value);
			uint8 mode = (uint8)m_value->GetItem().m_value;

			Msg* msg = new Msg( "DoorLockCmd_OperationSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( DoorLockCmd_OperationSet );
			msg->Append( mode );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case DoorLockIndex_ConfigManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();//(operation type)-(door mask)-(timeout minute):(timeout second)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string opType_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, opType_str.size()+1);

				string doorMask_str, timeoutMinute_str, timeoutSecond_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					doorMask_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, doorMask_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					timeoutMinute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, timeoutMinute_str.size()+1);
				} else
					return false;
				timeoutSecond_str = tmp_str;

				uint8 opType = atoi(opType_str.c_str());
				uint8 doorMask = atoi(doorMask_str.c_str());
				uint8 timeoutMinute = atoi(timeoutMinute_str.c_str());
				uint8 timeoutSecond = atoi(timeoutSecond_str.c_str());

				if ((opType == 1 || opType == 2) && (timeoutMinute < 255 && timeoutMinute >= 0) && (timeoutSecond < 60 && timeoutSecond >= 0)) {
					Msg* msg = new Msg( "DoorLockCmd_ConfigurationSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 6 );
					msg->Append( GetCommandClassId() );
					msg->Append( DoorLockCmd_ConfigurationSet );
					msg->Append( opType );
					msg->Append( doorMask );
					msg->Append( timeoutMinute );
					msg->Append( timeoutSecond );
					msg->Append( GetDriver()->GetTransmitOptions() );
					return SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
				break;
			}
		}
		case DoorLockIndex_Type:
		{
			if (ValueList const* m_value_list = static_cast<ValueList const*>(&_value)) {
				uint8 m_type = (uint8)m_value_list->GetItem().m_value;
				if (ValueList* m_list = static_cast<ValueList*>( GetValue(instance, DoorLockIndex_Type))) {
					m_list->OnValueRefreshed(m_type);
					m_list->Release();
				}
				UpdateConfigManager(instance);
				return true;
			}
			break;
		}
		case DoorLockIndex_OutDoor1:
		case DoorLockIndex_OutDoor2:
		case DoorLockIndex_OutDoor3:
		case DoorLockIndex_OutDoor4:
		case DoorLockIndex_InDoor1:
		case DoorLockIndex_InDoor2:
		case DoorLockIndex_InDoor3:
		case DoorLockIndex_InDoor4:
		{
			if (ValueBool const* m_value = static_cast<ValueBool const*>(&_value)) {
				if (ValueBool* m_value_bool = static_cast<ValueBool*>( GetValue(instance, _value.GetID().GetIndex()))) {
					m_value_bool->OnValueRefreshed(m_value->GetValue());
					m_value_bool->Release();
				}
				UpdateConfigManager(instance);
				return true;
			}
			break;
		}
		case DoorLockIndex_TimeoutMinute:
		case DoorLockIndex_TimeoutSecond:
		{
			if (ValueByte const* m_value = static_cast<ValueByte const*>(&_value)) {
				if (ValueByte* m_value_byte = static_cast<ValueByte*>( GetValue(instance, _value.GetID().GetIndex()))) {
					m_value_byte->OnValueRefreshed(m_value->GetValue());
					m_value_byte->Release();
				}
				UpdateConfigManager(instance);
				return true;
			}
			return true;
		}
		case DoorLockIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, DoorLockIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <DoorLock::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void DoorLock::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		vector<ValueList::Item> mode_items;
		mode_items.push_back(CreateItem("Unsecured", DoorLockMode_Operation_Unsecured));
		mode_items.push_back(CreateItem("Unsecured with timeout", DoorLockMode_Operation_UnsecuredTimeout));
		mode_items.push_back(CreateItem("Unsecured for inside door", DoorLockMode_Operation_UnsecuredInside));
		mode_items.push_back(CreateItem("Unsecured for inside door with timeout", DoorLockMode_Operation_UnsecuredInsideTimeout));
		mode_items.push_back(CreateItem("Unsecured for outside door", DoorLockMode_Operation_UnsecuredOutside));
		mode_items.push_back(CreateItem("Unsecured for outside door with timeout", DoorLockMode_Operation_UnsecuredOutsideTimeout));
		mode_items.push_back(CreateItem("Unknown state", DoorLockMode_Operation_StateUnknown));
		mode_items.push_back(CreateItem("Secured", DoorLockMode_Operation_Secured));
		node->CreateValueList( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockIndex_Mode, 
			"Mode", "", false, false, 1, mode_items, mode_items[0].m_value, 0);

		vector<ValueList::Item> type_items;
		type_items.push_back(CreateItem("Constant operation", DoorLockMode_Configuration_ConstantOperation));
		type_items.push_back(CreateItem("Timed operation", DoorLockMode_Configuration_TimedOperation));
		node->CreateValueList( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_Type, 
			"Operation Type", "", false, false, 1, type_items, type_items[0].m_value, 0);

		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_OutDoor1, 
			"Out door handle 1", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_OutDoor2, 
			"Out door handle 2", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_OutDoor3, 
			"Out door handle 3", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_OutDoor4, 
			"Out door handle 4", "", false, false, false, 0);

		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_InDoor1, 
			"In door handle 1", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_InDoor2, 
			"In door handle 2", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_InDoor3, 
			"In door handle 3", "", false, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_InDoor4, 
			"In door handle 4", "", false, false, false, 0);

		node->CreateValueBool( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockIndex_DoorOpenClose, 
			"Door status", "", true, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockIndex_BoltLockUnlock, 
			"Bolt status", "", true, false, false, 0);
		node->CreateValueBool( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockIndex_LatchOpenClose, 
			"Latch status", "", true, false, false, 0);

		node->CreateValueByte( ValueID::ValueGenre_System, GetCommandClassId(), _instance, DoorLockIndex_TimeoutMinute, 
			"Doorlock timeout", "Minutes", false, false, 0, 0);
		node->CreateValueByte( ValueID::ValueGenre_System, GetCommandClassId(), _instance, DoorLockIndex_TimeoutSecond, 
			"Doorlock timeout", "Seconds", false, false, 0, 0);

		node->CreateValueString( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, DoorLockIndex_ConfigManage, 
			"DoorLock config", "", false, false, "", 0);

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockIndex_IconName, "DoorLock Icon", "icon", false, false, "", 0);
	}
}

ValueList::Item DoorLock::CreateItem(string _label, int32 _value)
{
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

void DoorLock::UpdateConfigManager( uint8 const _instance )
{
	if (ValueString* m_value_config = static_cast<ValueString*>(GetValue(_instance, DoorLockIndex_ConfigManage))) {
		char tmp_chr[256];
		uint8 inside = 0;
		uint8 outside = 0;
		string tmp_value;
		uint8 opType = 0, doorState = 0, minut = 0, secd = 0;
		memset(tmp_chr, 0, sizeof(tmp_chr));

		if (ValueList* m_list = static_cast<ValueList*>( GetValue(_instance, DoorLockIndex_Type))) {
			ValueList::Item m_item = m_list->GetItem();
			opType = m_item.m_value;
			m_list->Release();
		}

		if (ValueBool* m_value_In_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor1))) {
			if (m_value_In_1->GetValue())
				inside |= DoorHandleMode_1;
			m_value_In_1->Release();
		}
		if (ValueBool* m_value_In_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor2))) {
			if (m_value_In_2->GetValue())
				inside |= DoorHandleMode_2;
			m_value_In_2->Release();
		}
		if (ValueBool* m_value_In_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor3))) {
			if (m_value_In_3->GetValue())
				inside |= DoorHandleMode_3;
			m_value_In_3->Release();
		}
		if (ValueBool* m_value_In_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_InDoor4))) {
			if (m_value_In_4->GetValue())
				inside |= DoorHandleMode_4;
			m_value_In_4->Release();
		}
		if (ValueBool* m_value_Out_1 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor1))) {
			if (m_value_Out_1->GetValue())
				outside |= DoorHandleMode_1;
			m_value_Out_1->Release();
		}
		if (ValueBool* m_value_Out_2 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor2))) {
			if (m_value_Out_2->GetValue())
				outside |= DoorHandleMode_2;
			m_value_Out_2->Release();
		}
		if (ValueBool* m_value_Out_3 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor3))) {
			if (m_value_Out_3->GetValue())
				outside |= DoorHandleMode_3;
			m_value_Out_3->Release();
		}
		if (ValueBool* m_value_Out_4 = static_cast<ValueBool*>(GetValue(_instance, DoorLockIndex_OutDoor4))) {
			if (m_value_Out_4->GetValue())
				outside |= DoorHandleMode_4;
			m_value_Out_4->Release();
		}
		doorState = inside + (outside << DoorLockMode_Configuration_Properties1_OutsideShift);

		if (ValueByte* m_value_min = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutMinute))) {
			minut = m_value_min->GetValue();
			m_value_min->Release();
		}
		if (ValueByte* m_value_sec = static_cast<ValueByte*>(GetValue(_instance, DoorLockIndex_TimeoutSecond))) {
			secd = m_value_sec->GetValue();
			m_value_sec->Release();
		}

		snprintf(tmp_chr, sizeof(tmp_chr), "%d-%d-%d:%d", opType, doorState, minut, secd);
		tmp_value = tmp_chr;
		m_value_config->OnValueRefreshed(tmp_value);
		m_value_config->Release();
	}
}
