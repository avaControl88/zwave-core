//-----------------------------------------------------------------------------
//
//	Security.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_Security
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------
#ifndef _Security_H
#define _Security_H

#include "CommandClass.h"

//#define SECURITY_SUPPORT 1

namespace OpenZWave
{
	class ValueString;
	class ValueBool;
	class Mutex;
	class ValueButton;

	class SecurityPayload
	{
	public:
		uint8			m_length;
		uint8			m_part;
		uint8			m_data[256];
		bool			m_replyRequired;

		uint8						m_receiverNonce[8];
		uint8						m_senderNonce[8];
		void Reset() { 
			m_length = 0;
			m_part = 0;
			memset(m_data, 0, sizeof(m_data));
			m_replyRequired = false;
		}
		void SetReceiver(uint8 const* _receiver) {
			memcpy(m_receiverNonce, _receiver, sizeof(m_receiverNonce));
		}
		void SetSender(uint8 const* _sender) {
			memcpy(m_senderNonce, _sender, sizeof(m_senderNonce));
		}
	};

	/** \brief Implements COMMAND_CLASS_SECURITY (0x98), a Z-Wave device command class.
	 */
	class Security: public CommandClass
	{
	public:

#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
		enum SecurityIncludeStage
		{
			SecurityIncludeStage_None = 0,
			SecurityIncludeStage_Start,
			SecurityIncludeStage_SchemeGet,
			SecurityIncludeStage_SchemeReport,
			SecurityIncludeStage_NonceGet1,
			SecurityIncludeStage_NonceReport1,
			SecurityIncludeStage_KeySet,
			SecurityIncludeStage_NonceGet2,
			SecurityIncludeStage_NonceReport2,
			SecurityIncludeStage_KeyVerify,

			//if target is controller
			SecurityIncludeStage_SecuritySchemeInherit,
			SecurityIncludeStage_SecuritySchemeReport,

			SecurityIncludeStage_Finish,
			SecurityIncludeStage_Error
		};

		SecurityIncludeStage GetCurrentStage()const { return m_securityStage; }
		virtual bool HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */ );
		virtual void initSupportSetting();
#endif

		virtual ~Security(){ m_queueMutex->Release(); m_decryptMutex->Release(); }
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new Security( _homeId, _nodeId ); }

		static uint8 const StaticGetCommandClassId(){ return 0x98; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_SECURITY"; }
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue);
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool HandleMsg(uint8 const* _data, uint32 const _length, uint32 const _instance = 1);
		void SendMsg(Msg* _msg);
		virtual bool SetValue( Value const& _value );
		virtual void ReadXML( TiXmlElement const* _ccElement );
		virtual void WriteXML( TiXmlElement* _ccElement );
	private:
		Security( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ){
			memset(m_privatekey, 0, sizeof(m_privatekey));
			memset(m_KeyE, 0, sizeof(m_KeyE));
			memset(m_KeyA, 0, sizeof(m_KeyA));
			memset(m_receiverNonce, 0, sizeof(m_receiverNonce));
			memset(m_senderNonce, 0, sizeof(m_senderNonce));
			m_queueMutex = new Mutex();
			m_decryptMutex = new Mutex();
			m_waitingForNonce = false;
			m_waitingForMsgEncap = false;
			memset(m_decryptPayload.m_data, 0, sizeof(m_decryptPayload.m_data));
			m_decryptPayload.m_length = 0;
			m_handlePayload.Reset();
			m_supportedReport = false;
			m_includingStart = false;
			m_securityMode = true;
			//InitKeyE_KeyA();
#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
			m_securityStage = SecurityIncludeStage_None;
#endif
		}
		virtual void CreateVars( uint8 const _instance );

#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
		bool HandleMsg_Secondary(uint8 const* _data, uint32 const _length, uint32 const _instance = 1);
		bool SecurityStageHandler(uint8 const* _data, uint32 const _length, uint32 const _instance = 1);
		void SetCurrentStage(SecurityIncludeStage _stage) { m_securityStage = _stage; }
#endif

		bool HandleMsg_Central(uint8 const* _data, uint32 const _length, uint32 const _instance = 1);
		void QueuePayload(SecurityPayload const& _payload);
		void RequestNonce();
		bool EncryptMessage(uint8 const* _nonce);
		bool DecryptMessage(uint8 const* _data, uint32 const _length);
		void GenerateAuthentication(uint8 const* _data,	uint32 const _datalength, uint8 const header, 
			uint8 const _sendingNode, uint8 const _receivingNode, uint8 const* _IV, SecurityPayload *_secure_payload);
		bool CheckMAC(uint8 const* _data, uint32 const _datalength, uint8 const header, 
			uint8 const _sendingNode, uint8 const _receivingNode, uint8 const* _IV, uint8 const* _mac);
		void GenerateSecureMessage(const SecurityPayload *_payload, 
			uint8 const* _sender_nonce, uint8 const* _receiver_nonce, 
			SecurityPayload *_secure_payload);
		bool DecryptSecureMessage(uint8 const* _data, uint32 const _datalength, 
			uint8 const* _sender_nonce, uint8 const* _receiver_nonce, 
			SecurityPayload *_secure_payload);
		void SendNonceReport();

		void SendMessageEncap();
		bool CheckIsZero(uint8 *buffer, uint8 length);
		void UpdateSecurityNodeInfo(uint8 const* _data, uint32 const _length);

		void InitAllKey(uint8 const _instance);
		void InitKeyE_KeyA();
		
		void HandleDecryptMsg(uint8 const* _data, uint32 const _length);
		bool HandleMsg_SupportedCmd(uint8 const* _data, uint32 const _length);
		bool HandleMsg_SchemeCmd(uint8 const* _data, uint32 const _length, bool _secure);
		bool HandleMsg_NetworkKeyCmd(uint8 const* _data, uint32 const _length);
		void StartIncluding();
		void StopIncluding();
		bool InIncluding();

		Mutex*						m_queueMutex;
		list<SecurityPayload>		m_queue;
		bool						m_waitingForNonce;
		bool						m_waitingForMsgEncap;
		uint8						m_initializationVector[16];
		uint8						m_privatekey[16];
		uint8						m_KeyE[16];
		uint8						m_KeyA[16];
		TimeStamp					m_send_nonceTimer;
		TimeStamp					m_receive_nonceTimer;
		TimeStamp					m_includingTimer;
		uint8						m_sequenceCounter;

		uint8						m_receiverNonce[8];
		uint8						m_senderNonce[8];
		uint8						m_encryptKey[16];
		uint8						m_authKey[16];

		Mutex*						m_decryptMutex;
		SecurityPayload				m_decryptPayload;
		SecurityPayload				m_handlePayload;

		bool						m_supportedReport;
		bool						m_includingStart;
		bool						m_securityMode;
#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
		SecurityIncludeStage		m_securityStage;
#endif
	};

} // namespace OpenZWave

#endif
