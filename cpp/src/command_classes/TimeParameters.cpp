//-----------------------------------------------------------------------------
//
//	TimeParameters.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_TIME_PARAMETERS
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "TimeParameters.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum TimeParametersCmd
{
	TimeParametersCmd_Version	= 0x01,
	TimeParametersCmd_Set		= 0x01,
	TimeParametersCmd_Get		= 0x02,
	TimeParametersCmd_Report	= 0x03
};

 enum
 {
	 TimeParametersIndex_TimeManage = 0, 

	 TimeParametersIndex_IconName = 99
 };

//-----------------------------------------------------------------------------
// <TimeParameters::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool TimeParameters::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( _requestFlags & RequestFlag_Dynamic ) {
		requests = RequestValue( _requestFlags, TimeParametersCmd_Get, _instance, _queue );
	}
	return requests;
}

//-----------------------------------------------------------------------------
// <TimeParameters::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool TimeParameters::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_getTypeEnum == TimeParametersCmd_Get || _getTypeEnum == TimeParametersIndex_TimeManage) {
		Msg* msg = new Msg( "TimeParametersCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( TimeParametersCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <TimeParameters::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool TimeParameters::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (TimeParametersCmd)_data[0] )
	{
		case TimeParametersCmd_Report:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received TimeParametersCmd_Report.");
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, TimeParametersIndex_TimeManage))) {
				string tmp_str = "";//year(16bits)/(month)/(day)/(hour):(minute):(second)
				char byteStr[256];
				uint16 _year = (_data[1]<<8 | _data[2]);
				snprintf( byteStr, sizeof(byteStr), "%d/%d/%d/%d:%d:%d", 
					_year, _data[3], _data[4], _data[5], _data[6], _data[7]);
				tmp_str += byteStr;
				m_value->OnValueRefreshed(tmp_str);
				m_value->Release();
			}
			break;
		}
		default:
		{
			return false;
		}
	}
	return true;
}

//-----------------------------------------------------------------------------
// <TimeParameters::SetValue>
// Set the TimeParameters's state
//-----------------------------------------------------------------------------
bool TimeParameters::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case TimeParametersIndex_TimeManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//year(16bit)/(month)/(day)/(hour):(minute):(second)
			int split_p = tmp_str.find_first_of("/", 0);
			if (split_p > 0) {
				string year_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, year_str.size()+1);

				string month_str, day_str, hour_str, minute_str, second_str;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					month_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, month_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					day_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, day_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					hour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, hour_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					minute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, minute_str.size()+1);
				} else
					return false;
				second_str = tmp_str;

				uint16 year = atoi(year_str.c_str());
				uint8 month = atoi(month_str.c_str());
				uint8 day = atoi(day_str.c_str());
				uint8 hour = atoi(hour_str.c_str());
				uint8 minute = atoi(minute_str.c_str());
				uint8 second = atoi(second_str.c_str());

				if (year >=2000 && (month > 0 && month <= 12) && 
					(day > 0 && day <= 31) && hour < 24 && minute < 60 && second < 60) {
						Msg* msg = new Msg( "TimeParametersCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
						msg->SetInstance( this, instance );
						msg->Append( GetNodeId() );
						msg->Append( 9 );
						msg->Append( GetCommandClassId() );
						msg->Append( TimeParametersCmd_Set );
						msg->Append( (year & 0xff00)>>8 );
						msg->Append( (year & 0xff) );
						msg->Append( month );
						msg->Append( day );
						msg->Append( hour );
						msg->Append( minute );
						msg->Append( second );
						msg->Append( GetDriver()->GetTransmitOptions() );
						return SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			}
		}
		case TimeParametersIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, TimeParametersIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <TimeParameters::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void TimeParameters::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() ) {
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, TimeParametersIndex_TimeManage, 
			"Time manage", "string", false, false, "", 0);

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, TimeParametersIndex_IconName, "TimeParameters Icon", "icon", false, false, "", 0);
	}
}

