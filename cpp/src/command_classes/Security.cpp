//-----------------------------------------------------------------------------
//
//	Security.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_Security
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------
#include "CommandClasses.h"
#include "Security.h"
#include "Association.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Notification.h"
#include "Log.h"

#include "ValueString.h"
#include "ValueBool.h"
#include "ValueButton.h"

#include "ZwavePlusInfo.h"
#include "Association.h"
#include "AssociationGroupInfo.h"
#include "Version.h"
#include "ManufacturerSpecific.h"
#include "Security.h"
#include "DeviceResetLocally.h"
#include "ApplicationStatus.h"
#include "Powerlevel.h"

using namespace OpenZWave;

#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include <cryptopp/filters.h>
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include <cryptopp/aes.h>
using CryptoPP::AES;

#include <cryptopp/modes.h>
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;

#include <cryptopp/secblock.h>
using CryptoPP::SecByteBlock;
SecByteBlock HexDecodeString(const char *hex);
void showArray(uint8 *buffer, const char* name, uint8 length);

OFB_Mode< AES >::Encryption e;
OFB_Mode< AES >::Decryption d;
CBC_Mode< AES >::Encryption cbc_e;
CBC_Mode< AES >::Decryption cbc_d;

enum SecurityCmd
{
	SecurityCmd_SupportedGet			= 0x02,
	SecurityCmd_SupportedReport			= 0x03,
	SecurityCmd_SchemeGet				= 0x04,
	SecurityCmd_SchemeReport			= 0x05,
	SecurityCmd_NetworkKeySet			= 0x06,
	SecurityCmd_NetworkKeyVerify		= 0x07,
	SecurityCmd_SchemeInherit			= 0x08,
	SecurityCmd_NonceGet				= 0x40,
	SecurityCmd_NonceReport				= 0x80,
	SecurityCmd_MessageEncap			= 0x81,
	SecurityCmd_MessageEncapNonceGet	= 0xc1
};

enum
{
	SecurityScheme_Zero					= 0x00,//David
	SecurityScheme_Reserved1			= 0x02,
	SecurityScheme_Reserved2			= 0x04,
	SecurityScheme_Reserved3			= 0x08,
	SecurityScheme_Reserved4			= 0x10,
	SecurityScheme_Reserved5			= 0x20,
	SecurityScheme_Reserved6			= 0x40,
	SecurityScheme_Reserved7			= 0x80
};

enum
{
	SecurityIndex_NetworkKey = 0,
	SecurityIndex_SecurityMode,
	SecurityIndex_DebugInfo
};

//#define USE_DEBUG_KEY 1
#define CRITICAL_CHECK 1
#define SECURITY_TIMEOUT 10*1000

//-----------------------------------------------------------------------------
// <Security::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool Security::RequestState
( 
	uint32 const _requestFlags, 
	uint8 const _instance, 
	Driver::MsgQueue const _queue
)
{
	if( _instance != 1 )
		return false;
	if (IsDriverNode())
		return false;

	if( _requestFlags & RequestFlag_Static )
	{
#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
		if (GetCurrentStage() == SecurityIncludeStage_Start) {
			Msg* msg = new Msg( "SecurityCmd_SchemeGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_SchemeGet );
			msg->Append( (uint8)SecurityScheme_Zero );
			msg->Append( GetDriver()->GetTransmitOptions() );
			GetDriver()->SendMsg( msg, _queue );
			SetCurrentStage(SecurityIncludeStage_SchemeGet);
			return true;
		}
#else
		if (GetDriver()->IsSecurityEnable()) {
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SecurityIndex_NetworkKey))) {
				string encoded;
				memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
				StringSource(m_privatekey, sizeof(m_privatekey), true, new HexEncoder(new StringSink(encoded)));
				m_value->OnValueRefreshed(encoded);
				m_value->Release();
			}
			if (!GetDriver()->IsInIncluding()) {
				Log::Write(LogLevel_Warning,  GetNodeId(), "Set security mode On.");
				if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(_instance, SecurityIndex_SecurityMode))) {
					m_value2->OnValueRefreshed(true);
					m_value2->Release();
				}
			}
		} else if (GetDriver()->IsInIncluded()) {
			return false;
		} else {
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SecurityIndex_NetworkKey))) {
				string encoded;
				memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
				StringSource(m_privatekey, sizeof(m_privatekey), true, new HexEncoder(new StringSink(encoded)));
				m_value->OnValueRefreshed(encoded);
				m_value->Release();
			}
		}
		if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(_instance, SecurityIndex_SecurityMode))) {
			if (m_value2->GetValue() == true) {
				if (m_supportedReport)
					return false;
				Msg* msg = new Msg( "SecurityCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->Append( GetNodeId() );
				msg->Append( 2 );
				msg->Append( GetCommandClassId() );
				msg->Append( SecurityCmd_SupportedGet );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendMsg(msg);
			} else if (GetDriver()->GetNodeId() == GetDriver()->GetSUCNodeId()) {
				if (GetDriver()->IsInIncluding()) {
					if (InIncluding()) {
						m_value2->Release();
						if (m_includingTimer.TimeRemaining() < 0)
							return false;
						else
							return true;
					}
					StartIncluding();
				}
				Msg* msg = new Msg( "SecurityCmd_SchemeGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( SecurityCmd_SchemeGet );
				msg->Append( (uint8)SecurityScheme_Zero );
				msg->Append( GetDriver()->GetTransmitOptions() );
				GetDriver()->SendMsg( msg, _queue );
				RequestNonce();
			} else {
				Log::Write(LogLevel_Warning,  GetNodeId(), "Security mode is off.");
			}
			m_value2->Release();
			return true;
		}
#endif
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Security::RequestValue>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool Security::RequestValue
( 
	uint32 const _requestFlags, 
	uint8 const _index, 
	uint8 const _instance, 
	Driver::MsgQueue const _queue 
)
{
	return true;
}

//-----------------------------------------------------------------------------
// <Security::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Security::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
	if (GetDriver()->IsPrimaryController() && GetDriver()->IsStaticUpdateController()) {
		return HandleMsg_Central(_data, _length, _instance);
	} else {
		return HandleMsg_Secondary(_data, _length, _instance);
	}
#else
	return HandleMsg_Central(_data, _length, _instance);
#endif
}

//-----------------------------------------------------------------------------
// <Security::HandleMsg_Central>
// 
//-----------------------------------------------------------------------------
bool Security::HandleMsg_Central(uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	switch( (SecurityCmd)_data[0] ) {
		case SecurityCmd_SchemeGet:
		case SecurityCmd_SchemeReport:
		{
			return HandleMsg_SchemeCmd(_data, _length, false);
		}
		case SecurityCmd_NonceGet:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_NonceGet.");
			SendNonceReport();
			return true;
		}
		case SecurityCmd_NonceReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_NonceReport.");
			m_queueMutex->Lock();
			if (m_waitingForNonce != true)
				Log::Write(LogLevel_Warning,  GetNodeId(), "We are not waiting nonce report.");
			m_waitingForNonce = false;
			m_queueMutex->Unlock();
			if (_length >= 10) {
				string str = "";
				for( uint32 i=1; i<=8; ++i ) {
					if(i) {
						str += ", ";
					}
					char byteStr[8];
					snprintf( byteStr, sizeof(byteStr), "0x%.2x", _data[i] );
					str += byteStr;
					m_receiverNonce[i-1] = _data[i];
				}
				//Log::Write( LogLevel_Detail, GetNodeId(), "  SecurityCmd_NonceReport data: %s", str.c_str() );
				if (CheckIsZero(m_privatekey, sizeof(m_privatekey)) == true) {
					if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SecurityIndex_NetworkKey))) {
						string key_str = m_value->GetValue();
						if (key_str.size() == 32) {
							SecByteBlock key_data = HexDecodeString(key_str.c_str());
							memcpy(m_privatekey, key_data.data(), key_data.size());
							InitAllKey(_instance);
						}
						m_value->Release();
					}
				}
				if (ValueBool* m_value = static_cast<ValueBool*>(GetValue(_instance, SecurityIndex_SecurityMode))) {
					if (m_value->GetValue()) {
						return EncryptMessage(&_data[1]);
					} else {
						SendMessageEncap();
						return true;
					}
				}
			} else {
				Log::Write(LogLevel_Warning,  GetNodeId(), "Unknown return value.");
			}
			return false;
		}
		case SecurityCmd_MessageEncap:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_MessageEncap.");
			//showArray((uint8*)_data, "SecurityCmd_MessageEncap", _length);
			if (m_waitingForMsgEncap) {
				m_waitingForMsgEncap = false;
				return DecryptMessage(_data, _length);
			}
			return false;
		}
		case SecurityCmd_MessageEncapNonceGet:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_MessageEncapNonceGet.");
			if (m_waitingForMsgEncap)
				m_waitingForMsgEncap = false;
			if( DecryptMessage( _data, _length ) ) {
				SendNonceReport();
				return true;
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

#if defined(SECURITY_SUPPORT) && SECURITY_SUPPORT != 0
//-----------------------------------------------------------------------------
// <Security::HandleRedirectMsg>
// Handle redirect message
//-----------------------------------------------------------------------------
bool Security::HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance) {
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	if (GetDriver()->IsPrimaryController() && GetDriver()->IsStaticUpdateController()) {
		//we are the central controller
		//create our own key ourself if not exist (set security device)

		switch ( (SecurityCmd)_data[0] ) {
			case SecurityCmd_SupportedGet:
			case SecurityCmd_SupportedReport:
			case SecurityCmd_SchemeGet:
			case SecurityCmd_SchemeReport:
			case SecurityCmd_NetworkKeySet:
			case SecurityCmd_NetworkKeyVerify:
			case SecurityCmd_SchemeInherit:
			case SecurityCmd_NonceGet:
			case SecurityCmd_NonceReport:
			case SecurityCmd_MessageEncap:
			case SecurityCmd_MessageEncapNonceGet:
			default:
				break;
		}
	} else {
		//we are the secondary controller
		//key will be set by centroller
		//may be disable security when security include timeout
	}
	return false;
}

//-----------------------------------------------------------------------------
// <Security::HandleMsg_Secondary>
// 
//-----------------------------------------------------------------------------
bool Security::HandleMsg_Secondary(uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	return true;
}

//-----------------------------------------------------------------------------
// <Security::SecurityStageHandler>
// Handle security stage
//-----------------------------------------------------------------------------
bool Security::SecurityStageHandler(uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	if (GetDriver()->IsPrimaryController() && GetDriver()->IsStaticUpdateController()) {
		switch ( (SecurityCmd)_data[0] ) {
			case SecurityCmd_SupportedReport:
				{
					break;
				}
			case SecurityCmd_SchemeReport:
				{
					uint8 schemes = _data[1];
					Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_SchemeReport. scheme %d", schemes);
					if (m_securityStage == SecurityCmd_SchemeGet) {
						if (schemes == SecurityScheme_Zero) {
							RequestNonce();
							SetCurrentStage(SecurityIncludeStage_NonceGet1);
						} else {
							Log::Write(LogLevel_Error,  GetNodeId(), "Unsupport scheme id %d, stop security include process.", schemes);
							SetCurrentStage(SecurityIncludeStage_Error);
						}
					} else {}//Unwant process, ignore it
					return true;
				}
			case SecurityCmd_NetworkKeyVerify:
			case SecurityCmd_SchemeInherit:
			case SecurityCmd_NonceGet:
			case SecurityCmd_NonceReport:
				{
					Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_NonceReport.");
					m_queueMutex->Lock();
					m_waitingForNonce = false;
					m_queueMutex->Unlock();
					if (_length >= 10) {
						uint8* nonce = &_data[1];
						if( m_nonceTimer.TimeRemaining() > 10000 ) {
							// The nonce was  not received within 10 seconds
							// of us sending the nonce request.  Send it again
							Log::Write(LogLevel_Warning,  GetNodeId(), "Request Nonce timeout!!!, Retry it!!");
							RequestNonce();
							return true;
						}
						memcpy(m_receiverNonce, nonce, sizeof(m_receiverNonce));
						return true;
					} else {
						//error format ignore it
						return false;
					}
				}
			case SecurityCmd_MessageEncap:
			case SecurityCmd_MessageEncapNonceGet:
			//case SecurityCmd_SupportedGet:
			//case SecurityCmd_SchemeGet:
			//case SecurityCmd_NetworkKeySet:
			default:
				break;
		}

		switch ( m_securityStage ) {
			case SecurityIncludeStage_None:
			case SecurityIncludeStage_Start:
				{
					//initial stage, start security include process
					//we do not handled here
					break;
				}
			/*case SecurityIncludeStage_SchemeGet:
				{
					break;
				}*/
			case SecurityIncludeStage_SchemeReport:
				{
					SetCurrentStage(SecurityIncludeStage_SchemeReport);
					break;
				}
			case SecurityIncludeStage_NonceGet1:
				{
					break;
				}
			case SecurityIncludeStage_NonceReport1:
				{
					break;
				}
			case SecurityIncludeStage_KeySet:
				{
					break;
				}
			case SecurityIncludeStage_NonceGet2:
				{
					break;
				}
			case SecurityIncludeStage_NonceReport2:
				{
					break;
				}
			case SecurityIncludeStage_KeyVerify:
				{
					break;
				}
			case SecurityIncludeStage_SecuritySchemeInherit:
				{
					break;
				}
			case SecurityIncludeStage_SecuritySchemeReport:
				{
					break;
				}
			case SecurityIncludeStage_Finish:
				{
					break;
				}
			default:
				break;
		}
	} else {
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Security::initSupportSetting>
// Initial base setting
//-----------------------------------------------------------------------------
void Security::initSupportSetting() {
	Node* node = GetNodeUnsafe();
	if (!node)
		return;

	if (node->GetNodeId() == GetDriver()->GetNodeId()) {
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(1, SecurityIndex_NetworkKey))) {
			/*string key_str = m_value->GetValue();
			if (key_str.size() == 32) {
				SecByteBlock key_data = HexDecodeString(key_str.c_str());
				memcpy(m_privatekey, key_data.data(), key_data.size());
				InitAllKey(_instance);
			}*/
			for(int i = 0; i < sizeof(m_privatekey); i++) {
				m_privatekey[i] = rand() % 256;
			}
			m_value->Release();
		}
		if (ValueBool* m_value = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
			m_value->Set(true);
			m_value->Release();
		}
	}
}

#endif

//-----------------------------------------------------------------------------
// <Security::SendMsg>
// Queue a message to be securely sent by the Z-Wave PC Interface
//-----------------------------------------------------------------------------
void Security::SendMsg(Msg* _msg)
{
	_msg->Finalize();

	uint8* buffer = _msg->GetBuffer();
	if( _msg->GetLength() < 7 )
		return;

	if( buffer[3] != FUNC_ID_ZW_SEND_DATA )
		return;

	if (CheckIsZero(m_privatekey, sizeof(m_privatekey)) == true) {
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(1, SecurityIndex_NetworkKey))) {
			string key_str = m_value->GetValue();
			if (key_str.size() == 32) {
				SecByteBlock key_data = HexDecodeString(key_str.c_str());
				memcpy(m_privatekey, key_data.data(), key_data.size());
				InitAllKey(1);
			}
			m_value->Release();
		}
	}

	uint8 length = buffer[5];
	if( length > 28 )
	{
		if ( length > 56) {
			Log::Write(LogLevel_Warning, GetNodeId(), "Message too big to be encrypted. Drop it.");
			return;
		}
		// Message must be split into two parts
		SecurityPayload payload1;
		payload1.m_length = 28;
		payload1.m_part = 1;
		memcpy( payload1.m_data, &buffer[6], payload1.m_length );

		if (_msg->GetExpectedCommandClassId() > 0)
			payload1.m_replyRequired = true;
		else 
			payload1.m_replyRequired = false;

		QueuePayload(payload1);

		SecurityPayload payload2;
		payload2.m_length = length-28;
		payload2.m_part = 2;
		memcpy( payload2.m_data, &buffer[34], payload2.m_length );

		if (_msg->GetExpectedCommandClassId() > 0)
			payload2.m_replyRequired = true;
		else 
			payload2.m_replyRequired = false;

		QueuePayload(payload2);
	}
	else
	{
		// The entire message can be encapsulated as one
		SecurityPayload payload;
		payload.m_length = length;
		payload.m_part = 0;				// Zero means not split into separate messages
		memcpy( payload.m_data, &buffer[6], payload.m_length );

		if (_msg->GetExpectedCommandClassId() > 0)
			payload.m_replyRequired = true;
		else 
			payload.m_replyRequired = false;

		QueuePayload(payload);
	}
}

//-----------------------------------------------------------------------------
// <Security::QueuePayload>
// Queue data to be encapsulated by the Security Command Class, on
// receipt of a nonce value from the remote node.
//-----------------------------------------------------------------------------
void Security::QueuePayload(SecurityPayload const& _payload)
{
	m_queueMutex->Lock();
	m_queue.push_back( _payload );
	Log::Write(LogLevel_Info,  GetNodeId(), "Queue New msg....");

	if (m_send_nonceTimer.TimeRemaining() < 0) {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Previous nonce is timeout. Reset nonce.");
		m_waitingForNonce = false;
	} else {
		Log::Write(LogLevel_Info,  GetNodeId(), "Nonce TimeRemaining:%d", m_send_nonceTimer.TimeRemaining());
	}
	if( !m_waitingForNonce)
	{
		// Request a nonce from the node.  Its arrival
		// will trigger the sending of the first payload
		//Log::Write(LogLevel_Info,  GetNodeId(), "Request Nonce for new msg...");
		RequestNonce();
	} else {
		Log::Write(LogLevel_Info,  GetNodeId(), "Waiting for Nonce");
	}

	m_queueMutex->Unlock();
}


//-----------------------------------------------------------------------------
// <Security::EncryptMessage>
// Encrypt and send a Z-Wave message securely.
//-----------------------------------------------------------------------------
bool Security::EncryptMessage(uint8 const* _nonce)
{
	if( m_send_nonceTimer.TimeRemaining() < 0 )
	{
		// The nonce was  not received within 10 seconds
		// of us sending the nonce request.  Send it again
		Log::Write(LogLevel_Warning,  GetNodeId(), "Request Nonce timeout!!!, Retry it!!");
		RequestNonce();
		return false;
	} else {
		memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
	}

	// Fetch the next payload from the queue and encapsulate it
	m_queueMutex->Lock();
	if( m_queue.empty() )
	{
		// Nothing to do
		Log::Write(LogLevel_Info,  GetNodeId(), "Security queue is empty.");
		m_queueMutex->Unlock();
		return false;
	}

	SecurityPayload const _payload = m_queue.front();
	bool parted = ((_payload.m_part != 0) ? true : false);
	bool reply_require = _payload.m_replyRequired;

	SecurityPayload s_payload, as_payload;
	for(int i = 0; i < sizeof(m_senderNonce); i++) {
		m_senderNonce[i] = rand() % 256;
	}
	GenerateSecureMessage(&_payload, m_senderNonce, _nonce, &s_payload);
	showArray(s_payload.m_data, "GenerateSecureMessage", s_payload.m_length);
	GenerateAuthentication(s_payload.m_data, s_payload.m_length, 
		(parted ? SecurityCmd_MessageEncapNonceGet : SecurityCmd_MessageEncap), 
		GetDriver()->GetNodeId(), GetNodeId(), m_initializationVector, &as_payload);
	//GenerateAuthentication(s_payload.m_data, s_payload.m_length, SecurityCmd_MessageEncap, 
	//	GetDriver()->GetNodeId(), GetNodeId(), &as_payload);
	showArray(as_payload.m_data, "GenerateAuthentication", as_payload.m_length);

	Msg* msg;
	if (reply_require)
		msg = new Msg( "SecurityCmd_MessageEncap", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId());
	else
		msg = new Msg( "SecurityCmd_MessageEncap", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
	msg->Append( GetNodeId() );
	msg->Append( 2+sizeof(m_senderNonce)+s_payload.m_length+1+as_payload.m_length );
	msg->Append( GetCommandClassId() );
	msg->Append( (parted ? SecurityCmd_MessageEncapNonceGet : SecurityCmd_MessageEncap) );
	for( int i = 0; i < sizeof(m_senderNonce); i++) {
		msg->Append(m_senderNonce[i]);
	}
	for( int i = 0; i < s_payload.m_length; i++) {
		msg->Append(s_payload.m_data[i]);
	}
	msg->Append(_nonce[0]);
	for (int i=0; i<as_payload.m_length; i++) {
		msg->Append(as_payload.m_data[i]);
	}
	msg->Append( GetDriver()->GetTransmitOptions() );
	GetDriver()->SendMsg(msg, Driver::MsgQueue_Security);

	m_queue.pop_front();
	if( !m_queue.empty() && !parted) {
		RequestNonce();
	}
	m_queueMutex->Unlock();

	return true;
}

//-----------------------------------------------------------------------------
// <Security::DecryptMessage>
// Decrypt a security-encapsulated message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Security::DecryptMessage
(
	uint8 const* _data,
	uint32 const _length
)
{
	if( m_receive_nonceTimer.TimeRemaining() < 0 ) {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Time is over 10 seconds.");
		m_waitingForMsgEncap = false;
		m_decryptMutex->Lock();
		m_decryptPayload.Reset();
		m_decryptMutex->Unlock();
		return false;
	} else {
		if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
			
			if (GetDriver()->IsInIncluded() && !GetDriver()->IsSecurityEnable() && 
				!GetDriver()->IsSecuritySettingTimeout() && !m_value2->GetValue()) {
					memset(m_privatekey, 0, sizeof(m_privatekey));
					InitKeyE_KeyA();
			} else
				memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
			m_value2->Release();
		}
	}
	if (_data[0] != SecurityCmd_MessageEncap && 
		_data[0] != SecurityCmd_MessageEncapNonceGet && _length < 20)
		return false;

	SecurityPayload s_payload;
	uint8 *in_data = (uint8*)(_data + 1);
	//memcpy(m_receiverNonce, m_senderNonce, sizeof(m_senderNonce));
	//memcpy(m_senderNonce, in_data, sizeof(m_senderNonce));
	m_handlePayload.SetReceiver(in_data);
	in_data = (uint8*)(in_data + sizeof(m_senderNonce));
	bool success = DecryptSecureMessage(in_data, (_length-2-sizeof(m_senderNonce)), 
		m_handlePayload.m_receiverNonce, m_handlePayload.m_senderNonce, &s_payload);
	if (!success)
		return false;

#if defined(CRITICAL_CHECK) && CRITICAL_CHECK !=0
	//check Authentication Code
	uint8 *mac_data = (uint8*)(in_data + s_payload.m_length + 1);
	uint8 temp_iv[16];
	memcpy(temp_iv, m_handlePayload.m_senderNonce, sizeof(m_senderNonce));
	memcpy((uint8*)(temp_iv + 8), m_handlePayload.m_receiverNonce, sizeof(m_receiverNonce));
	success = CheckMAC(in_data, s_payload.m_length, _data[0], 
		GetNodeId(), GetDriver()->GetNodeId(), temp_iv, mac_data);
	if (!success)
		return false;
#endif

	/*DecryptSecureMessage(in_data, (_length-2-sizeof(m_senderNonce)), 
		m_senderNonce, m_receiverNonce, &s_payload);*/
	bool secondFrame = ((s_payload.m_data[0] & 0x20) != 0);
	bool sequenced = ((s_payload.m_data[0] & 0x10) != 0);
	uint8 sequenceCount = s_payload.m_data[0] & 0x0f;
	if (sequenced) {
		uint8 data_length = s_payload.m_length-1;
		//Frame is separated into two frame
		m_decryptMutex->Lock();
		if (secondFrame) {
			//Frame is the second part
			Log::Write( LogLevel_Info, GetNodeId(), "Received frame part 2/%d.", sequenceCount);
			memcpy(&m_decryptPayload.m_data[m_decryptPayload.m_length], &s_payload.m_data[1], data_length);
			m_decryptPayload.m_length += data_length;
			HandleDecryptMsg(m_decryptPayload.m_data, m_decryptPayload.m_length);
			m_decryptPayload.Reset();
			m_decryptMutex->Unlock();
		} else {
			//Frame is the first part.
			Log::Write( LogLevel_Info, GetNodeId(), "Received frame part 1/%d.", sequenceCount);
			memset(m_decryptPayload.m_data, 0, sizeof(m_decryptPayload.m_data));
			m_decryptPayload.m_length = 0;
			memcpy(m_decryptPayload.m_data, &s_payload.m_data[1], data_length);
			m_decryptPayload.m_length = data_length;
			m_decryptMutex->Unlock();
		}
	} else {
		m_decryptMutex->Lock();
		m_decryptPayload.Reset();
		m_decryptMutex->Unlock();
		HandleDecryptMsg(&s_payload.m_data[1], s_payload.m_length-1);
	}

	return true;
}

void Security::HandleDecryptMsg(uint8 const* _data, uint32 const _length) {
	if (!_data || _length == 0)
		return;

	if( Node* node = GetNodeUnsafe() ) {
		uint8 commandClassId = _data[0];

		if( CommandClass* pCommandClass = node->GetCommandClass( commandClassId ) ) {
			if (!pCommandClass->IsSecurityMode()) {
				Log::Write( LogLevel_Warning, GetNodeId(), 
					"%s is not in security mode, Change it.", pCommandClass->GetCommandClassName().c_str() );
			}
			Log::Write( LogLevel_Info, GetNodeId(), 
				"Received a SecurityCmd_MessageEncap from node %d, for Command Class %s", 
				GetNodeId(), pCommandClass->GetCommandClassName().c_str() );
			if (commandClassId != GetCommandClassId()) {
				pCommandClass->ReceivedCntIncr();
				pCommandClass->HandleMsg( &_data[1], _length, 1);
			} else {
				switch((SecurityCmd)_data[1]) {
					case SecurityCmd_SupportedGet:
					case SecurityCmd_SupportedReport:
						HandleMsg_SupportedCmd(&_data[1], _length);
						break;
					case SecurityCmd_SchemeGet:
					case SecurityCmd_SchemeReport:
					case SecurityCmd_SchemeInherit:
						HandleMsg_SchemeCmd(&_data[1], _length, true);
						break;
					case SecurityCmd_NetworkKeySet:
					case SecurityCmd_NetworkKeyVerify:
						HandleMsg_NetworkKeyCmd(&_data[1], _length);
						break;
					default:
						break;
				}
			}
		} else {
			Log::Write( LogLevel_Warning, GetNodeId(), 
				"Receive unknown command class id 0x%.2x", commandClassId);
		}
	}
}

//-----------------------------------------------------------------------------
// <Security::GenerateAuthentication>
// Generate authentication data from a security-encrypted message
//-----------------------------------------------------------------------------
void Security::GenerateAuthentication(uint8 const* _data, uint32 const _datalength, uint8 const header,
									  uint8 const _sendingNode, uint8 const _receivingNode, uint8 const* _IV,
									  SecurityPayload *_secure_payload)
{
	_secure_payload->m_data[0] = header;
	_secure_payload->m_data[1] = _sendingNode;
	_secure_payload->m_data[2] = _receivingNode;
	_secure_payload->m_data[3] = _datalength;
	for (uint32 i=0; i<_datalength; i++) {
		_secure_payload->m_data[i+4] = _data[i];
	}
	_secure_payload->m_length = 4 + _datalength;

	string output0;
	if (CheckIsZero(m_KeyA, sizeof(m_KeyA)) == true) {
		OFB_Mode< AES >::Encryption tmp_e;
		uint8 tmp_iv[16];
		uint8 tmp_data[16];
		string tmp_str;
		memset(tmp_iv, 0x55, sizeof(tmp_iv));
		memset(tmp_data, 0, sizeof(tmp_data));
		tmp_e.SetKeyWithIV((const byte*)m_privatekey, sizeof(m_privatekey), (const byte*)tmp_iv, sizeof(tmp_iv));
		StringSource(tmp_data, sizeof(tmp_data), true, 
			new StreamTransformationFilter(tmp_e,
			new StringSink(tmp_str), StreamTransformationFilter::NO_PADDING
			)
		);
		memcpy(m_KeyA, tmp_str.c_str(), tmp_str.size());
		SecByteBlock key_a = HexDecodeString("9adae054f63dfaff5ea18e45edf6ea6f");
		cbc_e.SetKeyWithIV((const byte*)key_a.data(), key_a.size(), _IV, sizeof(m_initializationVector));
		
		StringSource(tmp_data, sizeof(tmp_data), true, 
			new StreamTransformationFilter(cbc_e, new StringSink(tmp_str), StreamTransformationFilter::ZEROS_PADDING) );
		
		StringSource(_secure_payload->m_data, _secure_payload->m_length, true, 
			new StreamTransformationFilter(cbc_e, new StringSink(output0), StreamTransformationFilter::ZEROS_PADDING) );
	} else {
		uint8 tmp_data[16];
		string tmp_str;
		memset(tmp_data, 0, sizeof(tmp_data));

		cbc_e.SetKeyWithIV((const byte*)m_KeyA, sizeof(m_KeyA), (const byte*)m_initializationVector, sizeof(m_initializationVector));

		StringSource(tmp_data, sizeof(tmp_data), true, 
			new StreamTransformationFilter(cbc_e, new StringSink(tmp_str), StreamTransformationFilter::ZEROS_PADDING) );

		StringSource(_secure_payload->m_data, _secure_payload->m_length, true, 
			new StreamTransformationFilter(cbc_e, new StringSink(output0), StreamTransformationFilter::ZEROS_PADDING) );
	}

	//get mac
	if (output0.size() > 16 && output0.size()%16 == 0) {
		int offset = output0.size() - 16;
		for (int i=0; i<8; i++) {
			_secure_payload->m_data[i] = output0.c_str()[i+offset];
		}
	} else if (output0.size() == 16) {
		//the length is 16
		for (int i=0; i<8; i++) {
			_secure_payload->m_data[i] = output0.c_str()[i];
		}
	} else {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Authentication output wrong size.");
	}
	_secure_payload->m_length = 8;
}

//-----------------------------------------------------------------------------
// <Security::CheckMAC>
// CheckMAC
//-----------------------------------------------------------------------------
bool Security::CheckMAC(uint8 const* _data, uint32 const _datalength, uint8 const header, 
						uint8 const _sendingNode, uint8 const _receivingNode, uint8 const* _IV, uint8 const* _mac) {
	SecurityPayload _payload;
	if (!_data || _datalength == 0 || !_IV || !_mac)
		return false;
	GenerateAuthentication(_data, _datalength, header, _sendingNode, _receivingNode, _IV, &_payload);
	for (int i=0; i<_payload.m_length; i++) {
		if (_payload.m_data[i] != _mac[i]) {
			Log::Write(LogLevel_Warning,  GetNodeId(), "Check secure message failed with MAC.");
			return false;
		}
	}
	return true;
}

void Security::GenerateSecureMessage(const SecurityPayload *_payload, 
									 uint8 const* _sender_nonce, uint8 const* _receiver_nonce, 
									 SecurityPayload *_secure_payload)
{
	if (_payload->m_part == 0) {
		assert(_payload->m_length < 29);
		_secure_payload->m_data[0] = 0x00;
	} else {
		uint8 sequence = 0;
		assert(_payload->m_length > 28);
		if (_payload->m_part == 1) {
			sequence = (++m_sequenceCounter) & 0x0f;
			sequence |= 0x10;		// Sequenced, first frame
		} else if (_payload->m_part == 2) {
			sequence = m_sequenceCounter & 0x0f;
			sequence |= 0x30;		// Sequenced, second frame
		} else 
			assert(_payload->m_part <= 2);
		_secure_payload->m_data[0] = sequence;
	}
	for (int i=0; i<_payload->m_length; i++) {
		_secure_payload->m_data[i+1] = _payload->m_data[i];
	}
	_secure_payload->m_length = 1 + _payload->m_length;

	uint8 iv[16];
	string cipher;
	for(int i=0; i<8; i++) {
		iv[i] = _sender_nonce[i];
		iv[i+8] = _receiver_nonce[i];
	}
	memcpy(m_initializationVector, iv, sizeof(iv));

	if (CheckIsZero(m_KeyE, sizeof(m_KeyE)) == true) {
		OFB_Mode< AES >::Encryption tmp_e;
		uint8 tmp_iv[16];
		uint8 tmp_data[16];
		string tmp_str;
		memset(tmp_iv, 0xaa, sizeof(tmp_iv));
		memset(tmp_data, 0, sizeof(tmp_data));
		tmp_e.SetKeyWithIV((const byte*)m_privatekey, sizeof(m_privatekey), (const byte*)tmp_iv, sizeof(tmp_iv));
		StringSource(tmp_data, sizeof(tmp_data), true, 
			new StreamTransformationFilter(tmp_e,
			new StringSink(tmp_str), StreamTransformationFilter::NO_PADDING
			)
		);
		memcpy(m_KeyE, tmp_str.c_str(), tmp_str.size());
		SecByteBlock key_e = HexDecodeString("8522717d3ad1fbfeafa1ceaafdf56565");
		e.SetKeyWithIV((const byte*)key_e.data(), key_e.size(), (const byte*)iv, sizeof(iv));
		StringSource(_secure_payload->m_data, _secure_payload->m_length, true, 
			new StreamTransformationFilter(e,
			new StringSink(cipher), StreamTransformationFilter::NO_PADDING
			)
		);
	} else {
		e.SetKeyWithIV((const byte*)m_KeyE, sizeof(m_KeyE), (const byte*)iv, sizeof(iv));
		StringSource(_secure_payload->m_data, _secure_payload->m_length, true, 
			new StreamTransformationFilter(e,
			new StringSink(cipher), StreamTransformationFilter::NO_PADDING
			)
		);
	}
	_secure_payload->m_length = cipher.size();
	memcpy(_secure_payload->m_data, cipher.c_str(), cipher.size());
}

bool Security::DecryptSecureMessage(uint8 const* _data, uint32 const _datalength, 
									uint8 const* _sender_nonce, uint8 const* _receiver_nonce, 
									SecurityPayload *_secure_payload)
{
	string decrypted_str;
	uint8 iv[16];
	uint8 recvNonceID;
	uint8 MAC[8];
	for(int i=0; i<8; i++) {
		iv[i] = _sender_nonce[i];
		iv[i+8] = _receiver_nonce[i];
	}
	_secure_payload->m_length = 0;
	for(uint32 i=0; i<(_datalength-9); i++) {
		_secure_payload->m_data[i] = _data[i];
		_secure_payload->m_length++;
	}

	recvNonceID = _data[_secure_payload->m_length];
	if (recvNonceID != _receiver_nonce[0]) {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Secure message's receive id not match!! (Recv:%d, Expected:%d)", 
			recvNonceID, _receiver_nonce[0]);
#if defined(CRITICAL_CHECK) && CRITICAL_CHECK != 0
		return false;
#endif
	}
	memcpy(MAC, (uint8*)(_data + 9), sizeof(MAC));
	memcpy(m_initializationVector, iv, sizeof(iv));

	//showArray((uint8*)m_initializationVector, "m_initializationVector",sizeof(m_initializationVector));
	//showArray((uint8*)m_KeyE, "m_KeyE",sizeof(m_KeyE));
	//showArray((uint8*)_secure_payload->m_data, "To Be Decrypt",_secure_payload->m_length);
	d.SetKeyWithIV((const byte*)m_KeyE, sizeof(m_KeyE), 
		(const byte*)m_initializationVector, sizeof(m_initializationVector));
	StringSource(_secure_payload->m_data, _secure_payload->m_length, true, 
		new StreamTransformationFilter(d,
			new StringSink(decrypted_str), StreamTransformationFilter::NO_PADDING
		)
	);
	//showArray((uint8*)decrypted_str.c_str(), "Test Decrypt", decrypted_str.size());
	_secure_payload->m_length = decrypted_str.size();
	memcpy(_secure_payload->m_data, decrypted_str.c_str(), _secure_payload->m_length);
	return true;
}

//-----------------------------------------------------------------------------
// <Security::RequestNonce>
// Request a nonce from the node
//-----------------------------------------------------------------------------
void Security::RequestNonce()
{
	m_waitingForNonce = true;

	Msg* msg = new Msg( "SecurityCmd_NonceGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( SecurityCmd_NonceGet );
	msg->Append( GetDriver()->GetTransmitOptions() );
	GetDriver()->SendMsg( msg, Driver::MsgQueue_Security);

	// Reset the nonce timer.  The nonce report
	// must be received within 10 seconds.
	//m_nonceTimer.Reset();
	m_send_nonceTimer.SetTime(SECURITY_TIMEOUT);
}

//-----------------------------------------------------------------------------
// <Security::SendNonceReport>
// Send a nonce to the node
//-----------------------------------------------------------------------------
void Security::SendNonceReport()
{
	Msg* msg;
	//if (GetDriver()->IsInIncluding())//we want to wait response during including security including process.
		msg = new Msg( "SecurityCmd_NonceReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	//else
	//	msg = new Msg( "SecurityCmd_NonceReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);

	msg->Append( GetNodeId() );
	msg->Append( 10 );
	msg->Append( GetCommandClassId() );
	msg->Append( SecurityCmd_NonceReport );
	for( int i = 0; i < sizeof(m_senderNonce); i++) {
		m_senderNonce[i] = rand() % 256;
		msg->Append( m_senderNonce[i] );
	}
	msg->Append( GetDriver()->GetTransmitOptions() );
	//GetDriver()->SendMsg(msg, Driver::MsgQueue_Security);
	GetDriver()->SendMsg(msg, Driver::MsgQueue_SecurityReport);

	// Reset the nonce timer.  The encapsulated message
	// must be received within 10 seconds.
	//m_nonceTimer.Reset();
	m_receive_nonceTimer.SetTime(SECURITY_TIMEOUT);
	m_handlePayload.SetSender(m_senderNonce);
	m_waitingForMsgEncap = true;
}

void Security::SendMessageEncap()
{
	if (CheckIsZero(m_receiverNonce, sizeof(m_receiverNonce)) == true) {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Our nounce is zero.");
	} 
	if (!GetDriver()->IsInIncluding())
		return;
	if (m_includingTimer.TimeRemaining() < 0) {
		Log::Write(LogLevel_Warning,  GetNodeId(), "Security including timeout.");
		return;
	}

	//create init nonce
	for(int i = 0; i < sizeof(m_senderNonce); i++) {
		m_senderNonce[i] = rand() % 256;
	}
	
	//create secure message
	SecurityPayload _payload, s_payload, as_payload;
	//if (CheckIsZero(m_privatekey, sizeof(m_privatekey)) == true) {
		//create first privatekey
#if defined(USE_DEBUG_KEY) && USE_DEBUG_KEY !=0
		SecByteBlock temp_privKey = HexDecodeString("9061edc1e3a497a52fd78fabbdc021ab");
		memcpy(m_privatekey, temp_privKey.data(), temp_privKey.size());
#else
		memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
#endif
		_payload.m_data[0] = GetCommandClassId();
		_payload.m_data[1] = SecurityCmd_NetworkKeySet;
		for (int i=0; i<sizeof(m_privatekey); i++) {
			_payload.m_data[i+2] = m_privatekey[i];
		}
		_payload.m_length = 2 + sizeof(m_privatekey);
		_payload.m_part = 0;
		//showArray(_payload.m_data, "SecurityCmd_NetworkKeySet", _payload.m_length);
	/*} else {
		_payload.m_data[0] = GetCommandClassId();
		_payload.m_data[1] = SecurityCmd_SupportedGet;
		_payload.m_length = 2;
		_payload.m_part = 0;
		//showArray(_payload.m_data, "SecurityCmd_SupportedGet", _payload.m_length);
	}*/
	GenerateSecureMessage(&_payload, m_senderNonce, m_receiverNonce, &s_payload);
	//showArray(s_payload.m_data, "GenerateSecureMessage", s_payload.m_length);

	//create auth msg
	GenerateAuthentication(s_payload.m_data, s_payload.m_length, SecurityCmd_MessageEncap, 
		GetDriver()->GetNodeId(), GetNodeId(), m_initializationVector, &as_payload);
	//showArray(as_payload.m_data, "GenerateAuthentication", as_payload.m_length);

	//create msg
	Msg* msg = new Msg( "SecurityCmd_MessageEncap", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2+sizeof(m_senderNonce)+s_payload.m_length+1+as_payload.m_length );
	msg->Append( GetCommandClassId() );
	msg->Append( SecurityCmd_MessageEncap );
	for( int i = 0; i < sizeof(m_senderNonce); i++) {
		msg->Append(m_senderNonce[i]);
	}
	for( int i = 0; i < s_payload.m_length; i++) {
		msg->Append(s_payload.m_data[i]);
	}
	msg->Append(m_receiverNonce[0]);
	for (int i=0; i<as_payload.m_length; i++) {
		msg->Append(as_payload.m_data[i]);
	}
	msg->Append( GetDriver()->GetTransmitOptions() );
	GetDriver()->SendMsg(msg, Driver::MsgQueue_Security);
}

void Security::InitAllKey(uint8 const _instance)
{
	if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SecurityIndex_NetworkKey))) {
		if (m_value->GetValue().size() > 10) {
			Log::Write( LogLevel_Info, GetNodeId(), "Network key exist.");
			InitKeyE_KeyA();
		}
		m_value->Release();
	}
}

void Security::InitKeyE_KeyA() {
	//init KeyE
	OFB_Mode< AES >::Encryption tmp_ee;
	uint8 tmp_iv[16];
	uint8 tmp_data[16];
	string tmp_str;
	memset(tmp_iv, 0xaa, sizeof(tmp_iv));
	memset(tmp_data, 0, sizeof(tmp_data));
	tmp_ee.SetKeyWithIV((const byte*)m_privatekey, sizeof(m_privatekey), (const byte*)tmp_iv, sizeof(tmp_iv));
	StringSource(tmp_data, sizeof(tmp_data), true, 
		new StreamTransformationFilter(tmp_ee,
		new StringSink(tmp_str), StreamTransformationFilter::NO_PADDING
		)
		);
	memcpy(m_KeyE, tmp_str.c_str(), tmp_str.size());

	//init KeyA
	OFB_Mode< AES >::Encryption tmp_ea;
	tmp_str.clear();
	memset(tmp_iv, 0x55, sizeof(tmp_iv));
	tmp_ea.SetKeyWithIV((const byte*)m_privatekey, sizeof(m_privatekey), (const byte*)tmp_iv, sizeof(tmp_iv));
	StringSource(tmp_data, sizeof(tmp_data), true, 
		new StreamTransformationFilter(tmp_ea,
		new StringSink(tmp_str), StreamTransformationFilter::NO_PADDING
		)
		);
	memcpy(m_KeyA, tmp_str.c_str(), tmp_str.size());
}

bool Security::CheckIsZero(uint8 *buffer, uint8 length)
{
	if (buffer == NULL)
		return true;

	for(int i = 0; i < length; i++) {
		if (buffer[i] != 0)
			return false;
	}

	return true;
}

SecByteBlock HexDecodeString(const char *hex)
{
	StringSource ss(hex, true, new HexDecoder);
	SecByteBlock result((size_t)ss.MaxRetrievable());
	ss.Get(result, result.size());
	return result;
}

void showArray(uint8 *buffer, const char* name, uint8 length) {
	if (buffer == NULL)
		return;
	string str = "";
	for( uint32 i=0; i<length; i++) {
		if(i) {
			str += ", ";
		}
		char byteStr[8];
		snprintf( byteStr, sizeof(byteStr), "0x%.2x", buffer[i] );
		str += byteStr;
	}
	Log::Write(LogLevel_Info,  15, "PRINT:: %s(%d):%s", name, length, str.c_str());
}

//-----------------------------------------------------------------------------
// <Security::SetValue>
// Set a Security value
//-----------------------------------------------------------------------------
bool Security::SetValue(Value const& _value) {
	if( ValueID::ValueType_Button == _value.GetID().GetType() ) {
		ValueButton const* button = static_cast<ValueButton const*>(&_value);
		if( button && button->IsPressed() ) {
			Log::Write(LogLevel_Info,  GetNodeId(), "Current Queue size:%d", m_queue.size());
			Log::Write(LogLevel_Info,  GetNodeId(), "Nonce TimeRemaining(Send):%d", m_send_nonceTimer.TimeRemaining());
			Log::Write(LogLevel_Info,  GetNodeId(), "Nonce TimeRemaining(Received):%d", m_receive_nonceTimer.TimeRemaining());
			if(m_waitingForNonce) {
				Log::Write(LogLevel_Info,  GetNodeId(), "Waiting for Nonce");
			}
			if (m_waitingForMsgEncap) {
				Log::Write(LogLevel_Info,  GetNodeId(), "Waiting for Msg Encap");
			}
		}
	}
	return false;
}

void Security::CreateVars( uint8 const _instance )
{
	if (_instance != 1)
		return;

	if( Node* node = GetNodeUnsafe() ) {
		node->CreateValueString(ValueID::ValueGenre_System, GetCommandClassId(), _instance, SecurityIndex_NetworkKey, 
			"Network key", "", true, false, "", 0);
		node->CreateValueBool(ValueID::ValueGenre_System, GetCommandClassId(), _instance, SecurityIndex_SecurityMode, 
			"Security", "", true, false, false, 0);
		node->CreateValueButton(ValueID::ValueGenre_System, GetCommandClassId(), _instance, SecurityIndex_DebugInfo, 
			"Security Debug", 0);
	}
}

void Security::UpdateSecurityNodeInfo(uint8 const* _data, uint32 const _length)
{
	if( Node* node = GetNodeUnsafe() ) {
			// Add the command classes specified by the device
			Log::Write( LogLevel_Info, GetNodeId(), "  Optional command classes for node(Security) %d:", GetNodeId() );
			bool newCommandClasses = false;
			uint32 i;

			bool afterMark = false;
			for( i=0; i<_length; ++i )
			{
				if( _data[i] == 0xef ) {
					// COMMAND_CLASS_MARK.
					// Marks the end of the list of supported command classes.  The remaining classes
					// are those that can be controlled by the device.  These classes are created
					// without values.  Messages received cause notification events instead.
					afterMark = true;

					if( !newCommandClasses ) {
						Log::Write( LogLevel_Info, GetNodeId(), "    None" );
					}
					Log::Write( LogLevel_Info, GetNodeId(), "  Optional command classes controlled by node %d:", GetNodeId() );
					newCommandClasses = false;
					continue;
				} else if (_data[i] >= 0xf1) {
					uint16 cmdclass = ((_data[i]<<8) | _data[i+1]);
					Log::Write( LogLevel_Warning, GetNodeId(), "Extended command class 0x%x. Temparary not support.", cmdclass );
					i++;
					continue;
				}

				if( CommandClasses::IsSupported( _data[i] ) ) {
					if (CommandClass* exitedCommandClass = node->GetCommandClass(_data[i])) {
						Log::Write( LogLevel_Info, GetNodeId(), "  Command class %s switch to security mode.", 
							exitedCommandClass->GetCommandClassName().c_str());
						exitedCommandClass->SetSecurityMode(true);
						if( afterMark ) {
							exitedCommandClass->SetAfterMark();
						}
					} 
					else if( CommandClass* pCommandClass = node->AddCommandClass( _data[i] ) ) {
						// If this class came after the COMMAND_CLASS_MARK, then we do not create values.
						if( afterMark ) {
							pCommandClass->SetAfterMark();
						}
						pCommandClass->SetSecurityMode(true);

						// Start with an instance count of one.  If the device supports COMMMAND_CLASS_MULTI_INSTANCE
						// then some command class instance counts will increase once the responses to the RequestState
						// call at the end of this method have been processed.
						pCommandClass->SetInstance( 1 );
						newCommandClasses = true;
						Log::Write( LogLevel_Info, GetNodeId(), "    %s", pCommandClass->GetCommandClassName().c_str() );
					}
				} else {
					Log::Write( LogLevel_Info, GetNodeId(), "  CommandClass 0x%.2x - NOT REQUIRED", _data[i] );
				}
			}
			if( !newCommandClasses )
			{
				// No additional command classes over the mandatory ones.
				Log::Write( LogLevel_Info, GetNodeId(), "    None" );
			}
			node->SetQueryStage(Node::QueryStage_ManufacturerSpecific2, false);
	}
}

bool Security::HandleMsg_SupportedCmd(uint8 const* _data, uint32 const _length) {
	switch((SecurityCmd)_data[0]) {
		case SecurityCmd_SupportedGet:
		{
			Log::Write(LogLevel_Info, GetNodeId(), "Received SecurityCmd_SupportedGet.");
			Msg* msg = new Msg( "SecurityCmd_SupportedReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( GetNodeId() );
			msg->Append( 12 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_SupportedReport );
			msg->Append(ZwavePlusInfo::StaticGetCommandClassId());
			msg->Append(Association::StaticGetCommandClassId());
			msg->Append(AssociationGroupInfo::StaticGetCommandClassId());
			msg->Append(Version::StaticGetCommandClassId());
			msg->Append(ManufacturerSpecific::StaticGetCommandClassId());
			msg->Append(Security::StaticGetCommandClassId());
			msg->Append(DeviceResetLocally::StaticGetCommandClassId());
			msg->Append(ApplicationStatus::StaticGetCommandClassId());
			msg->Append(Powerlevel::StaticGetCommandClassId());
			msg->Append(0xef);
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendMsg(msg);
			break;
		}
		case SecurityCmd_SupportedReport:
		{
			uint8 reportsToFollow = _data[1];
			m_supportedReport = true;
			Log::Write(LogLevel_Info, GetNodeId(), "Received SecurityCmd_SupportedReport. Reports to follow=%d", reportsToFollow);
			string str = "";
			for( uint32 i=1; i<(_length-1); ++i ) {
				if(i) str += ", ";
				char byteStr[8];
				snprintf( byteStr, sizeof(byteStr), "0x%.2x", _data[i] );
				str += byteStr;
			}
			Log::Write( LogLevel_Detail, GetNodeId(), "  SecurityCmd_SupportedReport data: %s", str.c_str() );
			if (ValueBool* m_value = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
				m_value->OnValueRefreshed(true);
				m_value->Release();
			}

			uint8* temp_ptr = (uint8*)(_data+2);
			UpdateSecurityNodeInfo((const uint8*)temp_ptr, _length-3);
			break;
		}
		default:
			return false;
	}
	return true;
}

bool Security::HandleMsg_SchemeCmd(uint8 const* _data, uint32 const _length, bool _secure) {
	switch((SecurityCmd)_data[0]) {
		case SecurityCmd_SchemeGet:
		{
			Log::Write(LogLevel_Info, GetNodeId(), "Received SecurityCmd_SchemeGet.");
			Msg* msg = new Msg( "SecurityCmd_SchemeReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_SchemeReport );
			msg->Append(0);
			msg->Append( GetDriver()->GetTransmitOptions() );
			if (!_secure)
				GetDriver()->SendMsg(msg, Driver::MsgQueue_Security);
			else
				SendMsg(msg);
			break;
		}
		case SecurityCmd_SchemeReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_SchemeReport.");
			uint8 schemes = _data[1];
			if (schemes == 0) {
				Log::Write(LogLevel_Info, GetNodeId(), "Security scheme only agreed with type %d.", schemes);
			} else {
				Log::Write(LogLevel_Info, GetNodeId(), "Security scheme agreed with type %d.", schemes);
			}
			break;
		}
		case SecurityCmd_SchemeInherit:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_SchemeInherit.");
			Msg* msg = new Msg( "SecurityCmd_SchemeReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_SchemeReport );
			msg->Append(0);
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendMsg(msg);
			break;
		}
		default:
			return false;
	}
	return true;
}

bool Security::HandleMsg_NetworkKeyCmd(uint8 const* _data, uint32 const _length) {
	switch((SecurityCmd)_data[0]) {
		case SecurityCmd_NetworkKeySet:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_NetworkKeySet.");
			if (GetDriver()->IsInIncluded() && !GetDriver()->IsSecurityEnable()) {
				if (GetDriver()->IsSecuritySettingTimeout()) {
					GetDriver()->SetIncludingStage(Driver::IncludingStage_Included);
					GetDriver()->SendApplicationStatus_RejectedRequest(GetNodeId());
					if (ValueString* m_value = static_cast<ValueString*>(GetValue(1, SecurityIndex_NetworkKey))) {
						string tmp = "0000";
						m_value->OnValueRefreshed(tmp);
						m_value->Release();
					}
					if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
						m_value2->OnValueRefreshed(false);
						m_value2->Release();
					}
					return true;
				}  else {
					GetDriver()->SetSecurityEnable(true);
					GetDriver()->SetIncludingStage(Driver::IncludingStage_SecurityIncluded);
					if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
						m_value2->OnValueRefreshed(true);
						m_value2->Release();
					}
				}
			}
			string encoded;
			GetDriver()->SetSecurityKey(&_data[1]);
			memcpy(m_privatekey, GetDriver()->GetSecurityKey(), sizeof(m_privatekey));
			StringSource(m_privatekey, sizeof(m_privatekey), true, new HexEncoder(new StringSink(encoded)));
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(1, SecurityIndex_NetworkKey))) {
				m_value->OnValueRefreshed(encoded);
				m_value->Release();
			}
			Msg* msg = new Msg( "SecurityCmd_NetworkKeyVerify", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_NetworkKeyVerify );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendMsg(msg);

			//Write new key back to chip
			//GetDriver()->UpdateNewKey();
			break;
		}
		case SecurityCmd_NetworkKeyVerify:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received SecurityCmd_NetworkKeyVerify.");
			GetDriver()->SetIncludingStage(Driver::IncludingStage_Normal);
			StopIncluding();
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(1, SecurityIndex_NetworkKey))) {
				string encoded;
				StringSource(m_privatekey, sizeof(m_privatekey), true, new HexEncoder(new StringSink(encoded)));
				Log::Write(LogLevel_Info,  GetNodeId(), "Update network key.");
				m_value->OnValueRefreshed(encoded);
				m_value->Release();
			}
			if (ValueBool* m_value2 = static_cast<ValueBool*>(GetValue(1, SecurityIndex_SecurityMode))) {
				Log::Write(LogLevel_Info,  GetNodeId(), "Enable security mode.");
				m_value2->OnValueRefreshed(true);
				m_value2->Release();
			}
			if (GetNodeUnsafe()->IsController()) {
				Msg* msg = new Msg( "SecurityCmd_SchemeInherit", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( SecurityCmd_SchemeInherit );
				msg->Append( 0 );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendMsg(msg);
			}
			/*Msg* msg = new Msg( "SecurityCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( SecurityCmd_SupportedGet );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendMsg(msg);*/
			if (Node* _node = GetNodeUnsafe()) {
				Log::Write(LogLevel_Info,  GetNodeId(), "Advance query stage.");
				_node->SetQueryStage(Node::QueryStage_NodeInfo, true);
			}
			break;
		}
		default:
			return false;
	}
	return true;
}

void Security::StartIncluding() {
	m_includingStart = true;
	m_includingTimer.SetTime(SECURITY_TIMEOUT);
}

void Security::StopIncluding() {
	m_includingStart = false;
}

bool Security::InIncluding() {
	return m_includingStart;
}

void Security::ReadXML( TiXmlElement const* _ccElement )
{
	CommandClass::ReadXML( _ccElement );
	char const* str = _ccElement->Attribute("security_mode");
	if (str) {
		m_securityMode = !strcmp( str, "enable");
	}
}

void Security::WriteXML( TiXmlElement* _ccElement )
{
	CommandClass::WriteXML( _ccElement );
	_ccElement->SetAttribute( "security_mode", (m_securityMode ? "enable":"disable") );
}
