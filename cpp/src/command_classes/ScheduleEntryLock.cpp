//-----------------------------------------------------------------------------
//
//	Lock.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SCHEDULE_ENTRY_LOCK
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ScheduleEntryLock.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"
#include "ValueBool.h"
#include "ValueByte.h"

using namespace OpenZWave;

enum ScheduleEntryLockCmd
{
	ScheduleEntryLockCmd_EnableAllSet			= 0x02,
	ScheduleEntryLockCmd_EnableSet				= 0x01,
	ScheduleEntryLockCmd_WeekDaySet				= 0x03,
	ScheduleEntryLockCmd_WeekDayGet				= 0x04,
	ScheduleEntryLockCmd_WeekDayReport			= 0x05,
	ScheduleEntryLockCmd_YearDaySet				= 0x06,
	ScheduleEntryLockCmd_YearDayGet				= 0x07,
	ScheduleEntryLockCmd_YearDayReport			= 0x08,
	ScheduleEntryLockCmd_SupportedGet			= 0x09,
	ScheduleEntryLockCmd_SupportedReport		= 0x0a,

	ScheduleEntryLockCmd_TimeOffsetGet			= 0x0b,
	ScheduleEntryLockCmd_TimeOffsetReport		= 0x0c,
	ScheduleEntryLockCmd_TimeOffsetSet			= 0x0d,

	ScheduleEntryLockCmd_DailyRepeatingGet		= 0x0e,
	ScheduleEntryLockCmd_DailyRepeatingReport	= 0x0f,
	ScheduleEntryLockCmd_DailyRepeatingSet		= 0x10
};

enum ScheduleEntryLockTimeOffset
{
	ScheduleEntryLockTimeOffset_LevelHourTZOMask = 0x7f,
	ScheduleEntryLockTimeOffset_LevelSignTZOBitMask = 0x80,
	ScheduleEntryLockTimeOffset_Level2MinuteOffsetDSTMask = 0x7f,
	ScheduleEntryLockTimeOffset_Level2SignOffsetDSTBitMask = 0x80
};

static char const* c_dayNames[] = 
{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday"
};

static uint8 const _dayMask[] =
{
	0x01, 
	(0x01<<1), 
	(0x01<<2), 
	(0x01<<3), 
	(0x01<<4), 
	(0x01<<5), 
	(0x01<<6)
};

enum
{
	ScheduleEntryLockIndex_WeekDaySlots = 0,
	ScheduleEntryLockIndex_YearDaySlots,
	ScheduleEntryLockIndex_DailyRepeatingSlots,
	ScheduleEntryLockIndex_UserIDManage,
	ScheduleEntryLockIndex_WeekDayManage,
	ScheduleEntryLockIndex_YearDayManage,
	ScheduleEntryLockIndex_EnabledAll,
	ScheduleEntryLockIndex_TimeOffsetManage,
	ScheduleEntryLockIndex_DailyRepeatingManage, 

	ScheduleEntryLockIndex_WeekDayNumber,
	ScheduleEntryLockIndex_YearDayNumber,
	ScheduleEntryLockIndex_DailyRepeatingNumber,

	ScheduleEntryLockIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <ScheduleEntryLock::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool ScheduleEntryLock::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( _requestFlags & RequestFlag_Static ) {
		requests = RequestValue( _requestFlags, ScheduleEntryLockCmd_SupportedGet, _instance, _queue );
	} else if (_requestFlags & RequestFlag_Session) {
		requests |= RequestValue( _requestFlags, ScheduleEntryLockCmd_WeekDayGet, _instance, _queue );
		requests |= RequestValue( _requestFlags, ScheduleEntryLockCmd_YearDayGet, _instance, _queue );
		if (GetVersion() == 2) {
			requests |= RequestValue( _requestFlags, ScheduleEntryLockCmd_TimeOffsetGet, _instance, _queue );
		} else if (GetVersion() > 2) {
			requests |= RequestValue( _requestFlags, ScheduleEntryLockCmd_TimeOffsetGet, _instance, _queue );
			requests |= RequestValue( _requestFlags, ScheduleEntryLockCmd_DailyRepeatingGet, _instance, _queue );
		}
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <ScheduleEntryLock::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool ScheduleEntryLock::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_getTypeEnum == ScheduleEntryLockCmd_SupportedGet || _getTypeEnum == ScheduleEntryLockCmd_TimeOffsetGet) {
		Msg* msg;
		if (_getTypeEnum == ScheduleEntryLockCmd_SupportedGet) {
			msg = new Msg( "ScheduleEntryLockCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		} else {
			msg = new Msg( "ScheduleEntryLockCmd_TimeOffsetGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		}
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( _getTypeEnum );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (_getTypeEnum == ScheduleEntryLockCmd_WeekDayGet || _getTypeEnum == ScheduleEntryLockCmd_YearDayGet || 
		_getTypeEnum == ScheduleEntryLockCmd_DailyRepeatingGet) {
			Msg* msg;
			switch( (ScheduleEntryLockCmd)_getTypeEnum ) {
				case ScheduleEntryLockCmd_WeekDayGet:
				{
					msg = new Msg( "ScheduleEntryLockCmd_WeekDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					break;
				}
				case ScheduleEntryLockCmd_YearDayGet:
				{
					msg = new Msg( "ScheduleEntryLockCmd_YearDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					break;
				}
				case ScheduleEntryLockCmd_DailyRepeatingGet:
				{
					msg = new Msg( "ScheduleEntryLockCmd_DailyRepeatingGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					break;
				}
				default:
					return false;
			}
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 4 );
			msg->Append( GetCommandClassId() );
			msg->Append( _getTypeEnum );
			msg->Append( 1 );
			msg->Append( 1 );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ScheduleEntryLock::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ScheduleEntryLock::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	switch( (ScheduleEntryLockCmd)_data[0] )
	{
		case ScheduleEntryLockCmd_SupportedReport:
		{
			if (GetVersion() > 2) {
				Log::Write(LogLevel_Info,  GetNodeId(), 
					"Received ScheduleEntryLockCmd_SupportedReport. Number of slots: WeekDay=%d, YearDay=%d, Daily repeating=%d", 
					_data[1], _data[2], _data[3]);
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDaySlots))) {
					m_value->OnValueRefreshed(_data[1]);
					m_value->Release();
				}
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ScheduleEntryLockIndex_YearDaySlots))) {
					m_value->OnValueRefreshed(_data[2]);
					m_value->Release();
				}
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingSlots))) {
					m_value->OnValueRefreshed(_data[3]);
					m_value->Release();
				}
				if (_data[3] == 0) {
					if(ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingManage))) {
						m_value->Release();
						Log::Write(LogLevel_Info,  GetNodeId(), "YearDay schedule not support.");
						node->RemoveValue(GetCommandClassId(), _instance, ScheduleEntryLockIndex_DailyRepeatingManage);
					}
				}
			} else {
				Log::Write(LogLevel_Info,  GetNodeId(), 
					"Received ScheduleEntryLockCmd_SupportedReport. Number of slots: WeekDay=%d, YearDay=%d", 
					_data[1], _data[2]);
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDaySlots))) {
					m_value->OnValueRefreshed(_data[1]);
					m_value->Release();
				}
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ScheduleEntryLockIndex_YearDaySlots))) {
					m_value->OnValueRefreshed(_data[2]);
					m_value->Release();
				}
			}
			if (_data[1] == 0) {
				if(ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDayManage))) {
					m_value->Release();
					Log::Write(LogLevel_Info,  GetNodeId(), "WeekDay schedule not support.");
					node->RemoveValue(GetCommandClassId(), _instance, ScheduleEntryLockIndex_WeekDayManage);
				}
			}
			if (_data[2] == 0) {
				if(ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_YearDayManage))) {
					m_value->Release();
					Log::Write(LogLevel_Info,  GetNodeId(), "YearDay schedule not support.");
					node->RemoveValue(GetCommandClassId(), _instance, ScheduleEntryLockIndex_YearDayManage);
				}
			}
			break;
		}
		case ScheduleEntryLockCmd_WeekDayReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received ScheduleEntryLockCmd_WeekDayReport.");

			if (_data[3] != 0xff) {
				Log::Write(LogLevel_Info,  GetNodeId(), "User ID=%d, Schedule slot ID=%d, %s, From %d:%d To %d:%d", 
					_data[1], _data[2], c_dayNames[_data[3]], _data[4], _data[5], _data[6], _data[7]);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDayManage))) {
					string tmp_str = "";//(ID)-(slot ID)-1-(week day)-(start hour)-(start minute)-(stop hour)-(stop minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-%d-%d-%d:%d-%d:%d", _data[1], _data[2], 1, 
						_data[3], _data[4], _data[5], _data[6], _data[7]);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			} else {
				Log::Write(LogLevel_Info,  GetNodeId(), "User ID=%d, Schedule slot ID=%d, Empty.", _data[1], _data[2]);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDayManage))) {
					string tmp_str = "";//(ID)-(slot ID)-0-(week day)-(start hour)-(start minute)-(stop hour)-(stop minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-0-0-0:0-0:0", _data[1], _data[2]);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			}
			break;
		}
		case ScheduleEntryLockCmd_YearDayReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received ScheduleEntryLockCmd_YearDayReport.");

			if (_data[3] != 0xff) {
				Log::Write(LogLevel_Info,  GetNodeId(), 
					"User ID=%d, Schedule slot ID=%d, From %d/%d/%d %d:%d To %d/%d/%d %d:%d", _data[1], _data[2], 
					(2000+_data[3]), _data[4], _data[5], _data[6], _data[7], 
					(2000+_data[8]), _data[9], _data[10], _data[11], _data[12]);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_YearDayManage))) {
					string tmp_str = "";//(ID)-(slot ID)-1-(start year)/(start month)/(start day)/(start hour):(start minute)-
										//(stop year)/(stop month)/(stop day)/(stop hour):(stop minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-%d-%d/%d/%d/%d:%d-%d/%d/%d/%d:%d", _data[1], _data[2], 1, 
						(2000+_data[3]), _data[4], _data[5], _data[6], _data[7], 
						(2000+_data[8]), _data[9], _data[10], _data[11], _data[12]);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			} else {
				Log::Write(LogLevel_Info,  GetNodeId(), "User ID=%d, Schedule slot ID=%d, Empty.", _data[1], _data[2]);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_YearDayManage))) {
					string tmp_str = "";//(ID)-(slot ID)-0-(start year)/(start month)/(start day)/(start hour):(start minute)-
					//(stop year)/(stop month)/(stop day)/(stop hour):(stop minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-0-0/0/0/0:0-0/0/0/0:0", _data[1], _data[2]);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			}
			break;
		}
		case ScheduleEntryLockCmd_TimeOffsetReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received ScheduleEntryLockCmd_TimeOffsetReport.");
			Log::Write(LogLevel_Info,  GetNodeId(), "TimeZone=%d:%d, Daylight saving=%d", 
				(int8)_data[1], _data[2], (int8)_data[3]);
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_TimeOffsetManage))) {
				string tmp_str = "";//(m or p)(TZO hour):(TZO minute)-(m or p)(DST)
				char byteStr[256];
				snprintf( byteStr, sizeof(byteStr), "%s%d:%d-%s%d", 
					((_data[1] & ScheduleEntryLockTimeOffset_LevelSignTZOBitMask) ? "m":"p"), 
					(_data[1] & ScheduleEntryLockTimeOffset_LevelHourTZOMask), _data[2], 
					((_data[3] & ScheduleEntryLockTimeOffset_Level2SignOffsetDSTBitMask) ? "m":"p"), 
					(_data[3] & ScheduleEntryLockTimeOffset_Level2MinuteOffsetDSTMask));
				tmp_str += byteStr;
				m_value->OnValueRefreshed(tmp_str);
				m_value->Release();
			}
			break;
		}
		case ScheduleEntryLockCmd_DailyRepeatingReport:
		{
			Log::Write(LogLevel_Info,  GetNodeId(), "Received ScheduleEntryLockCmd_DailyRepeatingReport.");
			
			if (_data[4] != 0xff) {
				string day_str = "";
				for (uint32 i=0; i<sizeof(c_dayNames); i++) {
					bool isMask = ((_data[3] & _dayMask[i]) != 0);
					if (isMask) {
						day_str += c_dayNames[i];
						day_str += ",";
					}
				}
				Log::Write(LogLevel_Info,  GetNodeId(), 
					"User ID=%d, Schedule slot ID=%d, In %s Start from %d:%d, Duration=%d:%d", 
					_data[1], _data[2], day_str.c_str(), _data[4], _data[5], _data[6], _data[7]);

				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingManage))) {
					string tmp_str = "";
					//(ID)-(slot ID)-1-(weekday bitmask)-(start hour):(start minute)-(duration hour):(duration minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-%d-%d-%d:%d-%d:%d", _data[1], _data[2], 1, _data[3], 
						_data[4], _data[5], _data[6], _data[7]);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			} else {
				Log::Write(LogLevel_Info,  GetNodeId(), "User ID=%d, Schedule slot ID=%d, Empty.", _data[1], _data[2]);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingManage))) {
					string tmp_str = "";
					//(ID)-(slot ID)-0-(weekday bitmask)-(start hour):(start minute)-(duration hour):(duration minute)
					char byteStr[256];
					snprintf( byteStr, sizeof(byteStr), "%d-%d-0-0-0:0-0:0", _data[1], _data[2], 1);
					tmp_str += byteStr;
					m_value->OnValueRefreshed(tmp_str);
					m_value->Release();
				}
			}
			break;
		}
		default:
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// <ScheduleEntryLock::SetValue>
// Set the DoorLockLogging's state
//-----------------------------------------------------------------------------
bool ScheduleEntryLock::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case ScheduleEntryLockIndex_UserIDManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();//(ID)-(bool)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string userID_str = tmp_str.substr(0, split_p);
				int userID = atoi(userID_str.c_str());
				if (tmp_str.c_str()[split_p+1] != NULL && userID > 0 && userID < 256) {
					bool enabled = false;
					if (tmp_str.c_str()[split_p+1] == '0')
					{
						enabled = false;
					} else if (tmp_str.c_str()[split_p+1] == '1') {
						enabled = true;
					} else {
						return false;
					}
					Msg* msg = new Msg( "ScheduleEntryLockCmd_EnableSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 4 );
					msg->Append( GetCommandClassId() );
					msg->Append( ScheduleEntryLockCmd_EnableSet );
					msg->Append( (uint8)userID );
					msg->Append( enabled );
					msg->Append( GetDriver()->GetTransmitOptions() );
					return SendSecureMsg(msg, Driver::MsgQueue_Send);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		case ScheduleEntryLockIndex_EnabledAll:
		{
			ValueBool const* m_value = static_cast<ValueBool const*>(&_value);
			bool isEnabled = m_value->GetValue();

			Msg* msg = new Msg( "ScheduleEntryLockCmd_EnableAllSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( ScheduleEntryLockCmd_EnableAllSet );
			msg->Append( isEnabled );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case ScheduleEntryLockIndex_WeekDayManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//(ID)-(slot ID)-(action)-(week day)-(start hour):(start minute)-(stop hour):(stop minute)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string userID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, userID_str.size()+1);
				
				string slotID_str, action_str, weekDay_str, 
					startHour_str, startMinute_str, stopHour_str, stopMinute_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					slotID_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, slotID_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					action_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, action_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					weekDay_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, weekDay_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					startHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startHour_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					startMinute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startMinute_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					stopHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, stopHour_str.size()+1);
				} else
					return false;
				stopMinute_str = tmp_str;

				int userID = atoi(userID_str.c_str());
				int slotID = atoi(slotID_str.c_str());
				bool action = (atoi(action_str.c_str()) != 0);
				uint8 weekDay = atoi(weekDay_str.c_str());
				uint8 startHour = atoi(startHour_str.c_str());
				uint8 startMinute = atoi(startMinute_str.c_str());
				uint8 stopHour = atoi(stopHour_str.c_str());
				uint8 stopMinute = atoi(stopMinute_str.c_str());

				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(instance, ScheduleEntryLockIndex_WeekDaySlots))) {
					m_value->GetValue();
					if (slotID > m_value->GetValue()) {
						m_value->Release();
						return false;
					}
					m_value->Release();
				}

				if (userID > 0 && slotID > 0 && weekDay < 7 && 
					startHour < 24 && startMinute < 60 && 
					stopHour < 24 && stopMinute < 60) {
						Msg* msg = new Msg( "ScheduleEntryLockCmd_WeekDaySet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
						msg->SetInstance( this, instance );
						msg->Append( GetNodeId() );
						msg->Append( 10 );
						msg->Append( GetCommandClassId() );
						msg->Append( ScheduleEntryLockCmd_WeekDaySet );
						msg->Append( action );
						msg->Append( (uint8)userID );
						msg->Append( (uint8)slotID );
						msg->Append( weekDay );
						msg->Append( startHour );
						msg->Append( startMinute );
						msg->Append( stopHour );
						msg->Append( stopMinute );
						msg->Append( GetDriver()->GetTransmitOptions() );
						SendSecureMsg(msg, Driver::MsgQueue_Send);

						Msg* msg2 = new Msg( "ScheduleEntryLockCmd_WeekDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
						msg2->SetInstance( this, instance );
						msg2->Append( GetNodeId() );
						msg2->Append( 4 );
						msg2->Append( GetCommandClassId() );
						msg2->Append( ScheduleEntryLockCmd_WeekDayGet );
						msg2->Append( (uint8)userID );
						msg2->Append( (uint8)slotID );
						msg2->Append( GetDriver()->GetTransmitOptions() );
						return SendSecureMsg(msg2, Driver::MsgQueue_Send);
				}
			}
		}
		case ScheduleEntryLockIndex_YearDayManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//(ID)-(slot ID)-(action)-(start year)/(start month)/(start day)/(start hour):(start minute)-
			//(stop year)/(stop month)/(stop day)/(stop hour):(stop minute)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string userID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, userID_str.size()+1);

				string slotID_str, action_str, 
					startYear_str, startMonth_str, startDay_str, startHour_str, startMinute_str, 
					stopYear_str, stopMonth_str, stopDay_str, stopHour_str, stopMinute_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					slotID_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, slotID_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					action_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, action_str.size()+1);
				} else
					return false;

				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					startYear_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startYear_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					startMonth_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startMonth_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					startDay_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startDay_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					startHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startHour_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					startMinute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startMinute_str.size()+1);
				} else
					return false;

				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					stopYear_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, stopYear_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					stopMonth_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, stopMonth_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("/", 0);
				if (split_p > 0) {
					stopDay_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, stopDay_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					stopHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, stopHour_str.size()+1);
				} else
					return false;
				stopMinute_str = tmp_str;

				int userID = atoi(userID_str.c_str());
				int slotID = atoi(slotID_str.c_str());
				bool action = (atoi(action_str.c_str()) != 0);
				int startYear = atoi(startYear_str.c_str());
				if (startYear > 2000 && startYear < 2100) {
					startYear -= 2000;
				} else {
					return false;
				}
				uint8 startMonth = atoi(startMonth_str.c_str());
				uint8 startDay = atoi(startDay_str.c_str());
				uint8 startHour = atoi(startHour_str.c_str());
				uint8 startMinute = atoi(startMinute_str.c_str());
				int stopYear = atoi(stopYear_str.c_str());
				if (stopYear > 2000 && stopYear < 2100) {
					stopYear -= 2000;
				} else {
					return false;
				}
				uint8 stopMonth = atoi(stopMonth_str.c_str());
				uint8 stopDay = atoi(stopDay_str.c_str());
				uint8 stopHour = atoi(stopHour_str.c_str());
				uint8 stopMinute = atoi(stopMinute_str.c_str());

				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(instance, ScheduleEntryLockIndex_YearDaySlots))) {
					m_value->GetValue();
					if (slotID > m_value->GetValue()) {
						m_value->Release();
						return false;
					}
					m_value->Release();
				}

				if (userID > 0 && slotID > 0 && 
					(startMonth <= 12 && startMonth > 0) && (startDay > 0 && startDay <= 31) && startHour < 24 && startMinute < 60 && 
					(stopMonth <= 12 && stopMonth > 0) && (stopDay > 0 && stopDay <= 31) && stopHour < 24 || stopMinute < 60) {
						Msg* msg = new Msg( "ScheduleEntryLockCmd_YearDaySet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
						msg->SetInstance( this, instance );
						msg->Append( GetNodeId() );
						msg->Append( 15 );
						msg->Append( GetCommandClassId() );
						msg->Append( ScheduleEntryLockCmd_YearDaySet );
						msg->Append( action );
						msg->Append( (uint8)userID );
						msg->Append( (uint8)slotID );
						msg->Append( (uint8)startYear );
						msg->Append( startMonth );
						msg->Append( startDay );
						msg->Append( startHour );
						msg->Append( startMinute );
						msg->Append( (uint8)stopYear );
						msg->Append( stopMonth );
						msg->Append( stopDay );
						msg->Append( stopHour );
						msg->Append( stopMinute );
						msg->Append( GetDriver()->GetTransmitOptions() );
						SendSecureMsg(msg, Driver::MsgQueue_Send);

						Msg* msg2 = new Msg( "ScheduleEntryLockCmd_YearDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
						msg2->SetInstance( this, instance );
						msg2->Append( GetNodeId() );
						msg2->Append( 4 );
						msg2->Append( GetCommandClassId() );
						msg2->Append( ScheduleEntryLockCmd_YearDayGet );
						msg2->Append( (uint8)userID );
						msg2->Append( (uint8)slotID );
						msg2->Append( GetDriver()->GetTransmitOptions() );
						return SendSecureMsg(msg2, Driver::MsgQueue_Send);
				}
			}
		}
		case ScheduleEntryLockIndex_TimeOffsetManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//(m or p)(TZO hour):(TZO minute)-(m or p)(DST)
			int split_p = tmp_str.find_first_of(":", 0);
			if (split_p > 0) {
				string TZOHour_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, TZOHour_str.size()+1);

				string TZO_Minute_str, DST_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					TZO_Minute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, TZO_Minute_str.size()+1);
				} else
					return false;
				DST_str = tmp_str;

				int8 TZOHour = 0;
				if (TZOHour_str.c_str()[0] == 'p') {
					TZOHour_str.erase(0,1);
					TZOHour = atoi(TZOHour_str.c_str());
				} else if (TZOHour_str.c_str()[0] == 'm') {
					TZOHour_str.erase(0,1);
					TZOHour = atoi(TZOHour_str.c_str())*(-1);
				} else
					return false;
				uint8 TZO_Minute = atoi(TZO_Minute_str.c_str());
				int8 DST = 0;
				if (DST_str.c_str()[0] == 'p') {
					DST_str.erase(0,1);
					DST = atoi(DST_str.c_str());
				} else if (DST_str.c_str()[0] == 'm') {
					DST_str.erase(0,1);
					DST = atoi(DST_str.c_str())*(-1);
				} else
					return false;

				if ( (TZOHour < 12 && TZOHour > -12) && TZO_Minute < 60 ) {
						Msg* msg = new Msg( "ScheduleEntryLockCmd_TimeOffsetSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
						msg->SetInstance( this, instance );
						msg->Append( GetNodeId() );
						msg->Append( 5 );
						msg->Append( GetCommandClassId() );
						msg->Append( ScheduleEntryLockCmd_TimeOffsetSet );
						msg->Append( TZOHour );
						msg->Append( TZO_Minute );
						msg->Append( DST );
						msg->Append( GetDriver()->GetTransmitOptions() );
						SendSecureMsg(msg, Driver::MsgQueue_Send);

						return RequestValue( 0, ScheduleEntryLockCmd_TimeOffsetGet, instance, Driver::MsgQueue_Send );
				}
			}
		}
		case ScheduleEntryLockIndex_DailyRepeatingManage:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//(ID)-(slot ID)-(action)-(weekday bitmask)-(start hour):(start minute)-(duration hour):(duration minute)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string userID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, userID_str.size()+1);

				string slotID_str, action_str, 
					weekDayBitmask_str, startHour_str, startMinute_str, 
					durationHour_str, durationMinute_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					slotID_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, slotID_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					action_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, action_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					weekDayBitmask_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, weekDayBitmask_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					startHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startHour_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					startMinute_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, startMinute_str.size()+1);
				} else
					return false;
				split_p = tmp_str.find_first_of(":", 0);
				if (split_p > 0) {
					durationHour_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, durationHour_str.size()+1);
				} else
					return false;
				durationMinute_str = tmp_str;

				int userID = atoi(userID_str.c_str());
				int slotID = atoi(slotID_str.c_str());
				bool action = (atoi(action_str.c_str()) != 0);
				uint8 weekDayBitmask = atoi(weekDayBitmask_str.c_str());
				uint8 startHour = atoi(startHour_str.c_str());
				uint8 startMinute = atoi(startMinute_str.c_str());
				uint8 durationHour = atoi(durationHour_str.c_str());
				uint8 durationMinute = atoi(durationMinute_str.c_str());

				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(instance, ScheduleEntryLockIndex_DailyRepeatingSlots))) {
					m_value->GetValue();
					if (slotID > m_value->GetValue()) {
						m_value->Release();
						return false;
					}
					m_value->Release();
				}

				if (userID > 0 && slotID > 0 && startHour < 24 && startMinute < 60 && 
					durationHour < 24 && durationMinute < 60) {
						Msg* msg = new Msg( "ScheduleEntryLockCmd_DailyRepeatingSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
						msg->SetInstance( this, instance );
						msg->Append( GetNodeId() );
						msg->Append( 10 );
						msg->Append( GetCommandClassId() );
						msg->Append( ScheduleEntryLockCmd_DailyRepeatingSet );
						msg->Append( action );
						msg->Append( userID );
						msg->Append( slotID );
						msg->Append( weekDayBitmask );
						msg->Append( startHour );
						msg->Append( startMinute );
						msg->Append( durationHour );
						msg->Append( durationMinute );
						msg->Append( GetDriver()->GetTransmitOptions() );
						SendSecureMsg(msg, Driver::MsgQueue_Send);

						Msg* msg2 = new Msg( "ScheduleEntryLockCmd_DailyRepeatingGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
						msg2->SetInstance( this, instance );
						msg2->Append( GetNodeId() );
						msg2->Append( 4 );
						msg2->Append( GetCommandClassId() );
						msg2->Append( ScheduleEntryLockCmd_DailyRepeatingGet );
						msg2->Append( (uint8)userID );
						msg2->Append( (uint8)slotID );
						msg2->Append( GetDriver()->GetTransmitOptions() );
						return SendSecureMsg(msg2, Driver::MsgQueue_Send);
				}
			}
		}
		case ScheduleEntryLockIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ScheduleEntryLockIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		case ScheduleEntryLockIndex_DailyRepeatingNumber:
		case ScheduleEntryLockIndex_WeekDayNumber:
		case ScheduleEntryLockIndex_YearDayNumber:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string userID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, userID_str.size()+1);
				string slotID_str = tmp_str;
				int userID = atoi(userID_str.c_str());
				int slotID = atoi(slotID_str.c_str());
				if (userID > 0 && slotID > 0) {
					Msg* msg;
					if (_value.GetID().GetIndex() == ScheduleEntryLockIndex_WeekDayNumber) {
						msg = new Msg( "ScheduleEntryLockCmd_WeekDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					} else if (_value.GetID().GetIndex() == ScheduleEntryLockIndex_YearDayNumber) {
						msg = new Msg( "ScheduleEntryLockCmd_YearDayGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					} else {
						msg = new Msg( "ScheduleEntryLockCmd_DailyRepeatingGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
					}
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 4 );
					msg->Append( GetCommandClassId() );
					if (_value.GetID().GetIndex() == ScheduleEntryLockIndex_WeekDayNumber) {
						msg->Append( ScheduleEntryLockCmd_WeekDayGet );
					} else if (_value.GetID().GetIndex() == ScheduleEntryLockIndex_YearDayNumber) {
						msg->Append( ScheduleEntryLockCmd_YearDayGet );
					} else {
						msg->Append( ScheduleEntryLockCmd_DailyRepeatingGet );
					}
					msg->Append( (uint8)userID );
					msg->Append( (uint8)slotID );
					msg->Append( GetDriver()->GetTransmitOptions() );
					return SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			}
			return true;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ScheduleEntryLock::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ScheduleEntryLock::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() ) {
		uint8 _version = GetVersion();
		if (_version >= 3) {
			node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_DailyRepeatingManage, 
				"Daily repeating schedule", "", false, false, "", 0);
			node->CreateValueByte(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_DailyRepeatingSlots, 
				"Max daily repeating slots", "", true, false, 0, 0);

			//for info get
			node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_DailyRepeatingNumber, 
				"Daily repeating schedule number", "", false, true, "", 0);
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingNumber))) {
				string dailyInfo = "format (user ID)-(slot ID)";
				m_value->SetHelp(dailyInfo);
				m_value->Release();
			}
		}
		if (_version >= 2) {
			node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_TimeOffsetManage, 
				"Time offset", "", false, false, "", 0);
		}
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_UserIDManage, 
			"User manage", "", false, false, "", 0);
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_WeekDayManage, 
			"Week schedule", "", false, false, "", 0);
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_YearDayManage, 
			"Year schedule", "", false, false, "", 0);
		node->CreateValueBool(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_EnabledAll, 
			"Enable All", "", false, true, false, 0);
		node->CreateValueByte(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_WeekDaySlots, 
			"Max week day slots", "", true, false, 0, 0);
		node->CreateValueByte(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_YearDaySlots, 
			"Max year day slots", "", true, false, 0, 0);

		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_UserIDManage))) {
			string dailyInfo = "format (user ID)-(bool)";
			m_value->SetHelp(dailyInfo);
			m_value->Release();
		}
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_DailyRepeatingManage))) {
			string dailyInfo = "format (user ID)-(slot ID)-(action)-(weekday bitmask)-(start hour):(start minute)-(duration hour):(duration minute)";
			m_value->SetHelp(dailyInfo);
			m_value->Release();
		}
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDayManage))) {
			string dailyInfo = "format (user ID)-(slot ID)-(action)-(week day)-(start hour):(start minute)-(stop hour):(stop minute)";
			m_value->SetHelp(dailyInfo);
			m_value->Release();
		}
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_YearDayManage))) {
			string dailyInfo = "format (user ID)-(slot ID)-(action)-(start year)/(start month)/(start day)/(start hour):(start minute)-(stop year)/(stop month)/(stop day)/(stop hour):(stop minute)";
			m_value->SetHelp(dailyInfo);
			m_value->Release();
		}

		//for info get
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_WeekDayNumber, 
			"Week day schedule number", "", false, true, "", 0);
		node->CreateValueString(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ScheduleEntryLockIndex_YearDayNumber, 
			"Year day schedule number", "", false, true, "", 0);

		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_WeekDayNumber))) {
			string weekdayInfo = "format (user ID)-(slot ID)";
			m_value->SetHelp(weekdayInfo);
			m_value->Release();
		}
		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, ScheduleEntryLockIndex_YearDayNumber))) {
			string yeardayInfo = "format (user ID)-(slot ID)";
			m_value->SetHelp(yeardayInfo);
			m_value->Release();
		}

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ScheduleEntryLockIndex_IconName, "ScheduleEntryLock Icon", "icon", false, false, "", 0);
	}
}

