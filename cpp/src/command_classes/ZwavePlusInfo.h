//-----------------------------------------------------------------------------
//
//	ZwavePlusInfo.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_ZWAVEPLUS_INFO
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _ZwavePlusInfo_H
#define _ZwavePlusInfo_H

#include "CommandClass.h"

#define INSTALLER_ICON_TYPE_MSB 0x01
#define INSTALLER_ICON_TYPE_LSB 0x00
#define USER_ICON_TYPE_MSB 0x01
#define USER_ICON_TYPE_LSB 0x00

namespace OpenZWave
{
	class ValueString;
	class ValueList;

	/** \brief Implements COMMAND_CLASS_ZWAVEPLUS_INFO (0x5e), a Z-Wave device command class.
	 */
	class ZwavePlusInfo: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new ZwavePlusInfo( _homeId, _nodeId ); }
		virtual ~ZwavePlusInfo(){}

		static uint8 const StaticGetCommandClassId(){ return 0x5e; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_ZWAVEPLUS_INFO"; }

		// From CommandClass
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );
		virtual uint8 GetMaxVersion(){ return 2; }
		virtual void initSupportSetting();

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		ZwavePlusInfo( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ) {}
		ValueList::Item CreateItem(string _label, int32 _value);

		vector<ValueList::Item> roleType_items;
		vector<ValueList::Item> nodeType_items;
	};

} // namespace OpenZWave

#endif

