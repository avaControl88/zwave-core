//-----------------------------------------------------------------------------
//
//	ZwavePlusInfo.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_ZWAVEPLUS_INFO
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ZwavePlusInfo.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"
#include "Options.h"
#include "ValueString.h"

using namespace OpenZWave;

enum ZwavePlusInfoCmd
{
	ZwavePlusInfoCmd_Get				= 0x1,
	ZwavePlusInfoCmd_Report				= 0x2,
};

enum
{
	ZwavePlusInfo_RoleType_CentralStaticController		= 0x0, 
	ZwavePlusInfo_RoleType_SubStaticController			= 0x1,
	ZwavePlusInfo_RoleType_PortableController			= 0x2,
	ZwavePlusInfo_RoleType_PortableReportingController	= 0x3,
	ZwavePlusInfo_RoleType_PortableSlave				= 0x4,
	ZwavePlusInfo_RoleType_AlwaysOnSlave				= 0x5,
	ZwavePlusInfo_RoleType_SleepingReportingSlave		= 0x6,
	ZwavePlusInfo_RoleType_ReachableSleepingSlave		= 0x7,

	ZwavePlusInfo_NodeType_ZwavePlusNode				= 0x0,
	ZwavePlusInfo_NodeType_ZwavePlus_IPRouter			= 0x1,
	ZwavePlusInfo_NodeType_ZwavePlus_IPGateway			= 0x2,
	ZwavePlusInfo_NodeType_ZwavePlus_IPClientIPNode		= 0x3,
	ZwavePlusInfo_NodeType_ZwavePlus_IPClientZwaveNode	= 0x4
};

enum
{
	ZwavePlusInfoIndex_Version = 0, 
	ZwavePlusInfoIndex_RoleType,
	ZwavePlusInfoIndex_NodeType,
	ZwavePlusInfoIndex_InstallerIconType,
	ZwavePlusInfoIndex_UserIconType
};

//-----------------------------------------------------------------------------
// <ZwavePlusInfo::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool ZwavePlusInfo::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if(_requestFlags & RequestFlag_Static)
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ZwavePlusInfo::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool ZwavePlusInfo::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,		// = 0
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( _instance != 1 )
	{
		// This command class doesn't work with multiple instances
		return false;
	}

	Msg* msg = new Msg( "ZwavePlusInfoCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( ZwavePlusInfoCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <ZwavePlusInfo::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ZwavePlusInfo::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (ZwavePlusInfoCmd)_data[0] ) {
		case ZwavePlusInfoCmd_Get:
		{
			Log::Write( LogLevel_Info, GetNodeId(), 
				"Received Z-Wave+ Info request from node %d", GetNodeId());
			Msg* msg = new Msg( "ZwavePlusInfoCmd_Report", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( GetNodeId() );
			msg->Append( 9 );
			msg->Append( GetCommandClassId() );
			msg->Append( ZwavePlusInfoCmd_Report );
			msg->Append(1);
			msg->Append(ZwavePlusInfo_RoleType_CentralStaticController);
			msg->Append(ZwavePlusInfo_NodeType_ZwavePlus_IPGateway);
			msg->Append(INSTALLER_ICON_TYPE_MSB);
			msg->Append(INSTALLER_ICON_TYPE_LSB);
			msg->Append(USER_ICON_TYPE_MSB);
			msg->Append(USER_ICON_TYPE_LSB);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case  ZwavePlusInfoCmd_Report:
		{
			char version[8];
			string roleType;
			string nodeType;
			uint16 _installerIconTypeId = (((uint16)_data[4])<<8) | (uint16)_data[5];
			uint16 _userIconTypeId = (((uint16)_data[6])<<8) | (uint16)_data[7];
			string installerIconTypeLabel = GetDriver()->GetIconTypeLabel(_installerIconTypeId);
			string userIconTypeLabel = GetDriver()->GetIconTypeLabel(_userIconTypeId);
			memset(version, 0, sizeof(version));
			if ( ValueString* versionValue = static_cast<ValueString*>( GetValue( _instance, ZwavePlusInfoIndex_Version ) ) ) {
				snprintf(version, sizeof(version), "%d", _data[1]);
				versionValue->OnValueRefreshed(version);
				versionValue->Release();
			}

			if ( ValueString* roleTypeValue = static_cast<ValueString*>( GetValue( _instance, ZwavePlusInfoIndex_RoleType ) ) ) {
				for (uint32 i = 0; i<roleType_items.size(); i++) {
					if (roleType_items[i].m_value == _data[2]) {
						roleType = roleType_items[i].m_label;
						roleTypeValue->OnValueRefreshed(roleType_items[i].m_label);
						roleTypeValue->Release();
						break;
					}
				}
			}

			if ( ValueString* nodeTypeValue = static_cast<ValueString*>( GetValue( _instance, ZwavePlusInfoIndex_NodeType ) ) )
			{
				for (uint32 i = 0; i<nodeType_items.size(); i++) {
					if (nodeType_items[i].m_value == _data[3]) {
						nodeType = nodeType_items[i].m_label;
						nodeTypeValue->OnValueRefreshed(nodeType_items[i].m_label);
						nodeTypeValue->Release();
						break;
					}
				}
			}

			if ( ValueString* installerIconTypeValue = static_cast<ValueString*>( GetValue( _instance, ZwavePlusInfoIndex_InstallerIconType ) ) )
			{
				installerIconTypeValue->OnValueRefreshed(installerIconTypeLabel);
				installerIconTypeValue->Release();
			}

			if ( ValueString* userIconTypeValue = static_cast<ValueString*>( GetValue( _instance, ZwavePlusInfoIndex_UserIconType ) ) )
			{
				userIconTypeValue->OnValueRefreshed(userIconTypeLabel);
				userIconTypeValue->Release();
			}

			Log::Write( LogLevel_Info, GetNodeId(), 
				"Received Z-Wave+ Info report from node %d: Version=%s, RoleType=%s, NodeType=%s, InstallIconType=%s, UserIconType=%s", 
				GetNodeId(), version, roleType.c_str(), nodeType.c_str(), installerIconTypeLabel.c_str(), userIconTypeLabel.c_str());

			return true;
		}
		default:
			return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
// <ZwavePlusInfo::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ZwavePlusInfo::CreateVars
(
	uint8 const _instance
)
{
	if (_instance != 1)
		return;

	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ZwavePlusInfoIndex_Version, "Z-Wave+ Version", "", true, false, "Unknown", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ZwavePlusInfoIndex_RoleType, "Role Type", "", true, false, "Unknown", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ZwavePlusInfoIndex_NodeType, "Node Type", "", true, false, "Unknown", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ZwavePlusInfoIndex_InstallerIconType, "Installer Icon Type", "", true, false, "Unknown", 0);
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ZwavePlusInfoIndex_UserIconType, "User Icon Type", "", true, false, "Unknown", 0);

		roleType_items.push_back(CreateItem("Central Static Controller", ZwavePlusInfo_RoleType_CentralStaticController));
		roleType_items.push_back(CreateItem("Sub Static Controller", ZwavePlusInfo_RoleType_SubStaticController));
		roleType_items.push_back(CreateItem("Portable Controller", ZwavePlusInfo_RoleType_PortableController));
		roleType_items.push_back(CreateItem("Reporting Portable Controller", ZwavePlusInfo_RoleType_PortableReportingController));
		roleType_items.push_back(CreateItem("Portable Slave", ZwavePlusInfo_RoleType_PortableSlave));
		roleType_items.push_back(CreateItem("Always On Slave", ZwavePlusInfo_RoleType_AlwaysOnSlave));
		roleType_items.push_back(CreateItem("Reporting Sleeping Slave", ZwavePlusInfo_RoleType_SleepingReportingSlave));
		roleType_items.push_back(CreateItem("Listening Sleeping Slave", ZwavePlusInfo_RoleType_ReachableSleepingSlave));

		nodeType_items.push_back(CreateItem("Z-Wave+ node", ZwavePlusInfo_NodeType_ZwavePlusNode));
		nodeType_items.push_back(CreateItem("Z-Wave+ ip router", ZwavePlusInfo_NodeType_ZwavePlus_IPRouter));
		nodeType_items.push_back(CreateItem("Z-Wave+ ip gateway", ZwavePlusInfo_NodeType_ZwavePlus_IPGateway));
		nodeType_items.push_back(CreateItem("Z-Wave+ ip client and ip node", ZwavePlusInfo_NodeType_ZwavePlus_IPClientIPNode));
		nodeType_items.push_back(CreateItem("Z-Wave+ ip client and zwave node", ZwavePlusInfo_NodeType_ZwavePlus_IPClientZwaveNode));
	}
}

ValueList::Item ZwavePlusInfo::CreateItem(string _label, int32 _value)
{
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

void ZwavePlusInfo::initSupportSetting() {
	char version[8];
	string roleType;
	string nodeType;
	uint16 _installerIconTypeId = (INSTALLER_ICON_TYPE_MSB<<8) | INSTALLER_ICON_TYPE_LSB;
	uint16 _userIconTypeId = (USER_ICON_TYPE_MSB<<8) | USER_ICON_TYPE_LSB;
	string installerIconTypeLabel = GetDriver()->GetIconTypeLabel(_installerIconTypeId);
	string userIconTypeLabel = GetDriver()->GetIconTypeLabel(_userIconTypeId);
	memset(version, 0, sizeof(version));
	if ( ValueString* versionValue = static_cast<ValueString*>( GetValue( 1, ZwavePlusInfoIndex_Version ) ) ) {
		snprintf(version, sizeof(version), "%d", 1);
		versionValue->OnValueRefreshed(version);
		versionValue->Release();
	}

	if ( ValueString* roleTypeValue = static_cast<ValueString*>( GetValue( 1, ZwavePlusInfoIndex_RoleType ) ) ) {
		roleTypeValue->OnValueRefreshed(string("Central Static Controller"));
		roleTypeValue->Release();
	}

	if ( ValueString* nodeTypeValue = static_cast<ValueString*>( GetValue( 1, ZwavePlusInfoIndex_NodeType ) ) )
	{
		nodeTypeValue->OnValueRefreshed(string("Z-Wave+ ip gateway"));
		nodeTypeValue->Release();
	}

	if ( ValueString* installerIconTypeValue = static_cast<ValueString*>( GetValue( 1, ZwavePlusInfoIndex_InstallerIconType ) ) )
	{
		installerIconTypeValue->OnValueRefreshed(installerIconTypeLabel);
		installerIconTypeValue->Release();
	}

	if ( ValueString* userIconTypeValue = static_cast<ValueString*>( GetValue( 1, ZwavePlusInfoIndex_UserIconType ) ) )
	{
		userIconTypeValue->OnValueRefreshed(userIconTypeLabel);
		userIconTypeValue->Release();
	}

	Log::Write( LogLevel_Info, GetNodeId(), 
		"Received Z-Wave+ Info report from node %d: Version=%s, RoleType=%s, NodeType=%s, InstallIconType=%s, UserIconType=%s", 
		GetNodeId(), version, "Central Static Controller", "Z-Wave+ ip gateway", installerIconTypeLabel.c_str(), userIconTypeLabel.c_str());
}
