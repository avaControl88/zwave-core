//-----------------------------------------------------------------------------
//
//	Version.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_VERSION
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _Version_H
#define _Version_H

#include "CommandClass.h"

#define ZWAVE_LIBRARY_TYPE 6
#define ZWAVE_PROTOCAL_VERSION 3
#define ZWAVE_PROTOCAL_SUB_VERSION 79
#define APPLICATION_VERSION 0x01
#define APPLICATION_SUB_VERSION 0x01
#define HARDWARE_VERSION 0x01

namespace OpenZWave
{
	class ValueString;

	/** \brief Implements COMMAND_CLASS_VERSION (0x86), a Z-Wave device command class.
	 */
	class Version: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new Version( _homeId, _nodeId ); }
		virtual ~Version(){}

		static uint8 const StaticGetCommandClassId(){ return 0x86; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_VERSION"; }

		bool RequestCommandClassVersion( CommandClass const* _commandClass );

		// From CommandClass
		virtual void ReadXML( TiXmlElement const* _ccElement );
		virtual void WriteXML( TiXmlElement* _ccElement );
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );
		virtual uint8 GetMaxVersion(){ return 2; }
		virtual void initSupportSetting();
		virtual bool HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance = 1);

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		Version( uint32 const _homeId, uint8 const _nodeId );

		bool m_classGetSupported;
	};

} // namespace OpenZWave

#endif

