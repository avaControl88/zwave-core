//-----------------------------------------------------------------------------
//
//	ColorControl.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_COLOR_CONTROL
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _ColorControl_H
#define _ColorControl_H

#include <vector>
#include <string>
#include "CommandClass.h"

namespace OpenZWave
{
	class ValueString;
	class ValueByte;
	class ValueRaw;

	/** \brief Implements COMMAND_CLASS_COLOR_CONTROL (0x33), a Z-Wave device command class.
	 */
	class ColorControl: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new ColorControl( _homeId, _nodeId ); }
		virtual ~ColorControl(){}

		static uint8 const StaticGetCommandClassId(){ return 0x33; }		
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_COLOR_CONTROL"; }

		// From CommandClass
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );
		virtual uint8 GetMaxVersion(){ return 2; }
		virtual bool SetValue( Value const& _value );

		virtual void ReadXML( TiXmlElement const* _ccElement );
		virtual void WriteXML( TiXmlElement* _ccElement );

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		ColorControl( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ){
			m_autoReport = false;
			m_delayPoll = false;
			m_delay_time = 5;
			wait_poll = false;
			wait_autoReport = false;
		}

		bool CreateValue_RGB(uint8 const _instance);//return RGB support
		void CreateValue_RGBW(uint8 const _instance);
		void CreateValue_6CH(uint8 const _instance);

		void UpdateValue_RGB(uint8 const _instance, uint8 const _componentID, uint8 const _level);
		void UpdateValue_RGBW(uint8 const _instance, uint8 const _componentID, uint8 const _level);
		void UpdateValue_6CH(uint8 const _instance, uint8 const _componentID, uint8 const _level);

		bool m_autoReport;
		bool m_delayPoll;
		uint8 m_delay_time;

		bool wait_poll;
		bool wait_autoReport;
	};

} // namespace OpenZWave

#endif




