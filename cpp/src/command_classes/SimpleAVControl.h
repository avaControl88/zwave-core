//-----------------------------------------------------------------------------
//
//	SimpleAVControl.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_SIMPLE_AV_CONTROL
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _SimpleAVControl_H
#define _SimpleAVControl_H

#include "CommandClass.h"

namespace OpenZWave
{
	class ValueList;
	class ValueButton;
	class ValueString;

	/** \brief Implements COMMAND_CLASS_SIMPLE_AV_CONTROL (0x94), a Z-Wave device command class.
	 */
	class SimpleAVControl: public CommandClass
	{
	public:
		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new SimpleAVControl( _homeId, _nodeId ); }
		virtual ~SimpleAVControl(){}

		static uint8 const StaticGetCommandClassId(){ return 0x94; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_SIMPLE_AV_CONTROL"; }

		// From CommandClass
		virtual bool RequestState( uint32 const _requestFlags, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual bool RequestValue( uint32 const _requestFlags, uint8 const _index, uint8 const _instance, Driver::MsgQueue const _queue );
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );
		virtual uint8 GetMaxVersion(){ return 4; }
		virtual bool SetValue( Value const& _value );

	protected:
		virtual void CreateVars( uint8 const _instance );

	private:
		SimpleAVControl( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ) {
			numReports = 0;
			key_items.clear();
			sequence_number = 0;
			scanned_key = false;
		}

		ValueList::Item CreateItem(string _label, int32 _value);
		void RequestAllReports(uint8 const _instance, uint8 const _numReports);
		void AddSupportKey(uint32 keyValue);

		uint8 numReports;
		uint8 lastReportId;
		uint32 lastReportKey;
		vector<ValueList::Item> key_items;
		uint32 sequence_number;
		bool scanned_key;
	};

} // namespace OpenZWave

#endif

