//-----------------------------------------------------------------------------
//
//	CentralScene.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_CENTRAL_SCENE
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "CentralScene.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "Notification.h"

using namespace OpenZWave;

enum CentralSceneCmd
{
	CentralSceneCmd_SupportedGet		= 0x01,
	CentralSceneCmd_SupportedReport		= 0x02,
	CentralSceneCmd_Notification		= 0x03
};

enum
{
	CentralSceneNotification_Properties1_KeyAttributesMask = 0x07
};

//-----------------------------------------------------------------------------
// <CentralScene::RequestState>
// Get the static central scene details from the device
//-----------------------------------------------------------------------------
bool CentralScene::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	return false;
}

//-----------------------------------------------------------------------------
// <CentralScene::RequestValue>
// Get the central scene details from the device
//-----------------------------------------------------------------------------
bool CentralScene::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	return false;
}

//-----------------------------------------------------------------------------
// <CentralScene::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool CentralScene::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch((CentralSceneCmd)_data[0]) {
		case CentralSceneCmd_SupportedGet:
		{
			Msg* msg = new Msg( "CentralSceneCmd_SupportedReport", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 4 );
			msg->Append( GetCommandClassId() );
			msg->Append( CentralSceneCmd_SupportedReport );
			msg->Append(0xff);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case CentralSceneCmd_SupportedReport:
		{
			Log::Write( LogLevel_Info, GetNodeId(), "Received CentralSceneCmd_SupportedReport: supported scenes=%d", _data[1]);
			return true;
		}
		case CentralSceneCmd_Notification:
		{
			uint8 _seq = _data[1];
			uint8 _key_attr = (_data[2] & CentralSceneNotification_Properties1_KeyAttributesMask);
			uint8 scene_number = _data[3];
			if(sequence != 0 && sequence == _seq)
				return false;
			switch(_key_attr) {
				case 0:
					Log::Write( LogLevel_Info, GetNodeId(), 
						"Received CentralSceneCmd_Notification: sequence=%d, Key Pressed, scene id=%d", _seq, scene_number);
					break;
				case 1:
					Log::Write( LogLevel_Info, GetNodeId(), 
						"Received CentralSceneCmd_Notification: sequence=%d, Key Released, scene id=%d", _seq, scene_number);
					break;
				case 2:
					Log::Write( LogLevel_Info, GetNodeId(), 
						"Received CentralSceneCmd_Notification: sequence=%d, Key Held Down, scene id=%d", _seq, scene_number);
					break;
				default:
					return false;
			}
			Notification* notification = new Notification( Notification::Type_CentralSceneEvent );
			notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
			notification->SetSceneId(scene_number);
			notification->SetKeyEvent(_key_attr);
			GetDriver()->QueueNotification( notification );
			return true;
		}
		default:
			break;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// <CentralScene::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void CentralScene::CreateVars(uint8 const _instance)
{
	//we only want to support, not control.
}
