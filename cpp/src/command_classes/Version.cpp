//-----------------------------------------------------------------------------
//
//	Version.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_VERSION
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "Version.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum VersionCmd
{
	VersionCmd_Get					= 0x11,
	VersionCmd_Report				= 0x12,
	VersionCmd_CommandClassGet			= 0x13,
	VersionCmd_CommandClassReport			= 0x14
};

enum
{
	VersionIndex_Library = 0,
	VersionIndex_Protocol,
	VersionIndex_Application, 
	VersionIndex_Hardware
};

//-----------------------------------------------------------------------------
// <Version::Version>
// Constructor
//-----------------------------------------------------------------------------
Version::Version
(
	uint32 const _homeId,
	uint8 const _nodeId
):
	CommandClass( _homeId, _nodeId ),
	m_classGetSupported( true )
{
	SetStaticRequest( StaticRequest_Values );
}

//-----------------------------------------------------------------------------
// <Version::ReadXML>
// Read configuration.
//-----------------------------------------------------------------------------
void Version::ReadXML
( 
	TiXmlElement const* _ccElement
)
{
	CommandClass::ReadXML( _ccElement );

	char const* str = _ccElement->Attribute("classgetsupported");
	if( str )
	{
		m_classGetSupported = !strcmp( str, "true");
	}
}

//-----------------------------------------------------------------------------
// <Version::WriteXML>
// Save changed configuration
//-----------------------------------------------------------------------------
void Version::WriteXML
( 
	TiXmlElement* _ccElement
)
{
	CommandClass::WriteXML( _ccElement );

	if( !m_classGetSupported )
	{
		_ccElement->SetAttribute( "classgetsupported", "false" );
	}
}

//-----------------------------------------------------------------------------
// <Version::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool Version::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Version::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool Version::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,		// = 0
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( _instance != 1 )
	{
		// This command class doesn't work with multiple instances
		return false;
	}

	Msg* msg = new Msg( "VersionCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( VersionCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <Version::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Version::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		switch((VersionCmd)_data[0]) {
			case VersionCmd_Report:
			{
				char library[8];
				char protocol[16];
				char application[16];

				snprintf( library, sizeof(library), "%d", _data[1] );
				snprintf( protocol, sizeof(protocol), "%d.%.2d", _data[2], _data[3] );
				snprintf( application, sizeof(application), "%d.%.2d", _data[4], _data[5] );

				if (GetVersion() >= 2) {
					char hardware[8];
					snprintf( hardware, sizeof(hardware), "%d", _data[6]);
					if( ValueString* hardwareValue = static_cast<ValueString*>( GetValue( _instance, VersionIndex_Hardware ) ) )
					{
						hardwareValue->OnValueRefreshed( hardware );
						hardwareValue->Release();
					}
				} else {
					Log::Write( LogLevel_Info, GetNodeId(), 
						"Received Version report: Library=%s, Protocol=%s, Application=%s", library, protocol, application );
				}
				ClearStaticRequest( StaticRequest_Values );

				if( ValueString* libraryValue = static_cast<ValueString*>( GetValue( _instance, VersionIndex_Library ) ) )
				{
					libraryValue->OnValueRefreshed( library );
					libraryValue->Release();
				}
				if( ValueString* protocolValue = static_cast<ValueString*>( GetValue( _instance, VersionIndex_Protocol ) ) )
				{
					protocolValue->OnValueRefreshed( protocol );
					protocolValue->Release();
				}
				if( ValueString* applicationValue = static_cast<ValueString*>( GetValue( _instance, VersionIndex_Application ) ) )
				{
					applicationValue->OnValueRefreshed( application );
					applicationValue->Release();
				}

				return true;
			}
			case VersionCmd_CommandClassReport:
			{
				if( CommandClass* pCommandClass = node->GetCommandClass( _data[1] ) )
				{
					Log::Write( LogLevel_Info, GetNodeId(), "Received Command Class Version report from node %d: CommandClass=%s, Version=%d", GetNodeId(), pCommandClass->GetCommandClassName().c_str(), _data[2] );
					pCommandClass->ClearStaticRequest( StaticRequest_Version );
					pCommandClass->SetVersion( _data[2] );
				}

				return true;
			}
			case VersionCmd_Get:
			case VersionCmd_CommandClassGet:
			{
				//Redirect to driver node to handle it.
				bool handled = false;
				if(Node* driver_node = GetDriver()->GetNodeUnsafe(GetDriver()->GetNodeId())) {
					Log::Write( LogLevel_Info, GetNodeId(), "Redirect to node %d %s handler.", 
						driver_node->GetNodeId(), GetCommandClassName().c_str());
					CommandClass* exitedCommandClass = driver_node->GetCommandClass(GetCommandClassId());
					if (exitedCommandClass) {
						handled = exitedCommandClass->HandleRedirectMsg(GetNodeId(), _data, _length, _instance);
					} else 
						Log::Write( LogLevel_Warning, GetNodeId(), "Can not find %s in node %d.", 
						GetCommandClassName().c_str(), GetNodeId());
				}
				return handled;
			}
			default:
				break;
		}
	}
	return false;
}

bool Version::HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	bool handled = false;
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	switch((VersionCmd)_data[0]) {
		case VersionCmd_Get:
		{
			Msg* msg = new Msg( "VersionCmd_Report", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( src_node );
			msg->Append( 9 );
			msg->Append( GetCommandClassId() );
			msg->Append( VersionCmd_Report );
			msg->Append(ZWAVE_LIBRARY_TYPE);
			msg->Append(ZWAVE_PROTOCAL_VERSION);
			msg->Append(ZWAVE_PROTOCAL_SUB_VERSION);
			msg->Append(APPLICATION_VERSION);
			msg->Append(APPLICATION_SUB_VERSION);
			msg->Append(HARDWARE_VERSION);
			msg->Append(0);
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		case VersionCmd_CommandClassGet:
		{
			Msg* msg = new Msg( "VersionCmd_CommandClassReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( src_node );
			msg->Append( 4 );
			msg->Append( GetCommandClassId() );
			msg->Append( VersionCmd_CommandClassReport );
			msg->Append(_data[1]);
			if( CommandClass* pCommandClass = node->GetCommandClass( _data[1] ) ) {
				msg->Append(pCommandClass->GetMaxVersion());
			} else {
				msg->Append(0);
			}
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <Version::RequestCommandClassVersion>
// Request the version of a command class used by the device 
//-----------------------------------------------------------------------------
bool Version::RequestCommandClassVersion
(
	CommandClass const* _commandClass
)
{
	if( m_classGetSupported )
	{
		if( _commandClass->HasStaticRequest( StaticRequest_Version ) )
		{
			Msg* msg = new Msg( "VersionCmd_CommandClassGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( VersionCmd_CommandClassGet );
			msg->Append( _commandClass->GetCommandClassId() );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Version::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void Version::CreateVars
(
	uint8 const _instance
)
{
	if (_instance != 1)
		return;

	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, VersionIndex_Library, "Library Version", "", true, false, "Unknown", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, VersionIndex_Protocol, "Protocol Version", "", true, false, "Unknown", 0 );
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, VersionIndex_Application, "Application Version", "", true, false, "Unknown", 0 );
		if (GetVersion() >= 2) {
			node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, VersionIndex_Hardware, "Hardware Version", "", true, false, "Unknown", 0);
		}
	}
}

void Version::initSupportSetting()
{
	char library[8];
	char protocol[16];
	char application[16];
	char hardware[8];

	snprintf( library, sizeof(library), "%d", ZWAVE_LIBRARY_TYPE );
	snprintf( protocol, sizeof(protocol), "%d.%.2d", ZWAVE_PROTOCAL_VERSION, ZWAVE_PROTOCAL_SUB_VERSION );
	snprintf( application, sizeof(application), "%d.%.2d", APPLICATION_VERSION, APPLICATION_SUB_VERSION );
	snprintf( hardware, sizeof(hardware), "%d", HARDWARE_VERSION);

	Log::Write( LogLevel_Info, GetNodeId(), "Received Version report from node %d: Library=%s, Protocol=%s, Application=%s, Hardware=%s", 
		GetNodeId(), library, protocol, application, hardware);
	ClearStaticRequest( StaticRequest_Values );

	if( ValueString* libraryValue = static_cast<ValueString*>( GetValue( 1, VersionIndex_Library ) ) )
	{
		libraryValue->OnValueRefreshed( library );
		libraryValue->Release();
	}
	if( ValueString* protocolValue = static_cast<ValueString*>( GetValue( 1, VersionIndex_Protocol ) ) )
	{
		protocolValue->OnValueRefreshed( protocol );
		protocolValue->Release();
	}
	if( ValueString* applicationValue = static_cast<ValueString*>( GetValue( 1, VersionIndex_Application ) ) )
	{
		applicationValue->OnValueRefreshed( application );
		applicationValue->Release();
	}
	if ( ValueString* hardwareValue = static_cast<ValueString*>( GetValue( 1, VersionIndex_Hardware ) ) )
	{
		hardwareValue->OnValueRefreshed( hardware );
		hardwareValue->Release();
	}
}
