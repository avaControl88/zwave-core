//-----------------------------------------------------------------------------
//
//	ApplicationStatus.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_APPLICATION_STATUS
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ApplicationStatus.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Log.h"
#include "Notification.h"

using namespace OpenZWave;

enum {
	ApplicationStatusCmd_Busy_TryAgain = 0,
	ApplicationStatusCmd_Busy_TriAgainWithTime = 1,
	ApplicationStatusCmd_Busy_RequestQueued = 2,
	ApplicationStatusCmd_RejectedRequest_Rejected = 0
};

//-----------------------------------------------------------------------------
// <ApplicationStatus::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ApplicationStatus::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ApplicationStatusCmd_Busy == (ApplicationStatusCmd)_data[0] )
	{
		char msg[64];
		switch( _data[1] )
		{
			case ApplicationStatusCmd_Busy_TryAgain:
			{
				snprintf( msg, sizeof(msg), "Try again later" );
				break;
			}
			case ApplicationStatusCmd_Busy_TriAgainWithTime:
			{
				snprintf( msg, sizeof(msg), "Try again in %d seconds", _data[2] );
				break;
			}
			case ApplicationStatusCmd_Busy_RequestQueued:
			{
				snprintf( msg, sizeof(msg), "Request queued, will be executed later" );
				break;
			}
			default:
			{
				// Invalid status
				snprintf( msg, sizeof(msg), "Unknown status %d", _data[1] );
			}
		}
		Log::Write( LogLevel_Info, GetNodeId(), "Received Application Status Busy: %s", msg );
		Notification* notification = new Notification( Notification::Type_NodeBusy );
		notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
		notification->SetBusyEvent(_data[1]);
		GetDriver()->QueueNotification( notification );
		return true;
	}

	if( ApplicationStatusCmd_RejectedRequest == (ApplicationStatusCmd)_data[0] )
	{
		Log::Write( LogLevel_Info, "Received Application Rejected Request: Status=%d", _data[1] );
		Notification* notification = new Notification( Notification::Type_NodeRejectRequest );
		notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
		notification->SetRejectRequestEvent(_data[1]);
		GetDriver()->QueueNotification( notification );
		return true;
	}

	return false;
}

bool ApplicationStatus::HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	switch ( (ApplicationStatusCmd)_data[0] ) {
		case ApplicationStatusCmd_Busy:
		{
			SendApplicationStatus_Busy(src_node);
			return true;
		}
		case ApplicationStatusCmd_RejectedRequest:
		{
			SendApplicationStatus_RejectedRequest(src_node);
			return true;
		}
		default:
			break;
	}
	return false;
}

void ApplicationStatus::SendApplicationStatus_Busy(const uint8 target_node) {
	if (target_node == 0 || target_node == 255 || target_node == GetDriver()->GetNodeId())
		return;
	Msg* msg = new Msg( "ApplicationStatusCmd_Busy", target_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
	msg->Append( target_node );
	msg->Append( 4 );
	msg->Append( GetCommandClassId() );
	msg->Append( ApplicationStatusCmd_Busy );
	msg->Append(ApplicationStatusCmd_Busy_TryAgain);
	msg->Append(3);
	msg->Append( GetDriver()->GetTransmitOptions() );
	SendSecureMsg(msg, Driver::MsgQueue_Command);
}

void ApplicationStatus::SendApplicationStatus_RejectedRequest(const uint8 target_node) {
	if (target_node == 0 || target_node == 255 || target_node == GetDriver()->GetNodeId())
		return;
	Msg* msg = new Msg( "ApplicationStatusCmd_Busy", target_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
	msg->Append( target_node );
	msg->Append( 3 );
	msg->Append( GetCommandClassId() );
	msg->Append( ApplicationStatusCmd_RejectedRequest );
	msg->Append(ApplicationStatusCmd_RejectedRequest_Rejected);
	msg->Append( GetDriver()->GetTransmitOptions() );
	SendSecureMsg(msg, Driver::MsgQueue_Command);
}

void ApplicationStatus::initSupportSetting()
{
}
