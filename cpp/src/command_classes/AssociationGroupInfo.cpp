//-----------------------------------------------------------------------------
//
//	AssociationGroupInfo.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_ASSOCIATION_GROUP_INFO
//
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "AssociationGroupInfo.h"
#include "Association.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"
#include "Group.h"
#include "SensorMultilevel.h"
#include "Alarm.h"

using namespace OpenZWave;

enum AssociationGroupInfoCmd
{
	AssociationGroupInfoCmd_GroupNameGet				= 0x1,
	AssociationGroupInfoCmd_GroupNameReport				= 0x2,
	AssociationGroupInfoCmd_GroupInfoGet				= 0x3,
	AssociationGroupInfoCmd_GroupInfoReport				= 0x4,
	AssociationGroupInfoCmd_GroupCommandListGet			= 0x5,
	AssociationGroupInfoCmd_GroupCommandListReport		= 0x6
};

enum
{
	AssociationGroupInfo_Get_PropertiesListModeMask				= 0x40,
	AssociationGroupInfo_Get_PropertiesRefreshCacheMask			= 0x80, 

	AssociationGroupInfo_Report_PropertiesGroupCountMask		= 0x3f,
	AssociationGroupInfo_Report_PropertiesDynamicInfoBitMask	= 0x40,
	AssociationGroupInfo_Report_PropertiesListModeBitMask		= 0x80, 

	AssociationGroupCommandList_Get_PropertiesAllowCacheBitMask = 0x80
};

#define GROUP_NAME_1 "Lifeline"

//-----------------------------------------------------------------------------
// <AssociationGroupInfo::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool AssociationGroupInfo::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if (_requestFlags & RequestFlag_Static)
	{
		//RequestValue( 1, AssociationGroupInfoCmd_GroupNameGet, _instance, _queue );
		//return RequestValue( 1, AssociationGroupInfoCmd_GroupInfoGet, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <AssociationGroupInfo::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool AssociationGroupInfo::RequestValue
(
	uint32 const _requestFlags,//group number
	uint8 const _dummy1,		// = 0
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( _instance != 1 )
	{
		// This command class doesn't work with multiple instances
		return false;
	}
	
	if (_dummy1 == AssociationGroupInfoCmd_GroupNameGet) {
		Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupNameGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance(this, _instance);
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( AssociationGroupInfoCmd_GroupNameGet );
		msg->Append( _requestFlags );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
	} else {
		Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupInfoGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance(this, _instance);
		msg->Append( GetNodeId() );
		msg->Append( 4 );
		msg->Append( GetCommandClassId() );
		msg->Append( AssociationGroupInfoCmd_GroupInfoGet );
		msg->Append( 0 );
		msg->Append( _requestFlags );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
	}
	return true;
}

void AssociationGroupInfo::RequestAllGroups( uint32 const _requestFlags ) {
	RequestValue( _requestFlags, AssociationGroupInfoCmd_GroupNameGet, 1, Driver::MsgQueue_Send );
}

void AssociationGroupInfo::RequestAllGroupInfo( uint32 const _requestFlags ) {
	RequestValue( _requestFlags, AssociationGroupInfoCmd_GroupInfoGet, 1, Driver::MsgQueue_Send );
}

//-----------------------------------------------------------------------------
// <AssociationGroupInfo::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool AssociationGroupInfo::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	switch( (AssociationGroupInfoCmd)_data[0] ) {
		case  AssociationGroupInfoCmd_GroupNameReport:
		{
			uint8 groupIdx = _data[1];
			uint8 groupName_length = _data[2];
			uint8 const* groupName = &_data[3];
			char _groupName[128];
			snprintf(_groupName, sizeof(_groupName), "%.*s", groupName_length, groupName);
			Log::Write( LogLevel_Info, GetNodeId(), "Group Information: Group=%d, Name=%.*s", groupIdx, strlen(_groupName), _groupName);
			Group *group =  node->GetGroup(groupIdx);
			if (group == NULL) {
				Log::Write( LogLevel_Error, GetNodeId(), "AssociationGroupInfoCmd_GroupNameReport: Group %d is not exist.", groupIdx);
				return false;
			}
			group->SetLabel(string(_groupName));
			return true;
		}
		case  AssociationGroupInfoCmd_GroupInfoReport:
		{
			bool listMode = ((_data[1] & AssociationGroupInfo_Report_PropertiesListModeBitMask) != 0);
			bool dynamicInfo = ((_data[1] & AssociationGroupInfo_Report_PropertiesDynamicInfoBitMask) != 0);
			uint8 groupCount = ((_data[1] & AssociationGroupInfo_Report_PropertiesGroupCountMask) != 0);
			for (int i=0; i<groupCount; i++) {
				uint8 groupIdx = _data[2+i*7];
				uint8 mode = _data[3+i*7];
				uint8 profile_msb = _data[4+i*7];
				uint8 profile_lsb = _data[5+i*7];
				uint16 profile = (profile_msb << 8 | profile_lsb);
				Log::Write( LogLevel_Info, GetNodeId(), "AssociationGroupInfoCmd_GroupInfoReport: Group=%d, Profile=%.4x", groupIdx, profile);

				Group *group =  node->GetGroup(groupIdx);
				if (group == NULL) {
					Log::Write( LogLevel_Error, GetNodeId(), "AssociationGroupInfoCmd_GroupInfoReport: Group %d is not exist.", groupIdx);
					continue;
				}

				switch( profile_msb ) {
					case AGI_Profile_General:
					{
						if (profile_lsb == AGI_Profile_General_NA) {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_General(AGI_Profile_General_NA)");
						} else {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_General(%.2x)", profile_lsb);
						}
						break;
					}
					case AGI_Profile_Control:
					{
						if (profile_lsb >= 0x01 && profile_lsb <= 0x20) {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Control(AGI_Profile_Control_Key%.2d)", profile_lsb);
						} else {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Control(%.2x)", profile_lsb);
						}
						Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Control");
						break;
					}
					case AGI_Profile_Sensor:
					{
						if (profile_lsb < (sizeof(c_sensorTypeNames)/sizeof(c_sensorTypeNames[0])) ) {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Sensor(%s)", c_sensorTypeNames[profile_lsb]);
						} else {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Sensor(%.2x)", profile_lsb);
						}
						break;
					}
					case AGI_Profile_Notification:
					{
						if (profile_lsb < (sizeof(c_notificationTypeNames)/sizeof(c_notificationTypeNames))) {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Notification(%s)", c_notificationTypeNames[profile_lsb]);
						} else if (profile_lsb == NotificationType_First) {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Notification(Default)");
						} else {
							Log::Write( LogLevel_Info, GetNodeId(), "AGI_Profile_Notification(%.2x)", profile_lsb);
						}
						break;
					}
					default:
						Log::Write( LogLevel_Warning, GetNodeId(), "Unknown Profile %.4x", profile);
						break;
				}
				group->SetProfile(profile);
			}
			return true;
		}
		case  AssociationGroupInfoCmd_GroupCommandListReport:
		{
			uint8 groupIdx = _data[1];
			uint8 listLength = _data[2];
			Log::Write( LogLevel_Info, GetNodeId(), "AssociationGroupInfoCmd_GroupCommandListReport Group=%d:", groupIdx);
			uint8* cmdlist;
			uint8 count = 0;
			if (listLength > 0) {
				cmdlist = (uint8*)(&_data[3]);
				for (int i=0; i<listLength;) {
					if (cmdlist[i] >= 0x20 && cmdlist[i] <= 0xee) {
						//normal commnd class
						Log::Write( LogLevel_Info, GetNodeId(), "	Command %d: 0x%.2d, 0x%.2d", 
							count, cmdlist[i], cmdlist[i+1]);
						i += 2;
						count++;
					} else if (cmdlist[i] >= 0xf1 && cmdlist[i] <= 0xff) {
						//exetended command class
						Log::Write( LogLevel_Info, GetNodeId(), "	Command %d: 0x%.2d, 0x%.2d, 0x%.2d", 
							count, cmdlist[i], cmdlist[i+1], cmdlist[i+2]);
						i += 3;
						count++;
					} else {
						Log::Write( LogLevel_Warning, GetNodeId(), "	Unknown Command %d: 0x%.2d", count, cmdlist[i]);
						break;
					}
				}
			} else {
				Log::Write( LogLevel_Info, GetNodeId(), "	No command list.");
			}
			return true;
		}
		case AssociationGroupInfoCmd_GroupNameGet:
		case AssociationGroupInfoCmd_GroupInfoGet:
		case AssociationGroupInfoCmd_GroupCommandListGet:
		{
			//Redirect to driver node to handle it.
			if(Node* driver_node = GetDriver()->GetNodeUnsafe(GetDriver()->GetNodeId())) {
				bool handled = false;
				Log::Write( LogLevel_Info, GetNodeId(), "Redirect to node %d %s handler.", 
					driver_node->GetNodeId(), GetCommandClassName().c_str());
				CommandClass* exitedCommandClass = driver_node->GetCommandClass(GetCommandClassId());
				if (exitedCommandClass) {
					handled = exitedCommandClass->HandleRedirectMsg(GetNodeId(), _data, _length, _instance);
				} else 
					Log::Write( LogLevel_Warning, GetNodeId(), "Can not find %s in node %d.", 
					GetCommandClassName().c_str(), GetNodeId());
				return handled;
			}
			break;
		}
		default:
			break;
	}

	return false;
}

bool AssociationGroupInfo::HandleRedirectMsg(uint8 const src_node, uint8 const* _data, uint32 const _length, uint32 const _instance /* = 1 */) {
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;
	switch( (AssociationGroupInfoCmd)_data[0] ) {
		case AssociationGroupInfoCmd_GroupNameGet:
		{
			uint8 groupIdx = _data[1];
			Group* group = node->GetGroup(groupIdx);
			Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupNameReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( src_node );
			if (group) {
				string groupName = group->GetLabel();
				msg->Append( (4+groupName.size()) );
				msg->Append( GetCommandClassId() );
				msg->Append( AssociationGroupInfoCmd_GroupNameReport );
				msg->Append( groupIdx );
				msg->Append( groupName.size() );
				for(uint32 i=0; i<groupName.size(); i++) {
					msg->Append((uint8)groupName.c_str()[i]);
				}
			} else {
				msg->Append(4);
				msg->Append( GetCommandClassId() );
				msg->Append( AssociationGroupInfoCmd_GroupNameReport );
				msg->Append(groupIdx);
				msg->Append(0);
			}
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case AssociationGroupInfoCmd_GroupInfoGet:
		{
			bool listMode = ((_data[1] & AssociationGroupInfo_Get_PropertiesListModeMask) != 0);
			bool refreshCache = ((_data[1] & AssociationGroupInfo_Get_PropertiesRefreshCacheMask) != 0);
			uint8 groupID = _data[2];
			uint8 info = 0;

			if (listMode) {
				int frame_count = 0;
				uint8 group_count = node->GetNumGroups();
				if (group_count > 6) {
					Log::Write( LogLevel_Warning, GetNodeId(), "Only support no more than 6 groups.");
					group_count = 6;
				}

				info = group_count;
				info = (info | AssociationGroupInfo_Report_PropertiesListModeBitMask | 
					AssociationGroupInfo_Report_PropertiesDynamicInfoBitMask);

				if (group_count == 0) {
					Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupInfoReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->Append( src_node );
					msg->Append( 3 );
					msg->Append( GetCommandClassId() );
					msg->Append( AssociationGroupInfoCmd_GroupInfoReport );
					msg->Append( info );
					msg->Append( GetDriver()->GetTransmitOptions() );
					SendSecureMsg(msg, Driver::MsgQueue_Send);
				} else {
					Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupInfoReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->Append( src_node );
					msg->Append( (3+group_count*7) );
					msg->Append( GetCommandClassId() );
					msg->Append( AssociationGroupInfoCmd_GroupInfoReport );
					msg->Append( info );
					for (int i=0; i<group_count; i++) {
						uint16 _profile = 0;
						Group* group = node->GetGroup(i+1);
						if (group) 
							_profile = group->GetProfile();
						uint8 profile_msb = (_profile & 0xff00)>>8;
						uint8 profile_lsb = (_profile & 0xff);
						msg->Append(i+1);
						msg->Append(0);
						msg->Append(profile_msb);
						msg->Append(profile_lsb);
						msg->Append(0);
						msg->Append(0);
						msg->Append(0);
					}
					msg->Append( GetDriver()->GetTransmitOptions() );
					SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			} else {
				Group* group = node->GetGroup(groupID);
				
				Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupInfoReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->Append( src_node );
				if (group) {
					uint16 _profile = 0;
					_profile = group->GetProfile();
					uint8 profile_msb = (_profile & 0xff00)>>8;
					uint8 profile_lsb = (_profile & 0xff);
					info = 1;
					info = (info | AssociationGroupInfo_Report_PropertiesDynamicInfoBitMask);
					msg->Append(10);
					msg->Append( GetCommandClassId() );
					msg->Append( AssociationGroupInfoCmd_GroupInfoReport );
					msg->Append( info );
					msg->Append(groupID);
					msg->Append(0);
					msg->Append(profile_msb);
					msg->Append(profile_lsb);
					msg->Append(0);
					msg->Append(0);
					msg->Append(0);
				} else {
					info = (info | AssociationGroupInfo_Report_PropertiesDynamicInfoBitMask);
					msg->Append(3);
					msg->Append( GetCommandClassId() );
					msg->Append( AssociationGroupInfoCmd_GroupInfoReport );
					msg->Append( info );
				}
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
			return true;
		}
		case AssociationGroupInfoCmd_GroupCommandListGet:
		{
			bool allowCache = ((_data[1] & AssociationGroupCommandList_Get_PropertiesAllowCacheBitMask) != 0);
			uint8 groupID = _data[2];
			Msg* msg = new Msg( "AssociationGroupInfoCmd_GroupCommandListReport", src_node, REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->Append( src_node );

			if (groupID == 1) {
				msg->Append(6);
				msg->Append( GetCommandClassId() );
				msg->Append( AssociationGroupInfoCmd_GroupCommandListReport );
				msg->Append(groupID);
				msg->Append( 2 );
				//0x20~0xee 1 byte, 0xf1~0xff 2 byte
				//DeviceResetLocally
				msg->Append(0x5a);
				msg->Append(0x01);
			} else if (Group* group = node->GetGroup(groupID)) {
				msg->Append(4);
				msg->Append( GetCommandClassId() );
				msg->Append( AssociationGroupInfoCmd_GroupCommandListReport );
				msg->Append(groupID);
				msg->Append( 0 );
			} else {
				msg->Append(4);
				msg->Append( GetCommandClassId() );
				msg->Append( AssociationGroupInfoCmd_GroupCommandListReport );
				msg->Append(0);
				msg->Append(0);
			}
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <AssociationGroupInfo::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void AssociationGroupInfo::CreateVars
(
	uint8 const _instance
)
{
}

void AssociationGroupInfo::initSupportSetting()
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return;
	Group* group = node->GetGroup(ASSOCIATION_SUPPORT_GROUP);
	if (group)
		group->SetProfile(0x0000);
}
