//-----------------------------------------------------------------------------
//
//	ColorControl.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_COLOR_CONTROL
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ColorControl.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"
#include "ValueByte.h"
#include "ValueRaw.h"
#include "Notification.h"

using namespace OpenZWave;

enum ColorControlCmd
{
	ColorControlCmd_CapabilityGet				= 0x01,
	ColorControlCmd_CapabilityReport			= 0x02,
	ColorControlCmd_StateGet					= 0x03,
	ColorControlCmd_StateReport					= 0x04,
	ColorControlCmd_StateSet					= 0x05,

	//no need
	ColorControlCmd_StartCapabilityLevelChange	= 0x06,
	ColorControlCmd_StopStateChange				= 0x07
};

enum
{
	ColorControlStateSet_Properties1_StateDataLengthMask = 0x1f,
	ColorControlStartCapabilityLevelChange_Properties_IgnoreStartStateBitMask = 0x20,
	ColorControlStartCapabilityLevelChange_Properties_UpDownBitMask = 0x40
};

enum
{
	ColorControlIndex_Duration = 95,
	ColorControlIndex_RGB = 96,
	ColorControlIndex_RGBW = 97,
	ColorControlIndex_6CH_Mixing = 98,
	ColorControlIndex_IconName = 99
};

enum {
	ComponentID_WarmWhite = 0,
	ComponentID_ColdWhite,
	ComponentID_Red,
	ComponentID_Green,
	ComponentID_Blue,
	ComponentID_Amber,
	ComponentID_Cyan,
	ComponentID_Purple,
	ComponentID_IndexedColor
};

static char const* c_componentID_Label[] = {
	"Warm White",
	"Cold White",
	"Red",
	"Green",
	"Blue",
	"Amber",
	"Cyan",
	"Purple",
	"Indexed Color"
};

//-----------------------------------------------------------------------------
// <ColorControl::ReadXML>
// Read configuration.
//-----------------------------------------------------------------------------
void ColorControl::ReadXML( TiXmlElement const* _ccElement ) {
	CommandClass::ReadXML( _ccElement );

	char const* str = _ccElement->Attribute("auto_report");
	if (str) {
		m_autoReport = !strcmp( str, "true");
	}

	char const* _delay_poll = _ccElement->Attribute("delay_poll");
	if (_delay_poll) {
		m_delayPoll = !strcmp( _delay_poll, "true");
	}

	char const* _time = _ccElement->Attribute("delay_time");
	if (_time) {
		m_delay_time = atoi(_time);
	}

	char const* _wait_autoReport = _ccElement->Attribute("wait_autoReport");
	if (_wait_autoReport) {
		wait_autoReport = !strcmp(_wait_autoReport, "true");
	}
}

//-----------------------------------------------------------------------------
// <ColorControl::WriteXML>
// Save changed configuration
//-----------------------------------------------------------------------------
void ColorControl::WriteXML( TiXmlElement* _ccElement ) {
	CommandClass::WriteXML( _ccElement );

	if (m_autoReport) {
		_ccElement->SetAttribute( "auto_report", "true" );
	}
	if (m_delayPoll) {
		_ccElement->SetAttribute( "delay_poll", "true" );
		_ccElement->SetAttribute("delay_time", m_delay_time);
	}
	if (wait_autoReport) {
		_ccElement->SetAttribute("wait_autoReport", "true");
	}
}

//-----------------------------------------------------------------------------
// <ColorControl::RequestState>
// Get the color control details from the device
//-----------------------------------------------------------------------------
bool ColorControl::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;

	if (_requestFlags & RequestFlag_Static) {
		Msg* msg = new Msg( "ColorControlCmd_CapabilityGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ColorControlCmd_CapabilityGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		res |= SendSecureMsg(msg, _queue);
	}

	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current state
		for (uint8 i=0; i<(sizeof(c_componentID_Label)/sizeof(c_componentID_Label[0])); i++) {
			if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, i))) {
				m_value->Release();
				res |= RequestValue( _requestFlags, i, _instance, _queue);
			}
		}
	}

	return res;
}

//-----------------------------------------------------------------------------
// <ColorControl::RequestValue>
// Get the color control details from the device
//-----------------------------------------------------------------------------
bool ColorControl::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _index,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	// Request the current state
	if (_index < (sizeof(c_componentID_Label)/sizeof(c_componentID_Label[0]))) {
		if (m_autoReport == true)
			return true;
		if (m_delayPoll && !wait_poll) {
			Notification* notif = new Notification(Notification::Type_SchedulePoll);
			notif->SetHomeNodeIdAndInstance(GetHomeId(), GetNodeId(), _instance );
			notif->SetScheduleTime(m_delay_time);
			notif->SetValueId(GetValue(_instance, _index)->GetID());
			GetDriver()->QueueNotification( notif );
			wait_poll = true;
			return true;
		} else if (m_delayPoll && wait_poll) {
			wait_poll = false;
		}

		Msg* msg = new Msg( "ColorControlCmd_StateGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( ColorControlCmd_StateGet );
		msg->Append(_index);
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (_index == ColorControlIndex_RGB || _index == ColorControlIndex_RGBW) {
		if (m_autoReport == true)
			return true;
		if (m_delayPoll && !wait_poll) {
			Notification* notif = new Notification(Notification::Type_SchedulePoll);
			notif->SetHomeNodeIdAndInstance(GetHomeId(), GetNodeId(), _instance );
			notif->SetScheduleTime(m_delay_time);
			notif->SetValueId(GetValue(_instance, _index)->GetID());
			GetDriver()->QueueNotification( notif );
			wait_poll = true;
			return true;
		} else if (m_delayPoll && wait_poll) {
			wait_poll = false;
		}

		bool res = false;
		res |= RequestValue( _requestFlags, ComponentID_Red, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Green, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Blue, _instance, _queue);
		if (_index == ColorControlIndex_RGBW) {
			if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ComponentID_WarmWhite))) {
				m_value->Release();
				res |= RequestValue( _requestFlags, ComponentID_WarmWhite, _instance, _queue);
			}
			if (ValueByte* m_value2 = static_cast<ValueByte*>(GetValue(_instance, ComponentID_ColdWhite))) {
				m_value2->Release();
				res |= RequestValue( _requestFlags, ComponentID_ColdWhite, _instance, _queue);
			}
		}
		return res;
	} else if (_index == ColorControlIndex_6CH_Mixing) {
		if (m_autoReport == true)
			return true;
		if (m_delayPoll && !wait_poll) {
			Notification* notif = new Notification(Notification::Type_SchedulePoll);
			notif->SetHomeNodeIdAndInstance(GetHomeId(), GetNodeId(), _instance );
			notif->SetScheduleTime(m_delay_time);
			notif->SetValueId(GetValue(_instance, _index)->GetID());
			GetDriver()->QueueNotification( notif );
			wait_poll = true;
			return true;
		} else if (m_delayPoll && wait_poll) {
			wait_poll = false;
		}

		bool res = false;
		res |= RequestValue( _requestFlags, ComponentID_Red, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Green, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Blue, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Amber, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Cyan, _instance, _queue);
		res |= RequestValue( _requestFlags, ComponentID_Purple, _instance, _queue);
		return res;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ColorControl::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ColorControl::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch((ColorControlCmd)_data[0]) {
		case ColorControlCmd_CapabilityReport:
		{
			uint16 componentMask = (uint16)(_data[2]<<8 | _data[1]);
			Log::Write(LogLevel_Info, GetNodeId(), "Received ColorControlCmd_CapabilityReport: componentMask=0x%4x", componentMask);

			bool support_red = false;
			bool support_green = false;
			bool support_blue = false;
			for (uint8 i=0; i<16; i++) {
				uint8 index = 1<<i;
				bool supported = ((componentMask & index) != 0);
				if (supported) {
					if(Node* node = GetNodeUnsafe()) {
						if(ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, i)))
							m_value->Release();
						else {
							if (i < (sizeof(c_componentID_Label)/sizeof(c_componentID_Label[0])))
								node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, i, c_componentID_Label[i], "", false, false, 0, 0);
						}
					}
				}
			}
			CreateValue_RGBW(_instance);
			CreateValue_6CH(_instance);
			RequestState(RequestFlag_Dynamic, _instance, Driver::MsgQueue_Send);
			return true;
		}
		case ColorControlCmd_StateReport:
		{
			uint8 componentID = _data[1];
			uint8 level = _data[2];
			Log::Write(LogLevel_Info, GetNodeId(), "Received ColorControlCmd_StateReport: componentID=%d, level=%d", componentID, level);
			if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, componentID))) {
				m_value->OnValueRefreshed(level);
				m_value->Release();
			}

			if (componentID == ComponentID_Red || componentID == ComponentID_Green || componentID == ComponentID_Blue) {
				UpdateValue_RGB(_instance, componentID, level);
				UpdateValue_RGBW(_instance, componentID, level);
				UpdateValue_6CH(_instance, componentID, level);
			} else if (componentID == ComponentID_WarmWhite || componentID == ComponentID_ColdWhite) {
				UpdateValue_RGBW(_instance, componentID, level);
			} else if (componentID == ComponentID_Amber || componentID == ComponentID_Cyan || componentID == ComponentID_Purple) {
				UpdateValue_6CH(_instance, componentID, level);
			}
			return true;
		}
		default:
			break;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// <ColorControl::SetValue>
// Set the device's state
//-----------------------------------------------------------------------------
bool ColorControl::SetValue (Value const& _value) 
{
	uint8 instance = _value.GetID().GetInstance();
	uint8 index = (ColorControlCmd)_value.GetID().GetIndex();
	uint8 duration = 0;
	if (GetVersion() >= 2) {
		if (ValueByte* m_duration = static_cast<ValueByte*>(GetValue(instance, ColorControlIndex_Duration))) {
			duration = m_duration->GetValue();
			m_duration->Release();
		}
	}
	if (index < (sizeof(c_componentID_Label)/sizeof(c_componentID_Label[0])) ) {
		if (ValueByte const* m_value = static_cast<ValueByte const*>(&_value)) {
			Msg* msg = new Msg( "ColorControlCmd_StateSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );

			if (GetVersion() >= 2)
				msg->Append(6);
			else
				msg->Append(5);

			msg->Append( GetCommandClassId() );
			msg->Append( ColorControlCmd_StateSet );
			msg->Append(1);
			msg->Append(index);
			msg->Append(m_value->GetValue());
			if (GetVersion() >= 2)
				msg->Append(duration);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	} else if (index == ColorControlIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ColorControlIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	} else if (index == ColorControlIndex_RGB) {
		if (ValueRaw const* m_value = static_cast<ValueRaw const*>(&_value)) {
			uint8* _raw = m_value->GetValue();
			uint8 _length = m_value->GetLength();

			if (_length != 3)
				return false;

			Msg* msg = new Msg( "ColorControlCmd_StateSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );
			
			if (GetVersion() >= 2)
				msg->Append(10);
			else
				msg->Append( 9 );
			
			msg->Append( GetCommandClassId() );
			msg->Append( ColorControlCmd_StateSet );
			msg->Append(3);//Red, Green, Blue
			//Red
			msg->Append(ComponentID_Red);
			msg->Append(_raw[0]);
			//Green
			msg->Append(ComponentID_Green);
			msg->Append(_raw[1]);
			//Blue
			msg->Append(ComponentID_Blue);
			msg->Append(_raw[2]);
			if (GetVersion() >= 2)
				msg->Append(duration);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	} else if (index == ColorControlIndex_RGBW) {
		if (ValueRaw const* m_value = static_cast<ValueRaw const*>(&_value)) {
			uint8* _raw = m_value->GetValue();
			uint8 _length = m_value->GetLength();
			bool warm_support = false;
			bool cold_support = false;
			uint8 white_count = 0;

			if (ValueByte* warm_white = static_cast<ValueByte*>(GetValue(instance, ComponentID_WarmWhite))) {
				warm_white->Release();
				warm_support = true;
				white_count++;
			}
			if (ValueByte* cold_white = static_cast<ValueByte*>(GetValue(instance, ComponentID_ColdWhite))) {
				cold_white->Release();
				cold_support = true;
				white_count++;
			}

			if (_length != 4 || white_count == 0)
				return false;

			Msg* msg = new Msg( "ColorControlCmd_StateSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );
			
			if (GetVersion() >= 2)
				msg->Append(10+(white_count*2));
			else
				msg->Append( 9+(white_count*2) );
			
			msg->Append( GetCommandClassId() );
			msg->Append( ColorControlCmd_StateSet );
			msg->Append(3+(white_count));//(Warm White & Cold White), Red, Green, Blue
			//(Warm White/Cold White)
			if (warm_support) {
				msg->Append(ComponentID_WarmWhite);
				msg->Append(_raw[3]);
			}
			if (cold_support) {
				msg->Append(ComponentID_ColdWhite);
				msg->Append(_raw[3]);
			}
			//Red
			msg->Append(ComponentID_Red);
			msg->Append(_raw[0]);
			//Green
			msg->Append(ComponentID_Green);
			msg->Append(_raw[1]);
			//Blue
			msg->Append(ComponentID_Blue);
			msg->Append(_raw[2]);
			if (GetVersion() >= 2)
				msg->Append(duration);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	} else if (index == ColorControlIndex_6CH_Mixing) {
		if (ValueRaw const* m_value = static_cast<ValueRaw const*>(&_value)) {
			uint8* _raw = m_value->GetValue();
			uint8 _length = m_value->GetLength();

			if (_length != 6)
				return false;

			Msg* msg = new Msg( "ColorControlCmd_StateSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
			msg->SetInstance( this, instance );
			msg->Append( GetNodeId() );
			
			if (GetVersion() >= 2)
				msg->Append(16);
			else
				msg->Append( 15 );
			
			msg->Append( GetCommandClassId() );
			msg->Append( ColorControlCmd_StateSet );
			msg->Append(6);//Red, Green, Blue, Amber, Cyan, Purple
			//Red
			msg->Append(ComponentID_Red);
			msg->Append(_raw[0]);
			//Green
			msg->Append(ComponentID_Green);
			msg->Append(_raw[1]);
			//Blue
			msg->Append(ComponentID_Blue);
			msg->Append(_raw[2]);
			//Amber
			msg->Append(ComponentID_Amber);
			msg->Append(_raw[3]);
			//Cyan
			msg->Append(ComponentID_Cyan);
			msg->Append(_raw[4]);
			//Purple
			msg->Append(ComponentID_Purple);
			msg->Append(_raw[5]);
			if (GetVersion() >= 2)
				msg->Append(duration);
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	} else if (index == ColorControlIndex_Duration) {
		if (ValueByte const* m_value = static_cast<ValueByte const*>(GetValue(instance, ColorControlIndex_Duration))) {
			uint8 _duration = m_value->GetValue();
			if (ValueByte* m_duration = static_cast<ValueByte*>( GetValue(instance, ColorControlIndex_Duration))) {
				m_duration->OnValueRefreshed(_duration);
				m_duration->Release();
			}
		}
		return false;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ColorControl::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ColorControl::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ColorControlIndex_IconName, 
			"ColorControl Icon", "icon", false, false, "", 0);
		if (GetVersion() >= 2) {
			node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ColorControlIndex_Duration, 
				"Duration", "", false, false, 255, 0);
		}
	}
}

bool ColorControl::CreateValue_RGB(uint8 const _instance) {
	bool support_red = false;
	bool support_green = false;
	bool support_blue = false;
	if (ValueRaw* m_rgb = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_RGB))) {
		m_rgb->Release();
		return true;
	}
	if (ValueByte* m_red = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Red))) {
		m_red->Release();
		support_red = true;
	}
	if (ValueByte* m_green = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Green))) {
		m_green->Release();
		support_green = true;
	}
	if (ValueByte* m_blue = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Blue))) {
		m_blue->Release();
		support_blue = true;
	}

	if (support_red && support_green && support_blue) {
		if (Node* node = GetNodeUnsafe()) {
			uint8 data[3];
			memset(data, 0, sizeof(data));
			node->CreateValueRaw(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ColorControlIndex_RGB, 
				"RGB Color", "", false, false, data, sizeof(data), 0);
		}
		return true;
	}
	return false;
}

void ColorControl::CreateValue_RGBW(uint8 const _instance) {
	if (ValueRaw* m_rgbw = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_RGBW))) {
		m_rgbw->Release();
		return;
	}
	if (CreateValue_RGB(_instance) == false)
		return;

	bool support_white = false;
	if (ValueByte* m_white1 = static_cast<ValueByte*>(GetValue(_instance, ComponentID_WarmWhite))) {
		m_white1->Release();
		support_white = true;
	}
	if (ValueByte* m_white2 = static_cast<ValueByte*>(GetValue(_instance, ComponentID_ColdWhite))) {
		m_white2->Release();
		support_white = true;
	}
	if (support_white) {
		if (Node* node = GetNodeUnsafe()) {
			uint8 data[4];
			memset(data, 0, sizeof(data));
			node->CreateValueRaw(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ColorControlIndex_RGBW, 
				"RGBW Color", "", false, false, data, sizeof(data), 0);
		}
	}
}

void ColorControl::CreateValue_6CH(uint8 const _instance) {
	if (ValueRaw* m_6ch = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_6CH_Mixing))) {
		m_6ch->Release();
		return;
	}
	if (CreateValue_RGB(_instance) == false)
		return;
	bool support_Amber = false;
	bool support_Cyan = false;
	bool support_Purple = false;
	if (ValueByte* m_Amber = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Amber))) {
		m_Amber->Release();
		support_Amber = true;
	}
	if (ValueByte* m_Cyan = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Cyan))) {
		m_Cyan->Release();
		support_Cyan = true;
	}
	if (ValueByte* m_Purple = static_cast<ValueByte*>(GetValue(_instance, ComponentID_Purple))) {
		m_Purple->Release();
		support_Purple = true;
	}
	if (support_Amber && support_Cyan && support_Purple) {
		if (Node* node = GetNodeUnsafe()) {
			uint8 data[6];
			memset(data, 0, sizeof(data));
			node->CreateValueRaw(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ColorControlIndex_6CH_Mixing, 
				"6CH Color", "", false, false, data, sizeof(data), 0);
		}
	}
}

void ColorControl::UpdateValue_RGB(uint8 const _instance, uint8 const _componentID, uint8 const _level) {
	if (ValueRaw* m_rgb = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_RGB))) {
		uint8 data[3];
		memcpy(data, m_rgb->GetValue(), sizeof(data));
		switch(_componentID) {
			case ComponentID_Red:
				data[0] = _level;
				break;
			case ComponentID_Green:
				data[1] = _level;
				break;
			case ComponentID_Blue:
				data[2] = _level;
				break;
			default:
			{
				m_rgb->Release();
				return;
			}
		}
		m_rgb->OnValueRefreshed(data, sizeof(data));
		m_rgb->Release();
	}
}

void ColorControl::UpdateValue_RGBW(uint8 const _instance, uint8 const _componentID, uint8 const _level) {
	if (ValueRaw* m_rgbw = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_RGBW))) {
		uint8 data[4];
		memcpy(data, m_rgbw->GetValue(), sizeof(data));
		switch(_componentID) {
			case ComponentID_Red:
				data[0] = _level;
				break;
			case ComponentID_Green:
				data[1] = _level;
				break;
			case ComponentID_Blue:
				data[2] = _level;
				break;
			case ComponentID_WarmWhite:
			case ComponentID_ColdWhite:
				data[3] = _level;
				break;
			default:
				{
					m_rgbw->Release();
					return;
				}
		}
		m_rgbw->OnValueRefreshed(data, sizeof(data));
		m_rgbw->Release();
	}
}

void ColorControl::UpdateValue_6CH(uint8 const _instance, uint8 const _componentID, uint8 const _level) {
	if (ValueRaw* m_6ch = static_cast<ValueRaw*>(GetValue(_instance, ColorControlIndex_6CH_Mixing))) {
		uint8 data[6];
		memcpy(data, m_6ch->GetValue(), sizeof(data));
		switch(_componentID) {
			case ComponentID_Red:
				data[0] = _level;
				break;
			case ComponentID_Green:
				data[1] = _level;
				break;
			case ComponentID_Blue:
				data[2] = _level;
				break;
			case ComponentID_Amber:
				data[3] = _level;
				break;
			case ComponentID_Cyan:
				data[4] = _level;
				break;
			case ComponentID_Purple:
				data[5] = _level;
				break;
			default:
				{
					m_6ch->Release();
					return;
				}
		}
		m_6ch->OnValueRefreshed(data, sizeof(data));
		m_6ch->Release();
	}
}
