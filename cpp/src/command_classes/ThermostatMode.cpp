//-----------------------------------------------------------------------------
//
//	ThermostatMode.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_THERMOSTAT_MODE
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ThermostatMode.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueList.h"
#include "ValueString.h"
#include "ValueRaw.h"

using namespace OpenZWave;

enum ThermostatModeCmd
{
	ThermostatModeCmd_Set				= 0x01,
	ThermostatModeCmd_Get				= 0x02,
	ThermostatModeCmd_Report			= 0x03,
	ThermostatModeCmd_SupportedGet		= 0x04,
	ThermostatModeCmd_SupportedReport	= 0x05
};

enum {
	ThermostatModeReport_LevelModeMask = 0x1f,

	//Version 3
	ThermostatModeReport_LevelNoOfManufacturerDataFieldsMask = 0xe0,
	ThermostatModeReport_LevelNoOfManufacturerDataFieldsShift = 0x05
};

enum
{
	ThermostatModeIndex_Mode = 0,
	ThermostatModeIndex_Specific,

	ThermostatModeIndex_IconName = 99
};

static char const* c_modeName[] = 
{
	"Off",
	"Heat",
	"Cool",
	"Auto",
	"Auxiliary/Emergency Heat",
	"Resume",
	"Fan Only",
	"Furnace",
	"Dry Air",
	"Moist Air",
	"Auto Changeover",
	"Energy Save Heat",
	"Energy Save Cool",
	"Away",
	"Reserved", 
	"Full Power", //0x0f
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved", //0x14
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved", //0x19
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved", //0x1e
	"Manufacture Specific"
};

//-----------------------------------------------------------------------------
// <ThermostatMode::ReadXML>
// Read the supported modes
//-----------------------------------------------------------------------------
void ThermostatMode::ReadXML
( 
	TiXmlElement const* _ccElement
)
{
	CommandClass::ReadXML( _ccElement );

	if( Node* node = GetNodeUnsafe() )
	{
		node = node;
		vector<ValueList::Item>	supportedModes;

		TiXmlElement const* supportedModesElement = _ccElement->FirstChildElement( "SupportedModes" );
		if( supportedModesElement )
		{
			TiXmlElement const* modeElement = supportedModesElement->FirstChildElement();
			while( modeElement )
			{
				char const* str = modeElement->Value();
				if( str && !strcmp( str, "Mode" ) )
				{
					int index;
					if( TIXML_SUCCESS == modeElement->QueryIntAttribute( "index", &index ) )
					{
						ValueList::Item item;
						item.m_value = index;
						item.m_label = c_modeName[index];
						supportedModes.push_back( item );					
					}
				}

				modeElement = modeElement->NextSiblingElement();
			}
		}

		if( !supportedModes.empty() )
		{
			m_supportedModes = supportedModes;
			ClearStaticRequest( StaticRequest_Values );
			CreateVars( 1 ); 
		}
	}
}

//-----------------------------------------------------------------------------
// <ThermostatMode::WriteXML>
// Save the supported modes
//-----------------------------------------------------------------------------
void ThermostatMode::WriteXML
(
	TiXmlElement* _ccElement
)
{
	if( m_supportedModes.empty() )
	{
		return;
	}

	CommandClass::WriteXML( _ccElement );

	if( Node* node = GetNodeUnsafe() )
	{
		node = node;
		TiXmlElement* supportedModesElement = new TiXmlElement( "SupportedModes" );
		_ccElement->LinkEndChild( supportedModesElement );

		for( vector<ValueList::Item>::iterator it = m_supportedModes.begin(); it != m_supportedModes.end(); ++it )
		{
			ValueList::Item const& item = *it;

			TiXmlElement* modeElement = new TiXmlElement( "Mode" );
			supportedModesElement->LinkEndChild( modeElement );

			char str[8];
			snprintf( str, 8, "%d", item.m_value );
			modeElement->SetAttribute( "index", str );
			modeElement->SetAttribute( "label", item.m_label.c_str() );
		}
	}
}

//-----------------------------------------------------------------------------
// <ThermostatMode::RequestState>
// Get the static thermostat mode details from the device
//-----------------------------------------------------------------------------
bool ThermostatMode::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		// request supported mode list
		Msg* msg = new Msg( "Request Supported Thermostat Modes", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatModeCmd_SupportedGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		requests |= SendSecureMsg(msg, _queue);
	}

	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current mode
		requests |= RequestValue( _requestFlags, ThermostatModeIndex_Mode, _instance, _queue );
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <ThermostatMode::RequestValue>
// Get the static thermostat mode details from the device
//-----------------------------------------------------------------------------
bool ThermostatMode::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _getTypeEnum == ThermostatModeIndex_Mode || _getTypeEnum == ThermostatModeIndex_Specific )		// get current mode
	{
		// Request the current mode
		Msg* msg = new Msg( "Request Current Thermostat Mode", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatModeCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatMode::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ThermostatMode::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	if( ThermostatModeCmd_Report == (ThermostatModeCmd)_data[0] )
	{
		uint8 mode = _data[1]&ThermostatModeReport_LevelModeMask;
		uint8 specific_size = 0;
		if (GetVersion() >= 3) {
			if (!node)
			specific_size = 
				(_data[1]&ThermostatModeReport_LevelNoOfManufacturerDataFieldsMask)>>ThermostatModeReport_LevelNoOfManufacturerDataFieldsShift;
			ValueRaw* m_value = static_cast<ValueRaw*>(GetValue(_instance, ThermostatModeIndex_Specific));
			if (m_value == NULL) {
				uint8 data[7];
				memset(data, 0, sizeof(data));
				node->CreateValueRaw(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatModeIndex_Specific, 
					"Specific Mode", "", false, false, data, sizeof(data), 0);
				m_value = static_cast<ValueRaw*>(GetValue(_instance, ThermostatModeIndex_Specific));
			} 
			if (specific_size > 0 && specific_size <= 7) {
				uint8 const* specific_data = &_data[2];
				m_value->OnValueRefreshed(specific_data, specific_size);
			}
			m_value->Release();
		}
		
		if( mode < m_supportedModes.size() )
		{
			// We have received the thermostat mode from the Z-Wave device
			if( ValueList* valueList = static_cast<ValueList*>( GetValue( _instance, ThermostatModeIndex_Mode ) ) )
			{
				valueList->OnValueRefreshed( mode );
				Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat mode: %s", valueList->GetItem().m_label.c_str() );
				valueList->Release();
			}
			else
			{
				Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat mode: index %d", mode );
			}
		}
		else
		{
			 Log::Write( LogLevel_Info, GetNodeId(), "Received unknown thermostat mode: index %d", mode );
		}
		return true;
	}
	
	if( ThermostatModeCmd_SupportedReport == (ThermostatModeCmd)_data[0] )
	{
		// We have received the supported thermostat modes from the Z-Wave device
		// these values are used to populate m_supportedModes which, in turn, is used to "seed" the values
		// for each m_modes instance
		Log::Write( LogLevel_Info, GetNodeId(), "Received supported thermostat modes" );		

		m_supportedModes.clear();
		for( uint32 i=1; i<_length-1; ++i )
		{
			for( int32 bit=0; bit<8; ++bit )
			{
				if( ( _data[i] & (1<<bit) ) != 0 )
				{
					ValueList::Item item;
					item.m_value = (int32)((i-1)<<3) + bit;
					
					if ((size_t)item.m_value >= sizeof(c_modeName)/sizeof(*c_modeName))
					{
						Log::Write( LogLevel_Info, GetNodeId(), "Received unknown thermostat mode: 0x%x", item.m_value);
					}
					else
					{
						item.m_label = c_modeName[item.m_value];
						m_supportedModes.push_back( item );

						Log::Write( LogLevel_Info, GetNodeId(), "    Added mode: %s", c_modeName[item.m_value] );
					}
				}
			}
		}

		ClearStaticRequest( StaticRequest_Values );
		CreateVars( _instance );
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatMode::SetValue>
// Set the device's thermostat mode
//-----------------------------------------------------------------------------
bool ThermostatMode::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	if( ValueID::ValueType_List == _value.GetID().GetType() )
	{
		ValueList const* value = static_cast<ValueList const*>(&_value);
		uint8 state = (uint8)value->GetItem().m_value;

		Msg* msg = new Msg( "Set Thermostat Mode", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatModeCmd_Set );
		msg->Append( state );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	} else if (ValueID::ValueType_Raw == _value.GetID().GetType()) {
		ValueRaw const* value = static_cast<ValueRaw const*>(&_value);
		uint8* s = value->GetValue();
		uint8 len = value->GetLength();
		uint8 state = 0;
		if (len > 7 || len == 0)
			return false;

		if (ValueList* m_list = static_cast<ValueList*>( GetValue(instance, ThermostatModeIndex_Mode))) {
			state = (uint8)m_list->GetItem().m_value;
			m_list->Release();
		}
		state |= len<<ThermostatModeReport_LevelNoOfManufacturerDataFieldsShift;

		Msg* msg = new Msg( "Set Thermostat Mode", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
		msg->Append( GetNodeId() );
		msg->Append( 3+len );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatModeCmd_Set );
		msg->Append( state );
		for (int i=0; i<len; i++) {
			msg->Append( s[i] );
		}
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	}

	if (_value.GetID().GetIndex() == ThermostatModeIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ThermostatModeIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatMode::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ThermostatMode::CreateVars
(
	uint8 const _instance
)
{
	// There are three ways to get here...each needs to be handled differently:
	//	QueryStage_ProtocolInfo:	
	//		Don't know what's supported yet, so do nothing
	//	QueryStage_NodeInfo:	
	//		Need to create the instance so the values can be read from the xml file 
	//	QueryStage_Static:
	//		Need to create the instance (processing SupportedReport) if it doesn't exist
	//		If it does, populate with the appropriate values

	if( Node* node = GetNodeUnsafe() )
	{
		Node::QueryStage qs = node->GetCurrentQueryStage();
		if( qs == Node::QueryStage_ProtocolInfo )
		{
			// this call is from QueryStage_ProtocolInfo,
			// so just return (don't know which modes are supported yet)
			return;
		}

		// identify the lowest supported mode as the "default" (or default to 0 if no supported modes identified yet)
		int32 defaultValue = 0;
		if( !m_supportedModes.empty() )
		{
			defaultValue = m_supportedModes[0].m_value;
		}

		if( qs == Node::QueryStage_Static )
		{
			// This instance might already have been created (in NodeInfo, in preparation for loading the values
			// from zwcfg xml file).  So, if the instance already exists, we delete its value and add a new one below
			if( ValueList* valueList = static_cast<ValueList*>( GetValue( _instance, 0 ) ) )
			{
				node->RemoveValueList( valueList );
				valueList->Release();
			}
		}

		node->CreateValueList( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatModeIndex_Mode, "Mode", "", false, false, 1, m_supportedModes, defaultValue, 0 );

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatModeIndex_IconName, "ThermostatMode Icon", "icon", false, false, "", 0);

		if (GetVersion() >= 3) {
			uint8 data[7];
			memset(data, 0, sizeof(data));
			node->CreateValueRaw(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatModeIndex_Specific, 
				"Specific Mode", "", false, false, data, sizeof(data), 0);
		}
	}
}
