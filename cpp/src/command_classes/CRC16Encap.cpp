//-----------------------------------------------------------------------------
//
//	CRC16Encap.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_CRC_16_ENCAP
//
//	Copyright (c) 2012 Greg Satz <satz@iranger.com>
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "CRC16Encap.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

using namespace OpenZWave;

//-----------------------------------------------------------------------------
// <Hail::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool CRC16Encap::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	if ( (CRC16EncapCmd)_data[0] == CRC16EncapCmd_Encap) {
		uint16 crc = (_data[_length-2] << 8) | (_data[_length-1]);
		uint8* payload = (uint8*)&_data[1];
		uint32 payload_len = _length - 2;
		if (CheckCRC16( payload, payload_len)) {
			if( CommandClass* pCommandClass = node->GetCommandClass( payload[0] ) ) {
				Log::Write( LogLevel_Info, GetNodeId(), 
					"Received a CRC16EncapCmd_Encap from node %d, instance %d, for Command Class %s", 
					GetNodeId(), _instance, pCommandClass->GetCommandClassName().c_str() );
				pCommandClass->ReceivedCntIncr();
				return pCommandClass->HandleMsg( &payload[1], (payload_len-2), _instance );
			} else {
				Log::Write(LogLevel_Warning, GetNodeId(), "Unsupport command class 0x%x.(CRC)", payload[0]);
			}
		} else {
			Log::Write(LogLevel_Warning, GetNodeId(), "Received wrong CRC package.");
		}
	}
	
	return false;
}

uint16 CRC16Encap::CreateCRC16(const uint8* _data, const uint32 _length) {
	if (!_data || _length == 0)
		return 0;
	uint16 crc = CRC16_INIT_VECTOR;
	uint8 headerData[] = {
		GetCommandClassId(), CRC16EncapCmd_Encap
	};
	crc = CRC16_Checking(crc, headerData, 2);
	crc = CRC16_Checking(crc, _data, _length);
	return crc;
}

bool CRC16Encap::CheckCRC16(const uint8* _data, const uint32 _length) {
	if (!_data || _length == 0)
		return false;
	uint16 crc = CRC16_INIT_VECTOR;
	uint8 headerData[] = {
		GetCommandClassId(), CRC16EncapCmd_Encap
	};
	crc = CRC16_Checking(crc, headerData, 2);
	crc = CRC16_Checking(crc, _data, _length);
	if (!crc)
		return true;
	
	return false;
}

uint16 CRC16Encap::CRC16_Checking(uint16 crc, uint8 const* _data, uint16 _length) {
	uint8 workData;
	uint8 bitMask;
	uint8 newBit;

	while(_length--) {
		workData = *_data++;
		for (bitMask=0x80; bitMask != 0; bitMask >>= 1) {
			/* Align test bit with next bit of the message byte, starting with msb. */
			newBit = ((workData & bitMask) != 0) ^ ((crc & 0x8000) != 0);
			crc <<= 1;
			if (newBit) 
				crc ^= POLY;
		}/* for (bitMask = 0x80; bitMask != 0; bitMask >>= 1) */
	}
	return crc;
}
