//-----------------------------------------------------------------------------
//
//	Alarm.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_ALARM
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "Alarm.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueByte.h"
#include "ValueList.h"
#include "ValueString.h"

#include "NodeNaming.h"
#include "UserCode.h"
#include "Notification.h"

using namespace OpenZWave;

enum AlarmCmd
{
	AlarmCmd_Get					= 0x04,
	AlarmCmd_Report					= 0x05,

	//Version 2
	AlarmCmd_Set					= 0x06,
	AlarmCmd_TypeSupportedGet		= 0x07,
	AlarmCmd_TypeSupportedReport	= 0x08,

	//Version 3
	AlarmCmd_EventSupportedGet		= 0x01,
	AlarmCmd_EventSupportedReport	= 0x02
};

enum {
	//Version 2
	AlarmTypeSupportedReportProperties1_V1AlarmBitMask = 0x80,
	AlarmTypeSupportedReportProperties1_NumberOfBitMasksMask = 0x1f,

	//Version 3
	AlarmTypeSupportedReportProperties1_EventParametersLengthMask = 0x1f,
	AlarmTypeSupportedReportProperties1_SequenceBitMask = 0x80
};

enum 
{
	AlarmMode_Arm,
	AlarmMode_Bypass
};

static char const* c_notificationType_SmokeEvent[] = 
{
	"(0x00)Previous Events cleared",
	"(0x01)Smoke detected", //Node Location Report
	"(0x02)Smoke detected, Unknown Location", 
	"(0x03)Smoke Alarm Test"
};

static char const* c_notificationType_COEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Carbon monoxide detected",							//Node Location Report
	"(0x02)Carbon monoxide detected, Unknown Location"
};

static char const* c_notificationType_CO2Event[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Carbon dioxide detected",							//Node Location Report
	"(0x02)Carbon dioxide detected, Unknown Location"
};

static char const* c_notificationType_HeatEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Overheat detected",								//Node Location Report
	"(0x02)Overheat detected, Unknown Location",
	"(0x03)Rapid Temperature Rise",							//Node Location Report
	"(0x04)Rapid Temperature Rise, Unknown Location",
	"(0x05)Underheat detected",								//Node Location Report
	"(0x06)Underheat detected, Unknown Location"
};

static char const* c_notificationType_WaterEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Water Leak detected",								//Node Location Report
	"(0x02)Water Leak detected, Unknown Location",
	"(0x03)Water Level Dropped",								//Node Location Report
	"(0x04)Water Level Dropped, Unknown Location",
	"(0x05)Replace Water Filter"
};

static char const* c_notificationType_AccessControlEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Manual Lock Operation",
	"(0x02)Manual Unlock Operation",
	"(0x03)RF Lock Operation",
	"(0x04)RF Unlock Operation",
	"(0x05)Keypad Lock Operation",							//User ID, User Code Report
	"(0x06)Keypad Unlock Operation",							//User ID, User Code Report
	"(0x07)Manual Not Fully Locked Operation",
	"(0x08)RF Not Fully Locked Operation",
	"(0x09)Auto Lock Locked Operation",
	"(0x0a)Auto Lock Not Fully Operation",
	"(0x0b)Lock Jammed",
	"(0x0c)All user codes deleted",
	"(0x0d)Single user code deleted",
	"(0x0e)New user code added",
	"(0x0f)New user code not added due to duplicate code",
	"(0x10)Keypad temporary disabled",
	"(0x11)Keypad busy",
	"(0x12)New Program code Entered - Unique code for lock configuration",
	"(0x13)Manually Enter user Access code exceeds code limit",
	"(0x14)Unlock By RF with invalid user code",
	"(0x15)Locked by RF with invalid user codes",
	"(0x16)Window/Door is open",
	"(0x17)Window/Door is closed",
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x1c
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x21
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x26
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x2b
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x30
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x35
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x3a
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",//0x3f
	
	/**
	 * 0x40
	 * (1 byte)
	 * 0xFF = Performing Process
	 * 0x00 = Process Complete
	 * 0x01 - 0xFE = Reserved
	 */
	"(0x40)Barrier performing Initialization process",
	
	"(0x41)Barrier operation (Open / Close) force has been exceeded.",

	/**
	 * (2 Bytes)
	 * Manufacturer's time limit in seconds
	 */
	"(0x42)Barrier motor has exceeded manufacturer's operational time limit",

	"(0x43)Barrier operation has exceeded physical mechanical limits.",
	"(0x44)Barrier unable to perform requested operation due to UL requirements.",
	"(0x45)Barrier Unattended operation has been disabled per UL requirements.",
	"(0x46)Barrier failed to perform Requested operation, device malfunction",

	/**
	 * (1 byte)
	 * 0xFF = Mode Enabled
	 * 0x00 = Mode Disabled
	 * 0x01 - 0xFE = Reserved
	 */
	"(0x47)Barrier Vacation Mode",

	/**
	 * (1 byte)
	 * 0xFF = Obstruction 
	 * 0x00 = No Obstruction
	 * 0x01 - 0xFE = Reserved
	 */
	"(0x48)Barrier Safety Beam Obstacle",

	/**
	 * (1 byte)
	 * 0x00 = Sensor not defined
	 * 0x01 - 0xFF = Sensor ID
	 */
	"(0x49)Barrier Sensor Not Detected / Supervisory Error",

	/**
	 * (1 byte)
	 * 0x00 = Sensor not defined
	 * 0x01 - 0xFF = Sensor ID
	 */
	"(0x4a)Barrier Sensor Low Battery Warning",
	
	"(0x4b)Barrier detected short in Wall Station wires",
	"(0x4c)Barrier associated with non-Z-wave remote control."
};

static char const* c_notificationType_HomeSecurityEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Intrusion",										//Node Location Report
	"(0x02)Intrusion, Unknown Location",
	"(0x03)Tampering, product covering removed",
	"(0x04)Tampering, Invalid Code",
	"(0x05)Glass Breakage",									//Node Location Report
	"(0x06)Glass Breakage, Unknown Location",
	"(0x07)Motion Detection",									//Node Location Report
	"(0x08)Motion Detection, Unknown Location"
};

static char const* c_notificationType_PowerManagementEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Power has been applied",
	"(0x02)AC mains disconnected",
	"(0x03)AC mains re-connected",
	"(0x04)Surge Detection",
	"(0x05)Voltage Drop/Drift",
	"(0x06)Over-current detected",
	"(0x07)Over-voltage detected",
	"(0x08)Over-load detected",
	"(0x09)Load error",
	"(0x0a)Replace battery soon",
	"(0x0b)Replace battery now",
	"(0x0c)Battery is charging",
	"(0x0d)Battery is fully charged",
	"(0x0e)Charge battery soon",
	"(0x0f)Charge battery now!"
};

static char const* c_notificationType_SystemEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)System hardware failure",
	"(0x02)System software failure",
	"(0x03)System hardware failure with OEM proprietary failure code",	//Manufacturer proprietary system failure codes.
	"(0x04)System software failure with OEM proprietary failure code"		//Manufacturer proprietary system failure codes.
};

static char const* c_notificationType_EmergencyEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Contact Police",
	"(0x02)Contact Fire Service",
	"(0x03)Contact Medical Service"
};

static char const* c_notificationType_ClockEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Wake Up Alert",
	"(0x02)Timer Ended",

	/**
	 * Event Parameter 1 = hour(s)
	 * Event Parameter 2 = minute(s)
	 * Event Parameter 3 = second(s)
	 */
	"(0x03)Time remaining"
};

static char const* c_notificationType_ApplianceEvent[] =
{
	"(0x00)Previous Events cleared", 
	"(0x01)Program started",
	"(0x02)Program in progress",
	"(0x03)Program completed",
	"(0x04)Replace main filter",
	"(0x05)Failure to set target temperature",
	"(0x06)Supplying water",
	"(0x07)Water supply failure",
	"(0x08)Boiling",
	"(0x09)Boiling failure",
	"(0x0a)Washing",
	"(0x0b)Washing failure",
	"(0x0c)Rinsing",
	"(0x0d)Rinsing failure",
	"(0x0e)Draining",
	"(0x0f)Draining failure",
	"(0x10)Spinning",
	"(0x11)Spinning failure",
	"(0x12)Drying",
	"(0x13)Drying failure",
	"(0x14)Fan failure",
	"(0x15)Compressor failure"
};

static char const* c_notificationType_HomeHealthEvent[] =
{
	"(0x00)Previous Events cleared",
	"(0x01)Leaving Bed",
	"(0x02)Sitting on bed",
	"(0x03)Lying on bed",
	"(0x04)Posture changed",
	"(0x05)Sitting on edge of bed",

	/**
	 * Event Parameter 1 (1 byte) = pollution level
	 * 0x01 = Clean
	 * 0x02 = Slightly polluted
	 * 0x03 = Moderately polluted
	 * 0x04 = Highly polluted
	 */
	"(0x06)Volatile Organic Compound level"
};

//-----------------------------------------------------------------------------
// <Alarm::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool Alarm::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if (_requestFlags & RequestFlag_Static) {
		if (GetVersion() >= 2) {
			Msg* msg = new Msg( "AlarmCmd_TypeSupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( AlarmCmd_TypeSupportedGet );
			msg->Append( GetDriver()->GetTransmitOptions() );
			res |= SendSecureMsg(msg, _queue);
		}
	}

	if( _requestFlags & RequestFlag_Session )
	{
		res |= RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return res;
}

//-----------------------------------------------------------------------------
// <Alarm::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool Alarm::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if (!IsGetSupported())
		return false;

	if (GetVersion() == 1) {
		Msg* msg = new Msg( "AlarmCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( AlarmCmd_Get );
		msg->Append( NotificationType_First );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (GetVersion() == 2) {
		for( uint8 i = 0; i < NotificationType_MaxType; i++ )
		{
			Value* value = GetValue( _instance, i );
			if( value != NULL ) {
				value->Release();
				Msg* msg = new Msg( "AlarmCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->SetInstance( this, _instance );
				msg->Append( GetNodeId() );
				msg->Append( 4 );
				msg->Append( GetCommandClassId() );
				msg->Append( AlarmCmd_Get );
				msg->Append(proprietary_alarm);
				msg->Append( i );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, _queue);
				res = true;
			}
		}
	} else {
		for( uint8 i = 0; i < NotificationType_MaxType; i++ )
		{
			Value* value = GetValue( _instance, i );
			if( value != NULL ) {
				value->Release();
				Msg* msg = new Msg( "NotificationCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->SetInstance( this, _instance );
				msg->Append( GetNodeId() );
				msg->Append( 5 );
				msg->Append( GetCommandClassId() );
				msg->Append( AlarmCmd_Get );
				msg->Append(proprietary_alarm);
				msg->Append( i );
				msg->Append(0x01);//get first event of type.
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, _queue);
				res = true;
			}
		}
	}
	return res;
}

//-----------------------------------------------------------------------------
// <Alarm::SetValue>
// Set the Alarm's state
//-----------------------------------------------------------------------------
bool Alarm::SetValue
(
 Value const& _value
 )
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case AlarmIndex_Mode:
		{
			if (ValueList const* m_value = static_cast<ValueList const*>(&_value)) {
				uint8 mode = (uint8)m_value->GetItem().m_value;
				if (ValueList* m_mode = static_cast<ValueList*>( GetValue(instance, AlarmIndex_Mode))) {
					m_mode->OnValueRefreshed(mode);
					m_mode->Release();
				}
			}
			return false;
		}
		case AlarmIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, AlarmIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Alarm::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Alarm::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;

	switch ( (AlarmCmd)_data[0] ) {
		case AlarmCmd_Report: 
		{
			if (GetVersion() == 1) {
				return HandleAlarmReport_V1(_data, _length, _instance);
			}
			uint8 _alarm_type = _data[1];
			uint8 _alarm_level = _data[2];
			uint8 _zensorSrcID = _data[3];
			uint8 _zwave_alarm_status = _data[4];
			uint8 _zwave_alarm_type = _data[5];
			uint8 _zwave_alarm_event = _data[6];
			uint8 event_parameters_length;
			bool _sequenced = false;
			uint8 _sequence_number = 0;

			ValueByte* value;
			if( (value = static_cast<ValueByte*>( GetValue( _instance, AlarmIndex_Type ) )) )
			{
				value->OnValueRefreshed( _alarm_type );
				value->Release();
			}
			if( (value = static_cast<ValueByte*>( GetValue( _instance, AlarmIndex_Level ) )) )
			{
				value->OnValueRefreshed( _alarm_level );
				value->Release();
			}

			string type_name = "";
			if (_alarm_type < (sizeof(c_notificationTypeNames)/sizeof(c_notificationTypeNames[0]))) {
				type_name = c_notificationTypeNames[_alarm_type];
			}
			if (ValueString* m_name = static_cast<ValueString*>( GetValue(_instance, AlarmIndex_TypeName))) {
				m_name->OnValueRefreshed(type_name);
				m_name->Release();
			}

			if (GetVersion() == 2) {
				event_parameters_length = _data[7];
			} else {
				event_parameters_length = (_data[7] & AlarmTypeSupportedReportProperties1_EventParametersLengthMask);
				_sequenced = ((_data[7] & AlarmTypeSupportedReportProperties1_SequenceBitMask) != 0);
				if (_sequenced && event_parameters_length > 0)
					_sequence_number = _data[8+event_parameters_length];
			}

			string msg = ExtractEventMsg(_zwave_alarm_type, _zwave_alarm_event);

			ValueString* m_value = static_cast<ValueString*>( GetValue( _instance, _zwave_alarm_type ) );
			if (m_value == NULL) {
				node->CreateValueString(  ValueID::ValueGenre_User, GetCommandClassId(), _instance, _zwave_alarm_type, c_notificationTypeNames[_zwave_alarm_type], "", true, false, "", 0  );
				m_value = static_cast<ValueString*>( GetValue( _instance, _zwave_alarm_type ) );
			}
			if (_zwave_alarm_status == 0x00) {
				msg += "(Disable).";
			} else if (_zwave_alarm_status == 0xff) {
				msg += "(Enable).";
			} else if (_zwave_alarm_status == 0xfe) {
				msg += "(No pending).";
			} else {
				msg += "(Reserved).";
			}
			if (_sequenced) {
				char temp_chr[52];
				memset(temp_chr, 0, sizeof(temp_chr));
				snprintf(temp_chr, sizeof(temp_chr), "(%d)", _sequence_number);
				msg += temp_chr;
			}
			m_value->OnValueRefreshed(msg);
			m_value->Release();

			Log::Write( LogLevel_Info, GetNodeId(), 
				"Received AlarmCmd_Report: type=%d, level=%d, zensorSrcID=%d, %s: %s, status=0x%x", 
				_alarm_type, _alarm_level, _zensorSrcID, 
				c_notificationTypeNames[_zwave_alarm_type], msg.c_str(), _zwave_alarm_status);

			Notification* notification = new Notification( Notification::Type_NodeAlarm );
			notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
			notification->SetAlarmEvent(_alarm_type, _alarm_level, _zwave_alarm_type, _zwave_alarm_event);

			if (event_parameters_length > 0) {
				const uint8* event_parameter_data = &_data[8];
				uint32 specific_param = 0;
				uint8 classID = CheckNormalEventParams(_zwave_alarm_type, _zwave_alarm_event);
				if (classID > 0) {
					if (classID == UserCode::StaticGetCommandClassId() && event_parameters_length >= 3) {
						specific_param = event_parameter_data[2];
						notification->SetAlarmExtra(specific_param);
					} 
					HandleEvent(event_parameter_data, event_parameters_length, _instance);
				} else if (_zwave_alarm_type == NotificationType_AccessControl) {
					switch (_zwave_alarm_event) {
						case 0x40: case 0x47: case 0x48: case 0x49: case 0x4a:
							specific_param = event_parameter_data[0];
							notification->SetAlarmExtra(specific_param);
							break;
						case 0x42:
							if (event_parameters_length == 2) {
								specific_param = (event_parameter_data[0]<<8 | event_parameter_data[1]);
								notification->SetAlarmExtra(specific_param);
							}
							break;
						default:
							break;
					}
				} else if (_zwave_alarm_type == NotificationType_System) {
					switch (_zwave_alarm_event) {
						case 0x03: case 0x04:
							switch (event_parameters_length) {
								case 0:
									break;
								case 1:
									specific_param = event_parameter_data[0];
									notification->SetAlarmExtra(specific_param);
									break;
								case 2:
									specific_param = ( event_parameter_data[0]<<8 | event_parameter_data[1]);
									notification->SetAlarmExtra(specific_param);
									break;
								case 3:
									specific_param = ( event_parameter_data[0]<<16 | event_parameter_data[1]<<8 | 
										event_parameter_data[2]);
									notification->SetAlarmExtra(specific_param);
									break;
								case 4:
								default:
									specific_param = ( event_parameter_data[0]<<24 | event_parameter_data[1]<<16 | 
										event_parameter_data[2]<<8 | event_parameter_data[3]);
									notification->SetAlarmExtra(specific_param);
									break;
							}
							break;
						default:
							break;
					}
				} else if (_zwave_alarm_type == NotificationType_Clock) {
					if (_zwave_alarm_event == 0x03 && event_parameters_length == 3) {
						specific_param = ( event_parameter_data[0]<<16 | event_parameter_data[1]<<8 | 
							event_parameter_data[2]);
						notification->SetAlarmExtra(specific_param);
					}
				} else if (_zwave_alarm_type == NotificationType_HomeHealth) {
					if (_zwave_alarm_event == 0x06) {
						specific_param = event_parameter_data[0];
						notification->SetAlarmExtra(specific_param);
					}
				}
			}

			GetDriver()->QueueNotification( notification );
			return true;
		}
		case AlarmCmd_TypeSupportedReport:
		{
			string msg = "";
			uint8 numBitMasks = (_data[1] & AlarmTypeSupportedReportProperties1_NumberOfBitMasksMask);
			proprietary_alarm = ((_data[1] & AlarmTypeSupportedReportProperties1_V1AlarmBitMask) != 0);
			if ((numBitMasks+3) != _length) {
				Log::Write(LogLevel_Warning, GetNodeId(), "AlarmCmd_TypeSupportedReport: Wrong data length. expected=%d, current=%d.", 
					(numBitMasks+3), _length);
				if ((numBitMasks+3) > _length)
					numBitMasks = _length - 3;
			}
			for( uint8 i = 1; i <= numBitMasks; i++ ) {
				for( uint8 j = 0; j < 8; j++ ) {
					if( _data[i+1] & ( 1 << j ) ) {
						char temp_chr[128];
						memset(temp_chr, 0, sizeof(temp_chr));
						if( msg != "" )
							msg += ", ";
						uint8 index = ( ( i - 1 ) * 8 ) + j;
						if (index < NotificationType_MaxType) {
							msg += c_notificationTypeNames[index];
							snprintf(temp_chr, sizeof(temp_chr), "%s", c_notificationTypeNames[index]);
						} else {
							snprintf(temp_chr, sizeof(temp_chr), "Proprietary Type 0x%x", index);
							msg += temp_chr;
						}
						ValueString* value = static_cast<ValueString*>( GetValue( _instance, index ) );
						if( value == NULL) {
							node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, temp_chr, "", true, false, "", 0  );
						} else
							value->Release();

						if (GetVersion() >= 3) {
							Msg* msg = new Msg( "AlarmCmd_EventSupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
							msg->SetInstance( this, _instance );
							msg->Append( GetNodeId() );
							msg->Append( 3 );
							msg->Append( GetCommandClassId() );
							msg->Append( AlarmCmd_EventSupportedGet );
							msg->Append(index);
							msg->Append( GetDriver()->GetTransmitOptions() );
							SendSecureMsg(msg, Driver::MsgQueue_Query);
						}
					}
				}
			}
			Log::Write( LogLevel_Info, GetNodeId(), "Received AlarmCmd_TypeSupportedReport from node %d: %s", GetNodeId(), msg.c_str() );
			return true;
		}
		case AlarmCmd_EventSupportedReport:
		{
			string msg = "";
			uint8 _notification_type = _data[1];
			for( uint8 i = 1; i <= ( _length - 4 ); i++ ) {
				for( uint8 j = 0; j < 8; j++ ) {
					if( _data[i+2] & ( 1 << j ) ) {
						if( msg != "" )
							msg += ", ";
						uint8 index = ( ( i - 1 ) * 8 ) + j;
						char temp_chr[128];
						memset(temp_chr, 0, sizeof(temp_chr));
						snprintf(temp_chr, sizeof(temp_chr), "0x%x", index);
						msg += temp_chr;
					}
				}
			}
			if (_notification_type >= NotificationType_MaxType) {
				Log::Write( LogLevel_Info, GetNodeId(), "Received AlarmCmd_EventSupportedReport from node %d(Proprietary Type 0x%x): %s", 
					GetNodeId(), _notification_type, msg.c_str() );
			} else
				Log::Write( LogLevel_Info, GetNodeId(), "Received AlarmCmd_EventSupportedReport from node %d(%s): %s", 
				GetNodeId(), c_notificationTypeNames[_notification_type], msg.c_str() );
			return true;
		}
		default:
			break;
	}

	return false;
}

bool Alarm::HandleAlarmReport_V1(uint8 const* _data, uint32 const _length, uint32 const _instance) {
	uint8 _alarm_type = _data[1];
	uint8 _alarm_level = _data[2];

	ValueByte* value;
	if( (value = static_cast<ValueByte*>( GetValue( _instance, AlarmIndex_Type ) )) )
	{
		value->OnValueRefreshed( _alarm_type );
		value->Release();
	}
	if( (value = static_cast<ValueByte*>( GetValue( _instance, AlarmIndex_Level ) )) )
	{
		value->OnValueRefreshed( _alarm_level );
		value->Release();
	}

	Log::Write( LogLevel_Info, GetNodeId(), "Received AlarmCmd_Report(v1): type=%d, level=%d", 
		_alarm_type, _alarm_level);
	Notification* notification = new Notification( Notification::Type_NodeAlarm );
	notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
	notification->SetAlarmEvent(_alarm_type, _alarm_level, 0, 0);
	GetDriver()->QueueNotification( notification );
	return true;
}

string Alarm::ExtractEventMsg(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event) {
	string msg = "";
	char temp_chr[128];
	memset(temp_chr, 0, sizeof(temp_chr));
	snprintf(temp_chr, sizeof(temp_chr), "Unknown Event 0x%x", _zwave_alarm_event);
	msg = temp_chr;
	switch( _zwave_alarm_type ) {
		case NotificationType_Smoke:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_SmokeEvent)/sizeof(c_notificationType_SmokeEvent[0])) )
				msg = c_notificationType_SmokeEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_CO:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_COEvent)/sizeof(c_notificationType_COEvent[0])) )
				msg = c_notificationType_COEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_CO2:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_CO2Event)/sizeof(c_notificationType_CO2Event[0])) )
				msg = c_notificationType_CO2Event[_zwave_alarm_event];
			break;
		}
		case NotificationType_Heat:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_HeatEvent)/sizeof(c_notificationType_HeatEvent[0])) )
				msg = c_notificationType_HeatEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_Water:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_WaterEvent)/sizeof(c_notificationType_WaterEvent[0])) )
				msg = c_notificationType_WaterEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_AccessControl:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_AccessControlEvent)/sizeof(c_notificationType_AccessControlEvent[0])) )
				msg = c_notificationType_AccessControlEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_Burglar:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_HomeSecurityEvent)/sizeof(c_notificationType_HomeSecurityEvent[0])) )
				msg = c_notificationType_HomeSecurityEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_PowerManagement:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_PowerManagementEvent)/sizeof(c_notificationType_PowerManagementEvent[0])) )
				msg = c_notificationType_PowerManagementEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_System:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_SystemEvent)/sizeof(c_notificationType_SystemEvent[0])) )
				msg = c_notificationType_SystemEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_Emergency:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_EmergencyEvent)/sizeof(c_notificationType_EmergencyEvent[0])) )
				msg = c_notificationType_EmergencyEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_Clock:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_ClockEvent)/sizeof(c_notificationType_ClockEvent[0])) )
				msg = c_notificationType_ClockEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_Appliance:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_ApplianceEvent)/sizeof(c_notificationType_ApplianceEvent[0])) )
				msg = c_notificationType_ApplianceEvent[_zwave_alarm_event];
			break;
		}
		case NotificationType_HomeHealth:
		{
			if ( _zwave_alarm_event < 
				(sizeof(c_notificationType_HomeHealthEvent)/sizeof(c_notificationType_HomeHealthEvent[0])) )
				msg = c_notificationType_HomeHealthEvent[_zwave_alarm_event];
			break;
		}
		default:
			break;
	}

	return msg;
}

//-----------------------------------------------------------------------------
// <Alarm::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void Alarm::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		vector<ValueList::Item> alarm_items;
		node->CreateValueByte( ValueID::ValueGenre_User, GetCommandClassId(), _instance, AlarmIndex_Type, "Alarm Type", "", true, false, 0, 0 );
		node->CreateValueByte( ValueID::ValueGenre_User, GetCommandClassId(), _instance, AlarmIndex_Level, "Alarm Level", "", true, false, 0, 0 );
		
		vector<ValueList::Item> mode_items;
		mode_items.push_back(CreateItem("Arm", AlarmMode_Arm));
		mode_items.push_back(CreateItem("Bypass", AlarmMode_Bypass));
		node->CreateValueList( ValueID::ValueGenre_User, GetCommandClassId(), _instance, AlarmIndex_Mode, 
			"Mode", "", false, false, 1, mode_items, mode_items[0].m_value, 0);

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, AlarmIndex_TypeName, "Alarm Type Name", "", true, false, "", 0);

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, AlarmIndex_IconName, "Alarm Icon", "icon", false, false, "", 0);
	}
}

ValueList::Item Alarm::CreateItem(string _label, int32 _value)
{
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

bool Alarm::HandleEvent(uint8 const* _data, uint32 const _length, uint32 const _instance) {
	if (!_data || _length == 0)
		return false;

	if( Node* node = GetNodeUnsafe() ) {
		if( CommandClass* pCommandClass = node->GetCommandClass( _data[0] ) ) {
			Log::Write( LogLevel_Info, GetNodeId(), 
				"Received a AlarmCmd_Report Event from node %d, for Command Class %s", 
				GetNodeId(), pCommandClass->GetCommandClassName().c_str() );
			return pCommandClass->HandleMsg( &_data[1], _length, _instance);
		}
	}

	return false;
}

uint8 Alarm::CheckNormalEventParams(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event) {
	switch (_zwave_alarm_type) {
		case NotificationType_Smoke:
			if (_zwave_alarm_event == 0x01)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_CO:
			if (_zwave_alarm_event == 0x01)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_CO2:
			if (_zwave_alarm_event == 0x01)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_Heat:
			if (_zwave_alarm_event == 0x01 || 
				_zwave_alarm_event == 0x03 || 
				_zwave_alarm_event == 0x05)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_Water:
			if (_zwave_alarm_event == 0x01 || 
				_zwave_alarm_event == 0x03)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_AccessControl:
			if (_zwave_alarm_event == 0x05 || 
				_zwave_alarm_event == 0x06)
				return UserCode::StaticGetCommandClassId();
			break;
		case NotificationType_Burglar:
			if (_zwave_alarm_event == 0x01 || 
				_zwave_alarm_event == 0x05 || 
				_zwave_alarm_event == 0x07)
				return NodeNaming::StaticGetCommandClassId();
			break;
		case NotificationType_PowerManagement:
		case NotificationType_System:
		case NotificationType_Emergency:
		case NotificationType_Clock:
		case NotificationType_Appliance:
		case NotificationType_HomeHealth:
		default:
			break;
	}
	return 0;
}

bool Alarm::CheckSpecificEventParams(uint8 const _zwave_alarm_type, uint8 const _zwave_alarm_event, 
									 uint8* _param_length) 
{
	if (!_param_length)
		return false;

	switch (_zwave_alarm_type) {
		case NotificationType_Smoke:
		case NotificationType_CO:
		case NotificationType_CO2:
		case NotificationType_Heat:
		case NotificationType_Water:
			break;
		case NotificationType_AccessControl:
			if (_zwave_alarm_event == 0x40 || _zwave_alarm_event == 0x47 || 
				_zwave_alarm_event == 0x48 || _zwave_alarm_event == 0x49 || 
				_zwave_alarm_event == 0x4a) {
				*_param_length = 1;
				return true;
			} else if (_zwave_alarm_event == 0x42) {
				*_param_length = 2;
				return true;
			}
			break;
		case NotificationType_Burglar:
		case NotificationType_PowerManagement:
			break;
		case NotificationType_System:
			if (_zwave_alarm_event == 0x03 || _zwave_alarm_event == 0x04) {
				return true;
			}
			break;
		case NotificationType_Emergency:
			break;
		case NotificationType_Clock:
			if (_zwave_alarm_event == 0x03) {
				*_param_length = 3;
				return true;
			}
			break;
		case NotificationType_Appliance:
			break;
		case NotificationType_HomeHealth:
			if (_zwave_alarm_event == 0x06) {
				*_param_length = 1;
				return true;
			}
			break;
		default:
			break;
	}

	*_param_length = 0;
	return false;
}

void Alarm::ReadXML( TiXmlElement const* _ccElement ) {
	CommandClass::ReadXML( _ccElement );
	char const* str = _ccElement->Attribute("proprietary_alarm");
	if (str) {
		proprietary_alarm = !strcmp( str, "true");
	}
}

void Alarm::WriteXML( TiXmlElement* _ccElement ) {
	CommandClass::WriteXML( _ccElement );
	if (proprietary_alarm) {
		_ccElement->SetAttribute( "proprietary_alarm", "true" );
	}
}
