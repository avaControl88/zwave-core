//-----------------------------------------------------------------------------
//
//	SwitchBinary.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SWITCH_BINARY
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "SwitchBinary.h"
#include "WakeUp.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"

#include "ValueBool.h"
#include "ValueString.h"
#include "Notification.h"

using namespace OpenZWave;

enum SwitchBinaryCmd
{
	SwitchBinaryCmd_Set		= 0x01,
	SwitchBinaryCmd_Get		= 0x02,
	SwitchBinaryCmd_Report	= 0x03
};

enum
{
	SwitchBinaryIndex_Switch = 0,

	SwitchBinaryIndex_State = 98,
	SwitchBinaryIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <SwitchBinary::ReadXML>
// Read configuration.
//-----------------------------------------------------------------------------
void SwitchBinary::ReadXML( TiXmlElement const* _ccElement ) {
	CommandClass::ReadXML( _ccElement );

	char const* str = _ccElement->Attribute("auto_report");
	if (str) {
		m_autoReport = !strcmp( str, "true");
	}

	char const* _delay_poll = _ccElement->Attribute("delay_poll");
	if (_delay_poll) {
		m_delayPoll = !strcmp( _delay_poll, "true");
	}

	char const* _time = _ccElement->Attribute("delay_time");
	if (_time) {
		m_delay_time = atoi(_time);
	}

	char const* _wait_autoReport = _ccElement->Attribute("wait_autoReport");
	if (_wait_autoReport) {
		wait_autoReport = !strcmp(_wait_autoReport, "true");
	}
}

//-----------------------------------------------------------------------------
// <SwitchBinary::WriteXML>
// Save changed configuration
//-----------------------------------------------------------------------------
void SwitchBinary::WriteXML( TiXmlElement* _ccElement ) {
	CommandClass::WriteXML( _ccElement );

	if (m_autoReport) {
		_ccElement->SetAttribute( "auto_report", "true" );
	}
	if (m_delayPoll) {
		_ccElement->SetAttribute( "delay_poll", "true" );
		_ccElement->SetAttribute("delay_time", m_delay_time);
	}
	if (wait_autoReport) {
		_ccElement->SetAttribute("wait_autoReport", "true");
	}
}

//-----------------------------------------------------------------------------
// <SwitchBinary::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool SwitchBinary::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		return RequestValue( _requestFlags, SwitchBinaryIndex_State, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SwitchBinary::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool SwitchBinary::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_dummy1 == SwitchBinaryIndex_Switch && m_autoReport == true)
		return true;

	if (m_delayPoll && !wait_poll) {
		Notification* notif = new Notification(Notification::Type_SchedulePoll);
		notif->SetHomeNodeIdAndInstance(GetHomeId(), GetNodeId(), _instance );
		notif->SetScheduleTime(m_delay_time);
		notif->SetValueId(GetValue(_instance, SwitchBinaryIndex_Switch)->GetID());
		GetDriver()->QueueNotification( notif );
		wait_poll = true;
		return true;
	} else if (m_delayPoll && wait_poll) {
		wait_poll = false;
	}

	Msg* msg = new Msg( "SwitchBinaryCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( SwitchBinaryCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	if (_dummy1 == SwitchBinaryIndex_Switch)
		return SendSecureMsg(msg, Driver::MsgQueue_Query);
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <SwitchBinary::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool SwitchBinary::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if (SwitchBinaryCmd_Report == (SwitchBinaryCmd)_data[0])
	{
		Log::Write( LogLevel_Info, GetNodeId(), "Received SwitchBinary report from node %d: level=%s", GetNodeId(), _data[1] ? "On" : "Off" );

		if( ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, SwitchBinaryIndex_Switch ) ) )
		{
			value->OnValueRefreshed( _data[1] != 0 );
			value->Release();
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SwitchBinary::SetValue>
// Set the state of the switch
//-----------------------------------------------------------------------------
bool SwitchBinary::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	if( ValueID::ValueType_Bool == _value.GetID().GetType() )
	{
		ValueBool const* value = static_cast<ValueBool const*>(&_value);

		Log::Write( LogLevel_Info, GetNodeId(), "SwitchBinary::Set - Setting node %d to %s", GetNodeId(), value->GetValue() ? "On" : "Off" );
		Msg* msg;
		if (!wait_autoReport)
			msg = new Msg("SwitchBinary Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
		else
			msg = new Msg("SwitchBinary Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId());
		msg->SetInstance( this, _value.GetID().GetInstance() );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( SwitchBinaryCmd_Set );
		msg->Append( value->GetValue() ? 0xff : 0x00 );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, Driver::MsgQueue_Send);
	}

	if (_value.GetID().GetIndex() == SwitchBinaryIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, SwitchBinaryIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SwitchBinary::SetValueBasic>
// Update class values based in BASIC mapping
//-----------------------------------------------------------------------------
void SwitchBinary::SetValueBasic
(
	uint8 const _instance,
	uint8 const _value
)
{
	// Send a request for new value to synchronize it with the BASIC set/report.
	// In case the device is sleeping, we set the value anyway so the BASIC set/report
	// stays in sync with it. We must be careful mapping the uint8 BASIC value
	// into a class specific value.
	// When the device wakes up, the real requested value will be retrieved.
	RequestValue( 0, 0, _instance, Driver::MsgQueue_Send );
	if( Node* node = GetNodeUnsafe() )
	{
		if( WakeUp* wakeUp = static_cast<WakeUp*>( node->GetCommandClass( WakeUp::StaticGetCommandClassId() ) ) )
		{
			if( !wakeUp->IsAwake() )
			{
				if( ValueBool* value = static_cast<ValueBool*>( GetValue( _instance, SwitchBinaryIndex_Switch ) ) )
				{
					value->OnValueRefreshed( _value != 0 );
					value->Release();
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
// <SwitchBinary::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void SwitchBinary::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueBool( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SwitchBinaryIndex_Switch, "Switch", "", false, false, false, 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SwitchBinaryIndex_IconName, "SwitchBinary Icon", "icon", false, false, "", 0);
	}
}
