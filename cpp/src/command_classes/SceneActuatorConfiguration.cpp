//-----------------------------------------------------------------------------
//
//	SceneActuatorConfiguration.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SCENE_ACTUATOR_CONF
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "SceneActuatorConfiguration.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueByte.h"
#include "ValueString.h"

using namespace OpenZWave;

enum SceneActuatorConfigurationCmd
{
	SceneActuatorConfigurationCmd_Set		= 0x01,
	SceneActuatorConfigurationCmd_Get		= 0x02,
	SceneActuatorConfigurationCmd_Report	= 0x03
};

 

enum
{
	SceneActuatorConfigurationIndex_ConfigSet,
	SceneActuatorConfigurationIndex_ConfigGet,
	SceneActuatorConfigurationIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <SceneActuatorConfiguration::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool SceneActuatorConfiguration::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if (_requestFlags & RequestFlag_Session) {
		requests = RequestValue( 1, SceneActuatorConfigurationIndex_ConfigSet, _instance, _queue );
	}
	return requests;
}

//-----------------------------------------------------------------------------
// <SceneActuatorConfiguration::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool SceneActuatorConfiguration::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_getTypeEnum == SceneActuatorConfigurationIndex_ConfigSet) {
		Msg* msg = new Msg( "SceneActuatorConfigurationCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( SceneActuatorConfigurationCmd_Get );
		msg->Append((uint8)_requestFlags);
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <SceneActuatorConfiguration::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool SceneActuatorConfiguration::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (SceneActuatorConfigurationCmd)_data[0] )
	{
		case SceneActuatorConfigurationCmd_Report:
		{
			uint8 sceneID = _data[1];
			uint8 level = _data[2];
			uint8 duration = _data[3];
			Log::Write(LogLevel_Info,  GetNodeId(), 
				"Received SceneActuatorConfigurationCmd_Report. Scene ID=%d, Level=%d, Dimming Duration=%d.", 
				sceneID, level, duration);
			if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SceneActuatorConfigurationIndex_ConfigSet))) {
				char temp_chr[32];
				memset(temp_chr, 0, sizeof(temp_chr));
				snprintf(temp_chr, sizeof(temp_chr), "%d-%d-%d-0", sceneID, duration, level);
				m_value->OnValueRefreshed(string(temp_chr));
				m_value->Release();
			}
			break;
		}
		default:
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// <SceneActuatorConfiguration::SetValue>
// Set the SceneActuatorConfiguration's state
//-----------------------------------------------------------------------------
bool SceneActuatorConfiguration::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case SceneActuatorConfigurationIndex_ConfigSet:
		{
			ValueString const* m_value = static_cast<ValueString const*>(&_value);
			string tmp_str = m_value->GetValue();
			//(Scene ID)-(Dimming Duration)-(Level)-(Override)
			int split_p = tmp_str.find_first_of("-", 0);
			if (split_p > 0) {
				string sceneID_str = tmp_str.substr(0, split_p);
				tmp_str.erase(0, sceneID_str.size()+1);

				string duration_str, level_str, override_str;
				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					duration_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, duration_str.size()+1);
				} else
					return false;

				split_p = tmp_str.find_first_of("-", 0);
				if (split_p > 0) {
					level_str = tmp_str.substr(0, split_p);
					tmp_str.erase(0, level_str.size()+1);
				} else
					return false;
				override_str = tmp_str;

				uint8 sceneID = atoi(sceneID_str.c_str());
				uint8 duration = atoi(duration_str.c_str());
				uint8 level = atoi(level_str.c_str());
				bool need_override = (atoi(override_str.c_str()) != 0);
				if (sceneID > 0) {
					Msg* msg = new Msg( "SceneActuatorConfigurationCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 6 );
					msg->Append( GetCommandClassId() );
					msg->Append( SceneActuatorConfigurationCmd_Set );
					msg->Append(sceneID);
					msg->Append(duration);
					msg->Append( (need_override ? 0x80 : 0x00) );
					msg->Append(level);
					msg->Append( GetDriver()->GetTransmitOptions() );
					SendSecureMsg(msg, Driver::MsgQueue_Send);

					return RequestValue( sceneID, SceneActuatorConfigurationIndex_ConfigSet, instance, Driver::MsgQueue_Send );
				}
			}
		}
		case SceneActuatorConfigurationIndex_ConfigGet:
		{
			ValueByte const* m_value = static_cast<ValueByte const*>(&_value);
			uint8 sceneID = m_value->GetValue();
			if (sceneID > 0) {
				return RequestValue( sceneID, SceneActuatorConfigurationIndex_ConfigSet, instance, Driver::MsgQueue_Send );
			}
		}
		case SceneActuatorConfigurationIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, SceneActuatorConfigurationIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SceneActuatorConfiguration::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void SceneActuatorConfiguration::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SceneActuatorConfigurationIndex_ConfigSet, 
			"Scene Actuator manage", "", false, true, "", 0);
		node->CreateValueByte( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SceneActuatorConfigurationIndex_ConfigGet, 
			"Scene Actuator info", "", false, true, 1, 0);
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SceneActuatorConfigurationIndex_IconName, 
			"SceneActuatorConfiguration Icon", "icon", false, false, "", 0);

		if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, SceneActuatorConfigurationIndex_ConfigSet))) {
			string info = "format (Scene ID)-(Dimming Duration)-(Level)-(Override). "
				"Duration: 0(Instantly), 1~127(1~127 seconds), 128~254(1~127 minutes), 255(default). "
				"Override: 1(yes), 0(default level). Level: (byte)";
			m_value->SetHelp(info);
			m_value->Release();
		}
	}
}

