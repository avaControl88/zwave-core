//-----------------------------------------------------------------------------
//
//	ThermostatSetBack.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_THERMOSTAT_SETBACK
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ThermostatSetBack.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"
#include "ValueByte.h"
#include "ValueList.h"

using namespace OpenZWave;

enum ThermostatSetBackCmd
{
	ThermostatSetBackCmd_Set		= 0x01,
	ThermostatSetBackCmd_Get		= 0x02,
	ThermostatSetBackCmd_Report		= 0x03
};

enum
{
	ThermostatSetBackIndex_Type = 0,
	ThermostatSetBackIndex_State,
	ThermostatSetBackIndex_IconName = 99
};

enum
{
	ThermostatSetBackReport_Properties1_SetBackTypeMask = 0x03
};

static char const* c_setbackTypeLabel[] = 
{
	"No override",
	"Temporary override",
	"Permanent override",
	"Reserved"
};

//-----------------------------------------------------------------------------
// <ThermostatSetBack::RequestState>
// Get the static thermostat setback details from the device
//-----------------------------------------------------------------------------
bool ThermostatSetBack::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current state
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetBack::RequestValue>
// Get the thermostat set back details from the device
//-----------------------------------------------------------------------------
bool ThermostatSetBack::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	// Request the current state
	Msg* msg = new Msg( "ThermostatSetBackCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( ThermostatSetBackCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <ThermostatSetBack::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ThermostatSetBack::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ThermostatSetBackCmd_Report == (ThermostatSetBackCmd)_data[0] )
	{
		uint8 _type = (_data[1] & ThermostatSetBackReport_Properties1_SetBackTypeMask);
		uint8 _state = _data[2];
		if (ValueList* valueList = static_cast<ValueList*>(GetValue(_instance, ThermostatSetBackIndex_Type))) {
			valueList->OnValueRefreshed(_type);
			valueList->Release();
		}
		if (ValueByte* valueByte = static_cast<ValueByte*>(GetValue(_instance, ThermostatSetBackIndex_State))) {
			valueByte->OnValueRefreshed(_state);
			valueByte->Release();
		}
		Log::Write( LogLevel_Info, GetNodeId(), "Received ThermostatSetBackCmd_Report: type=%s state=0x%.2x", c_setbackTypeLabel[_type], _state);		
		return true;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetBack::SetValue>
// Set the device's thermostat fan state
//-----------------------------------------------------------------------------
bool ThermostatSetBack::SetValue (Value const& _value) 
{
	uint8 instance = _value.GetID().GetInstance();
	switch((ThermostatSetBackCmd)_value.GetID().GetIndex()) {
		case ThermostatSetBackIndex_Type:
		{
			if (ValueList const* m_value = static_cast<ValueList const*>(&_value)) {
				if (ValueByte* _value = static_cast<ValueByte*>(GetValue(instance, ThermostatSetBackIndex_State))) {
					uint8 state = _value->GetValue();
					_value->Release();
					Msg* msg = new Msg( "ThermostatSetBackCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 4 );
					msg->Append( GetCommandClassId() );
					msg->Append( ThermostatSetBackCmd_Set );
					msg->Append(m_value->GetItem().m_value);
					msg->Append(state);
					msg->Append( GetDriver()->GetTransmitOptions() );
					return SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			}
			break;
		}
		case ThermostatSetBackIndex_State:
		{
			if (ValueByte const* m_value = static_cast<ValueByte const*>(&_value)) {
				if (ValueList* _value = static_cast<ValueList*>(GetValue(instance, ThermostatSetBackIndex_Type))) {
					uint8 type = _value->GetItem().m_value;
					_value->Release();
					Msg* msg = new Msg( "ThermostatSetBackCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
					msg->SetInstance( this, instance );
					msg->Append( GetNodeId() );
					msg->Append( 4 );
					msg->Append( GetCommandClassId() );
					msg->Append( ThermostatSetBackCmd_Set );
					msg->Append(type);
					msg->Append(m_value->GetValue());
					msg->Append( GetDriver()->GetTransmitOptions() );
					return SendSecureMsg(msg, Driver::MsgQueue_Send);
				}
			}
			break;
		}
		case ThermostatSetBackIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ThermostatSetBackIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatSetBack::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ThermostatSetBack::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		vector<ValueList::Item> type_items;
		for (uint8 i=0; i<(sizeof(c_setbackTypeLabel)/sizeof(c_setbackTypeLabel[0])); i++)
			type_items.push_back(CreateItem(c_setbackTypeLabel[i], i));

		node->CreateValueList(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatSetBackIndex_Type, "Type", "", false, false, 1, type_items, type_items[0].m_value, 0);
		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatSetBackIndex_State, "State", "", false, false, 0, 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatSetBackIndex_IconName, 
			"ThermostatSetBack Icon", "icon", false, false, "", 0);
	}
}

ValueList::Item ThermostatSetBack::CreateItem(string _label, int32 _value) {
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}
