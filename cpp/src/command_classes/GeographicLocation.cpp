//-----------------------------------------------------------------------------
//
//	GeographicLocation.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_GEOGRAPHICLOCATION
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "GeographicLocation.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum GeographicLocationCmd
{
	GeographicLocationCmd_Set		= 0x01,
	GeographicLocationCmd_Get		= 0x02,
	GeographicLocationCmd_Report	= 0x03
};

enum
{
	GeographicLocationIndex_Longitude = 0,
	GeographicLocationIndex_Latitude,
	GeographicLocationIndex_IconName = 99
};

enum
{
	GeographicLocationReportLevel_LongitudeMinutesMask	= 0x7f,
	GeographicLocationReportLevel_LongSignBitMask		= 0x80,
	GeographicLocationReportLevel_LatitideMinutesMask	= 0x7f,
	GeographicLocationReportLevel_LatSignBitMask		= 0x80
};

//-----------------------------------------------------------------------------
// <GeographicLocation::RequestState>
// Get the static geographic location details from the device
//-----------------------------------------------------------------------------
bool GeographicLocation::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current state
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}
	return false;
}

//-----------------------------------------------------------------------------
// <GeographicLocation::RequestValue>
// Get the geographic location details from the device
//-----------------------------------------------------------------------------
bool GeographicLocation::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	// Request the current state
	Msg* msg = new Msg( "GeographicLocationCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( GeographicLocationCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <GeographicLocation::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool GeographicLocation::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( GeographicLocationCmd_Report == (GeographicLocationCmd)_data[0] )
	{
		uint8 _longitude = _data[1];
		uint8 _latitude = _data[3];
		bool longitude_sign = ((_data[2] & GeographicLocationReportLevel_LongSignBitMask) != 0);
		bool latitude_sign = ((_data[4] & GeographicLocationReportLevel_LatSignBitMask) != 0);
		uint8 _longitude_minute = (_data[2] & GeographicLocationReportLevel_LongitudeMinutesMask);
		uint8 _latitude_minute = (_data[4] & GeographicLocationReportLevel_LatitideMinutesMask);
		char longitude_chr[32];
		char latitude_chr[32];
		memset(longitude_chr, 0, sizeof(longitude_chr));
		memset(latitude_chr, 0, sizeof(latitude_chr));
		snprintf(longitude_chr, sizeof(longitude_chr), "%s%d'%d", (longitude_sign ? "-":""), _longitude, _longitude_minute);
		snprintf(latitude_chr, sizeof(latitude_chr), "%s%d'%d", (latitude_sign ? "-":""), _latitude, _latitude_minute);
		Log::Write( LogLevel_Info, GetNodeId(), "Received GeographicLocationCmd_Report: Location=(%s, %s).", 
			longitude_chr, latitude_chr);
		if (ValueString* m_long = static_cast<ValueString*>(GetValue(_instance, GeographicLocationIndex_Longitude))) {
			m_long->OnValueRefreshed(longitude_chr);
			m_long->Release();
		}
		if (ValueString* m_lat = static_cast<ValueString*>(GetValue(_instance, GeographicLocationIndex_Latitude))) {
			m_lat->OnValueRefreshed(latitude_chr);
			m_lat->Release();
		}
		return true;
	}
		
	return false;
}

//-----------------------------------------------------------------------------
// <GeographicLocation::SetValue>
// Set the device's state
//-----------------------------------------------------------------------------
bool GeographicLocation::SetValue (Value const& _value) 
{
	uint8 instance = _value.GetID().GetInstance();
	switch((GeographicLocationCmd)_value.GetID().GetIndex()) {
		case GeographicLocationIndex_Longitude:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				int16 long_degree = 0;
				int16 lat_degree = 0;
				uint8 long_sub = 0;
				uint8 lat_sub = 0;
				if (!ExtractLocation(m_value->GetValue(), &long_degree, &long_sub))
					return false;
				if(long_degree < -180 || long_degree > 180 || long_sub >= 60)
					return false;
				if (ValueString* m_value2 = static_cast<ValueString*>(GetValue(instance, GeographicLocationIndex_Latitude))) {
					ExtractLocation(m_value2->GetValue(), &lat_degree, &lat_sub);
					m_value2->Release();
				}
				Msg* msg = new Msg( "GeographicLocationCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->SetInstance( this, instance );
				msg->Append( GetNodeId() );
				msg->Append( 6 );
				msg->Append( GetCommandClassId() );
				msg->Append( GeographicLocationCmd_Set );
				if (long_degree < 0) {
					msg->Append(-long_degree);
					msg->Append(long_sub | GeographicLocationReportLevel_LongSignBitMask);
				} else {
					msg->Append(long_degree);
					msg->Append(long_sub);
				}
				if (lat_degree < 0) {
					msg->Append(-lat_degree);
					msg->Append(lat_sub | GeographicLocationReportLevel_LatSignBitMask);
				} else {
					msg->Append(lat_degree);
					msg->Append(lat_sub);
				}
				msg->Append( GetDriver()->GetTransmitOptions() );
				return SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
			break;
		}
		case GeographicLocationIndex_Latitude:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				int16 long_degree = 0;
				int16 lat_degree = 0;
				uint8 long_sub = 0;
				uint8 lat_sub = 0;
				if (!ExtractLocation(m_value->GetValue(), &lat_degree, &lat_sub))
					return false;
				if (lat_degree < -90 || lat_degree > 90 || lat_sub >= 60)
					return false;
				if (ValueString* m_value2 = static_cast<ValueString*>(GetValue(instance, GeographicLocationIndex_Longitude))) {
					ExtractLocation(m_value2->GetValue(), &long_degree, &long_sub);
					m_value2->Release();
				}
				Msg* msg = new Msg( "GeographicLocationCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->SetInstance( this, instance );
				msg->Append( GetNodeId() );
				msg->Append( 6 );
				msg->Append( GetCommandClassId() );
				msg->Append( GeographicLocationCmd_Set );
				if (long_degree < 0) {
					msg->Append(-long_degree);
					msg->Append(long_sub | GeographicLocationReportLevel_LongSignBitMask);
				} else {
					msg->Append(long_degree);
					msg->Append(long_sub);
				}
				if (lat_degree < 0) {
					msg->Append(-lat_degree);
					msg->Append(lat_sub | GeographicLocationReportLevel_LatSignBitMask);
				} else {
					msg->Append(lat_degree);
					msg->Append(lat_sub);
				}
				msg->Append( GetDriver()->GetTransmitOptions() );
				return SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
			break;
		}
		case GeographicLocationIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, GeographicLocationIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <GeographicLocation::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void GeographicLocation::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueString(ValueID::ValueGenre_System, GetCommandClassId(), _instance, GeographicLocationIndex_Longitude, 
			"Longitude", "degree", false, false, "0'0", 0);
		node->CreateValueString(ValueID::ValueGenre_System, GetCommandClassId(), _instance, GeographicLocationIndex_Latitude, 
			"Latitude", "degree", false, false, "0'0", 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, GeographicLocationIndex_IconName, 
			"GeographicLocation Icon", "icon", false, false, "", 0);
	}
}

bool GeographicLocation::ExtractLocation(string location, int16* main_degree, uint8* sub_degree) {
	if (location.size() == 0 || !main_degree || !sub_degree)
		return false;
	int split_p = location.find_first_of("'", 0);
	if (split_p > 0) {
		string main_degree_str = location.substr(0, split_p);
		location.erase(0, main_degree_str.size()+1);
		string sub_degree_str = location;
		*main_degree = (int16)atoi(main_degree_str.c_str());
		*sub_degree = (uint8)atoi(sub_degree_str.c_str());
		return true;
	}
	return false;
}
