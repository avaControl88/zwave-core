//-----------------------------------------------------------------------------
//
//	ThermostatFanMode.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_THERMOSTAT_FAN_MODE
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include <string>
#include "CommandClasses.h"
#include "ThermostatFanMode.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueList.h"
#include "ValueString.h"
#include "ValueBool.h"

using namespace OpenZWave;

enum ThermostatFanModeCmd
{
	ThermostatFanModeCmd_Set				= 0x01,
	ThermostatFanModeCmd_Get				= 0x02,
	ThermostatFanModeCmd_Report				= 0x03,

	//Version 4
	ThermostatFanModeCmd_SupportedGet			= 0x04,
	ThermostatFanModeCmd_SupportedReport			= 0x05
};

enum
{
	ThermostatFanModeIndex_Mode = 0,
	ThermostatFanModeIndex_OnOff,

	ThermostatFanModeIndex_IconName = 99
};

enum
{
	ThermostatFanMode_ReportProperties1_FanModeMask = 0x0f,
	ThermostatFanMode_SetProperties1_OffBitMask = 0x80
};

static string const c_modeName[] = 
{
	"Auto Low",
	"On Low",
	"Auto High",
	"On High",
	"Auto Medium",
	"On Medium",
	"Circulation",
	"Humidity",
	"Left Right",
	"Up Down",
	"Quiet"
};

//-----------------------------------------------------------------------------
// <ThermostatFanMode::ReadXML>
// Read the supported modes
//-----------------------------------------------------------------------------
void ThermostatFanMode::ReadXML
( 
	TiXmlElement const* _ccElement
)
{
	CommandClass::ReadXML( _ccElement );

	if( Node* node = GetNodeUnsafe() )
	{
		node = node;
		vector<ValueList::Item>	supportedModes;

		TiXmlElement const* supportedModesElement = _ccElement->FirstChildElement( "SupportedModes" );
		if( supportedModesElement )
		{
			TiXmlElement const* modeElement = supportedModesElement->FirstChildElement();
			while( modeElement )
			{
				char const* str = modeElement->Value();
				if( str && !strcmp( str, "Mode" ) )
				{
					int index;
					if( TIXML_SUCCESS == modeElement->QueryIntAttribute( "index", &index ) )
					{
						ValueList::Item item;
						item.m_value = index;
						item.m_label = c_modeName[index];
						supportedModes.push_back( item );					
					}
				}

				modeElement = modeElement->NextSiblingElement();
			}
		}

		if( !supportedModes.empty() )
		{
			m_supportedModes = supportedModes;
			ClearStaticRequest( StaticRequest_Values );
			CreateVars( 1 ); 
		}
	}
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::WriteXML>
// Save the supported modes
//-----------------------------------------------------------------------------
void ThermostatFanMode::WriteXML
( 
	TiXmlElement* _ccElement
)
{
	CommandClass::WriteXML( _ccElement );

	if( Node* node = GetNodeUnsafe() )
	{
		node = node;
		TiXmlElement* supportedModesElement = new TiXmlElement( "SupportedModes" );
		_ccElement->LinkEndChild( supportedModesElement );

		for( vector<ValueList::Item>::iterator it = m_supportedModes.begin(); it != m_supportedModes.end(); ++it )
		{
			ValueList::Item const& item = *it;

			TiXmlElement* modeElement = new TiXmlElement( "Mode" );
			supportedModesElement->LinkEndChild( modeElement );

			char str[8];
			snprintf( str, 8, "%d", item.m_value );
			modeElement->SetAttribute( "index", str );
			modeElement->SetAttribute( "label", item.m_label.c_str() );
		}
	}
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::RequestState>
// Get the static thermostat fan mode details from the device
//-----------------------------------------------------------------------------
bool ThermostatFanMode::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		// Request the supported modes
		Msg* msg = new Msg( "Request Supported Thermostat Fan Modes", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatFanModeCmd_SupportedGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		requests |= SendSecureMsg(msg, _queue);
	}

	if( _requestFlags & RequestFlag_Dynamic )
	{
		// Request the current fan mode
		requests |= RequestValue( _requestFlags, ThermostatFanModeIndex_Mode, _instance, _queue );
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::RequestValue>
// Get the thermostat fan mode details from the device
//-----------------------------------------------------------------------------
bool ThermostatFanMode::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _getTypeEnum == ThermostatFanModeIndex_Mode || _getTypeEnum == ThermostatFanModeIndex_OnOff )
	{
		// Request the current fan mode
		Msg* msg = new Msg( "Request Current Thermostat Fan Mode", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatFanModeCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ThermostatFanMode::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ThermostatFanModeCmd_Report == (ThermostatFanModeCmd)_data[0] )
	{
		uint8 mode = (_data[1] & ThermostatFanMode_ReportProperties1_FanModeMask);
		bool off = ((_data[1] & ThermostatFanMode_SetProperties1_OffBitMask) != 0);
		
		if( mode < m_supportedModes.size() ) {
			// We have received the thermostat mode from the Z-Wave device
			if( ValueList* valueList = static_cast<ValueList*>( GetValue( _instance, ThermostatFanModeIndex_Mode ) ) )
			{
				valueList->OnValueRefreshed( mode );
				Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat fan mode: %s", valueList->GetItem().m_label.c_str() );		
				valueList->Release();
			} else {
				Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat fan mode: index %d", mode );
			}
		} else {
			Log::Write( LogLevel_Info, GetNodeId(), "Received unknown thermostat fan mode: %d", mode );		
		}
		if (ValueBool* m_value = static_cast<ValueBool*>(GetValue(_instance, ThermostatFanModeIndex_OnOff))) {
			m_value->OnValueRefreshed(!off);
			m_value->Release();
		}
		return true;
	}
	
	if( ThermostatFanModeCmd_SupportedReport == (ThermostatFanModeCmd)_data[0] )
	{
		// We have received the supported thermostat fan modes from the Z-Wave device
		Log::Write( LogLevel_Info, GetNodeId(), "Received supported thermostat fan modes" );		

		m_supportedModes.clear();
		for( uint32 i=1; i<_length-1; ++i )
		{
			for( int32 bit=0; bit<8; ++bit )
			{
				if( ( _data[i] & (1<<bit) ) != 0 )
				{
					ValueList::Item item;
					item.m_value = (i-1)*8 + bit;
					
					if ((size_t)item.m_value >= sizeof(c_modeName)/sizeof(*c_modeName))
					{
						Log::Write( LogLevel_Info, GetNodeId(), "Received unknown fan mode: 0x%x", item.m_value);
					} else {
						item.m_label = c_modeName[item.m_value];
						m_supportedModes.push_back( item );

						Log::Write( LogLevel_Info, GetNodeId(), "    Added fan mode: %s", c_modeName[item.m_value].c_str() );
					}
				}
			}
		}

		ClearStaticRequest( StaticRequest_Values );
		CreateVars( _instance );
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::SetValue>
// Set the device's thermostat fan mode
//-----------------------------------------------------------------------------
bool ThermostatFanMode::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch (_value.GetID().GetIndex()) {
		case ThermostatFanModeIndex_Mode:
		{
			ValueList const* value = static_cast<ValueList const*>(&_value);
			uint8 state = (uint8)value->GetItem().m_value;

			if( ValueBool* m_value = static_cast<ValueBool*>( GetValue( instance, ThermostatFanModeIndex_OnOff ) ) ) {
				if (m_value->GetValue())
					state |= ThermostatFanMode_SetProperties1_OffBitMask;
				m_value->Release();
			}

			Msg* msg = new Msg( "Set Thermostat Fan Mode", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
			msg->SetInstance( this, _value.GetID().GetInstance() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( ThermostatFanModeCmd_Set );
			msg->Append( state );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
		case ThermostatFanModeIndex_OnOff:
		{
			ValueBool const* value = static_cast<ValueBool const*>(&_value);
			bool _off = (value->GetValue() == 0);
			if( ValueList* valueList = static_cast<ValueList*>( GetValue( instance, ThermostatFanModeIndex_Mode ) ) )
			{
				uint8 state = (uint8)valueList->GetItem().m_value;
				valueList->Release();

				if (_off)
					state |= ThermostatFanMode_SetProperties1_OffBitMask;

				Msg* msg = new Msg( "Set Thermostat Fan Mode On/Off", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
				msg->SetInstance( this, _value.GetID().GetInstance() );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( ThermostatFanModeCmd_Set );
				msg->Append( state );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, Driver::MsgQueue_Send);

				if( ValueBool* m_value = static_cast<ValueBool*>( GetValue( instance, ThermostatFanModeIndex_OnOff ) ) ) {
					m_value->OnValueRefreshed(_off);
					m_value->Release();
				}
				return true;
			}
			break;
		}
		case ThermostatFanModeIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ThermostatFanModeIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatFanMode::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ThermostatFanMode::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() ) {
		switch (GetVersion()) {
			default:
			case 2:
				if (!m_supportedModes.empty()) {
					node->CreateValueBool(ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatFanModeIndex_OnOff, 
						"Fan Switch", "", false, false, true, 0);
				}
			case 1:
				if (!m_supportedModes.empty()) {
					node->CreateValueList( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatFanModeIndex_Mode, 
						"Fan Mode", "", false, false, 1, m_supportedModes, m_supportedModes[0].m_value, 0 );
				}
				node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatFanModeIndex_IconName, 
					"ThermostatFanMode Icon", "icon", false, false, "", 0);
				break;
		}
	}
}

