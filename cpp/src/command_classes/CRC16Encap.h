//-----------------------------------------------------------------------------
//
//	CRC16Encap.h
//
//	Implementation of the Z-Wave COMMAND_CLASS_CRC_16_ENCAP
//
//	Copyright (c) 2012 Greg Satz <satz@iranger.com>
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#ifndef _CRC16_ENCAP_H
#define _CRC16_ENCAP_H

#include "CommandClass.h"

#define CRC16_INIT_VECTOR 0x1d0f
#define POLY 0x1021 /* crc-ccitt mask */

namespace OpenZWave
{
	/** \brief Implements COMMAND_CLASS_CRC_16_ENCAP (0x56), a Z-Wave device command class. Must not be encapsulated
	 */
	class CRC16Encap: public CommandClass
	{
	public:
		enum CRC16EncapCmd
		{
			CRC16EncapCmd_Encap = 0x01
		};

		static CommandClass* Create( uint32 const _homeId, uint8 const _nodeId ){ return new CRC16Encap( _homeId, _nodeId ); }
		virtual ~CRC16Encap(){}

		static uint8 const StaticGetCommandClassId(){ return 0x56; }
		static string const StaticGetCommandClassName(){ return "COMMAND_CLASS_CRC_16_ENCAP"; }

		// From CommandClass
		virtual uint8 const GetCommandClassId()const{ return StaticGetCommandClassId(); }
		virtual string const GetCommandClassName()const{ return StaticGetCommandClassName(); }
		virtual bool HandleMsg( uint8 const* _data, uint32 const _length, uint32 const _instance = 1 );

		uint16 CreateCRC16(const uint8* _data, const uint32 _length);//data without crc16
		bool CheckCRC16(const uint8* _data, const uint32 _length);//data with crc16

	private:
		CRC16Encap( uint32 const _homeId, uint8 const _nodeId ): CommandClass( _homeId, _nodeId ){}

		uint16 CRC16_Checking(uint16 crc, uint8 const* _data, uint16 _length);
	};

} // namespace OpenZWave

#endif
