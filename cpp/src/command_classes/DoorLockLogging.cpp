//-----------------------------------------------------------------------------
//
//	DoorLockLogging.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_DOOR_LOCK_LOGGING
//
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "DoorLockLogging.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueByte.h"
#include "ValueString.h"
#include "ValueList.h"

using namespace OpenZWave;

enum DoorLockLoggingCmd
{
	DoorLockLoggingCmd_Version					= 0x01,
	DoorLockLoggingCmd_RecordsSupportedGet		= 0x01,
	DoorLockLoggingCmd_RecordsSupportedReport	= 0x02,
	DoorLockLoggingCmd_RecordGet				= 0x03,
	DoorLockLoggingCmd_RecordReport				= 0x04
};

 enum DoorLockLoggingMode
 {
	 DoorLockLoggingMode_Properties1_HourLocalTimeMask	= 0x1f,
	 DoorLockLoggingMode_Properties1_RecordStatusMask	= 0xe0,
	 DoorLockLoggingMode_Properties1_RecordStatusShift	= 0x05
 };

 //protect by webserver
 enum
 {
	 DoorLockLoggingIndex_MaxRecordStored = 0,
	 DoorLockLoggingIndex_RecordNumber,
	 DoorLockLoggingIndex_Time,
	 DoorLockLoggingIndex_EventType,
	 DoorLockLoggingIndex_UserId,
	 DoorLockLoggingIndex_UserCode,

	 DoorLockLoggingIndex_LogPage,
	 DoorLockLoggingIndex_LogDetail,
	 DoorLockLoggingIndex_IconName = 99
 };

//-----------------------------------------------------------------------------
// <DoorLockLogging::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool DoorLockLogging::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool requests = false;
	if (_requestFlags & RequestFlag_Static) {
		requests = RequestValue( _requestFlags, DoorLockLoggingCmd_RecordsSupportedGet, _instance, _queue );
	} else if ( _requestFlags & RequestFlag_Session) {
		requests = RequestValue( _requestFlags, DoorLockLoggingCmd_RecordGet, _instance, _queue );
	}

	return requests;
}

//-----------------------------------------------------------------------------
// <DoorLockLogging::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool DoorLockLogging::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _getTypeEnum,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_getTypeEnum == DoorLockLoggingCmd_RecordsSupportedGet) {
		Msg* msg = new Msg( "DoorLockLoggingCmd_RecordsSupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( _getTypeEnum );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (_getTypeEnum == DoorLockLoggingCmd_RecordGet) {
		Msg* msg = new Msg( "DoorLockLoggingCmd_RecordGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( _getTypeEnum );
		msg->Append( record_number );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <DoorLockLogging::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool DoorLockLogging::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (DoorLockLoggingCmd)_data[0] )
	{
		case DoorLockLoggingCmd_RecordsSupportedReport:
		{
			uint8 max_records = _data[1];
			Log::Write(LogLevel_Info,  GetNodeId(), 
				"Received DoorLockLoggingCmd_RecordsSupportedReport. Max records stored=%d", max_records);
			if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, DoorLockLoggingIndex_MaxRecordStored))) {
				if (log_perPage == 0) 
					log_perPage = 10;
				max_logPage = (uint8)(max_records/log_perPage);
				m_value->OnValueRefreshed(max_records);
				m_value->Release();
			}
			break;
		}
		case DoorLockLoggingCmd_RecordReport:
		{
			uint8 _record_number = _data[1];
			Log::Write(LogLevel_Info,  GetNodeId(), "Received DoorLockLoggingCmd_RecordReport.");
			if ( (_data[6] & DoorLockLoggingMode_Properties1_RecordStatusMask) )
			{
				Log::Write(LogLevel_Info,  GetNodeId(), "Record number=%d, %d%d/%d/%d %d:%d:%d", _data[1], 
					_data[2], _data[3], _data[4], _data[5], 
					(_data[6] & DoorLockLoggingMode_Properties1_HourLocalTimeMask), _data[7], _data[8]);
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, DoorLockLoggingIndex_RecordNumber))) {
					record_number = _data[1];
					m_value->OnValueRefreshed(_data[1]);
					m_value->Release();
				}

				char eventTime_chr[128];
				memset(eventTime_chr, 0, sizeof(eventTime_chr));
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_Time))) {
					uint16 _year = (_data[2]<<8 | _data[3]);
					snprintf( eventTime_chr, sizeof(eventTime_chr), "%d/%d/%d %d:%d:%d", _year, _data[4], _data[5],
						(_data[6] & DoorLockLoggingMode_Properties1_HourLocalTimeMask), _data[7], _data[8]);
					m_value->OnValueRefreshed(string(eventTime_chr));
					m_value->Release();
				}

				Log::Write(LogLevel_Info,  GetNodeId(), "Event type=%d, user %d, code_length=%d", _data[9], _data[10], _data[11]);
				char code_chr[50];
				memset(code_chr, 0, sizeof(code_chr));
				snprintf(code_chr, sizeof(code_chr), "%.*s", _data[11], (char*)&_data[12]);
				Log::Write(LogLevel_Info,  GetNodeId(), "User code=%s", code_chr);
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_EventType))) {
					char _type[20];
					memset(_type, 0, sizeof(_type));
					snprintf(_type, sizeof(_type), "%d", _data[9]);
					m_value->OnValueRefreshed(string(_type));
					m_value->Release();
				}
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, DoorLockLoggingIndex_UserId))) {
					m_value->OnValueRefreshed(_data[10]);
					m_value->Release();
				}
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_UserCode))) {
					m_value->OnValueRefreshed(string(code_chr));
					m_value->Release();
				}

				if (stop_id > 0) {
					char _log[128];
					memset(_log, 0, sizeof(_log));
					if (strlen(code_chr) > 0) {
						snprintf(_log, sizeof(_log), "[%d:(%s)(Type=%d)(User=%d)(Code=%s)]", 
							_record_number, eventTime_chr, _data[9], _data[10], code_chr);
					} else {
						snprintf(_log, sizeof(_log), "[%d:(%s)(Type=%d)(User=%d)(Code=Empty)]", 
							_record_number, eventTime_chr, _data[9], _data[10]);
					}
					log_detail = log_detail + _log;

					if (_record_number == stop_id) {
						if (ValueString* m_detail = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_LogDetail))) {
							m_detail->OnValueRefreshed(log_detail);
							m_detail->Release();
						}
						stop_id = 0;
					}
				}
			} else {
				Log::Write(LogLevel_Info,  GetNodeId(), "Record number=%d. Data is empty.", _data[1]);
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, DoorLockLoggingIndex_RecordNumber))) {
					m_value->OnValueRefreshed(_data[1]);
					m_value->Release();
				}
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_Time))) {
					m_value->OnValueRefreshed("");
					m_value->Release();
				}
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_EventType))) {
					m_value->OnValueRefreshed("0");
					m_value->Release();
				}
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, DoorLockLoggingIndex_UserId))) {
					m_value->OnValueRefreshed(0);
					m_value->Release();
				}
				if (ValueString* m_value = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_UserCode))) {
					m_value->OnValueRefreshed("");
					m_value->Release();
				}
				if (stop_id > 0) {
					char _log[128];
					memset(_log, 0, sizeof(_log));
					snprintf(_log, sizeof(_log), "[%d:()(Type=0)(User=0)(Code=Empty)]", _record_number);
					log_detail = log_detail + _log;
					if (_record_number == stop_id) {
						if (ValueString* m_detail = static_cast<ValueString*>(GetValue(_instance, DoorLockLoggingIndex_LogDetail))) {
							m_detail->OnValueRefreshed(log_detail);
							m_detail->Release();
						}
						stop_id = 0;
					}
				}
			}
			if (stop_id > 0) {
				record_number++;
				RequestValue( RequestFlag_Dynamic, DoorLockLoggingCmd_RecordGet, _instance, Driver::MsgQueue_Send );
			}
			break;
		}
		default:
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// <DoorLockLogging::SetValue>
// Set the DoorLockLogging's state
//-----------------------------------------------------------------------------
bool DoorLockLogging::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case DoorLockLoggingIndex_RecordNumber:
		{
			ValueByte const* m_value = static_cast<ValueByte const*>(&_value);
			record_number = m_value->GetValue();

			return RequestValue( RequestFlag_Dynamic, DoorLockLoggingCmd_RecordGet, instance, Driver::MsgQueue_Send );
		}
		case DoorLockLoggingIndex_LogPage:
		{
			ValueByte const* m_value = static_cast<ValueByte const*>(&_value);
			log_page = m_value->GetValue();

			if (ValueByte* m_max_record = static_cast<ValueByte*>(GetValue(instance, DoorLockLoggingIndex_MaxRecordStored))) {
				if (log_perPage == 0) 
					log_perPage = 10;
				uint8 max_records = m_max_record->GetValue();
				max_logPage = (uint8)(max_records/log_perPage);
				m_max_record->Release();
			}

			if (log_page > max_logPage || log_page == 0)
				return false;

			if (ValueString* m_detail = static_cast<ValueString*>(GetValue(instance, DoorLockLoggingIndex_LogDetail))) {
				m_detail->OnValueRefreshed("Waiting...");
				m_detail->Release();
				log_detail.clear();
			}
			stop_id = log_page*log_perPage;
			uint8 start_id = stop_id - (log_perPage-1);

			record_number = start_id;
			return RequestValue( RequestFlag_Dynamic, DoorLockLoggingCmd_RecordGet, instance, Driver::MsgQueue_Send );
		}
		case DoorLockLoggingIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, DoorLockLoggingIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <DoorLockLogging::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void DoorLockLogging::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_MaxRecordStored, 
			"Max Record", "", true, false, 0, 0);
		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_RecordNumber, 
			"Record Number", "", false, false, 1, 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_Time, 
			"Event Time", "", true, false, "1970/1/1", 0);
		
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_EventType, 
			"Event Type", "", true, false, "0", 0);

		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_UserId, 
			"User Id", "", true, false, 0, 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_UserCode, 
			"User Code", "", true, false, "", 0);

		node->CreateValueByte(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_LogPage, 
			"Log Page", "", false, false, 0, 0);
		node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_LogDetail, 
			"Log Detail", "", true, false, "none", 0);
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, DoorLockLoggingIndex_IconName, 
			"DoorLockLogging Icon", "icon", false, false, "", 0);
	}
}

ValueList::Item DoorLockLogging::CreateItem(string _label, int32 _value)
{
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

