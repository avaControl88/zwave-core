//-----------------------------------------------------------------------------
//
//	Protection.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_PROTECTION
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include <vector>

#include "CommandClasses.h"
#include "Protection.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueList.h"
#include "ValueByte.h"
#include "ValueString.h"

using namespace OpenZWave;

enum ProtectionCmd
{
	ProtectionCmd_Set						= 0x01,
	ProtectionCmd_Get						= 0x02,
	ProtectionCmd_Report					= 0x03,

	//Version 2
	ProtectionCmd_SupportedGet				= 0x04,
	ProtectionCmd_SupportedReport			= 0x05,
	ProtectionCmd_ExclusiveControlSet		= 0x06,
	ProtectionCmd_ExclusiveControlGet		= 0x07,
	ProtectionCmd_ExclusiveControlReport	= 0x08,
	ProtectionCmd_TimeoutSet				= 0x09,
	ProtectionCmd_TimeoutGet				= 0x0a,
	ProtectionCmd_TimeoutReport				= 0x0b
};

enum {
	ProtectionSupportedReport_LevelTimeoutBitMask = 0x01,
	ProtectionSupportedReport_LevelExclusiveControlBitMask = 0x02
};

enum
{
	ProtectionIndex_Local = 0,
	ProtectionIndex_RF,
	ProtectionIndex_ExclusiveControl,
	ProtectionIndex_Timeout,

	ProtectionIndex_IconName = 240
};

static char const* c_localProtectionStateNames[] = 
{
	"Unprotected",
	"Protection by Sequence",
	"No Operation Possible"
};

static char const* c_RFProtectionStateNames[] = 
{
	"Unprotected",
	"No RF control",
	"No RF response at all"
};

//-----------------------------------------------------------------------------
// <Protection::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool Protection::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Session )
	{
		if (m_exclusive)
			RequestValue( _requestFlags, ProtectionIndex_ExclusiveControl, _instance, _queue );
		if (m_timeout)
			RequestValue( _requestFlags, ProtectionIndex_Timeout, _instance, _queue );

		return RequestValue( _requestFlags, ProtectionIndex_Local, _instance, _queue );
	}
	else if (_requestFlags & RequestFlag_Static)
	{
		if (GetVersion() > 1) {
			Msg* msg = new Msg( "ProtectionCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( ProtectionCmd_SupportedGet );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, _queue);
		}
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Protection::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool Protection::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (_dummy1 == ProtectionIndex_Local || _dummy1 == ProtectionIndex_RF) {
		Msg* msg = new Msg( "ProtectionCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ProtectionCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
		return true;
	} else if (_dummy1 == ProtectionIndex_ExclusiveControl) {
		Msg* msg = new Msg( "ProtectionCmd_ExclusiveControlGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ProtectionCmd_ExclusiveControlGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
	} else if (_dummy1 == ProtectionIndex_Timeout) {
		Msg* msg = new Msg( "ProtectionCmd_TimeoutGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ProtectionCmd_TimeoutGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, _queue);
	}
	return true;
}

//-----------------------------------------------------------------------------
// <Protection::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Protection::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	Node* node = GetNodeUnsafe();
	if (!node)
		return false;
	if (ProtectionCmd_Report == (ProtectionCmd)_data[0])
	{
		uint8 local_state = _data[1];
		if( ValueList* local_value = static_cast<ValueList*>( GetValue( _instance, ProtectionIndex_Local ) ) )
		{
			local_value->OnValueRefreshed( local_state );
			local_value->Release();
		}
		if (GetVersion() == 1) {
			Log::Write( LogLevel_Info, GetNodeId(), "Received a Protection report: %s", c_localProtectionStateNames[local_state] );
		} else {
			uint8 rf_state = _data[2];
			Log::Write( LogLevel_Info, GetNodeId(), "Received a Protection report: Local State=%s, RF State=%s.", 
				c_localProtectionStateNames[local_state], c_RFProtectionStateNames[rf_state] );
			if ( ValueList* rf_value = static_cast<ValueList*>( GetValue( _instance, ProtectionIndex_RF ) ) )
			{
				rf_value->OnValueRefreshed( rf_state );
				rf_value->Release();
			}
		}
		return true;
	} 
	else if (ProtectionCmd_SupportedReport == (ProtectionCmd)_data[0])
	{
		m_exclusive = ((_data[1] & ProtectionSupportedReport_LevelExclusiveControlBitMask) != 0);
		m_timeout = ((_data[1] & ProtectionSupportedReport_LevelTimeoutBitMask) != 0);
		uint16 localStateMask = (_data[2] | _data[3]<<8);
		uint16 RFStateMask = (_data[4] | _data[5]<<8);
		Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_SupportedReport: Exclusive(%s), Timeout(%s). Local state=0x%x, RF state=0x%x", 
			(m_exclusive ? "Supported":"Not support"), (m_timeout ? "Supported":"Not support"), localStateMask, RFStateMask);
		if (m_exclusive) {
			ValueByte* ec_value = static_cast<ValueByte*>( GetValue( _instance, ProtectionIndex_ExclusiveControl ) );
			if( ec_value == NULL) {
				node->CreateValueByte(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ProtectionIndex_ExclusiveControl, "Exclusive Control", "", false, false, 0, 0);
			} else
				ec_value->Release();
		}
		if (m_timeout) {
			ValueByte* timeout_value = static_cast<ValueByte*>( GetValue( _instance, ProtectionIndex_Timeout ) );
			if( timeout_value == NULL) {
				node->CreateValueByte(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ProtectionIndex_Timeout, "Timeout", "", false, false, 0, 0);
				if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ProtectionIndex_Timeout))) {
					string info = "0 - No timer is set. "
						"1~60 - Timout is set from 1 to 60 seconds. "
						"65~254 - Timeout is set from 2 minutes to 191 minutes. "
						"255 - No timeout.";
					m_value->SetHelp(info);
					m_value->Release();
				}
			} else
				timeout_value->Release();
		}
		return true;
	}
	else if (ProtectionCmd_ExclusiveControlReport == (ProtectionCmd)_data[0])
	{
		uint8 node_id = _data[1];
		Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_ExclusiveControlReport: node=%d", node_id);
		if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ProtectionIndex_ExclusiveControl))) {
			m_value->OnValueRefreshed(node_id);
			m_value->Release();
		}
		return true;
	}
	else if (ProtectionCmd_TimeoutReport == (ProtectionCmd)_data[0])
	{
		uint8 timeout = _data[1];
		if (timeout == 0) {
			Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_TimeoutReport: No timer is set");
		} 
		else if (timeout >= 1 && timeout <= 60) {
			Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_TimeoutReport: Timeout is %d seconds.", timeout);
		}
		else if (timeout >= 65 && timeout <= 254) {
			Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_TimeoutReport: Timeout is %d minutes.", (timeout-63));
		}
		else {
			Log::Write( LogLevel_Info, GetNodeId(), "Received ProtectionCmd_TimeoutReport: No timeout");
		}
		if (ValueByte* m_value = static_cast<ValueByte*>(GetValue(_instance, ProtectionIndex_Timeout))) {
			m_value->OnValueRefreshed(timeout);
			m_value->Release();
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Protection::SetValue>
// Set the device's protection state
//-----------------------------------------------------------------------------
bool Protection::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case ProtectionIndex_Local:
		{
			ValueList const* local_value = static_cast<ValueList const*>(&_value);
			ValueList::Item const& item = local_value->GetItem();

			Log::Write( LogLevel_Info, GetNodeId(), "Protection::Set - Setting protection state to '%s'", item.m_label.c_str() );
			Msg* msg = new Msg( "Protection Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );		
			msg->SetInstance( this, _value.GetID().GetInstance() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( ProtectionCmd_Set );
			msg->Append( (uint8)item.m_value );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		case ProtectionIndex_RF:
		{
			ValueList const* rf_value = static_cast<ValueList const*>(&_value);
			ValueList::Item const& rf_item = rf_value->GetItem();

			ValueList const* local_value = static_cast<ValueList const*>(GetValue(instance, ProtectionIndex_Local));
			ValueList::Item const& local_item = local_value->GetItem();

			Log::Write( LogLevel_Info, GetNodeId(), "Protection::Set - Setting protection state to '%s'", rf_item.m_label.c_str() );
			Msg* msg = new Msg( "Protection Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );		
			msg->SetInstance( this, _value.GetID().GetInstance() );
			msg->Append( GetNodeId() );
			msg->Append( 4 );
			msg->Append( GetCommandClassId() );
			msg->Append( ProtectionCmd_Set );
			msg->Append( (uint8)local_item.m_value );
			msg->Append( (uint8)rf_item.m_value );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		case ProtectionIndex_ExclusiveControl:
		{
			ValueByte const* ec_value = static_cast<ValueByte const*>(&_value);
			uint8 node_id = ec_value->GetValue();

			Msg* msg = new Msg( "ProtectionCmd_ExclusiveControlSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );		
			msg->SetInstance( this, _value.GetID().GetInstance() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( ProtectionCmd_ExclusiveControlSet );
			msg->Append( node_id );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		case ProtectionIndex_Timeout:
		{
			ValueByte const* timeout_value = static_cast<ValueByte const*>(&_value);
			uint8 timeout = timeout_value->GetValue();

			Msg* msg = new Msg( "ProtectionCmd_TimeoutSet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );		
			msg->SetInstance( this, _value.GetID().GetInstance() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( ProtectionCmd_TimeoutSet );
			msg->Append( timeout );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, Driver::MsgQueue_Send);
			return true;
		}
		case ProtectionIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ProtectionIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Protection::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void Protection::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		vector<ValueList::Item> items;

		ValueList::Item item;
		for( uint8 i=0; i<3; ++i )
		{
			item.m_label = c_localProtectionStateNames[i];
			item.m_value = i;
			items.push_back( item ); 
		}
		node->CreateValueList( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ProtectionIndex_Local, "Protection", "", false, false, 1, items, 0, 0 );

		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ProtectionIndex_IconName, "Protection Icon", "icon", false, false, "", 0);

		if (GetVersion() > 1) {
			vector<ValueList::Item> items2;
			ValueList::Item item2;
			for( uint8 i=0; i<3; ++i )
			{
				item2.m_label = c_RFProtectionStateNames[i];
				item2.m_value = i;
				items2.push_back( item2 ); 
			}
			node->CreateValueList( ValueID::ValueGenre_Config, GetCommandClassId(), _instance, ProtectionIndex_RF, "RF Protection", "", false, false, 1, items2, 0, 0 );
		}
	}
}

void Protection::ReadXML( TiXmlElement const* _ccElement ) {
	CommandClass::ReadXML( _ccElement );
	char const* str = _ccElement->Attribute("exclusive_support");
	if (str) {
		m_exclusive = !strcmp( str, "true");
	}
	char const* str2 = _ccElement->Attribute("timeout_support");
	if (str2) {
		m_timeout = !strcmp( str2, "true");
	}
}

void Protection::WriteXML( TiXmlElement* _ccElement ) {
	CommandClass::WriteXML( _ccElement );
	if (m_exclusive) {
		_ccElement->SetAttribute( "exclusive_support", "true" );
	}
	if (m_timeout) {
		_ccElement->SetAttribute( "timeout_support", "true" );
	}
}
