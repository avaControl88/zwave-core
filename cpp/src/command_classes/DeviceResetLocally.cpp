//-----------------------------------------------------------------------------
//
//	DeviceResetLocally.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_DEVICE_RESET_LOCALLY
//
//	Copyright (c) 2014 David Chen <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "DeviceResetLocally.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"
#include "Group.h"
#include "Notification.h"
#include "Association.h"
using namespace OpenZWave;

enum ZwavePlusInfoCmd
{
	DeviceResetLocallyCmd_Notification				= 0x1
};

//-----------------------------------------------------------------------------
// <ZwavePlusInfo::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool DeviceResetLocally::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	return false;
}

//-----------------------------------------------------------------------------
// <DeviceResetLocally::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool DeviceResetLocally::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,		// = 0
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	return false;
}

//-----------------------------------------------------------------------------
// <DeviceResetLocally::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool DeviceResetLocally::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if (_data[0] == DeviceResetLocallyCmd_Notification) {
		Log::Write( LogLevel_Warning, GetNodeId(), "Node %d has been reset.", GetNodeId());
		Notification* notification = new Notification( Notification::Type_NodeReset );
		notification->SetHomeNodeIdAndInstance( GetHomeId(), GetNodeId(), _instance );
		GetDriver()->QueueNotification( notification );
		if (Node* node = GetNodeUnsafe()) {
			//The node has been reset.
			node->SetNodeAlive(false);
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <DeviceResetLocally::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void DeviceResetLocally::CreateVars
(
	uint8 const _instance
)
{
}

void DeviceResetLocally::initSupportSetting()
{
}

void DeviceResetLocally::SendEventToGroup(uint8 const _groupIdx) {
	Node* _node = GetNodeUnsafe();
	if (!_node)
		return;
	uint8* receivedNodes;
	Group* _group = _node->GetGroup(ASSOCIATION_SUPPORT_GROUP);
	if (_group) {
		uint32 node_count = _group->GetAssociations(&receivedNodes);
		if (node_count) {
			for (uint32 i=0; i<node_count; i++) {
				//No need to wait callback
				Msg* msg = new Msg( "DeviceResetLocallyCmd_Notification", receivedNodes[i], REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->Append( receivedNodes[i] );
				msg->Append( 2 );
				msg->Append( GetCommandClassId() );
				msg->Append( DeviceResetLocallyCmd_Notification );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
		}
	}
}
