//-----------------------------------------------------------------------------
//
//	ManufacturerSpecific.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_MANUFACTURER_SPECIFIC
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ManufacturerSpecific.h"
#include "tinyxml.h"

#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Options.h"
#include "Manager.h"
#include "Driver.h"
#include "Notification.h"
#include "Log.h"

#include "ValueStore.h"
#include "ValueString.h"

using namespace OpenZWave;

enum ManufacturerSpecificCmd
{
	ManufacturerSpecificCmd_Get		= 0x04,
	ManufacturerSpecificCmd_Report	= 0x05,
	DeviceSpecificCmd_Get			= 0x06,
	DeviceSpecificCmd_Report		= 0x07
};

enum {
	DeviceSpecificGet_DeviceIDType_FactoryDefault = 0x00,
	DeviceSpecificGet_DeviceIDType_SerialNumber = 0x01,
	DeviceSpecificGet_DeviceIDType_PseudoRandom = 0x02,

	DeviceSpecificReport_DeviceIDType_Mask		=0x07,
	DeviceSpecific_ReportProperties2_DeviceIDDataLengthIndicatorMask = 0x1f,
	DeviceSpecific_ReportProperties2_DeviceIDDataFormatMask = 0xe0,
	DeviceSpecific_ReportProperties2_DeviceIDDataFormatShift = 0x05,

	DeviceSpecific_ReportProperties2_DeviceIDDataFormatUTF8 = 0x00,
	DeviceSpecific_ReportProperties2_DeviceIDDataFormatBinary = 0x01
};

enum {
	ManufacturerSpecificIndex_Default = 0,
	ManufacturerSpecificIndex_SerialNumber
};

map<uint16,string> ManufacturerSpecific::s_manufacturerMap;
map<int64,ManufacturerSpecific::Product*> ManufacturerSpecific::s_productMap;
bool ManufacturerSpecific::s_bXmlLoaded = false;
bool ManufacturerSpecific::s_blockUncertified = false;

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool ManufacturerSpecific::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( ( _requestFlags & RequestFlag_Static ) && HasStaticRequest( StaticRequest_Values ) )
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	} else if (_requestFlags & RequestFlag_Static) {
		if (GetVersion() > 1) {
			//Get factory default id
			Msg* msg = new Msg( "DeviceSpecificCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( DeviceSpecificCmd_Get );
			msg->Append( DeviceSpecificGet_DeviceIDType_FactoryDefault );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, _queue);

			//Get serial number
			msg = new Msg( "DeviceSpecificCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->Append( GetNodeId() );
			msg->Append( 3 );
			msg->Append( GetCommandClassId() );
			msg->Append( DeviceSpecificCmd_Get );
			msg->Append( DeviceSpecificGet_DeviceIDType_SerialNumber );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, _queue);
		}
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool ManufacturerSpecific::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (IsDriverNode())
		return false;

	if( _instance != 1 )
	{
		// This command class doesn't work with multiple instances
		return false;
	}

	Msg* msg = new Msg( "ManufacturerSpecificCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( ManufacturerSpecificCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	SendSecureMsg(msg, _queue);
	return true;
}

string ManufacturerSpecific::SetProductDetails
(
	Node* node,
	uint16 manufacturerId,
	uint16 productType,
	uint16 productId
)
{
	char str[64];

	if (!s_bXmlLoaded) LoadProductXML();

	snprintf( str, sizeof(str), "Unknown: id=%.4x", manufacturerId );
	string manufacturerName = str;
	
	snprintf( str, sizeof(str), "Unknown: type=%.4x, id=%.4x", productType, productId );
	string productName = str;	
	
	string configPath = "";
	
	// Try to get the real manufacturer and product names
	map<uint16,string>::iterator mit = s_manufacturerMap.find( manufacturerId );
	if( mit != s_manufacturerMap.end() )
	{
		// Replace the id with the real name
		manufacturerName = mit->second;

		// Get the product
		map<int64,Product*>::iterator pit = s_productMap.find( Product::GetKey( manufacturerId, productType, productId ) );
		if( pit != s_productMap.end() )
		{
			productName = pit->second->GetProductName();
			configPath = pit->second->GetConfigPath();
		}
	}

	if (s_blockUncertified) {
		if (manufacturerId == 0x0109 && (
			(productType == 0x2002 && productId == 0x0205) || 
			(productType == 0x2004 && productId == 0x04a5) || 
			(productType == 0x201a && productId == 0x1a04) || 
			(productType == 0x201a && productId == 0x1aa4) || 
			(productType == 0x201f && productId == 0x1f10)
			)) {
				//do nothing
		} else {
			//remove all the command class
			for (int i=1; i<0x9f; i++) {
				if (i == 0x20 || //BASIC
					i == 0x21 || //CONTROLLER REPLICATION
					i == 0x22 || //APPLICATION STATUS
					i == 0x59 || //ASSOCIATION GRP INFO
					i == 0x5a || //DEVICE RESET LOCALLY
					i == 0x5e || //ZWAVEPLUS INFO
					i == 0x72 || //MANUFACTURER SPECIFIC
					i == 0x84 || //WAKE UP
					i == 0x85 || //ASSOCIATION
					i == 0x86) { //VERSION
						continue;
				}
				node->RemoveCommandClass(i);
			}
			manufacturerName = "Uncertified";
		}
	}

	// Set the values into the node

	// Only set the manufacturer and product name if they are
	// empty - we don't want to overwrite any user defined names.
	if( node->GetManufacturerName() == "" )
	{
		node->SetManufacturerName( manufacturerName );
	}

	if( node->GetProductName() == "" )
	{
		node->SetProductName( productName );
	}

	snprintf( str, sizeof(str), "%.4x", manufacturerId );
	node->SetManufacturerId( str );

	snprintf( str, sizeof(str), "%.4x", productType );
	node->SetProductType( str );

	snprintf( str, sizeof(str), "%.4x", productId );
	node->SetProductId( str );
	
	return configPath;
}


//-----------------------------------------------------------------------------
// <ManufacturerSpecific::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ManufacturerSpecific::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ManufacturerSpecificCmd_Report == (ManufacturerSpecificCmd)_data[0] )
	{

		// first two bytes are manufacturer id code
		uint16 manufacturerId = (((uint16)_data[1])<<8) | (uint16)_data[2];

		// next four are product type and product id
		uint16 productType = (((uint16)_data[3])<<8) | (uint16)_data[4];
		uint16 productId = (((uint16)_data[5])<<8) | (uint16)_data[6];

		if( Node* node = GetNodeUnsafe() )
		{
			// Attempt to create the config parameters
			string configPath = SetProductDetails( node, manufacturerId, productType, productId);
			if( configPath.size() > 0 )
			{
				LoadConfigXML( node, configPath );
			}

			Log::Write( LogLevel_Info, GetNodeId(), "Received manufacturer specific report from node %d: Manufacturer=%s, Product=%s", 
				    GetNodeId(), node->GetManufacturerName().c_str(), node->GetProductName().c_str() );
			ClearStaticRequest( StaticRequest_Values );
			node->m_manufacturerSpecificClassReceived = true;
		}
		
		// Notify the watchers of the name changes
		Notification* notification = new Notification( Notification::Type_NodeNaming );
		notification->SetHomeAndNodeIds( GetHomeId(), GetNodeId() );
		GetDriver()->QueueNotification( notification );

		return true;
	} 
	else if ( DeviceSpecificCmd_Report == (ManufacturerSpecificCmd)_data[0] )
	{
		uint8 deviceIDType = (_data[1] & DeviceSpecificReport_DeviceIDType_Mask);
		uint8 dataFormat = (_data[2] & DeviceSpecific_ReportProperties2_DeviceIDDataFormatMask)>>DeviceSpecific_ReportProperties2_DeviceIDDataFormatShift;
		uint8 data_length = (_data[2] & DeviceSpecific_ReportProperties2_DeviceIDDataLengthIndicatorMask);
		uint8 const* deviceIDData = &_data[3];
		string deviceID = "";
		for (int i=0; i<data_length; i++) {
			char temp_chr[32];
			memset(temp_chr, 0, sizeof(temp_chr));
			if (dataFormat == DeviceSpecific_ReportProperties2_DeviceIDDataFormatUTF8) {
				temp_chr[0] = deviceIDData[i];
			} else {
				snprintf(temp_chr, sizeof(temp_chr), "%.2x", deviceIDData[i]);
			}
			deviceID += temp_chr;
		}
		if( Node* node = GetNodeUnsafe() ) {
			if (deviceIDType == DeviceSpecificGet_DeviceIDType_FactoryDefault) {
				ValueString *default_value = static_cast<ValueString*>( GetValue(_instance, ManufacturerSpecificIndex_Default) );
				if (default_value == NULL) {
					node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ManufacturerSpecificIndex_Default, "Device ID", "", true, false, "", 0);
					default_value = static_cast<ValueString*>( GetValue(_instance, ManufacturerSpecificIndex_Default) );
				}
				default_value->OnValueRefreshed(deviceID);
				default_value->Release();
			} 
			else if (deviceIDType == DeviceSpecificGet_DeviceIDType_SerialNumber) {
				ValueString *serial_value = static_cast<ValueString*>( GetValue(_instance, ManufacturerSpecificIndex_SerialNumber) );
				if (serial_value == NULL) {
					node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), _instance, ManufacturerSpecificIndex_SerialNumber, "Serial Number", "", true, false, "", 0);
					serial_value = static_cast<ValueString*>( GetValue(_instance, ManufacturerSpecificIndex_SerialNumber) );
				}
				serial_value->OnValueRefreshed(deviceID);
				serial_value->Release();
			}
		}
		return true;
	}
	else if (ManufacturerSpecificCmd_Get == (ManufacturerSpecificCmd)_data[0]) {
		Msg* msg = new Msg( "ManufacturerSpecificCmd_Report", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
		msg->Append( GetNodeId() );
		msg->Append( 8 );
		msg->Append( GetCommandClassId() );
		msg->Append( ManufacturerSpecificCmd_Report );
		msg->Append(MANUFACTURER_ID1);
		msg->Append(MANUFACTURER_ID2);
		msg->Append(PRODUCT_TYPE1);
		msg->Append(PRODUCT_TYPE2);
		msg->Append(PRODUCT_ID1);
		msg->Append(PRODUCT_ID2);
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, Driver::MsgQueue_Send);
		return true;
	}
	else if (DeviceSpecificCmd_Get == (ManufacturerSpecificCmd)_data[0]) {
		uint8 deviceIDType = _data[1];
		string deviceID;
		uint8 dataFormat = (DeviceSpecific_ReportProperties2_DeviceIDDataFormatUTF8<<DeviceSpecific_ReportProperties2_DeviceIDDataFormatShift);
		
		switch (deviceIDType) {
			case DeviceSpecificGet_DeviceIDType_SerialNumber:
				deviceIDType = DeviceSpecificGet_DeviceIDType_SerialNumber;
				deviceID = DEVICE_SERIAL_NUMBER;
				break;
			case DeviceSpecificGet_DeviceIDType_FactoryDefault:
			case DeviceSpecificGet_DeviceIDType_PseudoRandom:
			default:
				deviceIDType = DeviceSpecificGet_DeviceIDType_FactoryDefault;
				deviceID = DEVICEID;
				break;
		}
		
		Msg* msg = new Msg( "DeviceSpecificCmd_Report", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
		msg->Append( GetNodeId() );
		msg->Append( 4+deviceID.size() );
		msg->Append( GetCommandClassId() );
		msg->Append( DeviceSpecificCmd_Report );
		msg->Append( deviceIDType );
		msg->Append( (dataFormat|deviceID.size()) );
		for (uint32 i=0; i<deviceID.size(); i++) {
			msg->Append(deviceID.c_str()[i]);
		}
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, Driver::MsgQueue_Send);
		return true;
	}
	
	return false;
}

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::LoadProductXML>
// Load the XML that maps manufacturer and product IDs to human-readable names
//-----------------------------------------------------------------------------
bool ManufacturerSpecific::LoadProductXML
(
)
{
	s_bXmlLoaded = true;

	// Parse the Z-Wave manufacturer and product XML file.
	string configPath;
	Options::Get()->GetOptionAsString( "ConfigPath", &configPath );

	string filename =  configPath + "manufacturer_specific.xml";

	TiXmlDocument* pDoc = new TiXmlDocument();
	if( !pDoc->LoadFile( filename.c_str(), TIXML_ENCODING_UTF8 ) )
	{
		delete pDoc;
		Log::Write( LogLevel_Info, "Unable to load %s", filename.c_str() );
		return false;
	}

	TiXmlElement const* root = pDoc->RootElement();

	//David
	char const* _block_uncer = root->Attribute("block_uncertified");
	if (_block_uncer && !strcmp(_block_uncer, "true")) {
		s_blockUncertified = true;
	}

	char const* str;
	char* pStopChar;
	
	TiXmlElement const* manufacturerElement = root->FirstChildElement();
	while( manufacturerElement )
	{
		str = manufacturerElement->Value();
		if( str && !strcmp( str, "Manufacturer" ) )
		{
			// Read in the manufacturer attributes
			str = manufacturerElement->Attribute( "id" );
			if( !str )
			{
				Log::Write( LogLevel_Info, "Error in manufacturer_specific.xml at line %d - missing manufacturer id attribute", manufacturerElement->Row() );
				delete pDoc;
				return false;
			}
			uint16 manufacturerId = (uint16)strtol( str, &pStopChar, 16 );

			str = manufacturerElement->Attribute( "name" );
			if( !str )
			{
				Log::Write( LogLevel_Info, "Error in manufacturer_specific.xml at line %d - missing manufacturer name attribute", manufacturerElement->Row() );
				delete pDoc;
				return false;
			}
			
			// Add this manufacturer to the map
			s_manufacturerMap[manufacturerId] = str;

			// Parse all the products for this manufacturer
			TiXmlElement const* productElement = manufacturerElement->FirstChildElement();
			while( productElement )
			{
				str = productElement->Value();
				if( str && !strcmp( str, "Product" ) )
				{
					str = productElement->Attribute( "type" );
					if( !str )
					{
						Log::Write( LogLevel_Info, "Error in manufacturer_specific.xml at line %d - missing product type attribute", productElement->Row() );
						delete pDoc;	
						return false;
					}
					uint16 productType = (uint16)strtol( str, &pStopChar, 16 );
					
					str = productElement->Attribute( "id" );
					if( !str )
					{
						Log::Write( LogLevel_Info, "Error in manufacturer_specific.xml at line %d - missing product id attribute", productElement->Row() );
						delete pDoc;	
						return false;
					}
					uint16 productId = (uint16)strtol( str, &pStopChar, 16 );

					str = productElement->Attribute( "name" );
					if( !str )
					{
						Log::Write( LogLevel_Info, "Error in manufacturer_specific.xml at line %d - missing product name attribute", productElement->Row() );
						delete pDoc;	
						return false;
					}
					string productName = str;

					// Optional config path
					string configPath;
					str = productElement->Attribute( "config" );
					if( str )
					{
						configPath = str;
					}

					// Add the product to the map
					Product* product = new Product( manufacturerId, productType, productId, productName, configPath );
					if ( s_productMap[product->GetKey()] != NULL )
					{
						Product *c = s_productMap[product->GetKey()];
						Log::Write( LogLevel_Info, "Product name collision: %s type %x id %x manufacturerid %x, collides with %s, type %x id %x manufacturerid %x", productName.c_str(), productType, productId, manufacturerId, c->GetProductName().c_str(), c->GetProductType(), c->GetProductId(), c->GetManufacturerId());
						delete product;
					}
					else
					{
						s_productMap[product->GetKey()] = product;
					}
				}

				// Move on to the next product.
				productElement = productElement->NextSiblingElement();
			}
		}

		// Move on to the next manufacturer.
		manufacturerElement = manufacturerElement->NextSiblingElement();
	}

	delete pDoc;	
	return true;
}

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::UnloadProductXML>
// Free the XML that maps manufacturer and product IDs
//-----------------------------------------------------------------------------
void ManufacturerSpecific::UnloadProductXML
(
)
{
	if (s_bXmlLoaded)
	{
		map<int64,Product*>::iterator pit = s_productMap.begin();
		while( !s_productMap.empty() )
		{
		  	delete pit->second;
			s_productMap.erase( pit );
			pit = s_productMap.begin();
		}

		map<uint16,string>::iterator mit = s_manufacturerMap.begin();
		while( !s_manufacturerMap.empty() )
		{
			s_manufacturerMap.erase( mit );
			mit = s_manufacturerMap.begin();
		}

		s_bXmlLoaded = false;
	}
}

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::LoadConfigXML>
// Try to find and load an XML file describing the device's config params
//-----------------------------------------------------------------------------
bool ManufacturerSpecific::LoadConfigXML
(
	Node* _node,
	string const& _configXML
)
{
	string configPath;
	Options::Get()->GetOptionAsString( "ConfigPath", &configPath );
		
	string filename =  configPath + _configXML;

	TiXmlDocument* doc = new TiXmlDocument();
	Log::Write( LogLevel_Info, _node->GetNodeId(), "  Opening config param file %s", filename.c_str() );
	if( !doc->LoadFile( filename.c_str(), TIXML_ENCODING_UTF8 ) )
	{
		delete doc;	
		Log::Write( LogLevel_Info, _node->GetNodeId(), "Unable to find or load Config Param file %s", filename.c_str() );
		return false;
	}

	Node::QueryStage qs = _node->GetCurrentQueryStage();
	if( qs == Node::QueryStage_ManufacturerSpecific1 )
	{
		_node->ReadDeviceProtocolXML( doc->RootElement() );
	}
	else
	{
		if( !_node->m_manufacturerSpecificClassReceived )
		{
			_node->ReadDeviceProtocolXML( doc->RootElement() );
		}
		_node->ReadCommandClassesXML( doc->RootElement() );
	}

	delete doc;	
	return true;
}

//-----------------------------------------------------------------------------
// <ManufacturerSpecific::ReLoadConfigXML>
// Reload previously discovered device configuration.
//-----------------------------------------------------------------------------
void ManufacturerSpecific::ReLoadConfigXML
(
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		if (!s_bXmlLoaded) {
			LoadProductXML();
		} else {
			UnloadProductXML();
			LoadProductXML();
		}

		uint16 manufacturerId = (uint16)strtol( node->GetManufacturerId().c_str(), NULL, 16 );
		uint16 productType = (uint16)strtol( node->GetProductType().c_str(), NULL, 16 );
		uint16 productId = (uint16)strtol( node->GetProductId().c_str(), NULL, 16 );

		map<uint16,string>::iterator mit = s_manufacturerMap.find( manufacturerId );
		if( mit != s_manufacturerMap.end() )
		{
			map<int64,Product*>::iterator pit = s_productMap.find( Product::GetKey( manufacturerId, productType, productId ) );
			if( pit != s_productMap.end() )
			{
				string configPath = pit->second->GetConfigPath();
				if( configPath.size() > 0 )
				{
					LoadConfigXML( node, configPath );
				}
			}
		}
	}
}

void ManufacturerSpecific::initSupportSetting()
{
	if( Node* node = GetNodeUnsafe() ) {
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), 1, ManufacturerSpecificIndex_Default, "Device ID", "", true, false, DEVICEID, 0);
		node->CreateValueString( ValueID::ValueGenre_System, GetCommandClassId(), 1, ManufacturerSpecificIndex_SerialNumber, "Serial Number", "", true, false, DEVICE_SERIAL_NUMBER, 0);
	}
}
