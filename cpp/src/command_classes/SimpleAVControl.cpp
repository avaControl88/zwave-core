//-----------------------------------------------------------------------------
//
//	SimpleAVControl.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_SIMPLE_AV_CONTROL
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "SimpleAVControl.h"
#include "Defs.h"
#include "Msg.h"
#include "Driver.h"
#include "Node.h"
#include "Log.h"
#include "Options.h"

#include "ValueString.h"

using namespace OpenZWave;

enum SimpleAVControlCmd
{
	SimpleAVControlCmd_Set					= 0x01,
	SimpleAVControlCmd_Get					= 0x02,
	SimpleAVControlCmd_Report				= 0x03,
	SimpleAVControlCmd_SupportedGet			= 0x04,
	SimpleAVControlCmd_SupportedReport		= 0x05
};

enum
{
	SimpleAVControlSetProperties_KeyAttributesMask = 0x07,
	SimpleAVControlSetProperties_ReservedMask = 0xf8,
	SimpleAVControlSetProperties_ReservedShift = 0x03, 

	BitMask_0 = 0x01,
	BitMask_1 = 0x02,
	BitMask_2 = 0x04,
	BitMask_3 = 0x08, 
	BitMask_4 = 0x10,
	BitMask_5 = 0x20,
	BitMask_6 = 0x40,
	BitMask_7 = 0x80
};

enum
{
	SimpleAVControlIndex_SetKey,
	SimpleAVControlIndex_ReScan, 

	SimpleAVControlIndex_IconName = 99
};

static char const* c_keyCommandLabel[] = 
{
	"Unknown", 
	"Mute", "Volume Down", "Volume Up", "Channel Up", "Channel Down", "0", "1", "2", "3", "4",																																		//  1 -  10
	"5", "6", "7", "8", "9", "Last Channel", "Display", "Favorite Channel", "Play", "Stop",																																			// 11 -  20
	"Pause", "Fast Forward", "Rewind", "Instant Replay", "Record", "AC3", "PVR Menu", "Guide", "Menu", "Menu Up",																													// 21 -  30
	"Menu Down", "Menu Left", "Menu Right", "Page Up", "Page Down", "Select", "Exit", "Input", "Power", "Enter Channel",																											// 31 -  40
	"10", "11", "12", "13", "14", "15", "16", "+10", "+20", "+100",																																									// 41 -  50
	"-/--", "3-CH", "3D", "6-CH Input", "A", "Add", "Alarm", "AM", "Analog", "Angle",																																				// 51 -  60
	"Antenna", "Antenna East", "Antenna West", "Aspect", "Audio 1", "Audio 2", "Audio 3", "Audio Dubbing", "Audio Level Down", "Audio Level Up",																					// 61 -  70
	"Auto/Manual", "Aux 1", "Aux 2", "B", "Back", "Background", "Balance", "Balance Left", "Balance Right", "Band",																													// 71 -  80
	"Bandwidth", "Bass", "Bass Down", "Bass Up", "Blank", "Breeze Mode", "Bright", "Brightness", "Brightness Down", "Brightness Up",																								// 81 -  90
	"Buy", "C", "Camera", "Category Down", "Category Up", "Center", "Center Down", "Center Mode", "Center Up", "Channel/Program",																									// 91 - 100
	"Clear", "Close", "Closed Caption", "Cold", "Color", "Color Down", "Color Up", "Component 1", "Component 2", "Component 3",																										//101 - 110
	"Concert", "Confirm", "Continue", "Contrast", "Contrast Down", "Contrast Up", "Counter", "Counter Reset", "D", "Day Down",																										//111 - 120
	"Day Up", "Delay", "Delay Down", "Delay Up", "Delete", "Delimiter", "Digest", "Digital", "Dim", "Direct",																														//121 - 130
	"Disarm", "Disc", "Disc 1", "Disc 2", "Disc 3", "Disc 4", "Disc 5", "Disc 6", "Disc Down", "Disc Up",																															//131 - 140
	"Disco", "Edit", "Effect Down", "Effect Up", "Eject", "End", "EQ", "Fader", "Fan", "Fan High",																																	//141 - 150
	"Fan Low", "Fan Medium", "Fan Speed", "Fastext Blue", "Fastext Green", "Fastext Purple", "Fastext Red", "Fastext White", "Fastext Yellow", "Favorite Channel Down",																//151 - 160
	"Favorite Channel Up", "Finalize", "Fine Tune", "Flat", "FM", "Focus Down", "Focus Up", "Freeze", "Front", "Game",																												//161 - 170
	"GoTo", "Hall", "Heat", "Help", "Home", "Index", "Index Forward", "Index Reverse", "Interactive", "Intro Scan",																													//171 - 180
	"Jazz", "Karaoke", "Keystone", "Keystone Down", "Keystone Up", "Language", "Left Click", "Level", "Light", "List",																												//181 - 190
	"Live TV", "Local/Dx", "Loudness", "Mail", "Mark", "Memory Recall", "Monitor", "Movie", "Multi Room", "Music",																													//191 - 200
	"Music Scan", "Natural", "Night", "Noise Reduction", "Normalize", "Discrete input Cable", "Discrete input CD1", "Discrete input CD2", "Discrete input CDR", "Discrete input DAT",												//201 - 210
	"Discrete input DVD", "Discrete input DVI", "Discrete input HDTV", "Discrete input LD", "Discrete input MD", "Discrete input PC", "Discrete input PVR", "Discrete input TV", "Discrete input TV/VCR", "Discrete input VCR",		//211 - 220
	"One Touch Playback", "One Touch Record", "Open", "Optical", "Options", "Orchestra", "PAL/NTSC", "Parental Lock", "PBC", "Phono",																								//221 - 230
	"Photos", "Picture Menu", "Picture Mode", "Picture Mute", "PIP Channel Down", "PIP Channel Up", "PIP Freeze", "PIP Input", "PIP Move", "PIP Off",																				//231 - 240
	"PIP On", "PIP Size", "PIP Split", "PIP Swap", "Play Mode", "Play Reverse", "Power Off", "Power On", "PPV", "Preset",																											//241 - 250
	"Program", "Progressive Scan", "ProLogic", "PTY", "Quick Skip", "Random", "RDS", "Rear", "Rear Volume Down", "Rear Volume Up",																									//251 - 260
	"Record Mute", "Record Pause", "Repeat", "Repeat A-B", "Resume", "RGB", "Right Click", "Rock", "Rotate Left", "Rotate Right",																									//261 - 270
	"SAT", "Scan", "Scart", "Scene", "Scroll", "Services", "Setup Menu", "Sharp", "Sharpness", "Sharpness Down",																													//271 - 280
	"Sharpness Up", "Side A/B", "Skip Forward", "Skip Reverse", "Sleep", "Slow", "Slow Forward", "Slow Reverse", "Sound Menu", "Sound Mode",																						//281 - 290
	"Speed", "Speed Down", "Speed Up", "Sports", "Stadium", "Start", "Start ID Erase", "Start ID Renumber", "Start ID Write", "Step",																								//291 - 300
	"Stereo/Mono", "Still Forward", "Still Reverse", "Subtitle", "Subwoofer Down", "Subwoofer Up", "Super Bass", "Surround", "Surround Mode", "S-Video",																			//301 - 310
	"Sweep", "Synchro Record", "Tape 1", "Tape 1-2", "Tape 2", "Temperature Down", "Temperature Up", "Test Tone", "Text", "Text Expand",																							//311 - 320
	"Text Hold", "Text Index", "Text Mix", "Text Off", "Text Reveal", "Text Subpage", "Text Timed Page", "Text Update", "Theater", "Theme",																							//321 - 330
	"Thumbs Down", "Thumbs Up", "Tilt Down", "Tilt Up", "Time", "Timer", "Timer Down", "Timer Up", "Tint", "Tint Down",																												//331 - 340
	"Tint Up", "Title", "Track", "Tracking", "Tracking Down", "Tracking Up", "Treble", "Treble Down", "Treble Up", "Tune Down",																										//341 - 350
	"Tune Up", "Tuner", "VCR Plus+", "Video 1", "Video 2", "Video 3", "Video 4", "Video 5", "View", "Voice",																														//351 - 360
	"Zoom", "Zoom In", "Zoom Out", "eHome", "Details", "DVD Menu", "My TV", "Recorded TV", "My Videos", "DVD Angle",																												//361 - 370
	"DVD Audio", "DVD Subtitle", "Radio", "#", "*", "OEM 1", "OEM 2", "Info", "CAPS NUM", "TV MODE",																																//371 - 380
	"SOURCE", "FILEMODE", "Time Seek", "Mouse enable", "Mouse disable", "VOD", "Thumbs Up", "Thumbs Down", "Apps", "Mouse toggle",																									//381 - 390
	"TV Mode", "DVD Mode", "STB Mode", "AUX Mode", "BlueRay Mode", "Reserved 1", "Reserved 2", "Reserved 3", "Reserved 4", "Reserved 5",																							//391 - 400
	"Reserved 6", "Reserved 7", "Reserved 8", "Standby 1", "Standby 2", "Standby 3", "HDMI 1", "HDMI 2", "HDMI 3", "HDMI 4",																										//401 - 410
	"HDMI 5", "HDMI 6", "HDMI 7", "HDMI 8", "HDMI 9", "USB 1", "USB 2", "USB 3", "USB 4", "USB 5",																																	//411 - 420
	"ZOOM 4:3 Normal", "ZOOM 4:3 Zoom", "ZOOM 16:9 Normal", "ZOOM 16:9 Zoom", "ZOOM 16:9 Wide 1", "ZOOM 16:9 Wide 2", "ZOOM 16:9 Wide 3", "ZOOM 16:9 Cinema", "ZOOM Default", "Reserved 9",											//421 - 430
	"Reserved 10", "Auto Zoom", "ZOOM Set as Default Zoom", "Mute ON", "Mute OFF", "AUDIO Mode AUDYSSEY AUDIO OFF", "AUDIO Mode AUDYSSEY AUDIO LO", "AUDIO Mode AUDYSSEY AUDIO MED", "AUDIO Mode AUDYSSEY AUDIO HI", "Reserved 11",	//431 - 440
	"Reserved 12", "AUDIO Mode SRS SURROUND ON", "AUDIO Mode SRS SURROUND OFF", "Reserved 13", "Reserved 14", "Reserved 15", "Picture Mode Home", "Picture Mode Retail", "Picture Mode Vivid", "Picture Mode Standard",				//441 - 450
	"Picture Mode Theater", "Picture Mode Sports", "Picture Mode Energy savings", "Picture Mode Custom", "Cool", "Medium", "Warm_D65", "CC ON", "CC OFF", "Video Mute ON",															//451 - 460
	"Video Mute OFF", "Next Event", "Previous Event", "CEC device list", "MTS SAP" //461 - 465
};

//-----------------------------------------------------------------------------
// <SimpleAVControl::RequestState>
// Request current state from the device
//-----------------------------------------------------------------------------
bool SimpleAVControl::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if(_requestFlags & RequestFlag_Static)
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <SimpleAVControl::RequestValue>
// Request current value from the device
//-----------------------------------------------------------------------------
bool SimpleAVControl::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,		// = 0
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if (scanned_key)
		return false;

	Msg* msg = new Msg( "SimpleAVControlCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance(this, _instance);
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( SimpleAVControlCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <SimpleAVControl::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool SimpleAVControl::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	switch( (SimpleAVControlCmd)_data[0] ) {
		case SimpleAVControlCmd_Report:
		{
			numReports = _data[1];
			lastReportId = 0;
			Log::Write( LogLevel_Info, GetNodeId(), "SimpleAVControlCmd_Report: Reports count=%d", numReports);
			RequestAllReports(_instance, numReports);
			return true;
		}
		case SimpleAVControlCmd_SupportedReport:
		{
			uint8 report_number = _data[1];
			uint8 const* bitMask = &_data[2];
			uint8 bitMaskCount = _length - 3;
			Log::Write( LogLevel_Info, GetNodeId(), "SimpleAVControlCmd_SupportedReport: Report number=%d, instance=%d", report_number, _instance);
			if (report_number == 1) {
				Log::Write( LogLevel_Info, GetNodeId(), "Reset all key.");
				lastReportKey = 0;
				key_items.clear();
				//key_items.push_back(CreateItem( c_keyCommandLabel[0], 0));
			} else if (report_number == 0) {
				Log::Write( LogLevel_Warning, GetNodeId(), "Report number can not be zero.");
				return false;
			}
			for (int i=0; i<bitMaskCount; i++) {
				uint32 key_nomber = lastReportKey + 1;
				bool check_0 = ((bitMask[i] & BitMask_0) != 0);
				bool check_1 = ((bitMask[i] & BitMask_1) != 0);
				bool check_2 = ((bitMask[i] & BitMask_2) != 0);
				bool check_3 = ((bitMask[i] & BitMask_3) != 0);
				bool check_4 = ((bitMask[i] & BitMask_4) != 0);
				bool check_5 = ((bitMask[i] & BitMask_5) != 0);
				bool check_6 = ((bitMask[i] & BitMask_6) != 0);
				bool check_7 = ((bitMask[i] & BitMask_7) != 0);
				Log::Write( LogLevel_Info, GetNodeId(), "Key%d:%s, Key%d:%s, Key%d:%s, Key%d:%s, Key%d:%s, Key%d:%s, Key%d:%s, Key%d:%s", 
					key_nomber, (check_0 ? "Support":"Unsupported"), (key_nomber+1), (check_1 ? "Support":"Unsupported"), 
					(key_nomber+2), (check_2 ? "Support":"Unsupported"), (key_nomber+3), (check_3 ? "Support":"Unsupported"), 
					(key_nomber+4), (check_4 ? "Support":"Unsupported"), (key_nomber+5), (check_5 ? "Support":"Unsupported"), 
					(key_nomber+6), (check_6 ? "Support":"Unsupported"), (key_nomber+7), (check_7 ? "Support":"Unsupported"));
				if(check_0) AddSupportKey(key_nomber);
				if(check_1) AddSupportKey(key_nomber+1);
				if(check_2) AddSupportKey(key_nomber+2);
				if(check_3) AddSupportKey(key_nomber+3);
				if(check_4) AddSupportKey(key_nomber+4);
				if(check_5) AddSupportKey(key_nomber+5);
				if(check_6) AddSupportKey(key_nomber+6);
				if(check_7) AddSupportKey(key_nomber+7);
				lastReportKey += 8;
			}
			if (report_number == numReports) {
				if( Node* node = GetNodeUnsafe() ) {
					if (Value* m_value = node->GetValue(GetCommandClassId(), _instance, SimpleAVControlIndex_SetKey)) {
						node->RemoveValue(GetCommandClassId(), _instance, SimpleAVControlIndex_SetKey);
					}
					node->CreateValueList( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SimpleAVControlIndex_SetKey, "Press Key", "", false, true, 4, key_items, key_items[0].m_value, 0);
					scanned_key = true;
				}
			}
			return true;
		}
		default:
			break;
	}

	return false;
}

bool SimpleAVControl::SetValue( Value const& _value ) {
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case SimpleAVControlIndex_SetKey:
		{
			if (ValueList const* m_value = static_cast<ValueList const*>(&_value)) {
				uint16 keyCode = (uint16)m_value->GetItem().m_value;
				uint8 keyMSB = ((keyCode & 0xff00) >> 8);
				uint8 keyLSB = (keyCode & 0xff);
				Msg* msg = new Msg( "SimpleAVControlCmd_Set", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true);
				msg->SetInstance( this, instance);
				msg->Append( GetNodeId() );
				msg->Append( 8 );
				msg->Append( GetCommandClassId() );
				msg->Append( SimpleAVControlCmd_Set );
				msg->Append( (uint8)(sequence_number++) );
				msg->Append(0x00);//Key Down (0x01:Key Up, 0x02:Keep Alive)
				
				//Item ID needed with AV Content Directory Meta Data Command Class
				msg->Append(0);
				msg->Append(0);

				msg->Append(keyMSB);
				msg->Append(keyLSB);
				msg->Append( GetDriver()->GetTransmitOptions() );
				return SendSecureMsg(msg, Driver::MsgQueue_Send);
			}
		}
		case SimpleAVControlIndex_ReScan:
		{
			//if (ValueButton const* m_value = static_cast<ValueButton const*>(&_value)) {
				scanned_key = false;
				return RequestValue( 0, 0, instance, Driver::MsgQueue_Send);
			//}
		}
		case SimpleAVControlIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, SimpleAVControlIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}

void SimpleAVControl::RequestAllReports(uint8 const _instance, uint8 const _numReports) {
	if (numReports == 0 || lastReportId >= numReports)
		return;
	for (int i=0; i<_numReports; i++) {
		Msg* msg = new Msg( "SimpleAVControlCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance);
		msg->Append( GetNodeId() );
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( SimpleAVControlCmd_SupportedGet );
		msg->Append( i+1 );
		msg->Append( GetDriver()->GetTransmitOptions() );
		SendSecureMsg(msg, Driver::MsgQueue_Query);
	}
}

//-----------------------------------------------------------------------------
// <SimpleAVControl::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void SimpleAVControl::CreateVars
(
	uint8 const _instance
)
{
	if (Node* node = GetNodeUnsafe()) {
		node->CreateValueButton(ValueID::ValueGenre_Config, GetCommandClassId(), _instance, SimpleAVControlIndex_ReScan, "Key Scan", 0);
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, SimpleAVControlIndex_IconName, "SimpleAVControl Icon", "icon", false, false, "", 0);
	}
}

ValueList::Item SimpleAVControl::CreateItem(string _label, int32 _value)
{
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

void SimpleAVControl::AddSupportKey(uint32 keyValue) {
	if (keyValue == 0)
		return;
	if (keyValue < (sizeof(c_keyCommandLabel)/sizeof(c_keyCommandLabel[0]))){
		key_items.push_back(CreateItem( c_keyCommandLabel[keyValue], keyValue));
	} else {
		char temp[52];
		memset(temp, 0, sizeof(temp));
		snprintf(temp, sizeof(temp), "Reserved 0x%.2x", keyValue);
		key_items.push_back(CreateItem( temp, keyValue));
	}
}
