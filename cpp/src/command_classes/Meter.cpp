//-----------------------------------------------------------------------------
//
//	Meter.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_METER
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "Meter.h"
#include "MultiInstance.h"
#include "Defs.h"
#include "Bitfield.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueDecimal.h"
#include "ValueList.h"
#include "ValueButton.h"
#include "ValueInt.h"
#include "ValueBool.h"
#include "ValueString.h"

using namespace OpenZWave;

enum MeterCmd
{
	MeterCmd_Get				= 0x01,
	MeterCmd_Report				= 0x02,
	// Version 2
	MeterCmd_SupportedGet			= 0x03,
	MeterCmd_SupportedReport		= 0x04,
	MeterCmd_Reset				= 0x05
};

enum MeterType
{
	MeterType_Reserved = 0,
	MeterType_Electric,
	MeterType_Gas,
	MeterType_Water
};

enum RateType
{
	RateType_Reserved = 0,
	RateType_Import,
	RateType_Export,
	RateType_NotToBeUsed
};

enum
{
	//MeterIndex_Exporting = 32,
	MeterIndex_Reset = 249, 
	//MeterIndex_Importing,

	MeterIndex_IconName = 250
};

enum
{
	MeterReportProperties1_MeterTypeMask = 0x1f,
	MeterReportProperties1_RateTypeMask = 0x60,
	MeterReportProperties1_RateTypeShift = 0x05,
	MeterReportProperties1_ReservedBitMask = 0x80,
	MeterReportProperties1_SizeMask = 0x07,
	MeterReportProperties1_ScaleMask = 0x18,
	MeterReportProperties1_ScaleShift = 0x03,
	MeterReportProperties1_PrecisionMask = 0xe0,
	MeterReportProperties1_PrecisionShift = 0x05,

	MeterGetProperties1_ReservedMask = MeterReportProperties1_SizeMask,
	MeterGetProperties1_ScaleMask = MeterReportProperties1_ScaleMask,
	MeterGetProperties1_ScaleShift = MeterReportProperties1_ScaleShift,
	MeterGetProperties1_Reserved2Mask = MeterReportProperties1_PrecisionMask,
	MeterGetProperties1_Reserved2Shift = MeterReportProperties1_PrecisionShift,

	MeterSupportedReportProperties1_MeterTypeMask = 0x1f,
	MeterSupportedReportProperties1_ReservedMask = 0x60,
	MeterSupportedReportProperties1_ReservedShift = 0x05,
	MeterSupportedReportProperties1_MeterResetBitMask = 0x80,
	MeterSupportedReportProperties2_ScaleSupportedMask = 0x0f,
	MeterSupportedReportProperties2_ReservedMask = 0xf0,
	MeterSupportedReportProperties2_ReservedShift = 0x04,

	//Version 3
	MeterGetProperties1_ReservedMask_v3 = 0x07,
	MeterGetProperties1_ScaleMask_v3 = 0x38,
	MeterGetProperties1_ScaleShift_v3 = 0x03,
	MeterGetProperties1_Reserved2Mask_v3 = 0xc0,
	MeterGetProperties1_Reserved2Shift_v3 = 0x06,

	MeterReportProperties1_MeterTypeMask_v3 = MeterReportProperties1_MeterTypeMask,
	MeterReportProperties1_RateTypeMask_v3 = MeterReportProperties1_RateTypeMask,
	MeterReportProperties1_RateTypeShift_v3 = MeterReportProperties1_RateTypeShift,
	MeterReportProperties1_Scale2BitMask_v3 = 0x80,
	MeterReportProperties2_SizeMask_v3 = MeterReportProperties1_SizeMask,
	MeterReportProperties2_ScaleMask_v3 = MeterReportProperties1_ScaleMask,
	MeterReportProperties2_ScaleShift_v3 = MeterReportProperties1_ScaleShift,
	MeterReportProperties2_PrecisionMask_v3 = MeterReportProperties1_PrecisionMask,
	MeterReportProperties2_PrecisionShift_v3 = MeterReportProperties1_PrecisionShift,

	//Version 4
	MeterGetProperties1_ReservedMask_v4 = 0x07,
	MeterGetProperties1_ScaleMask_v4 = 0x38,
	MeterGetProperties1_ScaleShift_v4 = 0x03,
	MeterGetProperties1_Reserved2Mask_v4 = 0xc0,
	MeterGetProperties1_Reserved2Shift_v4 = 0x06,

	MeterSupportedReport_RateType_Reserved = 0x00,
	MeterSupportedReport_RateType_ImportOnly = 0x01,
	MeterSupportedReport_RateType_ExportOnly = 0x02,
	MeterSupportedReport_RateType_ImportAndExport = 0x03,
	MeterSupportedReportProperties2_MeterResetBitMask = 0x80,
	MeterSupportedReportProperties2_ScaleSupported0Mask = 0x7f,
	MeterSupportedReportProperties2_MSTBitMask = 0x80
};

static char const* c_meterTypes[] = 
{
	"Unknown",
	"Electric",
	"Gas",
	"Water"
};

static char const* c_electricityUnits[] = 
{
	"kWh",
	"kVAh",
	"W",
	"pulses",
	"V",
	"A",
	"Power Factor",
	"",
	"kVar",
	"kVarh",
	"", "", "", "", "", ""
};

static char const* c_gasUnits[] = 
{
	"cubic meters",
	"cubic feet",
	"",
	"pulses"
	"",
	"",
	"",
	"",
	"", "", "", "", "", "", "", ""
};

static char const* c_waterUnits[] = 
{
	"cubic meters",
	"cubic feet",
	"US gallons",
	"pulses"
	"",
	"",
	"",
	"",
	"", "", "", "", "", "", "", ""
};

static char const* c_electricityLabels[] = 
{
	"Energy",
	"Energy",
	"Power",
	"Count",
	"Voltage",
	"Current",
	"Power Factor",
	"Unknown",
	"Energy",//kVar
	"Energy",//kVarh
	"Unknown", "Unknown", "Unknown", "Unknown", "Unknown", "Unknown"
};

static char const* c_rateTypeLabels[] = 
{
	"Reserved",
	"Import(consumed)",
	"Export(produced)",
	"Not to be used"
};

//-----------------------------------------------------------------------------
// <Meter::Meter>												   
// Constructor
//-----------------------------------------------------------------------------
Meter::Meter
(
	uint32 const _homeId,
	uint8 const _nodeId
):
	CommandClass( _homeId, _nodeId ),
	m_scale( 0 )
{
	SetStaticRequest( StaticRequest_Values );
	m_extendRequestList.clear();
}

//-----------------------------------------------------------------------------
// <Meter::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool Meter::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if( GetVersion() > 1 )
	{
		if( _requestFlags & RequestFlag_Static )
		{
 			Msg* msg = new Msg( "MeterCmd_SupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			msg->SetInstance( this, _instance );
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( MeterCmd_SupportedGet );
			msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(msg, _queue);
			res = true;
		}
	}

	if( _requestFlags & RequestFlag_Dynamic )
	{
		res |= RequestValue( _requestFlags, 0, _instance, _queue );

		if (GetVersion() == 1) {
			res |= RequestValue( _requestFlags, 0, _instance, _queue );
		} else if (GetVersion() >= 4) {
			for( uint8 i=0; i<16; ++i ) {
				if (i == 7)
					continue;
				uint8 baseIndex = i<<3;
				res |= RequestValue( _requestFlags, baseIndex, _instance, _queue );
			}
		} else {
			for( uint8 i=0; i<8; ++i ) {
				uint8 baseIndex = i<<3;
				res |= RequestValue( _requestFlags, baseIndex, _instance, _queue );
			}
		}
	}

	return res;
}

//-----------------------------------------------------------------------------
// <Meter::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool Meter::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _index,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	bool res = false;
	if (GetVersion() == 1) {
		Msg* msg = new Msg( "MeterCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( MeterCmd_Get );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	} else if (GetVersion() >= 4) {
		if (_index%8 == 0) {
			uint8 i = _index>>3;
			uint8 baseIndex = _index;
			bool exporting = false;
			bool importing = false;
			if (ValueList* rateType_list = static_cast<ValueList*>(GetValue(_instance, baseIndex+1))) {
				if (rateType_list->GetItemIdxByValue(RateType_Import) >= 0)
					importing = true;
				if (rateType_list->GetItemIdxByValue(RateType_Export) >= 0)
					exporting = true;
				rateType_list->Release();
			}
			if (exporting || importing) {
				Value* value = GetValue( _instance, baseIndex );
				if( value != NULL ) {
					value->Release();
					if (importing)
						res |= SendMeterGet_v4(i, RateType_Import, _instance, _queue);
					if (exporting)
						res |= SendMeterGet_v4(i, RateType_Export, _instance, _queue);
				}
			}
		}

		/*for( uint8 i=0; i<16; ++i ) {
			if (i == 7)
				continue;

			uint8 baseIndex = i<<3;
			bool exporting = false;
			bool importing = false;
			if (ValueList* rateType_list = static_cast<ValueList*>(GetValue(_instance, baseIndex+1))) {
				if (rateType_list->GetItemIdxByValue(RateType_Import) >= 0)
					importing = true;
				if (rateType_list->GetItemIdxByValue(RateType_Export) >= 0)
					exporting = true;
				rateType_list->Release();
			}
			if (!exporting && !importing)
				continue;
			Value* value = GetValue( _instance, baseIndex );
			if( value != NULL ) {
				value->Release();
				if (importing)
					res |= SendMeterGet_v4(i, RateType_Import, _instance, _queue);
				if (exporting)
					res |= SendMeterGet_v4(i, RateType_Export, _instance, _queue);
			}
		}*/
	} else {
		if (_index%8 == 0) {
			uint8 i = _index>>3;
			uint8 baseIndex = _index;
			Value* value = GetValue( _instance, baseIndex );
			if( value != NULL )
			{
				value->Release();
				Msg* msg = new Msg( "MeterCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->SetInstance( this, _instance );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( MeterCmd_Get );
				msg->Append( (i<<MeterGetProperties1_ScaleShift_v3) );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, _queue);
				res |= true;
			}
		}

		/*for( uint8 i=0; i<8; ++i ) {
			uint8 baseIndex = i<<3;
			Value* value = GetValue( _instance, baseIndex );
			if( value != NULL )
			{
				value->Release();
				Msg* msg = new Msg( "MeterCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
				msg->SetInstance( this, _instance );
				msg->Append( GetNodeId() );
				msg->Append( 3 );
				msg->Append( GetCommandClassId() );
				msg->Append( MeterCmd_Get );
				msg->Append( (i<<MeterGetProperties1_ScaleShift_v3) );
				msg->Append( GetDriver()->GetTransmitOptions() );
				SendSecureMsg(msg, _queue);
				res |= true;
			}
		}*/
	}
	return res;
}

//-----------------------------------------------------------------------------
// <Meter::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool Meter::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	bool handled = false;
	if (MeterCmd_Report == (MeterCmd)_data[0])
	{
		handled = HandleReport( _data, _length, _instance );
	}
	else if (MeterCmd_SupportedReport == (MeterCmd)_data[0])
	{
		handled = HandleSupportedReport( _data, _length, _instance );
	}

	return handled;
}

//-----------------------------------------------------------------------------
// <Meter::HandleSupportedReport>
// Create the values for this command class based on the reported parameters
//-----------------------------------------------------------------------------
bool Meter::HandleSupportedReport
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance
)
{
	bool canReset = ((_data[1] & MeterSupportedReportProperties1_MeterResetBitMask) != 0);
	MeterType meterType = (MeterType)(_data[1] & MeterReportProperties1_MeterTypeMask);
	RateType rateType = (RateType)((_data[1] & MeterReportProperties1_RateTypeMask)>>MeterReportProperties1_RateTypeShift);

	ClearStaticRequest( StaticRequest_Version );

	if( Node* node = GetNodeUnsafe() )
	{
		string msg;
		string valueLabel;

		if ((uint8)(_data[1]+1) > sizeof(c_meterTypes))
			msg = c_meterTypes[MeterType_Reserved];
		else
			msg = c_meterTypes[meterType];
		msg += ": ";
		// Create the list of supported scales
		uint8 scaleSupported = (_data[2]&MeterSupportedReportProperties2_ScaleSupported0Mask);
		bool _MST = false;
		uint8 byteToFollow = 0;
		if( GetVersion() == 2 )
		{
			// Only four scales are allowed in version 2
			scaleSupported &= 0x0f;
		}

		for( uint8 i=0; i<8; ++i )
		{
			if (i==7) {
				_MST = ((_data[2]&MeterSupportedReportProperties2_MSTBitMask) != 0);
				if (GetVersion() >= 4 && _MST) {
					byteToFollow = _data[3];
					if (byteToFollow > 0) {
						uint8 const* extra_scale = &_data[4];
						for (int j=0; j<byteToFollow; i++) {
							for (int k=0; k<8; k++) {
								if (extra_scale[j] & (1<<j)) {
									uint8 index = (j+1)*8 + k;
									uint8 baseIndex = index<<3;
									switch( meterType ) {
										case MeterType_Electric:
										{
											if (index >= (sizeof(c_electricityLabels)/sizeof(c_electricityLabels[0])))
												break;

											if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
												value->SetLabel( c_electricityLabels[index] );
												value->SetUnits( c_electricityUnits[index] );
												value->Release();
											} else {
												node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_electricityLabels[index], c_electricityUnits[index], true, false, "0.0", 0 );
											}
											msg += ", ";
											msg += c_electricityUnits[index];
											CreateRateType(baseIndex, rateType, _instance);
											break;
										}
										case MeterType_Gas:
										{
											if (index >= (sizeof(c_gasUnits)/sizeof(c_gasUnits[0])))
												break;

											if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
												value->SetLabel( c_meterTypes[MeterType_Gas] );
												value->SetUnits( c_gasUnits[index] );
												value->Release();
											} else {
												node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_meterTypes[meterType], c_gasUnits[index], true, false, "0.0", 0 );
											}
											msg += ", ";
											msg += c_gasUnits[index];
											CreateRateType(baseIndex, rateType, _instance);
											break;
										}
										case MeterType_Water:
										{
											if (index >= (sizeof(c_waterUnits)/sizeof(c_waterUnits[0])))
												break;

											if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
												value->SetLabel( c_meterTypes[MeterType_Water] );
												value->SetUnits( c_waterUnits[index] );
												value->Release();
											} else {
												node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_meterTypes[meterType], c_waterUnits[index], true, false, "0.0", 0 );
											}
											msg += ", ";
											msg += c_waterUnits[index];
											CreateRateType(baseIndex, rateType, _instance);
											break;
										}
										default:
											break;
									}
								}
							}
						}
					}
				}
			} else if( scaleSupported & (1<<i) ) {
				uint8 baseIndex = i<<3;		// We leave space between the value indices to insert previous and time delta values if we need to later on.
				switch( meterType )
				{
					case MeterType_Electric:
					{
						if (i >= (sizeof(c_electricityLabels)/sizeof(c_electricityLabels[0])))
							break;

						if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
							value->SetLabel( c_electricityLabels[i] );
							value->SetUnits( c_electricityUnits[i] );
							value->Release();
						} else {
							node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_electricityLabels[i], c_electricityUnits[i], true, false, "0.0", 0 );
						}
						if( i != 0 )
							msg += ", ";
						msg += c_electricityUnits[i];
						CreateRateType(baseIndex, rateType, _instance);
						break;
					}
					case MeterType_Gas:
					{
						if (i >= (sizeof(c_gasUnits)/sizeof(c_gasUnits[0])))
							break;

						if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
							value->SetLabel( c_meterTypes[MeterType_Gas] );
							value->SetUnits( c_gasUnits[i] );
							value->Release();
						} else {
							node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_meterTypes[meterType], c_gasUnits[i], true, false, "0.0", 0 );
						}
						if( i != 0 )
							msg += ", ";
						msg += c_gasUnits[i];
						CreateRateType(baseIndex, rateType, _instance);
						break;
					}
					case MeterType_Water:
					{
						if (i >= (sizeof(c_waterUnits)/sizeof(c_waterUnits[0])))
							break;

						if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, baseIndex ) ) ) {
							value->SetLabel( c_meterTypes[MeterType_Water] );
							value->SetUnits( c_waterUnits[i] );
							value->Release();
						} else {
							node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex, c_meterTypes[meterType], c_waterUnits[i], true, false, "0.0", 0 );
						}
						if( i != 0 )
							msg += ", ";
						msg += c_waterUnits[i];
						CreateRateType(baseIndex, rateType, _instance);
						break;
					}
					default:
					{
						break;
					}
				}
			}
		}

		// Create the reset button
		if( canReset )
			node->CreateValueButton( ValueID::ValueGenre_System, GetCommandClassId(), _instance, MeterIndex_Reset, "Reset", 0 );

		Log::Write( LogLevel_Info, GetNodeId(), "Received Meter supported report from node %d, %s", GetNodeId(), msg.c_str() );
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Meter::HandleReport>
// Read the reported meter value
//-----------------------------------------------------------------------------
bool Meter::HandleReport
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance
)
{
	// Get the value and scale
	uint8 scale;
	uint8 precision = 0;
	string valueStr = ExtractValue( &_data[2], &scale, &precision );

	if( GetVersion() == 1 )
	{
		// In version 1, we don't know the scale until we get the first value report
		string label;
		string units;

		switch( (MeterType)(_data[1] & 0x1f) )
		{ 
			case MeterType_Electric:
			{
				// Electricity Meter
				label = c_electricityLabels[scale];
				units = c_electricityUnits[scale];
				break;
			}
			case MeterType_Gas:
			{
				// Gas Meter
				label = c_meterTypes[MeterType_Gas];
				units = c_gasUnits[scale];
				break;
			}
			case MeterType_Water:
			{
				// Water Meter
				label = c_meterTypes[MeterType_Water];
				units = c_waterUnits[scale];
				break;
			}
			default:
			{
				break;
			}
		}

		if( ValueDecimal* value = static_cast<ValueDecimal*>( GetValue( _instance, 0 ) ) )
		{
			Log::Write( LogLevel_Info, GetNodeId(), "Received Meter report from node %d: %s=%s%s", GetNodeId(), label.c_str(), valueStr.c_str(), units.c_str() );
			value->SetLabel( label );
			value->SetUnits( units );
			value->OnValueRefreshed( valueStr );
			if( value->GetPrecision() != precision )
			{
				value->SetPrecision( precision );
			}
			value->Release();
		}
	}
	else
	{
		uint8 scale2 = 0;
		uint8 size = _data[2] & 0x07;
		uint8 rate_type = ((_data[1] & MeterReportProperties1_RateTypeMask)>>MeterReportProperties1_RateTypeShift);
		// Version 2 and above
		uint8 baseIndex = scale << 3;

		if (GetVersion() == 3) {
			// In version 3, an extra scale bit is stored in the meter type byte.
			scale |= ((_data[1]&0x80)>>5);
			baseIndex = scale << 3;
		} else if (GetVersion() >= 4) {
			// Version 4 and above
			scale |= ((_data[1]&0x80)>>5);
			if (scale == 0x07) {
				if (_length != (size*2 + 7))
					Log::Write(LogLevel_Warning, GetNodeId(), "Meter report length invalid. expected=%d, received=%d.", (size*2 + 7), _length);
				scale2 = _data[size*2 + 5];
				baseIndex = (scale2+8) << 3;
			} else {
				baseIndex = scale << 3;
			}
		}

		HandleExtendRequest(_instance, baseIndex);

		if (ValueDecimal* value = static_cast<ValueDecimal*>( GetValue(_instance, baseIndex) )) {
			value->OnValueRefreshed(valueStr);
			if(value->GetPrecision() != precision)
				value->SetPrecision(precision);
			value->Release();
			Log::Write( LogLevel_Info, GetNodeId(), "Received Meter report from node %d: %s%s=%s%s", GetNodeId(), c_rateTypeLabels[rate_type], value->GetLabel().c_str(), valueStr.c_str(), value->GetUnits().c_str() );

			if (ValueList* rateType = static_cast<ValueList*>( GetValue(_instance, baseIndex+1) )) {
				rateType->OnValueRefreshed(rate_type);
				rateType->Release();
			}

			uint16 delta = (uint16)((_data[3+size]<<8) | _data[4+size]);
			if (delta) {
				// There is only a previous value if the time delta is non-zero
				ValueDecimal* previous = static_cast<ValueDecimal*>( GetValue(_instance, baseIndex+2) );
				if (previous == NULL) {
					if (Node* node = GetNodeUnsafe()) {
						node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex+2, "Previous Reading", value->GetUnits().c_str(), true, false, "0.0", 0 );
						previous = static_cast<ValueDecimal*>( GetValue(_instance, baseIndex+2) );
					}
				}
				if(previous) {
					precision = 0;
					valueStr = ExtractValue(&_data[2], &scale, &precision, 3+size);
					Log::Write( LogLevel_Info, GetNodeId(), "    Previous value was %s%s, received %d seconds ago.", valueStr.c_str(), previous->GetUnits().c_str(), delta );
					previous->OnValueRefreshed(valueStr);
					if(previous->GetPrecision() != precision)
						previous->SetPrecision(precision);
					previous->Release();
				}
				// Time delta
				ValueInt* interval = static_cast<ValueInt*>( GetValue(_instance, baseIndex+3) );
				if(interval == NULL) {
					// We need to create a value to hold the time delta
					if( Node* node = GetNodeUnsafe() ) {
						node->CreateValueInt( ValueID::ValueGenre_User, GetCommandClassId(), _instance, baseIndex+3, "Interval", "seconds", true, false, 0, 0 );
						interval = static_cast<ValueInt*>( GetValue(_instance, baseIndex+3) );
					}
				}
				if(interval) {
					interval->OnValueRefreshed((int32)delta);
					interval->Release();
				}
			}
		}
	}
 
	return true;
}

//-----------------------------------------------------------------------------
// <Meter::SetValue>
// Set the device's scale, or reset its accumulated values.
//-----------------------------------------------------------------------------
bool Meter::SetValue
(
	Value const& _value
)
{
	uint8 instance = _value.GetID().GetInstance();
	if( MeterIndex_Reset == _value.GetID().GetIndex() )
	{
		ValueButton const* button = static_cast<ValueButton const*>(&_value);
		if( button->IsPressed() )
		{
			Msg* msg = new Msg( "MeterCmd_Reset", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true );
			msg->SetInstance( this, _value.GetID().GetInstance() ); 
			msg->Append( GetNodeId() );
			msg->Append( 2 );
			msg->Append( GetCommandClassId() );
			msg->Append( MeterCmd_Reset );
			msg->Append( GetDriver()->GetTransmitOptions() );
			return SendSecureMsg(msg, Driver::MsgQueue_Send);
		}
	}

	if (_value.GetID().GetIndex() == MeterIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, MeterIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <Meter::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void Meter::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
		node->CreateValueDecimal( ValueID::ValueGenre_User, GetCommandClassId(), _instance, 0, "Unknown", "", true, false, "0.0", 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, MeterIndex_IconName, "Meter Icon", "icon", false, false, "", 0);
	}
}

//-----------------------------------------------------------------------------
// <Meter::CreateRateType>
// Create the rate type
//-----------------------------------------------------------------------------
void Meter::CreateRateType(const uint8 baseIndex, const uint8 rateType, const uint32 _instance) {
	Node* node = GetNodeUnsafe();
	if (!node)
		return;
	uint8 index = baseIndex + 1;

	if(ValueList* value = static_cast<ValueList*>( GetValue(_instance, index) )) {
		value->Release();
		return;
	}

	if (GetVersion() >= 4) {
		bool importing = false;
		bool exporting = false;
		exporting = ((rateType & MeterSupportedReport_RateType_ExportOnly) != 0);
		importing = ((rateType & MeterSupportedReport_RateType_ImportOnly) != 0);
		vector<ValueList::Item> _items;
		_items.clear();
		if (importing) {
			_items.push_back(CreateItem(c_rateTypeLabels[RateType_Import], RateType_Import));
		}
		if (exporting) {
			_items.push_back(CreateItem(c_rateTypeLabels[RateType_Export], RateType_Export));
		}
		if (importing || exporting) {
			node->CreateValueList(ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, 
				"Rate Type", "", true, false, 1, _items, _items[0].m_value, 0);
		} else 
			_items.clear();
	} else {
		vector<ValueList::Item> _items;
		_items.clear();
		_items.push_back(CreateItem(c_rateTypeLabels[RateType_Reserved], RateType_Reserved));
		_items.push_back(CreateItem(c_rateTypeLabels[RateType_Import], RateType_Import));
		_items.push_back(CreateItem(c_rateTypeLabels[RateType_Export], RateType_Export));
		_items.push_back(CreateItem(c_rateTypeLabels[RateType_NotToBeUsed], RateType_NotToBeUsed));
		node->CreateValueList(ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, 
			"Rate Type", "", true, false, 1, _items, _items[0].m_value, 0);
	}
}

//-----------------------------------------------------------------------------
// <Meter::CreateItem>
// Create the item
//-----------------------------------------------------------------------------
ValueList::Item Meter::CreateItem(string _label, int32 _value) {
	ValueList::Item m_item;
	m_item.m_label = _label;
	m_item.m_value = _value;
	return m_item;
}

//-----------------------------------------------------------------------------
// <Meter::SendMeterGet_v4>
// Send meter get for version 4.
//-----------------------------------------------------------------------------
bool Meter::SendMeterGet_v4(uint8 const _index, uint8 const _rateType, uint8 const _instance, Driver::MsgQueue const _queue) {
	Msg* msg = new Msg( "MeterCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	if (_index > 7) {
		//this has scale 2
		uint8 param_1 = ((_rateType<<5) | (0x07<<MeterGetProperties1_ScaleShift_v4));
		uint8 param_2 = (_index-8);
		msg->Append( 4 );
		msg->Append( GetCommandClassId() );
		msg->Append( MeterCmd_Get );
		msg->Append( param_1 );
		msg->Append( param_2 );
	} else {
		uint8 param_1 = ((_rateType<<5) | (uint8)(_index<<MeterGetProperties1_ScaleShift_v4));
		msg->Append( 3 );
		msg->Append( GetCommandClassId() );
		msg->Append( MeterCmd_Get );
		msg->Append( param_1 );
	}
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

void Meter::HandleExtendRequest(uint32 const _instance, uint16 const _index)
{
	if (_instance == 0)
		return;
	Node* _node = GetNodeUnsafe();
	for (list<ExtendRequest>::iterator it = m_extendRequestList.begin(); it != m_extendRequestList.end(); it++) {
		if (it->m_instance == _instance && it->m_index == _index) {
			for (list<ExtendRequest_Target>::iterator it_s = it->m_targetList.begin(); it_s != it->m_targetList.end(); it_s++) {
				if (CommandClass* targetClass = _node->GetCommandClass((uint8)it_s->class_id)) {
					targetClass->RequestValue(0, (uint8)it_s->index, it_s->instance, Driver::MsgQueue_Query);
				}
			}
			return;
		}
	}
}

//-----------------------------------------------------------------------------
// <Meter::ReadXML>
// Read configuration
//-----------------------------------------------------------------------------
void Meter::ReadXML( TiXmlElement const* _ccElement )
{
	CommandClass::ReadXML( _ccElement );

	TiXmlElement const* child = _ccElement->FirstChildElement("ExtendRequest");
	while(child) {
		int src_index;
		int src_instance;
		if (TIXML_SUCCESS == child->QueryIntAttribute("index", &src_index) &&
			TIXML_SUCCESS == child->QueryIntAttribute("instance", &src_instance)) {
				ExtendRequest _extRequest;
				_extRequest.m_index = src_index;
				_extRequest.m_instance = src_instance;
				TiXmlElement const* _target = child->FirstChildElement("Target");
				while (_target) {
					int dst_index;
					int dst_instance;
					int dst_class;
					if (TIXML_SUCCESS == _target->QueryIntAttribute("index", &dst_index) && 
						TIXML_SUCCESS == _target->QueryIntAttribute("instance", &dst_instance) && 
						TIXML_SUCCESS == _target->QueryIntAttribute("class_id", &dst_class)) {
							_extRequest.AddTarget(dst_class, dst_index, dst_instance);
					}
					_target = _target->NextSiblingElement("Target");
				}
				if (_extRequest.target_count() > 0)
					m_extendRequestList.push_back(_extRequest);
		}
		child = child->NextSiblingElement("ExtendRequest");
	}
}

//-----------------------------------------------------------------------------
// <Meter::WriteXML>
// Save configuration
//-----------------------------------------------------------------------------
void Meter::WriteXML( TiXmlElement* _ccElement )
{
	CommandClass::WriteXML( _ccElement );

	if (m_extendRequestList.size() > 0) {
		for (list<ExtendRequest>::iterator it = m_extendRequestList.begin(); it != m_extendRequestList.end(); it++) {
			if (it->target_count() > 0) {
				TiXmlElement* extElement = new TiXmlElement("ExtendRequest");
				_ccElement->LinkEndChild(extElement);
				extElement->SetAttribute("index", it->m_index);
				extElement->SetAttribute("instance", it->m_instance);
				for (list<ExtendRequest_Target>::iterator it_s = it->m_targetList.begin(); it_s != it->m_targetList.end(); it_s++) {
					TiXmlElement* targetElement = new TiXmlElement("Target");
					extElement->LinkEndChild(targetElement);
					targetElement->SetAttribute("class_id", it_s->class_id);
					targetElement->SetAttribute("index", it_s->index);
					targetElement->SetAttribute("instance", it_s->instance);
				}
			}
		}
	}
}
