//-----------------------------------------------------------------------------
//
//	MeterPulse.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_METER_PULSE
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "MeterPulse.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueInt.h"
#include "ValueString.h"

using namespace OpenZWave;

enum MeterPulseCmd
{
	MeterPulseCmd_Get		= 0x04,
	MeterPulseCmd_Report	= 0x05
};

enum
{
	MeterPulseIndex_Count = 0,
	MeterPulseIndex_IconName = 99
};

//-----------------------------------------------------------------------------
// <MeterPulse::RequestState>												   
// Request current state from the device									   
//-----------------------------------------------------------------------------
bool MeterPulse::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}

	return false;
}

//-----------------------------------------------------------------------------
// <MeterPulse::RequestValue>												   
// Request current value from the device									   
//-----------------------------------------------------------------------------
bool MeterPulse::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	Msg* msg = new Msg( "MeterPulseCmd_Get", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( MeterPulseCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <MeterPulse::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool MeterPulse::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( MeterPulseCmd_Report == (MeterPulseCmd)_data[0] )
	{
		int32 count = 0;
		for( uint8 i=0; i<4; ++i )
		{
			count <<= 8;
			count |= (uint32)_data[i+1];
		}

		Log::Write( LogLevel_Info, GetNodeId(), "Received a meter pulse count: Count=%d", count );
		if( ValueInt* value = static_cast<ValueInt*>( GetValue( _instance, MeterPulseIndex_Count ) ) )
		{
			value->OnValueRefreshed( count );
			value->Release();
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <MeterPulse::SetValue>
// Set the device's icon.
//-----------------------------------------------------------------------------
bool MeterPulse::SetValue (Value const& _value) {
	uint8 instance = _value.GetID().GetInstance();
	if (_value.GetID().GetIndex() == MeterPulseIndex_IconName) {
		if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
			string icon = m_value->GetValue();
			if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, MeterPulseIndex_IconName))) {
				m_icon->OnValueRefreshed(icon);
				m_icon->Release();
			}
		}
		return false;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <MeterPulse::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void MeterPulse::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueInt( ValueID::ValueGenre_User, GetCommandClassId(), _instance, MeterPulseIndex_Count, "Count", "", true, false, 0, 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, MeterPulseIndex_IconName, "MeterPulse Icon", "icon", false, false, "", 0);
	}
}



