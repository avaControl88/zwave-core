//-----------------------------------------------------------------------------
//
//	ThermostatOperatingState.cpp
//
//	Implementation of the Z-Wave COMMAND_CLASS_THERMOSTAT_OPERATING_STATE
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "CommandClasses.h"
#include "ThermostatOperatingState.h"
#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Driver.h"
#include "Log.h"

#include "ValueString.h"

using namespace OpenZWave;

enum ThermostatOperatingStateCmd
{
	ThermostatOperatingStateCmd_Get				= 0x02,
	ThermostatOperatingStateCmd_Report			= 0x03,

	//Version 2
	ThermostatOperatingStateCmd_LoggingSupportedGet = 0x01,
	ThermostatOperatingStateCmd_LoggingSupportedReport = 0x04,
	ThermostatOperatingStateCmd_LoggingGet = 0x05,
	ThermostatOperatingStateCmd_LoggingReport = 0x06
};

enum {
	ThermostatOperatingState_ReportProperties1_OperatingStateMask = 0x0f,
};

enum {
	ThermostatOperatingStateIndex_State = 230,
	ThermostatOperatingStateIndex_IconName = 240
};

static char const* c_stateName[] = 
{
	"Idle",
	"Heating",
	"Cooling",
	"Fan Only",
	"Pending Heat",
	"Pending Cool",
	"Vent/Economizer",
	"Aux Heating",
	"2nd Stage Heating", 
	"2nd Stage Cooling", 
	"2nd Stage Aux Heat", 
	"3rd Stage Aux Heat", 
	"State 12", 
	"State 13", 
	"State 14", 
	"State 15"
};

//-----------------------------------------------------------------------------
// <ThermostatOperatingState::RequestState>
// Get the static thermostat mode details from the device
//-----------------------------------------------------------------------------
bool ThermostatOperatingState::RequestState
(
	uint32 const _requestFlags,
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	if( _requestFlags & RequestFlag_Dynamic )
	{
		return RequestValue( _requestFlags, 0, _instance, _queue );
	}
	if ( (_requestFlags & RequestFlag_Static) && GetVersion() >= 2 ) {
		Msg* msg = new Msg( "ThermostatOperatingStateCmd_LoggingSupportedGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
		msg->SetInstance( this, _instance );
		msg->Append( GetNodeId() );
		msg->Append( 2 );
		msg->Append( GetCommandClassId() );
		msg->Append( ThermostatOperatingStateCmd_LoggingSupportedGet );
		msg->Append( GetDriver()->GetTransmitOptions() );
		return SendSecureMsg(msg, _queue);
	}
	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatOperatingState::RequestValue>
// Get a thermostat mode value from the device
//-----------------------------------------------------------------------------
bool ThermostatOperatingState::RequestValue
(
	uint32 const _requestFlags,
	uint8 const _dummy1,	// = 0 (not used)
	uint8 const _instance,
	Driver::MsgQueue const _queue
)
{
	Msg* msg = new Msg( "Request Current Thermostat Operating State", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
	msg->SetInstance( this, _instance );
	msg->Append( GetNodeId() );
	msg->Append( 2 );
	msg->Append( GetCommandClassId() );
	msg->Append( ThermostatOperatingStateCmd_Get );
	msg->Append( GetDriver()->GetTransmitOptions() );
	return SendSecureMsg(msg, _queue);
}

//-----------------------------------------------------------------------------
// <ThermostatOperatingState::HandleMsg>
// Handle a message from the Z-Wave network
//-----------------------------------------------------------------------------
bool ThermostatOperatingState::HandleMsg
(
	uint8 const* _data,
	uint32 const _length,
	uint32 const _instance	// = 1
)
{
	if( ThermostatOperatingStateCmd_Report == (ThermostatOperatingStateCmd)_data[0] )
	{
		// We have received the thermostat operating state from the Z-Wave device
		if( ValueString* valueString = static_cast<ValueString*>( GetValue( _instance, ThermostatOperatingStateIndex_State ) ) )
		{
			valueString->OnValueRefreshed( c_stateName[_data[1]&ThermostatOperatingState_ReportProperties1_OperatingStateMask] );
			valueString->Release();
			Log::Write( LogLevel_Info, GetNodeId(), "Received thermostat operating state: %s", valueString->GetValue().c_str() );		
		}
		return true;
	} else if (ThermostatOperatingStateCmd_LoggingSupportedReport == (ThermostatOperatingStateCmd)_data[0]) {
		if( Node* node = GetNodeUnsafe() )
		{
			string msg = "";
			for( uint8 i = 1; i <= ( _length - 2 ); i++ )
			{
				for( uint8 j = 0; j < 8; j++ )
				{
					if( _data[i] & ( 1 << j ) )
					{
						if( msg != "" )
							msg += ", ";
						uint8 index = ( ( i - 1 ) * 8 ) + j;
						if ( index < (sizeof(c_stateName)/sizeof(c_stateName[0])) )
							msg += c_stateName[index];
						else
							break;
						ValueString* value = static_cast<ValueString*>( GetValue( _instance, index ) );
						if( value == NULL) {
							node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, c_stateName[index], "", true, false, "", 0);
						} else
							value->Release();
					}
				}
			}
			Log::Write( LogLevel_Info, GetNodeId(), "Received ThermostatOperatingStateCmd_LoggingSupportedReport: %s.", msg.c_str());

			Msg* _msg = new Msg( "ThermostatOperatingStateCmd_LoggingGet", GetNodeId(), REQUEST, FUNC_ID_ZW_SEND_DATA, true, true, FUNC_ID_APPLICATION_COMMAND_HANDLER, GetCommandClassId() );
			_msg->SetInstance( this, _instance );
			_msg->Append( GetNodeId() );
			_msg->Append( _length );
			_msg->Append( GetCommandClassId() );
			_msg->Append( ThermostatOperatingStateCmd_LoggingGet );
			for (uint32 i=1; i<=(_length - 2); i++) {
				_msg->Append(_data[i]);
			}
			_msg->Append( GetDriver()->GetTransmitOptions() );
			SendSecureMsg(_msg, Driver::MsgQueue_Send);
		}
		return true;
	} else if (ThermostatOperatingStateCmd_LoggingReport == (ThermostatOperatingStateCmd)_data[0]) {
		uint8 reportToFollow = _data[1];
		uint8 const* _type = &_data[2];
		uint8 const data_length = _length-3;
		Node* node = GetNodeUnsafe();
		if (!node)
			return false;

		for (int i=0; i<(data_length-4); i++) {
			uint8 index = (_type[i] & ThermostatOperatingState_ReportProperties1_OperatingStateMask);
			if (index < (sizeof(c_stateName)/sizeof(c_stateName[0])) ) {
				ValueString* value = static_cast<ValueString*>( GetValue( _instance, index ) );
				if (value == NULL) {
					node->CreateValueString(ValueID::ValueGenre_User, GetCommandClassId(), _instance, index, c_stateName[index], "", true, false, "", 0);
					value = static_cast<ValueString*>( GetValue( _instance, index ) );
				}
				char temp_chr[256];
				memset(temp_chr, 0, sizeof(temp_chr));
				snprintf(temp_chr, sizeof(temp_chr), "Today: %.2d:%.2d, Yesterday: %.2d:%.2d.", 
					_type[i+1], _type[i+2], _type[i+3], _type[i+4]);
				string msg = temp_chr;
				value->OnValueRefreshed(msg);
				value->Release();
			} else
				break;
		}
		Log::Write( LogLevel_Info, GetNodeId(), "Received ThermostatOperatingStateCmd_LoggingReport: reportToFollow=%d.", reportToFollow);
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// <ThermostatOperatingState::CreateVars>
// Create the values managed by this command class
//-----------------------------------------------------------------------------
void ThermostatOperatingState::CreateVars
(
	uint8 const _instance
)
{
	if( Node* node = GetNodeUnsafe() )
	{
	  	node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatOperatingStateIndex_State, 
			"Operating State", "", true, false, c_stateName[0], 0 );
		node->CreateValueString( ValueID::ValueGenre_User, GetCommandClassId(), _instance, ThermostatOperatingStateIndex_IconName, 
			"ThermostatOperatingState Icon", "icon", false, false, "", 0);
	}
}

bool ThermostatOperatingState::SetValue( Value const& _value ) {
	uint8 instance = _value.GetID().GetInstance();
	switch(_value.GetID().GetIndex()) {
		case ThermostatOperatingStateIndex_IconName:
		{
			if (ValueString const* m_value = static_cast<ValueString const*>(&_value)) {
				string icon = m_value->GetValue();
				if (ValueString* m_icon = static_cast<ValueString*>( GetValue(instance, ThermostatOperatingStateIndex_IconName))) {
					m_icon->OnValueRefreshed(icon);
					m_icon->Release();
				}
			}
			return false;
		}
		default:
			break;
	}
	return false;
}
