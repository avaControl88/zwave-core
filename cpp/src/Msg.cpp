//-----------------------------------------------------------------------------
//
//	Msg.cpp
//
//	Represents a Z-Wave message
//
//	Copyright (c) 2010 Mal Lansell <openzwave@lansell.org>
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//
//	SOFTWARE NOTICE AND LICENSE
//
//	This file is part of OpenZWave.
//
//	OpenZWave is free software: you can redistribute it and/or modify
//	it under the terms of the GNU Lesser General Public License as published
//	by the Free Software Foundation, either version 3 of the License,
//	or (at your option) any later version.
//
//	OpenZWave is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public License
//	along with OpenZWave.  If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

#include "Defs.h"
#include "Msg.h"
#include "Node.h"
#include "Log.h"
#include "MultiInstance.h"
#include "CRC16Encap.h"
#include "Security.h"

using namespace OpenZWave;

uint8 Msg::s_nextCallbackId = 1;


//-----------------------------------------------------------------------------
// <Msg::Msg>
// Constructor
//-----------------------------------------------------------------------------
Msg::Msg
( 
	string const& _logText,
	uint8 _targetNodeId,
	uint8 const _msgType,
	uint8 const _function,
	bool const _bCallbackRequired,
	bool const _bReplyRequired,			// = true
	uint8 const _expectedReply,			// = 0
	uint8 const _expectedCommandClassId	// = 0
):
	m_logText( _logText ),
	m_bFinal( false ),
	m_bCallbackRequired( _bCallbackRequired ),
	m_callbackId( 0 ),
	m_expectedReply( 0 ),
	m_expectedCommandClassId( _expectedCommandClassId ),
	m_length( 4 ),
	m_targetNodeId( _targetNodeId ),
	m_sendAttempts( 0 ),
	m_maxSendAttempts( MAX_TRIES ),
	m_instance( 1 ),
	m_endPoint( 0 ),
	m_flags( 0 ),
	m_functionId( _function )
{
	if( _bReplyRequired )
	{
		// Wait for this message before considering the transaction complete 
		m_expectedReply = _expectedReply ? _expectedReply : _function;
	}

	m_buffer[0] = SOF;
	m_buffer[1] = 0;					// Length of the following data, filled in during Finalize.
	m_buffer[2] = _msgType;
	m_buffer[3] = _function;
}

//-----------------------------------------------------------------------------
// <Msg::SetInstance>
// Used to enable wrapping with MultiInstance/MultiChannel during finalize.
//-----------------------------------------------------------------------------
void Msg::SetInstance
(
	CommandClass* _cc,
	uint8 const _instance
)
{
	// Determine whether we should encapsulate the message in MultiInstance or MultiCommand
	if( Node* node = _cc->GetNodeUnsafe() )
	{
		MultiInstance* micc = static_cast<MultiInstance*>( node->GetCommandClass( MultiInstance::StaticGetCommandClassId() ) );
		m_instance = _instance;
		if( micc )
		{
			if( micc->GetVersion() > 1 )
			{
				m_endPoint = _cc->GetEndPoint( _instance );
				if( m_endPoint != 0 )
				{
					// Set the flag bit to indicate MultiChannel rather than MultiInstance
					m_flags |= m_MultiChannel;
					m_expectedCommandClassId = MultiInstance::StaticGetCommandClassId();
				}
			}
			else if( m_instance > 1 )
			{
				// Set the flag bit to indicate MultiInstance rather than MultiChannel
				m_flags |= m_MultiInstance;
				m_expectedCommandClassId = MultiInstance::StaticGetCommandClassId();
			}
		}
	}
}

//(David)
//-----------------------------------------------------------------------------
// <Msg::SetCRC16>
// Used to enable wrapping with CRC16Encap during finalize.
//-----------------------------------------------------------------------------
void Msg::SetCRC16() {
	m_flags |= m_CRC16Encap;

	//we will detect the command class in data
	//if (m_expectedCommandClassId != 0)
	//	m_expectedCommandClassId = CRC16Encap::StaticGetCommandClassId();
	
	//Security, CRC16, Multi, MultiInstance
	// (normal)->Multi->CRC16
	// (normal)->MultiInstance->CRC16
	// (normal)->Security
	// (normal)->Multi->Security
	// (normal)->MultiInstance->Security
}

//-----------------------------------------------------------------------------
// <Msg::Append>
// Add a byte to the message
//-----------------------------------------------------------------------------
void Msg::Append
(
	uint8 const _data
)
{
	m_buffer[m_length++] = _data;
}

//-----------------------------------------------------------------------------
// <Msg::Finalize>
// Fill in the length and checksum values for the message
//-----------------------------------------------------------------------------
void Msg::Finalize()
{
	if( m_bFinal )
	{
		// Already finalized
		return;
	}

	// Deal with Multi-Channel/Instance encapsulation
	if( ( m_flags & ( m_MultiChannel | m_MultiInstance ) ) != 0 )
	{
		MultiEncap();
	}

	if ( (m_flags & m_CRC16Encap) != 0 ) {
		CRCEncap();
	}

	// Add the callback id
	if( m_bCallbackRequired )
	{
		// Set the length byte
		m_buffer[1] = m_length;		// Length of following data

		if( 0 == s_nextCallbackId )
		{
			s_nextCallbackId = 1;
		}

		m_buffer[m_length++] = s_nextCallbackId;	
		m_callbackId = s_nextCallbackId++;
	}
	else
	{
		// Set the length byte
		m_buffer[1] = m_length - 1;		// Length of following data
	}

	// Calculate the checksum
	uint8 checksum = 0xff;
	for( uint32 i=1; i<m_length; ++i ) 
	{
		checksum ^= m_buffer[i];
	}
	m_buffer[m_length++] = checksum;

	m_bFinal = true;
}


//-----------------------------------------------------------------------------
// <Msg::UpdateCallbackId>
// If this message has a callback ID, increment it and recalculate the checksum
//-----------------------------------------------------------------------------
void Msg::UpdateCallbackId()
{
	if( m_bCallbackRequired )
	{
		// update the callback ID
		m_buffer[m_length-2] = s_nextCallbackId;
		m_callbackId = s_nextCallbackId++;

		// Recalculate the checksum
		uint8 checksum = 0xff;
		for( int32 i=1; i<m_length-1; ++i ) 
		{
			checksum ^= m_buffer[i];
		}
		m_buffer[m_length-1] = checksum;
	}
}


//-----------------------------------------------------------------------------
// <Msg::GetAsString>
// Create a string containing the raw data
//-----------------------------------------------------------------------------
string Msg::GetAsString()
{
	string str = m_logText;

	char byteStr[16];
	if( m_targetNodeId != 0xff )
	{
		snprintf( byteStr, sizeof(byteStr), " (Node=%d)", m_targetNodeId );
		str += byteStr;
	}

	str += ": ";

	for( uint32 i=0; i<m_length; ++i ) 
	{
		if( i )
		{
			str += ", ";
		}

		snprintf( byteStr, sizeof(byteStr), "0x%.2x", m_buffer[i] );
		str += byteStr;
	}

	return str;
}

//-----------------------------------------------------------------------------
// <Msg::MultiEncap>
// Encapsulate the data inside a MultiInstance/Multicommand message
//-----------------------------------------------------------------------------
void Msg::MultiEncap
(
)
{
	char str[256];
	if( m_buffer[3]	!= FUNC_ID_ZW_SEND_DATA )
	{
		return;
	}

	// Insert the encap header
	if( ( m_flags & m_MultiChannel ) != 0 )
	{
		// MultiChannel
		for( uint32 i=m_length-1; i>=6; --i )
		{
			m_buffer[i+4] = m_buffer[i];
		}

		m_buffer[5] += 4;
		m_buffer[6] = MultiInstance::StaticGetCommandClassId();
		m_buffer[7] = MultiInstance::MultiChannelCmd_Encap;
		m_buffer[8] = 1;
		m_buffer[9] = m_endPoint;
		m_length += 4;

		snprintf( str, sizeof(str), "MultiChannel Encapsulated (instance=%d): %s", m_instance, m_logText.c_str() );
		m_logText = str;
	}
	else
	{
		// MultiInstance
		for( uint32 i=m_length-1; i>=6; --i )
		{
			m_buffer[i+3] = m_buffer[i];
		}

		m_buffer[5] += 3;
		m_buffer[6] = MultiInstance::StaticGetCommandClassId();
		m_buffer[7] = MultiInstance::MultiInstanceCmd_Encap;
		m_buffer[8] = m_instance;
		m_length += 3;

		snprintf( str, sizeof(str), "MultiInstance Encapsulated (instance=%d): %s", m_instance, m_logText.c_str() );
		m_logText = str;
	}
}

//-----------------------------------------------------------------------------
// <Msg::CRCEncap>
// Encapsulate the data inside a CRC16Encap message
//-----------------------------------------------------------------------------
void Msg::CRCEncap() {
	char str[256];
	uint16 check_sum = 0;
	if( m_buffer[3]	!= FUNC_ID_ZW_SEND_DATA ) {
		return;
	}

	// MultiChannel
	for( uint32 i=m_length-1; i>=6; --i )
	{
		m_buffer[i+2] = m_buffer[i];
	}

	check_sum = CreateCRC16(&m_buffer[8], m_buffer[5]);

	m_length += 2;
	m_buffer[5] += 4;
	m_buffer[6] = CRC16Encap::StaticGetCommandClassId();
	m_buffer[7] = CRC16Encap::CRC16EncapCmd_Encap;
	uint8 transmit_opt = m_buffer[m_length-1];
	m_length--;
	Append( (check_sum & 0xff00)>>8 );
	Append( (check_sum & 0xff) );
	Append(transmit_opt);

	if ( ( m_flags & ( m_MultiChannel | m_MultiInstance ) ) != 0 ) {
		snprintf( str, sizeof(str), "%s+CRC16Encap Encapsulated(instance=%d): %s", 
			(( m_flags & m_MultiChannel ) != 0 ? "MultiChannel":"MultiInstance"), m_instance, m_logText.c_str() );
	} else {
		snprintf( str, sizeof(str), "CRC16Encap Encapsulated: %s", m_logText.c_str() );
	}
	m_logText = str;
}

//-----------------------------------------------------------------------------
// <Msg::CRC16_Checking>
// Check crc16
//-----------------------------------------------------------------------------
uint16 Msg::CRC16_Checking(uint16 crc, uint8 const* _data, uint16 _length) {
	uint8 workData;
	uint8 bitMask;
	uint8 newBit;

	while(_length--) {
		workData = *_data++;
		for (bitMask=0x80; bitMask != 0; bitMask >>= 1) {
			/* Align test bit with next bit of the message byte, starting with msb. */
			newBit = ((workData & bitMask) != 0) ^ ((crc & 0x8000) != 0);
			crc <<= 1;
			if (newBit) 
				crc ^= POLY;
		}/* for (bitMask = 0x80; bitMask != 0; bitMask >>= 1) */
	}
	return crc;
}

//-----------------------------------------------------------------------------
// <Msg::CreateCRC16>
// create crc16
//-----------------------------------------------------------------------------
uint16 Msg::CreateCRC16(const uint8* _data, const uint32 _length) {
	if (!_data || _length == 0)
		return 0;
	uint16 crc = CRC16_INIT_VECTOR;
	uint8 headerData[] = {
		CRC16Encap::StaticGetCommandClassId(), CRC16Encap::CRC16EncapCmd_Encap
	};
	crc = CRC16_Checking(crc, headerData, 2);
	crc = CRC16_Checking(crc, _data, _length);
	return crc;
}

bool Msg::CheckCRC16(const uint8* _data, const uint32 _length) {
	if (!_data || _length == 0)
		return false;
	uint16 crc = CRC16_INIT_VECTOR;
	uint8 headerData[] = {
		CRC16Encap::StaticGetCommandClassId(), CRC16Encap::CRC16EncapCmd_Encap
	};
	crc = CRC16_Checking(crc, headerData, 2);
	crc = CRC16_Checking(crc, _data, _length);
	if (!crc)
		return true;

	return false;
}
