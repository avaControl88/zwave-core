//-----------------------------------------------------------------------------
//
//	ExternalLogImpl.cpp
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//	All rights reserved.
//
//-----------------------------------------------------------------------------
#include "ExternalLogImpl.h"
#include <string.h>
#include <zmq.h>
#include <zmq_utils.h>

#define LOG_SERVICE_UUID "013011011011"

ExternalLogImpl::ExternalLogImpl() {
	context = NULL;
	publisher = NULL;
	connect_path = DEFAULT_LOG_SERVER;
	inited = false;
}

ExternalLogImpl::~ExternalLogImpl() {
	Destroy();
}

bool ExternalLogImpl::Destroy() {
	zmq_close (publisher);
	zmq_ctx_destroy (context);
	inited = false;
	return true;
}

bool ExternalLogImpl::Init(std::string _path) {
	int rc;
	context = zmq_ctx_new();
	if (!context)
		return false;

	publisher = zmq_socket(context, ZMQ_PUB);
	if (!publisher)
		return false;

	if (_path.size() != 0) {
		connect_path = _path;
	}
	rc = zmq_connect(publisher, connect_path.c_str());
	if (rc)
		return false;

	inited = true;
	return true;
}

bool ExternalLogImpl::Send(const uint8 log_level, const uint8 node_id, const char *msg) {
	if (!inited || !msg || strlen(msg) == 0)
		return false;
	char lineBuf[1200];
	snprintf(lineBuf, sizeof(lineBuf), "%s %d:%d:%s", LOG_SERVICE_UUID, log_level, node_id, msg);
	int size = zmq_send(publisher, lineBuf, strlen(lineBuf), 0);
	return (size == 0) ? false : true ;
}
