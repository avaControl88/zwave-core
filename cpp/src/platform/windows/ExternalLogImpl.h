//-----------------------------------------------------------------------------
//
//	ExternalLogImpl.h
//
//	Copyright (c) 2014 David <david@avadesign.com.tw>
//	All rights reserved.
//
//-----------------------------------------------------------------------------
#ifndef _ExternalLogImpl_H
#define _ExternalLogImpl_H
#include "Defs.h"
#include <string>
#include "Log.h"

#define DEFAULT_LOG_SERVER "ipc:///tmp/log_receiver"

class ExternalLogImpl {
	public:
		ExternalLogImpl();
		~ExternalLogImpl();

		bool Init(std::string _path);
		bool Send(const uint8 log_level, const uint8 node_id, const char *msg);
		bool Destroy();
	private:
		void *context;
		void *publisher;
		std::string connect_path;
		bool inited;
};

#endif //_ExternalLogImpl_H

